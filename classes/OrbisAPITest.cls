@IsTest
public with sharing class OrbisAPITest {

  private static String ENDPOINT = 'http://webservices.bvdep.com/201705/orbis/remoteaccess.asmx';

  /**
  * Tested Scenario:
  * Open session callout is made using API Framework
  *
  * Expected Result:
  * XML is parsed internally by response class and client can access Session Id
  * by getSessionID method.
  * */
  @IsTest
  static void testOpenCallout() {
    OrbisRequest openRequest = new OrbisOpenRequest()
      .setUsername('Test Username')
      .setPassword('Test Password')
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisOpenResponse openResponse = (OrbisOpenResponse) new OrbisAPI().send(openRequest);
    Test.stopTest();

    System.assertEquals('3BCSECYSC82VHUU', openResponse.getSessionID());
  }


  /**
  * Tested Scenario:
  * Endpoint server returns Fault message
  *
  * Expected Result:
  * Fault code is transfermed into exception on Salesforce side,
  * which can be handled in try-catch block
  * */
  @IsTest
  static void testCalloutFaultHandling() {
    OrbisRequest openRequest = new OrbisOpenRequest()
      .setUsername('Test Username')
      .setPassword('Test Password')
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks.OrbisFaultMock());
    try {
      OrbisOpenResponse openResponse = (OrbisOpenResponse) new OrbisAPI().send(openRequest);
      System.assert(false, 'Orbis Response Exception was not thrown');
    } catch (OrbisResponseException ex) {
      System.debug(ex.getMessage());
      System.debug(ex.getStackTraceString());
      System.assert(true);
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.debug(ex.getStackTraceString());
      System.assert(false, 'Other exception was thrown');
    }
    Test.stopTest();
  }


  /*
  * Tested Scenario:
  * Client performs GetAvailableFields callout
  *
  * Expected Result:
  * Callout is constructed and sent.
  * Response is parsed and available to client
  * in wrapper classess.
  * */
  @IsTest
  static void testGetAvailableFieldsCallout() {
    OrbisRequest request = new OrbisGetAvailableFieldsRequest()
      .setModelID('Test Model ID')
      .setSessionID('Session Id')
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisGetAvailableFieldsResponse response = (OrbisGetAvailableFieldsResponse) new OrbisAPI().send(request);
    Test.stopTest();

    List<OrbisGetAvailableFieldsResponse.Field> fields = response.getAvailableFields();
    System.assertEquals(8, fields.size());

    for (OrbisGetAvailableFieldsResponse.Field field : fields) {
      System.assert(String.isNotEmpty(field.code));
      System.assertEquals('MoneyValue', field.type);

      System.assert(field.dataDimensions != null);
      System.assertEquals('NrOfYears', field.dataDimensions.default_x);
      System.assertEquals(1, field.dataDimensions.dataDimensions.size());
      System.assertEquals('NrOfYears', field.dataDimensions.dataDimensions.get(0).name);
      System.assertEquals('Yearly', field.dataDimensions.dataDimensions.get(0).type);
    }
  }


  /*
  * Tested Scenario:
  * Client performs GetAvailableModels callout
  *
  * Expected Result:
  * Callout is constructed and sent.
  * Response is parsed and available to client
  * in wrapper classess.
  * */
  @IsTest
  static void testGetAvailableModelsCallout() {
    OrbisRequest request = new OrbisGetAvailableModelsRequest()
      .setSessionID('Session Id')
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisGetAvailableModelsResponse response = (OrbisGetAvailableModelsResponse) new OrbisAPI().send(request);
    Test.stopTest();

    List<OrbisGetAvailableModelsResponse.Model> models = response.getAvailableModels();
    System.assertEquals(15, models.size());

    System.assertEquals('A', models.get(0).id);
    System.assertEquals('All templates', models.get(0).name);

    System.assertEquals('IND_IT_SIM', models.get(12).id);
    System.assertEquals('Stockbrokers', models.get(12).name);
    System.assertEquals('IND_IT', models.get(12).parentModelId);
  }


  /*
  * Tested Scenario:
  * Client performs Match callout
  *
  * Expected Result:
  * Callout is constructed and sent.
  * Client get access Match results.
  * */
  @IsTest
  static void getMatchCallout() {
    OrbisRequest request = new OrbisMatchRequest()
      .setSessionID('Session Id')
      .setName('Test Name')
      .setAddress('Test Address')
      .setPostCode('21-500')
      .setCity('London')
      .setCountry('UK')
      .setPhoneOrFax('123465789')
      .setEMailOrWebsite('www.test.com')
      .setNationalId('123456789')
      .setTicker('123')
      .setIsin('123')
      .setState('123')
      .setBvD9('123')
      .setExclusionFlags(OrbisMatchRequest.ExclusionFlags.None)
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisMatchResponse response = (OrbisMatchResponse) new OrbisAPI().send(request);
    Test.stopTest();

    System.assertNotEquals(null, response);
    List<OrbisSchema.MatchResult> matches = response.getMatchResults();
    System.assertEquals(2, matches.size());

    System.assertEquals('PT501234567', matches.get(0).BvDID);
    System.assertEquals(1, matches.get(0).Score);
    System.assertEquals('Test 1', matches.get(0).Name);
    System.assertEquals('PotentialCandidate', matches.get(0).Hint_x);
    System.assertEquals('RUA D DO AEROPORTO DE LISBOA, EDIFICIO 120', matches.get(0).Address);
    System.assertEquals('1700-008', matches.get(0).PostCode);
    System.assertEquals('LISBOA', matches.get(0).City);
    System.assertEquals('PT', matches.get(0).Country);
    System.assertEquals('Lisboa | Lisboa', matches.get(0).Region);
    System.assertEquals('+351 123456789', matches.get(0).PhoneOrFax);
    System.assertEquals('www.test.pt', matches.get(0).EMailOrWebsite);
    System.assertEquals('123456789', matches.get(0).NationalId);
    System.assertEquals('Unlisted', matches.get(0).Ticker);
    System.assertEquals('Active', matches.get(0).Status);
    System.assertEquals('Public limited company - SA', matches.get(0).LegalForm);
    System.assertEquals('U2', matches.get(0).ConsolidationCode);

    System.assertEquals('GBML1234567', matches.get(1).BvDID);
    System.assertEquals(0.5, matches.get(1).Score);
    System.assertEquals('PotentialCandidate', matches.get(1).Hint_x);
    System.assertEquals('A N A', matches.get(1).Name);
    System.assertEquals('TestE HOUSE 200 MOCK ROAD', matches.get(1).Address);
    System.assertEquals('W6 7NL', matches.get(1).PostCode);
    System.assertEquals('LONDON', matches.get(1).City);
    System.assertEquals('GB', matches.get(1).Country);
    System.assertEquals('+44 20 1674 2424', matches.get(1).PhoneOrFax);
    System.assertEquals('www.test2.com', matches.get(1).EMailOrWebsite);
    System.assertEquals('Unlisted', matches.get(1).Ticker);
    System.assertEquals('Active', matches.get(1).Status);
    System.assertEquals('Unincorporated company', matches.get(1).LegalForm);
    System.assertEquals('LF', matches.get(1).ConsolidationCode);
  }

  /*
  * Tested Scenario:
  * Client performs Match callout, but there are no results
  *
  * Expected Result:
  * Response is deserialized without error.
  * */
  @IsTest
  static void testGetMatchNoResultsCallout() {
    OrbisRequest request = new OrbisMatchRequest()
      .setSessionID('Session Id')
      .setName('Test Name')
      .setAddress('Test Address')
      .setExclusionFlags(OrbisMatchRequest.ExclusionFlags.None)
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks.OrbisMatchNoResultsMock());
    OrbisMatchResponse response = (OrbisMatchResponse) new OrbisAPI().send(request);
    Test.stopTest();

    System.assertNotEquals(null, response);
    List<OrbisSchema.MatchResult> matches = response.getMatchResults();
    System.assertEquals(0, matches.size());
  }

  /*
  * Tested Scenario:
  * Client performs GetStructureCallout callout
  *
  * Expected Result:
  * Request body is constructed and sent
  * Response is deserialized without error.
  * Client can access nodes and edges directly from Response
  * */
  @IsTest
  static void testGetOwnershipStructureCallout() {
    OrbisRequest request = new OrbisGetOwnershipStructureRequest()
      .setSessionHandle('TEST_Session')
      .setSubjectCompanyBvDID('Test_Company_ID')
      .setFlowType(OrbisSchema.FlowType.All)
      .setMinimumPercentage(15.0)
      .setUseIntegratedOwnership(true)
      .setEndpoint(ENDPOINT);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisGetOwnershipStructureResponse response = (OrbisGetOwnershipStructureResponse) new OrbisAPI().send(request);
    Test.stopTest();

    System.assertEquals(2, response.getNodes().size());
    OrbisSchema.OwnershipGraphNode node = response.getNodes().get(0);
    System.assertEquals('BVD_01', node.BvDID);
    System.assertEquals('Name1', node.Name);
    System.assertEquals('EntityType', node.EntityType);
    System.assertEquals('USD', node.CountryISO2Code);
    System.assertEquals('London', node.City);
    System.assertEquals('BvDIndependenceIndicator', node.BvDIndependenceIndicator);
    System.assertEquals(true, node.IsInWorldCompliance);
    System.assertEquals(197, node.Distance);
    System.assertEquals(15.0, node.IntegratedPercentage);
    System.assertEquals(false, node.IsIntegratedPercentageEstimation);

    System.assertEquals(2, response.getEdges().size());
    OrbisSchema.OwnershipGraphEdge edge = response.getEdges().get(1);
    System.assertEquals('BVD_02', edge.ParentBvDID);
    System.assertEquals('BVD_01', edge.ChildBvDID);
    System.assertEquals('Source2', edge.Source);
    System.assertEquals('Date2', edge.Date_x);
    System.assertEquals('DirectPercentage2', edge.DirectPercentage);
    System.assertEquals('TotalPercentage2', edge.TotalPercentage);
  }

  /**
  * @description
  * This unit test shows how to use Orbis Find By BVD Id request and response.
  *
  * Tested Scenario:
  * Client performs a callout to find BvD Company by the BvD ID.
  *
  * Expected Result:
  * Client aquires token and selection count information, which can be then used in GetData callout.
  * */
  @IsTest
  static void testFindByBvDIDCallout() {
    OrbisRequest request = new OrbisFindByBvDIdRequest()
      .setSessionID('Test_session')
      .setID('UED000001')
      .setEndpoint(ENDPOINT);


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisFindByBvDIdResponse response = (OrbisFindByBvDIdResponse) new OrbisAPI().send(request);
    Test.stopTest();


    System.assertEquals('2', response.getToken());
    System.assertEquals(1, response.getSelectionCount());
  }

  @IsTest
  static void testGetDataCallout() {
    OrbisAPI orbisAPI = new OrbisAPI();

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());

    OrbisRequest findRequest = new OrbisFindByBvDIdRequest()
      .setSessionID('Test_session')
      .setID('UED000001')
      .setEndpoint(ENDPOINT);
    OrbisFindByBvDIdResponse findResponse = (OrbisFindByBvDIdResponse) orbisAPI.send(findRequest);


    OrbisRequest getDataRequest = new OrbisGetDataRequest()
      .setSessionID('Test_session')
      .setSelection(findResponse.getToken(), findResponse.getSelectionCount())
      .setFromRecord(0)
      .setNrRecords(-1)
      .setQuery('SELECT NAME, ADDR, ADDR2, POSTCODE, CITY, COUNTRY, CLOSDATE FROM RemoteAccess.A')
      .setEndpoint(ENDPOINT);
    OrbisGetDataResponse getDataResponse = (OrbisGetDataResponse) orbisAPI.send(getDataRequest);
    Test.stopTest();

    OrbisSchema.RecordElement record = getDataResponse.getRecordsMap().get('TEST#1');
    System.assertEquals('JOHN DOES', record.getFieldMap().get('NAME').value);
    System.assertEquals('STREET 22A LOK. 42', record.getFieldMap().get('ADDR').value);
    System.assertEquals('', record.getFieldMap().get('ADDR2').value);
    System.assertEquals('02-672', record.getFieldMap().get('POSTCODE').value);
    System.assertEquals('WARSZAWA', record.getFieldMap().get('CITY').value);
    System.assertEquals('Poland', record.getFieldMap().get('COUNTRY').value);
    System.assertEquals('2015/12/31', record.getFieldMap().get('CLOSDATE').value);

  }
  @IsTest
  static void testGetDatasourcesCallout() {
    OrbisAPI orbisAPI = new OrbisAPI();

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());

    OrbisGetAvailableDatasourcesRequest request = new OrbisGetAvailableDatasourcesRequest();
    request.setEndpoint(ENDPOINT);
    OrbisGetAvailableDatasourcesResponse response = (OrbisGetAvailableDatasourcesResponse) new OrbisAPI().send(request);
    Test.stopTest();

    System.assertEquals(1, response.getDataSources().size());
    OrbisSchema.Datasource datasource = response.getDataSources().get(0);
    System.assertEquals('Orbis', datasource.Id);
    System.assertEquals('Orbis', datasource.Label);
    System.assertEquals('https://webservices.bvdep.com/orbis4/remoteaccess.asmx', datasource.WebServiceUrl);
    System.assertEquals('https://orbis4.bvdinfo.com', datasource.WebsiteUrl);
  }

  /*
 * Tested Scenario:
 * Client performs UpdateMyDataTable callout
 *
 * Expected Result:
 * Request body is constructed and sent
 * Response is deserialized without error.
 * Client can access field errors directly from response
 * */
  @IsTest
  static void testUpdateMyDataTable() {

    List<OrbisSchema.UploadValue> uploadValues = new List<OrbisSchema.UploadValue>();
    uploadValues.addAll(new List<OrbisSchema.UploadValue>{
      new OrbisSchema.UploadValue(
        'CF00001', '0018000000rsH4K'
      ),
      new OrbisSchema.UploadValue(
        'CF00002', 'Account'
      )
    });


    OrbisRequest request = new OrbisUpdateMyDataTableRequest()
      .setSessionID('Test_session')
      .setTable('CFE00001')
      .setAppendData(true)
      .addValues(new OrbisSchema.UpdateMyDataTableDataUploadDataRowElement('NL33242490'), uploadValues);

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisUpdateMyDataTableResponse response = (OrbisUpdateMyDataTableResponse) new OrbisAPI().send(request);
    Test.stopTest();

    System.assertEquals(false, response.getIsSuccess());
    System.assertEquals(2, response.getErrors().size());
    System.assertEquals(2, response.getWarnings().size());

    System.assertEquals('No valid custom field in the file', response.getErrors().get(0).stringElement);
    System.assertEquals('No valid custom field in the file', response.getErrors().get(1).stringElement);

    System.assertEquals('No valid custom field in the file', response.getErrors().get(1).stringElement);
    System.assertEquals('No valid custom field in the file', response.getErrors().get(1).stringElement);
  }

  @IsTest
  static void testGetLabelsFromModel() {
    OrbisRequest request = new OrbisGetLabelsFromModelRequest()
      .setSessionID('SessionId')
      .setFullLabels(true)
      .addField('Name', 'UNIVERSAL')
      .addField('NEW_COMPANY_DATE', 'UNIVERSAL');


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    OrbisGetLabelsFromModelResponse response = (OrbisGetLabelsFromModelResponse) new OrbisAPI().send(request);
    Test.stopTest();


    List<String> labels = response.getLabels();
    System.assertEquals('Company name', labels.get(0));
    System.assertEquals('Addition of company in database', labels.get(1));
  }
}