/**
* Represents a single Action toward BvD: Link, Unlink, Insert, Update.
* Action is executed for one batch of records.
* @author Piotr Kożuchowski <piotr.kozuchowski@weare4c.com>
* @date 2017-11-09
* */
public abstract with sharing class BvDAction {

  /**
  * Factory method which creates concrete action type
  * */
  public static BvDAction getInstance(String action) {
    if (action == BvDJobs.TYPE_LINK) return new BvDAction.LinkAction();
    if (action == BvDJobs.TYPE_UNLINK) return new BvDAction.UnlinkAction();
    if (action == BvDJobs.TYPE_INSERT) return new BvDAction.InsertAction();
    if (action == BvDJobs.TYPE_UPDATE) return new BvDAction.UpdateAction();

    throw new BvDActionException('Unsupported Action: ' + action);
  }


  public class Result {
    public String status = BvDJobs.STATUS_IN_PROGRESS;
    public String error;
  }

  protected OrbisAPI orbisAPI = new OrbisAPI();
  protected String sessionId;
  protected ISObjectUnitOfWork unitOfWork;
  protected FieldMappings fieldMappingsService;
  protected BvD_Job__c job;
  protected List<BvD_Job_Work_Item__c> lWorkItems;
  protected SObjectType sObjectType;
  protected Map<Id, sObject> recordsByIds;
  protected Map<String, OrbisSchema.RecordElement> orbisRecords;
  protected Database.DMLOptions dmlOptions;
  protected SObjectUnitOfWork.DatabaseDML dml;
  protected List<BvD_Job_Work_Item__c> lWorkItemsOfUpsertedRecords;
  private Result result = new Result();

  public Result execute(BvD_Job__c job, List<BvD_Job_Work_Item__c> lWorkItems) {
    try {
      initialize(job, lWorkItems);
      execute();
      unitOfWork.commitWork();
      checkDMLResults();
      onAfterExecute();
    } catch (Exception ex) {
      setResult(BvDJobs.STATUS_ERROR, BvDJobs.ERROR_EXECUTION);
      Logger.error(ex);
    }
    return result;
  }

  /**
  * Strategy method - implemented in each subclass - specific for action.
  * */
  protected abstract void execute();
  protected virtual void onAfterExecute() {
  }

  private void setResult(String status, String error) {
    this.result.status = status;
    this.result.error = error;
  }

  private virtual void initialize(BvD_Job__c job, List<BvD_Job_Work_Item__c> lWorkItems) {
    this.orbisAPI = new OrbisAPI();
    this.job = job;
    this.lWorkItems = lWorkItems;
    this.dml = new SObjectUnitOfWork.DatabaseDML();
    this.sObjectType = SObjectDescribe.getDescribe(job.Object__c).getSObjectType();
    this.unitOfWork = new SObjectUnitOfWork(new List<Schema.SObjectType>{
      BvD_Job__c.getSObjectType(),
      Account.getSObjectType(),
      Contact.getSObjectType(),
      Lead.getSObjectType(),
      BvD_Job_Work_Item__c.getSObjectType(),
      Work_Item_Field_Conflict__c.getSObjectType(),
      Field_Mapping__c.getSObjectType()
    }, dml);
    this.unitOfWork.registerUpsert(job);
    this.fieldMappingsService = FieldMappings.getInstance(job.Object__c);
    this.dmlOptions = new Database.DMLOptions();
    this.dmlOptions.AllowFieldTruncation = true;
    this.lWorkItemsOfUpsertedRecords = new List<BvD_Job_Work_Item__c>();

    this.openOrbisSession();
    this.querySalesforceRecords();
    this.queryOrbisCompanies();
  }

  /**
  * Open Session with Orbis Webservice
  * */
  protected virtual void openOrbisSession() {
    try {
      OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());
      this.sessionId = openResponse.getSessionID();

    } catch (OrbisResponseException ex) {
      Logger.error(ex);
    }
  }

  /**
  * Query Salesforce records to work on
  * */
  protected virtual void querySalesforceRecords() {
    try {
      Set<Id> recordIds = new Set<Id>();
      for (BvD_Job_Work_Item__c workItem : this.lWorkItems) {
        recordIds.add(workItem.RecordId__c);
      }
      recordIds.remove(null);

      this.recordsByIds = new Map<Id, sObject>(
        Database.query(String.format(
          'SELECT {0}'
            + ' FROM {1}'
            + ' WHERE ID IN :recordIds',
          new List<String>{
            FieldMappings.getInstance(job.Object__c).getSalesforceFieldsCommaSeparated(),
            job.Object__c
          }))
      );
    } catch (Exception ex) {
      Logger.error(ex);
      this.recordsByIds = new Map<Id, sObject>();
    }
  }

  /**
   * Retrieve Orbis Data for all records processed in current batch
   * Orbis records are mapped by BvD ID
   */
  protected virtual void queryOrbisCompanies() {
    try {
      List<String> bvdIds = new List<String>();
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        bvdIds.add(workItem.BvD_Id__c);
      }

      OrbisFindByBvDIdResponse findResponse = (OrbisFindByBvDIdResponse) orbisAPI.send(
        new OrbisFindByBvDIdRequest()
          .setSessionID(this.sessionId)
          .setIDs(bvdIds)
      );

      OrbisGetDataResponse dataResponse = (OrbisGetDataResponse) orbisAPI.send(
        new OrbisGetDataRequest()
          .setSessionID(this.sessionId)
          .setFromRecord(0)
          .setNrRecords(-1)
          .setQuery('SELECT ' + fieldMappingsService.getBvDFieldsCommaSeparated() + ' FROM RemoteAccess.UNIVERSAL')
          .setSelection(findResponse.getToken(), findResponse.getSelectionCount())
      );

      this.orbisRecords = dataResponse.getRecordsMap();
    } catch (OrbisResponseException ex) {
      Logger.error(ex);
      setResult(BvDJobWorkItems.STATUS_ERROR, BVDJobs.ERROR_SERVICE);
      this.orbisRecords = new Map<String, OrbisSchema.RecordElement>();
    }
  }

  /**
  * Prepare Work Item for Manual Start job
  * */
  protected void prepareWorkItemForManualJob(BvD_Job_Work_Item__c workItem) {
    setResult(BvDJobs.STATUS_ACTION_REQUIRED, BvDJobs.ERROR_MANUAL_START);
    workItem.Status__c = BvDJobWorkItems.STATUS_NOT_STARTED;
  }

  /**
  * Assign BvD Value if it was selected by user. Otherwise, Salesforce value will remain
  * */
  protected void assignUserSelectedValues(sObject record, BvD_Job_Work_Item__c workItem) {
    record.setOptions(this.dmlOptions);

    for (Work_Item_Field_Conflict__c conflict : workItem.Field_Conflicts__r) {
      if (conflict.Choice__c == 'BvD') {
        DescribeFieldResult fieldDescribe = SObjectDescribe.getDescribe(job.Object__c).getField(conflict.Field__c).getDescribe();
        Object BVDValue = deserializeValue(conflict.BvD_Value__c, fieldDescribe);

        assignValue(record, conflict.Field__c, BVDValue, workItem);
      }
    }
    unitOfWork.registerUpsert(record);
    lWorkItemsOfUpsertedRecords.add(workItem);
  }

  /**
  * Deserialize value saved in the conflict to the real field type
  * */
  protected Object deserializeValue(String bvdValue, DescribeFieldResult fieldDescribe) {
    DisplayType fieldType = fieldDescribe.getType();

    if (fieldType == DisplayType.DOUBLE) return Double.valueOf(bvdValue);
    if (fieldType == DisplayType.INTEGER) return Integer.valueOf(bvdValue);
    if (fieldType == DisplayType.DATE) return Date.valueOf(bvdValue);
    if (fieldType == DisplayType.DATETIME) return Datetime.valueOf(bvdValue);
    if (fieldType == DisplayType.PERCENT) return Decimal.valueOf(bvdValue);
    if (fieldType == DisplayType.CURRENCY) return Decimal.valueOf(bvdValue);
    if (fieldType == DisplayType.BOOLEAN) return Boolean.valueOf(bvdValue);

    return bvdValue;
  }

  /**
   * Attempt to assign BvD field values to Salesforce record
   * If record does not exist in Orbis, status of the work item is set to Error
   * Otherwise Salesforce record is linked and Orbis data is saved in the record
   * according to the Field Mapping.
   *
   * Automatic Field Mapping is saved on the record, while Manual Field mapping creates Conflict record with both
   * Salesforce and BvD values.
   *
   * @param record - Salesforce record linked to Orbis
   * @param workItem - Work Item for that Salesforce record
   */
  protected virtual void assignBVDValues(sObject record, BvD_Job_Work_Item__c workItem) {
    OrbisSchema.RecordElement bvdRecord = orbisRecords.get(workItem.BvD_Id__c);
    List<Work_Item_Field_Conflict__c> conflicts = new List<Work_Item_Field_Conflict__c>();

    if (bvdRecord == null) {
      workItem.Status__c = BvDJobWorkItems.STATUS_ERROR;
      workItem.Failure_Reason__c = BvDJobWorkItems.ERROR_COMPANY_NOT_EXISTS;

    } else {
      record.put('Unlinked_from_BvD__c', false);
      record.put('BvD_Id__c', bvdRecord.getField('BVD_ID_NUMBER').value);
      record.setOptions(this.dmlOptions);


      for (FieldMapping fm : fieldMappingsService.getFieldMappings()) {
        OrbisSchema.FieldElement bvdField = bvdRecord.getField(fm.getBvDField());
        Object SFValue = record.get(fm.getSFField()),
          BVDValue = bvdField.getAs(fm.getSFFieldType());

        if (bvdField.resultType != 'NotAvailable') {
          if (FieldMatrix.isMapable(job.Object__c, fm.getSFField(), bvdField.fieldType)) {

            if (fm.isManual() || job.Processing__c == BvDJobs.PROCESSING_DRY_RUN) {
              conflicts.add(
                new Work_Item_Field_Conflict__c(
                  Field__c = fm.getSFField(),
                  Choice__c = fm.getMasterField(),
                  Salesforce_Value__c = String.valueOf(SFValue),
                  BvD_Value__c = String.valueOf(BVDValue)
                )
              );

            } else {
              assignValue(record, fm.getSFField(), BVDValue, workItem);
            }
          } else {
            unitOfWork.registerDirty(fm.invalidate());
//            setResult(BvDJobs.STATUS_ACTION_REQUIRED, BvDJobs.ERROR_INVALID_FIELD_MAPPING);
//            workItem.Status__c = BvDJobWorkItems.STATUS_ERROR;
//            workItem.Failure_Reason__c = BvDJobWorkItems.ERROR_INVALID_FIELD_MAPPING;
          }
        }
      }

      registerConflictsOnDifferentValues(workItem, conflicts);
      if (job.Processing__c != BvDJobs.PROCESSING_DRY_RUN) {
        unitOfWork.registerUpsert(record);
        lWorkItemsOfUpsertedRecords.add(workItem);
      }
    }
  }

  /**
   * Assign Bvd Value to Salesforce record. If assignment fails, the work item status for this record
   * is flagged as Error with Value Assignment failure reason.
   *
   * @param record - Salesforce record to which value is assigned
   * @param field - API Name of the field
   * @param bvdFieldValue - BvD Value in Salesforce friendly form (aka. Dates are parsed to Date object)
   * @param workItem - Work item for current record
   */
  protected void assignValue(sObject record, String field, Object bvdFieldValue, BvD_Job_Work_Item__c workItem) {
    try {
      record.put(field, bvdFieldValue);

    } catch (Exception ex) {
      setResult(BvDJobs.STATUS_ERROR, BvDJobs.ERROR_VALUE_ASSIGNMENT);
      workItem.Status__c = BvDJobWorkItems.STATUS_ERROR;
      workItem.Failure_Reason__c = BvDJobWorkItems.ERROR_VALUE_ASSIGNMENT;
      workItem.Error_Details__c = 'Invalid Assignment: "' + bvdFieldValue + '" to field: ' + field;
      Logger.error(ex);
    }
  }

  /**
   * If there's at least 1 different value between Salesforce and BVD, all conflicts
   * (both different values and the same values which has manual field mappping) are registered for insert.
   * Also work item is marked as error;
   *
   * @param workItem - Work Item for which action is taken
   * @param conflicts - Conflicts created for Current Work Item
   */
  protected void registerConflictsOnDifferentValues(BvD_Job_Work_Item__c workItem, List<Work_Item_Field_Conflict__c> conflicts) {
    Boolean hasDifferentValues = false;
    for (Work_Item_Field_Conflict__c conflict : conflicts) {
      hasDifferentValues |= conflict.Salesforce_Value__c != conflict.BvD_Value__c;
    }

    if (hasDifferentValues) {
      workItem.Status__c = BvDJobWorkItems.STATUS_ERROR;
    }

    for (Work_Item_Field_Conflict__c conflict : conflicts) {
      if (hasDifferentValues) {
        unitOfWork.registerNew(conflict);
        unitOfWork.registerRelationship(conflict, Work_Item_Field_Conflict__c.Work_Item__c, workItem);
        setResult(BvDJobs.STATUS_ACTION_REQUIRED, BvDJobs.ERROR_MANUAL_FIELD_MAPPING);
      }
    }
  }

  protected void checkDMLResults() {
    List<Database.SaveResult> lSaveResults = dml.saveResults.get(this.sObjectType);
    Boolean hasError = false;


    if (lSaveResults != null) {
      for (Integer i = 0; i < lSaveResults.size(); i++) {
        Database.SaveResult saveResult = lSaveResults.get(i);
        BvD_Job_Work_Item__c workItem = lWorkItemsOfUpsertedRecords.get(i);

        if (!saveResult.isSuccess()) {
          workItem.Status__c = BvDJobWorkItems.STATUS_ERROR;
          workItem.Failure_Reason__c = BvDJobWorkItems.ERROR_DML;
          workItem.Error_Details__c = saveResult.errors.get(0).message;
          hasError = true;
          setResult(BvDJobs.STATUS_ERROR, BvDJobs.ERROR_DML);
        }
      }
    }

    update lWorkItemsOfUpsertedRecords;
  }

  /**
  * Links Salesforce record with BvD record.
  * BvD Id is placed on Salesforce record and a callout is made
  * to update BvD record with Salesforce Id.
  * */
  public class LinkAction extends BvDAction {
    public override void execute() {
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        sObject record = recordsByIds.get(workItem.RecordId__c);

        workItem.Status__c = BvDJobWorkItems.STATUS_SUCCESS;
        workItem.Record_Name__c = (String) record.get('Name');

        unitOfWork.registerUpsert(workItem);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.RecordId__c, record);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.BvD_Job__c, job);

        if (job.Processing__c == BvDJobs.PROCESSING_MANUAL && String.isBlank(workItem.Id)) {
          prepareWorkItemForManualJob(workItem);
        } else if (workItem.Field_Conflicts__r.isEmpty()) {
          assignBVDValues(record, workItem);
        } else {
          assignUserSelectedValues(record, workItem);
        }
      }
    }

    /**
    * Enqueue Job to put Salesforce IDs in Orbis
    * */
    protected override void onAfterExecute() {
      Map<String, String> bvdIds = new Map<String, String>();
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        if (workItem.Status__c == BvDJobWorkItems.STATUS_SUCCESS) {
          bvdIds.put(workItem.BvD_Id__c, workItem.RecordId__c);
        }
      }

      if (!bvdIds.isEmpty() && !Test.isRunningTest()) {
        System.enqueueJob(new AsyncOrbisUpdater(job.Object__c, bvdIds));
      }
    }
  }


  /**
  * Unlinks Salesforce record with BvD record.
  * BvD Id is removed from Salesforce record and a callout is made
  * to remove Salesforce Id from BvD record.
  * */
  public class UnlinkAction extends BvDAction {
    public override void execute() {
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        sObject record = recordsByIds.get(workItem.RecordId__c);

        record.put('BvD_Id__c', null);
        record.put('Unlinked_from_BvD__c', true);
        workItem.Status__c = BvDJobWorkItems.STATUS_SUCCESS;

        unitOfWork.registerDirty(record);
        unitOfWork.registerUpsert(workItem);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.RecordId__c, record);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.BvD_Job__c, job);
        lWorkItemsOfUpsertedRecords.add(workItem);
      }
    }

    /**
    * Enqueue Job to remove Salesforce IDs from Orbis
    * */
    protected override void onAfterExecute() {
      Map<String, String> bvdIds = new Map<String, String>();
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        if (workItem.Status__c == BvDJobWorkItems.STATUS_SUCCESS) {
          bvdIds.put(workItem.BvD_Id__c, '');
        }
      }

      if (!bvdIds.isEmpty() && !Test.isRunningTest()) {
        System.enqueueJob(new AsyncOrbisUpdater(job.Object__c, bvdIds));
      }
    }

    /*No need to do callouts on Unlink*/
    protected override void openOrbisSession() {
    }
    protected override void queryOrbisCompanies() {
    }
  }


  /**
  * Queries BvD for data and inserts record with BvD data.
  * In this scenario, BvD is always master and all fields are automatically mapped.
  * */
  public class InsertAction extends BvDAction {
    public override void execute() {
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        sObject record = getBlankRecord(workItem.BvD_Id__c);

        workItem.Status__c = BvDJobWorkItems.STATUS_SUCCESS;
        workItem.Record_Name__c = workItem.BvD_Id__c;


        unitOfWork.registerUpsert(workItem);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.RecordId__c, record);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.BvD_Job__c, job);

        assignBVDValues(record, workItem);
      }
    }

    private sObject getBlankRecord(String name) {
      if (job.Object__c == 'Account') {
        return new Account(
          Name = name
        );
      } else if (job.Object__c == 'Contact') {
        return new Contact(
          LastName = name
        );
      } else {
        return new Lead(
          LastName = name,
          Company = name
        );
      }
    }

    /**
    * Insert never produces a Conflict record. All fields are always mapped to the record.
    * */
    protected override void assignBVDValues(sObject record, BvD_Job_Work_Item__c workItem) {
      OrbisSchema.RecordElement bvdRecord = orbisRecords.get(workItem.BvD_Id__c);

      if (bvdRecord == null) {
        workItem.Status__c = BvDJobWorkItems.STATUS_ERROR;
        workItem.Failure_Reason__c = BvDJobWorkItems.ERROR_COMPANY_NOT_EXISTS;

      } else {
        record.put('Unlinked_from_BvD__c', false);
        record.put('BvD_Id__c', workItem.BvD_Id__c);
        record.setOptions(this.dmlOptions);


        OrbisSchema.FieldElement nameField = bvdRecord.getField('NAME');
        workItem.Record_Name__c = nameField != null && String.isNotBlank(nameField.value) ? nameField.value : workItem.BvD_Id__c;

        for (FieldMapping fm : fieldMappingsService.getFieldMappings()) {
          OrbisSchema.FieldElement bvdField = bvdRecord.getField(fm.getBvDField());

          if (bvdField != null && bvdField.resultType != 'NotAvailable') {
            if (FieldMatrix.isMapable(job.Object__c, fm.getSFField(), bvdField.fieldType)) {
              assignValue(record, fm.getSFField(), bvdField.value, workItem);
            } else {
              unitOfWork.registerDirty(fm.invalidate());
            }
          }
        }

        unitOfWork.registerUpsert(record);
        lWorkItemsOfUpsertedRecords.add(workItem);
      }
    }
  }


  /**
  * Updates given record with BvD Data according to the Field Mapping.
  * Manual Field mapping produces an Conflict record which has to be resolved by admin
  * user.
  * */
  public class UpdateAction extends BvDAction {
    public override void execute() {
      for (BvD_Job_Work_Item__c workItem : lWorkItems) {
        sObject record = recordsByIds.get(workItem.RecordId__c);

        workItem.Status__c = BvDJobWorkItems.STATUS_SUCCESS;
        workItem.Record_Name__c = (String) record.get('Name');

        unitOfWork.registerUpsert(workItem);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.RecordId__c, record);
        unitOfWork.registerRelationship(workItem, BvD_Job_Work_Item__c.BvD_Job__c, job);

        if (job.Processing__c == BvDJobs.PROCESSING_MANUAL && String.isBlank(workItem.Id)) {
          prepareWorkItemForManualJob(workItem);
        } else if (workItem.Field_Conflicts__r.isEmpty()) {
          assignBVDValues(record, workItem);
        } else {
          assignUserSelectedValues(record, workItem);
        }
      }
    }
  }


  public class BvDActionException extends Exception {
  }
}