@IsTest
public with sharing class AdministrationController_Test {

  public static String sfsession  = null;
  public static String sfApiUrl   = null;

  @isTest
  static void getOrgId_01() {
    System.assertEquals(
      UserInfo.getOrganizationId(),
      AdministrationController.getOrgId()
    );
  }

  @isTest
  static void getBvDSetup_01() {
    Bvd_Setup__c setup = AdministrationController.getBvDSetup();

    System.assert(String.isBlank(setup.Datasource_Id__c));
  }

  @isTest
  static void getBvDSetup_02() {
    Bvd_Setup__c setup = new Bvd_Setup__c(
      Name = 'BvD - Setup',
      SSO_Confirmed__c = true,
      Datasource_Id__c = 'Id',
      Datasource_Label__c = 'Label'
    );
    insert setup;

    Bvd_Setup__c result = AdministrationController.getBvDSetup();

    System.assertEquals(
      setup.Name,
      result.Name
    );
  }

  @isTest
  static void setConfirmSSO_01() {
    try {
      AdministrationController.setConfirmSSO();
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void setConfirmSSO_02() {
    AdministrationController.setConfirmSSO();

    try {
      AdministrationController.setConfirmSSO();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setDataSource_01() {
    try {
      AdministrationController.setConfirmSSO();
      AdministrationController.setDataSource('Test', 'Test', 'url', 'url');
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void setDataSource_02() {
    try {
      AdministrationController.setConfirmSSO();
      AdministrationController.setDataSource('Test', 'Test', 'url', 'url');
      AdministrationController.setDataSource('Test', 'Test', 'url', 'url');
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setDataSource_03() {
    try {
      AdministrationController.setDataSource('Test', 'Test', 'url', 'url');
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setFieldMappingConfigured_01() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('Test', 'Test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();

    System.assertEquals(
      true,
      AdministrationController.getBvDSetup().Field_Mapping_Configured__c
    );
  }

  @isTest
  static void setFieldMappingConfigured_02() {
    try {
      AdministrationController.setFieldMappingConfigured();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setFieldMappingConfigured_03() {
    try {
      AdministrationController.setConfirmSSO();
      AdministrationController.setFieldMappingConfigured();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @IsTest
  public static void getAvailableDataSources_03() {

    Test.startTest();

    AdministrationController_Test.sfsession = 'sfsession';
    AdministrationController_Test.sfApiUrl  = 'sfApiUrl';

    Test.setMock(HttpCalloutMock.class, new SetupTestMocks());

    AdministrationController.DataSourceResponse response = AdministrationController.getAvailableDataSources();

    System.assertEquals(
      null,
      response.Error
    );

    System.assertEquals(
      1,
      response.Products.size()
    );

    System.assertEquals(
      'Mock Id',
      response.Products[0].Id
    );

    Test.stopTest();
  }

  @isTest
  static void getOrbisAvailableFields_01() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());

    AdministrationController.Field[] fields = AdministrationController.getOrbisAvailableFields();
    System.assert(true);

    Test.stopTest();
  }

  @isTest
  static void isExcluded_01() {
    System.assertEquals(
      true,
      AdministrationController.isExcluded('Account', 'REFERENCE', null)
    );
  }

  @isTest
  static void isExcluded_02() {
    System.assertEquals(
      true,
      AdministrationController.isExcluded('Portorico', 'atype', null)
    );
  }

  @isTest
  static void isExcluded_03() {
    System.assertEquals(
      true,
      AdministrationController.isExcluded('Portorico', 'atype', 'afield')
    );
  }

  @isTest
  static void isExcluded_04() {
    System.assertEquals(
      true,
      AdministrationController.isExcluded('Account', 'atype', 'CreatedDate')
    );
  }

  @isTest
  static void getFieldsNameAndTypes_01() {
    Map<String, String> nameAndType = AdministrationController.getFieldsNameAndTypes('Account');

    System.assertEquals(
      false,
      nameAndType.containsKey('Id')
    );
  }

  @isTest
  static void getAccountMapping_01() {
    insert new Field_Mapping__c(
      Name__c = 'Account.Id',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      Type__c = 'REFERENCE',
      SObject__c = 'Account',
      Sf_Field__c = 'Id'
    );

    Map<String, Field_Mapping__c[]> accountMapping = AdministrationController.getAccountMapping();

    System.assert(accountMapping.containsKey('available'));
    System.assert(accountMapping.containsKey('mapped'));
    System.assert(accountMapping.containsKey('excluded'));

    System.assert(!accountMapping.get('available').isEmpty());
    System.assert(accountMapping.get('mapped').isEmpty());
    System.assert(accountMapping.get('excluded').isEmpty());
  }

  @isTest
  static void getAccountMapping_02() {
    Map<String, Field_Mapping__c[]> accountMapping1 = AdministrationController.getAccountMapping();

    insert new Field_Mapping__c(
      Name__c = 'Account.Random',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      Type__c = 'AUTRE',
      SObject__c = 'Account',
      Sf_Field__c = 'Random'
    );

    Map<String, Field_Mapping__c[]> accountMapping = AdministrationController.getAccountMapping();

    System.assertEquals(
      accountMapping1.get('available').size(),
      accountMapping.get('available').size()
    );
  }

  @isTest
  static void getAccountMapping_03() {
    Map<String, Field_Mapping__c[]> accountMapping1 = AdministrationController.getAccountMapping();

    insert new Field_Mapping__c(
      Name__c = 'Account.Random',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      Type__c = 'AUTRE',
      SObject__c = 'Account',
      Sf_Field__c = 'Random'
    );

    insert new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      Type__c = 'STRING',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry'
    );

    Map<String, Field_Mapping__c[]> accountMapping = AdministrationController.getAccountMapping();

    System.assertNotEquals(
      accountMapping1.get('available').size(),
      accountMapping.get('available').size()
    );
  }

  @isTest
  static void getAccountMapping_04() {
    Map<String, Field_Mapping__c[]> accountMapping1 = AdministrationController.getAccountMapping();

    insert new Field_Mapping__c(
      Name__c = 'Account.Random',
      Type__c = 'AUTRE',
      SObject__c = 'Account',
      Sf_Field__c = 'Random',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      Excluded__c = true
    );

    insert new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      Type__c = 'STRING',
      SObject__c = 'Account',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      Sf_Field__c = 'BillingCountry',
      Mapped__c = true
    );

    Map<String, Field_Mapping__c[]> accountMapping = AdministrationController.getAccountMapping();

    System.assertNotEquals(
      accountMapping1.get('available').size(),
      accountMapping.get('available').size()
    );
  }

  @isTest
  static void getContactMapping_01() {
    insert new Field_Mapping__c(
      Name__c = 'Contact.Birthdate',
      Type__c = 'DATE',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      SObject__c = 'Contact',
      Sf_Field__c = 'Birthdate',
      Excluded__c = true
    );

    Map<String, Field_Mapping__c[]> contactMapping = AdministrationController.getContactMapping();

    System.assert(contactMapping.containsKey('available'));
    System.assert(contactMapping.containsKey('mapped'));
    System.assert(contactMapping.containsKey('excluded'));

    System.assert(!contactMapping.get('available').isEmpty());
    System.assert(contactMapping.get('mapped').isEmpty());
    System.assert(!contactMapping.get('excluded').isEmpty());
  }

  @isTest
  static void getLeadMapping_01() {
    insert new Field_Mapping__c(
      Name__c = 'Lead.City',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic',
      SObject__c = 'Lead',
      Sf_Field__c = 'City'
    );

    Map<String, Field_Mapping__c[]> leadMapping = AdministrationController.getLeadMapping();

    System.assert(leadMapping.containsKey('available'));
    System.assert(leadMapping.containsKey('mapped'));
    System.assert(leadMapping.containsKey('excluded'));

    System.assert(!leadMapping.get('available').isEmpty());
    System.assert(!leadMapping.get('mapped').isEmpty());
    System.assert(leadMapping.get('excluded').isEmpty());
  }

  @isTest
  static void updateFieldMapping_01() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('Test', 'Test', 'url', 'url');

    Map<String, Field_Mapping__c[]> fieldMappings = AdministrationController.updateFieldMapping(
      'Account',
      new Field_Mapping__c[] {
        new Field_Mapping__c(
          Name__c = 'Account.BillingCountry',
          SObject__c = 'Account',
          Sf_Field__c = 'BillingCountry',
          Type__c = 'TEXT',
          Master__c = 'Salesforce',
          Action__c = 'Automatic'
        )
      }
    );

    System.assert(!fieldMappings.get('mapped').isEmpty());
  }

  @isTest
  static void updateFieldMapping_02() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('Test', 'Test', 'url', 'url');

    Field_Mapping__c fm = new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic'
    );

    AdministrationController.updateFieldMapping(
      'Account',
      new Field_Mapping__c[] { fm }
    );

    fm.Mapped__c = false;
    fm.Excluded__c = true;

    Map<String, Field_Mapping__c[]> fieldMappings = AdministrationController.updateFieldMapping(
      'Account',
      new Field_Mapping__c[] { fm }
    );

    System.assert(fieldMappings.get('mapped').isEmpty());
    System.assert(!fieldMappings.get('excluded').isEmpty());
  }

  @isTest
  static void updateFieldMapping_03() {

    Field_Mapping__c fm = new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic'
    );

    try {
      AdministrationController.updateFieldMapping(
        'Account',
        new Field_Mapping__c[] { fm }
      );
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }

  }

  @isTest
  static void updateFieldMapping_04() {

    AdministrationController.setConfirmSSO();

    Field_Mapping__c fm = new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic'
    );

    try {
      AdministrationController.updateFieldMapping(
        'Account',
        new Field_Mapping__c[] { fm }
      );
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }

  }

  @isTest
  static void unExcludeFieldMapping_01() {
    Field_Mapping__c fm = new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic'
    );

    try {
      AdministrationController.unExcludeFieldMapping(
        'Account',
        new Field_Mapping__c[] { fm }
      );
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }

  }

  @isTest
  static void unExcludeFieldMapping_02() {
    AdministrationController.setConfirmSSO();
    Field_Mapping__c fm = new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic'
    );

    try {
      AdministrationController.unExcludeFieldMapping(
        'Account',
        new Field_Mapping__c[] { fm }
      );
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }

  }

  @isTest
  static void unExcludeFieldMapping_03() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    Field_Mapping__c fm = new Field_Mapping__c(
      Name__c = 'Account.BillingCountry',
      SObject__c = 'Account',
      Sf_Field__c = 'BillingCountry',
      Type__c = 'TEXT',
      Master__c = 'Salesforce',
      Action__c = 'Automatic'
    );

    insert fm;

    try {
      Map<String, Field_Mapping__c[]> mapping = AdministrationController.unExcludeFieldMapping(
        'Account',
        new Field_Mapping__c[] { fm }
      );

      System.assertEquals(
        0,
        mapping.get('excluded').size()
      );
    } catch (Exception ex) {
      System.assert(false);
    }

  }

  @isTest
  static void importFieldMapping_01() {
    try {
      AdministrationController.importFieldMapping('Account', new Field_Mapping__c[] {});
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void importFieldMapping_02() {
    AdministrationController.setConfirmSSO();
    try {
      AdministrationController.importFieldMapping('Account', new Field_Mapping__c[] {});
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void importFieldMapping_03() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    try {
      Field_Mapping__c fm = new Field_Mapping__c(
        Name__c = 'Account.BillingCountry',
        SObject__c = 'Account',
        Sf_Field__c = 'BillingCountry',
        Type__c = 'TEXT',
        Master__c = 'Salesforce',
        Action__c = 'Automatic'
      );

      insert fm;

      Map<String, Field_Mapping__c[]> mapping = AdministrationController.importFieldMapping('Account', new Field_Mapping__c[] { fm });

      System.assertEquals(
        1,
        mapping.get('mapped').size()
      );
    } catch (Exception ex) {
      System.assert(false); // should fail
    }
  }

  @isTest
  static void getFilteredOrbisAvailableFields_01() {
    try {
      Test.startTest();
      Test.setMock(HttpCalloutMock.class,  new OrbisTestMocks());
      List<AdministrationController.Field> fields = AdministrationController.getFilteredOrbisAvailableFields('Account');

      System.assert(!fields.isEmpty());
      Test.stopTest();
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void getImportSettings_01() {
    try {
      AdministrationController.getImportSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }

  @isTest
  static void getImportSettings_02() {
    AdministrationController.setConfirmSSO();
    try {
      AdministrationController.getImportSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void getImportSettings_03() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    try {
      AdministrationController.getImportSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void getImportSettings_04() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();

    try {
      insert new Import_Setting__c(
        Name = 'test',
        SObject__c = 'Account',
        Type__c = '',
        Processing__c = ''
      );

      Import_Setting__c[] importSettings = AdministrationController.getImportSettings();
      System.assertEquals(
        1,
        importSettings.size()
      );
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void setImportSettings_01() {
    try {
      Import_Setting__c settings = new Import_Setting__c(
        Name = 'test',
        SObject__c = 'Account',
        Type__c = '',
        Processing__c = ''
      );
      AdministrationController.setImportSettings(new Import_Setting__c[] { settings });
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void setImportSettings_02() {
    AdministrationController.setConfirmSSO();
    try {
      Import_Setting__c settings = new Import_Setting__c(
        Name = 'test',
        SObject__c = 'Account',
        Type__c = '',
        Processing__c = ''
      );
      AdministrationController.setImportSettings(new Import_Setting__c[] { settings });
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void setImportSettings_03() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    try {
      Import_Setting__c settings = new Import_Setting__c(
        Name = 'test',
        SObject__c = 'Account',
        Type__c = '',
        Processing__c = ''
      );
      AdministrationController.setImportSettings(new Import_Setting__c[] { settings });
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }
  @isTest
  static void setImportSettings_04() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    try {
      Import_Setting__c settings = new Import_Setting__c(
        Name = 'test',
        SObject__c = 'Account',
        Type__c = '',
        Processing__c = ''
      );
      Import_Setting__c[] importSettings = AdministrationController.setImportSettings(new Import_Setting__c[] { settings });
      System.assert(true); // should fail
    } catch (Exception ex) {
      System.assert(false); // Never reached
    }
  }

  @isTest
  static void setImportSettings_05() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    try {
      Import_Setting__c settings = new Import_Setting__c(
        Name = 'test',
        SObject__c = 'Account',
        Type__c = '',
        Processing__c = ''
      );

      insert settings;

      Import_Setting__c[] importSettings = AdministrationController.setImportSettings(new Import_Setting__c[] { settings });
      System.assert(true); // should fail
    } catch (Exception ex) {
      System.assert(false); // Never reached
    }
  }

  @isTest
  static void setBatchjobConfigured_01() {
    Test.startTest();
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    Import_Setting__c settings = new Import_Setting__c(
      Name = 'test',
      SObject__c = 'Account',
      Type__c = '',
      Processing__c = ''
    );

    insert settings;

    Import_Setting__c[] importSettings = AdministrationController.setImportSettings(new Import_Setting__c[] { settings });
    try {
      AdministrationController.setBatchJobSettingConfigured();
      System.assert(true); // should fail
    } catch (Exception ex) {
      System.assert(false); // Never reached
    }
    Test.stopTest();
  }
  @isTest
  static void setBatchjobConfigured_02() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    try {
      AdministrationController.setBatchJobSettingConfigured();
      System.assert(false); // should fail
    } catch (Exception ex) {
      System.assert(true); // Never reached
    }
  }
  @isTest
  static void setBatchjobConfigured_03() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    try {
      AdministrationController.setBatchJobSettingConfigured();
      System.assert(false); // should fail
    } catch (Exception ex) {
      System.assert(true); // Never reached
    }
  }
  @isTest
  static void setBatchjobConfigured_04() {
    AdministrationController.setConfirmSSO();
    try {
      AdministrationController.setBatchJobSettingConfigured();
      System.assert(false); // should fail
    } catch (Exception ex) {
      System.assert(true); // Never reached
    }
  }
  @isTest
  static void setBatchjobConfigured_05() {
    try {
      AdministrationController.setBatchJobSettingConfigured();
      System.assert(false); // should fail
    } catch (Exception ex) {
      System.assert(true); // Never reached
    }
  }

  @isTest
  static void getBatchJobSettings_01() {
    try {
      AdministrationController.getBatchJobSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }

  @isTest
  static void getBatchJobSettings_02() {
    AdministrationController.setConfirmSSO();
    try {
      AdministrationController.getBatchJobSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }

  @isTest
  static void getBatchJobSettings_03() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    try {
      AdministrationController.getBatchJobSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }

  @isTest
  static void getBatchJobSettings_04() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    try {
      AdministrationController.getBatchJobSettings();
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }

  @isTest
  static void getBatchJobSettings_05() {
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    AdministrationController.setImportSettingConfigured();

    try {
      insert new Batch_Settings__c(
        Name = 'Test',
        Object__c = 'Test',
        Batch_Size__c = 1,
        Disabled__c = true,
        Frequency__c = 'daily',
        End_Date__c = Date.today()
      );

      Batch_Settings__c[] settings = AdministrationController.getBatchJobSettings();

      System.assertEquals(
        1,
        settings.size()
      );
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.assert(false); // Never reached
    }
  }

  @isTest
  static void getOrbisFields_01() {
    try {
      Orbis_Field__c[] result = AdministrationController.getOrbisFields('Account');
      System.assertEquals(
        0,
        result.size()
      );
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void insertOrbisField_01() {
    Orbis_Field__c field = new Orbis_Field__c(
      Name = 'Test',
      Regular_Name__c = 'test',
      SObject__c = 'Account',
      Type__c = 'String'
    );

    Orbis_Field__c[] result = AdministrationController.insertOrbisFields(new Orbis_Field__c[] { field });

    System.assertEquals(
      1,
      result.size()
    );

    System.assertEquals(
      field.Name,
      result[0].Name
    );
  }
  @isTest
  static void insertOrbisField_02() {
    Orbis_Field__c field = new Orbis_Field__c(
      Name = 'Test',
      Regular_Name__c = 'test',
      SObject__c = 'Account',
      Type__c = 'String'
    );

    Orbis_Field__c[] result = AdministrationController.insertOrbisFields(new Orbis_Field__c[] { field });

    System.assertEquals(
      1,
      result.size()
    );

    System.assertEquals(
      field.Name,
      result[0].Name
    );

    try {
      Orbis_Field__c[] result2 = AdministrationController.insertOrbisFields(new Orbis_Field__c[] { field });
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
  }

  @isTest
  static void setBatchSettings_01() {
    Test.startTest();
    AdministrationController.setConfirmSSO();
    AdministrationController.setDataSource('test', 'test', 'url', 'url');
    AdministrationController.setFieldMappingConfigured();
    Import_Setting__c settings = new Import_Setting__c(
      Name = 'test',
      SObject__c = 'Account',
      Type__c = '',
      Processing__c = ''
    );

    insert settings;

    Import_Setting__c[] importSettings = AdministrationController.setImportSettings(new Import_Setting__c[] { settings });

    delete AdministrationController.getBatchJobSettings();

    Batch_Settings__c setting = new Batch_Settings__c(
      Name = 'Test',
      Batch_Size__c = 2,
      Frequency__c = 'Daily',
      Object__c = 'Account',
      Start_Hour__c = 13
    );

    Batch_Settings__c[] result = AdministrationController.setBatchSettings(new Batch_Settings__c[] { setting }, '', '');

    System.assertEquals(
        1,
        result.size()
    );

    System.assertEquals(
        setting.Frequency__c,
      result[0].Frequency__c
    );

    Test.stopTest();
  }

  @isTest
  static void deleteOrbisMappedField_01() {
    Orbis_Field__c field = new Orbis_Field__c(
      Name = 'Test',
      Regular_Name__c = 'test',
      SObject__c = 'Account',
      Type__c = 'String'
    );

    Orbis_Field__c[] result = AdministrationController.insertOrbisFields(new Orbis_Field__c[] { field });

    System.assertEquals(
      1,
      result.size()
    );

    System.assertEquals(
      field.Name,
      result[0].Name
    );

    AdministrationController.deleteOrbisMappedFields(field.Id);

    Integer resultCount = [ SELECT COUNT() FROM Orbis_Field__c ];
    System.assertEquals(
        0,
        resultCount
    );
  }

  @isTest
  static void deserializeDate_01() {
      Date today = Date.today();

    Date result = AdministrationController.deserializeDate(JSON.serialize(today).removeStart('"').removeEnd('"'));

    System.assert(true);
  }

  @isTest
  static void getMatrixLine_01() {
    Field_Matrix__mdt[] result = AdministrationController.getMatrixLine('UNKNOWNTYPEFORALLTIME');

    System.assertEquals(
        0,
        result.size()
    );
  }
}