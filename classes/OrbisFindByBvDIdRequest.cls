public with sharing class OrbisFindByBvDIdRequest extends OrbisRequest {
    private OrbisSchema.FindByBVDIdRequestElement requestElement = new OrbisSchema.FindByBVDIdRequestElement();

    public override OrbisResponse getOrbisResponse(HttpResponse response) {
        return new OrbisFindByBvDIdResponse(response);
    }
    protected override String getSOAPAction() {
        return 'http://bvdep.com/webservices/FindByBVDId';
    }
    protected override OrbisSchema.RequestElement getRequestSchema() {
        return requestElement;
    }

    public OrbisFindByBvDIdRequest setSessionID(String value) {
        requestElement.sessionHandle = value;
        return this;
    }

    public OrbisFindByBvDIdRequest setID(String value) {
        requestElement.id = value;
        return this;
    }

    public OrbisFindByBvDIdRequest setIDs(List<String> values) {
        requestElement.id = String.join(values,',');
        return this;
    }
}