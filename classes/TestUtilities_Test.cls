@IsTest
public with sharing class TestUtilities_Test {

  @isTest
  static void getStandardUser_01() {
    User user = TestUtilities.getStandardUser();

    System.assertEquals(
      TestUtilities.getProfile('Standard User').Id,
      user.ProfileId
    );
  }

  @isTest
  static void insertLogs_01() {
    Log__c[] logs = TestUtilities.insertLogs(0);

    System.assertEquals(
      0,
    [ SELECT Count() FROM Log__c ]
    );
  }

  @isTest
  static void insertLogs_02() {
    Log__c[] logs = TestUtilities.insertLogs(10);

    System.assertEquals(
        10,
    [ SELECT Count() FROM Log__c ]
    );
  }
}