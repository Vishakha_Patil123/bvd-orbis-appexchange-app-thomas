@IsTest
public with sharing class BatchSettingsTest {

  @TestSetup
  static void setup() {
    Batch_Settings__c settings = new Batch_Settings__c(
      Name = 'Account Batch Setting',
      Object__c = 'Account',
      Batch_Size__c = 50,
      Job_ID__c = null,
      Disabled__c = true,
      End_Date__c = Date.today().addYears(1),
      Start_Hour__c = 02,
      Start_Date__c = Date.today(),
      Frequency__c = 'daily',
      Day__c = 'Mon'
    );
    insert settings;
  }


  /**
  * Tested Scenario:
  * Custom Setting service accessor methods are invoked.
  *
  * Expected Result:
  * Custom Setting values are retrieved and returned by accesssor methods.
  * */
  @IsTest
  static void testAccessorMethods() {
    BatchSettings setting = BatchSettingsFactory.getSetting(Account.getSObjectType());
    System.assertEquals(Date.today().addYears(1), setting.getEndDate());
    System.assertEquals('Account', setting.getObjectName());
    System.assertEquals(50, setting.getBatchSize());
    System.assertEquals(null, setting.getJobID());
    System.assertEquals(true, setting.isDisabled());
    System.assertEquals(Account.getSObjectType(), setting.getSObjectType());
    System.assertEquals(2, setting.getStartHour());
    System.assertEquals(Date.today(), setting.getStartDate());
    System.assertEquals('Mon', setting.getDay());
    System.assertEquals('0 0 2 * * ?', setting.getCronExpression());

    String scheduledJobName = String.format(Label.BvD_Schedule_Job_Name, new String[]{
      'Account'
    });
    System.assertEquals(scheduledJobName, setting.getScheduledJobName());
  }


  /**
  * Tested Scenario:
  * Custom Setting service setter methods are invoked and update is requested.
  *
  * Expected Result:
  * Custom Setting values are updated.
  * */
  @IsTest
  static void testSetterMethods() {
    BatchSettings setting = BatchSettingsFactory.getSetting(Account.getSObjectType());
    setting.setJobID('08e0Y00000IIrEjQAL');
    setting.enable();
    setting.setStartHour(25);
    setting.setStartDate(Date.today());
    setting.setEndDate(Date.today().addDays(1));
    setting.setFrequency(BatchSettings.Frequency.Monthly);
    setting.updateSetting();

    Batch_Settings__c customSetting = [
      SELECT ID,
        Job_ID__c,
        Disabled__c,
        Start_Date__c,
        End_Date__c,
        Start_Hour__c,
        Frequency__c
      FROM Batch_Settings__c
      WHERE Object__c = 'Account'
    ];
    System.assertEquals('08e0Y00000IIrEjQAL', customSetting.Job_ID__c);
    System.assertEquals(false, customSetting.Disabled__c);
    System.assertEquals(1, customSetting.Start_Hour__c); /*25 Mod 24*/
    System.assertEquals(Date.today(), customSetting.Start_Date__c);
    System.assertEquals(Date.today().addDays(1), customSetting.End_Date__c);
    System.assertEquals('monthly', customSetting.Frequency__c);

    setting.disable();
    setting.updateSetting();
    customSetting = [
      SELECT ID,
        Job_ID__c,
        Disabled__c
      FROM Batch_Settings__c
      WHERE Object__c = 'Account'
    ];
    System.assertEquals(true, customSetting.Disabled__c);
  }


  /**
  * Tested Scenario:
  * Custom Setting accessor is requested for sObject Type that does not yet have
  * a setting record created.
  *
  * Expected Result:
  * Default setting record is created for given sObject Type
  * */
  @IsTest
  static void testDefaultSetting() {
    BatchSettings setting = BatchSettingsFactory.getSetting(Contact.getSObjectType());
    System.assertEquals(null, setting.getEndDate());
    System.assertEquals('Contact', setting.getObjectName());
    System.assertEquals(50, setting.getBatchSize());
    System.assertEquals(null, setting.getJobID());
    System.assertEquals(true, setting.isDisabled());
    System.assertEquals(0, setting.getStartHour());
    System.assertEquals('0 0 0 * * ?', setting.getCronExpression());

    System.assertEquals(1, [SELECT Count() FROM Batch_Settings__c WHERE Object__c = 'Contact']);
  }


  /**
  * Tested Scenario:
  * GetCronExpression method is invoked from BatchSettings instance
  *
  * Expected Result:
  * Cron Expression is constructed from Frequency and Start Hour
  * */
  @IsTest
  static void testCronExpression() {
    BatchSettings setting = BatchSettingsFactory.getSetting(Account.getSObjectType());
    System.assertEquals('0 0 2 * * ?', setting.getCronExpression());

    setting.setFrequency(BatchSettings.Frequency.Weekly);
    setting.setStartHour(3);
    setting.updateSetting();
    System.assertEquals('0 0 3 ? * Mon', setting.getCronExpression());

    setting.setFrequency(BatchSettings.Frequency.Monthly);
    setting.setStartHour(25);
    setting.updateSetting();
    System.assertEquals('0 0 1 L * ?', setting.getCronExpression());
  }
}