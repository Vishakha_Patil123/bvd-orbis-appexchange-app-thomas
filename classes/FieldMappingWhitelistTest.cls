@IsTest
public class FieldMappingWhitelistTest {

  @IsTest
  static void testLookupsAreBlacklisted() {
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'CreatedById'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'OwnerId'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'LastModifiedById'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'AccountId'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'CreatedById'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'OwnerId'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'LastModifiedById'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'CampaignId'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'DandbCompanyId'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'OwnerId'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'LastModifiedById'));
  }

  @IsTest
  static void testAddressCompoundFieldsAreBlacklisted() {
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'BillingAddress'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingCity'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingCountry'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingGeocodeAccuracy'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingLatitude'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingLongitude'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingPostalCode'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingState'));
    System.assertEquals(true, FieldMappingWhitelist.contains('Account', 'BillingStreet'));
  }

  @IsTest
  static void testNotExistingFieldsAreBlacklisted() {
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'Test'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'Test'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'Test'));
  }

  @IsTest
  static void testSystemFieldsAreBlacklisted() {
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'LastActivityDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'LastViewedDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'LastReferencedDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'IsDeleted'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Account', 'SystemModstamp'));

    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'LastActivityDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'LastViewedDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'LastReferencedDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'IsDeleted'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Contact', 'SystemModstamp'));

    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'LastActivityDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'LastViewedDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'LastReferencedDate'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'IsDeleted'));
    System.assertEquals(false, FieldMappingWhitelist.contains('Lead', 'SystemModstamp'));
  }
}