global class PostInstallScript  implements InstallHandler {

    global void onInstall(InstallContext context) {

        if(context.previousVersion() == null) {
            Log_Settings__c logsSettings = LogSettings.getSetting();
            logsSettings.Name = 'BvD - Logs Settings';
            logsSettings.Error__c = true;
            logsSettings.Info__c = false;
            logsSettings.Warn__c = false;
            logsSettings.Other__c = false;

            Import_Setting__c[] importSettings = new Import_Setting__c[] {
                    new Import_Setting__c( Name = 'Contact.Insert', Processing__c = null,  SObject__c = 'Contact', Type__c = 'Insert'),
                    new Import_Setting__c( Name = 'Account.Insert', Processing__c = null,  SObject__c = 'Account', Type__c = 'Insert'),
                    new Import_Setting__c( Name = 'Lead.Insert', Processing__c = null,  SObject__c = 'Lead', Type__c = 'Insert'),
                    new Import_Setting__c( Name = 'Lead.Update', Processing__c = null,  SObject__c = 'Lead', Type__c = 'Update'),
                    new Import_Setting__c( Name = 'Contact.Update', Processing__c = null,  SObject__c = 'Contact', Type__c = 'Update'),
                    new Import_Setting__c( Name = 'Account.Update', Processing__c = null,  SObject__c = 'Account', Type__c = 'Update')
            };

            Bvd_Setup__c bvdSetup = AdministrationController.getBvDSetup();
            bvdSetup.Name = 'BvD - Setup';
            bvdSetup.Rest_Url__c = 'https://clientlogin.bvdinfo.com/crm/salesforce/';
            bvdSetup.Organization_Token__c = GuidGenerator.generate().remove('-').left(16);
            BatchSettingsFactory.getSetting(Schema.Account.SObjectType);
            BatchSettingsFactory.getSetting(Schema.Contact.SObjectType);
            BatchSettingsFactory.getSetting(Schema.Lead.SObjectType);

            Field_Mapping__c[] originalMappings = new Field_Mapping__c[]{
                    new Field_Mapping__c (BvD_Field__C = 'BVD_ID_NUMBER',
                                          BvD_Field_Label__c = 'BvD ID number',
                                          SF_Field__c = 'BvD_Id__c',
                                          BvD_Field_Type__c = 'String',
                                          SObject__c = 'Lead',
                                          Type__c='STRING',
                                          Type_Info__c='255',
                                          SF_Field_Label__c = 'BvD Id',
                                          Master__c = 'BvD',
                                          Action__c = 'Automatic',
                                          Mapped__c = true,
                                          Restricted__c = true
                    ),
                    new Field_Mapping__c (BvD_Field__C = 'BVD_ID_NUMBER',
                                          BvD_Field_Label__c = 'BvD ID number',
                                          SF_Field__c = 'BvD_Id__c',
                                          BvD_Field_Type__c = 'String',
                                          SObject__c = 'Account',
                                          Type__c='STRING',
                                          Type_Info__c='255',
                                          SF_Field_Label__c = 'BvD Id',
                                          Master__c = 'BvD',
                                          Action__c = 'Automatic',
                                          Mapped__c = true,
                                          Restricted__c = true
                    ),
                    new Field_Mapping__c (BvD_Field__C = 'CPYCONTACTS_HEADER_BvdId',
                                          BvD_Field_Label__c = 'Corresponding BvDID (when applicable)',
                                          SF_Field__c = 'BvD_Id__c',
                                          BvD_Field_Type__c = 'String',
                                          SObject__c = 'Contact',
                                          Type__c='STRING',
                                          Type_Info__c='255',
                                          SF_Field_Label__c = 'BvD Id',
                                          Master__c = 'BvD',
                                          Action__c = 'Automatic',
                                          Mapped__c = true,
                                          Restricted__c = true
                    )

             };
            upsert originalMappings;
            upsert bvdSetup;
            upsert logsSettings;
            upsert importSettings;
        }
    }
}