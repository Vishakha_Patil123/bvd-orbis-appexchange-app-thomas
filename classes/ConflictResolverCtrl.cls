/**
* Controller for Conflict Resolver component.
* */
public with sharing class ConflictResolverCtrl {


  @AuraEnabled
  public static List<SyncedRecord> getWorkItems(Id jobId) {
    List<SyncedRecord> result = new List<SyncedRecord>();

    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    List<BvD_Job_Work_Item__c> workItemsWithConflicts = BvDJobWorkItemsSelector.getWorkItemsWithAnyConflicts(jobId);
    Map<Id, SObject> recordsByIds = getSalesforceRecords(job, workItemsWithConflicts);
    SObjectDescribe bvdJobSObjectDescribe = SObjectDescribe.getDescribe(BvDJobsSelector.getJobById(jobId).Object__c);


    for (BvD_Job_Work_Item__c wi : workItemsWithConflicts) {
      result.add(new SyncedRecord(wi, bvdJobSObjectDescribe, recordsByIds.get(wi.RecordId__c)));
    }

    return result;
  }

  /**
  * Update field conflicts with User choices
  * */
  @AuraEnabled
  public static void updateRecords(String recordsStr) {
    List<SyncedRecord> records = (List<SyncedRecord>) JSON.deserialize(recordsStr, List<SyncedRecord>.class);

    List<Work_Item_Field_Conflict__c> fieldConflicts = new List<Work_Item_Field_Conflict__c>();
    for (SyncedRecord record : records) {
      fieldConflicts.addAll(record.getFieldConflicts());
    }
      if(BVDUserPermissionCheck.permissionToUpdateSobject('Work_Item_Field_Conflict__c', true, new List<String>{'Field__c'})){
          update fieldConflicts;

        }else{
          Logger.error('Not enough privileges to update Work Item');
          throw new AuraHandledException('Not enough privileges to update Work Item');        
		}
  }

  @AuraEnabled
  public static BvD_Job__c retryJob(Id jobId, Boolean async) {
    if (async) {
      BvDJobs.retryJob(jobId);
      return new BvD_Job__c(Id = jobId, Status__c = BvDJobs.STATUS_SUCCESS);

    } else {
      BvD_Job__c job = BvDJobs.retryJobImmediatly(jobId);
      return job;
    }
  }

  private static Map<Id, SObject> getSalesforceRecords(BvD_Job__c job, List<BvD_Job_Work_Item__c> workItems) {
    Set<String> sfFields = new Set<String>();
    Set<String> ids = new Set<String>();

    for (BvD_Job_Work_Item__c workItem : workItems) {
      ids.add(workItem.RecordId__c);

      for (Work_Item_Field_Conflict__c fc : workItem.Field_Conflicts__r) {
        sfFields.add(fc.Field__c);
      }
    }

    return new Map<Id, sObject>(Database.query(
      'SELECT '
        + String.join(new List<String>(sfFields), ',')
        + ' FROM ' + job.Object__c
        + ' WHERE ID IN :ids'
        + ' LIMIT 49990'
    ));
  }


  /**
  * Wrapper class for a record synchronized with BvD.
  * Contains SF record ID,
  * synchronization status which informs if record is up to date or requires manual merge,
  * and list of synchronized fields.
  * */
  public class SyncedRecord {
    @AuraEnabled public ID recordId;
    @AuraEnabled public String recordName;
    @AuraEnabled public String status;
    @AuraEnabled public List<SyncedField> fields;
    @AuraEnabled public String sObjectLabel;

    public SyncedRecord(BvD_Job_Work_Item__c workItem, SObjectDescribe bvdJobSObjectDescribe, SObject record) {
      this.recordId = workItem.RecordId__c;
      this.recordName = workItem.Record_Name__c;
      this.status = workItem.Status__c;
      this.fields = new List<SyncedField>();
      this.sObjectLabel = bvdJobSObjectDescribe.getDescribe().label;

      for (Work_Item_Field_Conflict__c issue : workItem.Field_Conflicts__r) {
        if (issue.Field__c != null
          && bvdJobSObjectDescribe.getFieldsMap().containsKey(issue.Field__c)
          && bvdJobSObjectDescribe.getField(issue.Field__c).getDescribe().isAccessible()) {

          this.fields.add(new SyncedField(issue, bvdJobSObjectDescribe, record));
        }
      }
      this.fields.sort();
    }

    public List<Work_Item_Field_Conflict__c> getFieldConflicts() {
      List<Work_Item_Field_Conflict__c> fieldConflicts = new List<Work_Item_Field_Conflict__c>();
      for (SyncedField field : this.fields) {
        fieldConflicts.add(field.toSObject());
      }
      return fieldConflicts;
    }
  }

  /**
  * Wrapper for Synchronized field.
  * Contains information about field, Salesforce and BvD values and
  * User's choice which source should be used.
  * */
  public class SyncedField implements Comparable {
    @AuraEnabled public String id;
    @AuraEnabled public String workItemId;
    @AuraEnabled public String fieldLabel;
    @AuraEnabled public String fieldName;
    @AuraEnabled public String choice = 'Salesforce';
    @AuraEnabled public String defaultChoice = 'Salesforce';
    @AuraEnabled public String SFValue;
    @AuraEnabled public String BVDValue;

    public SyncedField(Work_Item_Field_Conflict__c issue, SObjectDescribe bvdJobSObjectDescribe, SObject record) {
      this.BVDValue = issue.BvD_Value__c;
      this.SFValue = record == null ? '' : String.valueOf(record.get(issue.Field__c));
      this.choice = issue.Choice__c;
      this.fieldName = issue.Field__c;
      this.id = issue.Id;
      this.workItemId = issue.Work_Item__c;
      this.fieldLabel = bvdJobSObjectDescribe.getField(this.fieldName).getDescribe().label;
      this.defaultChoice = FieldMappings.getInstance(bvdJobSObjectDescribe.getDescribe().getName()).getDefaultChoice(fieldName);

      if (String.isBlank(this.defaultChoice)) {
        this.defaultChoice = this.choice;
      }
    }

    public Work_Item_Field_Conflict__c toSObject() {
      return new Work_Item_Field_Conflict__c(
        Id = this.id,
        Choice__c = this.choice
      );
    }

    public Integer compareTo(Object other) {
      return this.fieldLabel.compareTo(((SyncedField) other).fieldLabel);
    }
  }
}