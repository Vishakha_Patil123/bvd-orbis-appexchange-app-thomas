public with sharing class LogSettings {

  private final static String SETTING_NAME = 'BvD - Logs Settings';
  private final static Log_Settings__c SETTING;

  static {
    SETTING = Log_Settings__c.getInstance(SETTING_NAME);

    if (SETTING == null) {
      SETTING = new Log_Settings__c(
        Name = 'BvD - Logs Settings',
        Error__c = true
      );
    }
  }

  public static Log_Settings__c getSetting() {
    return SETTING;
  }

  public static String getName() {
    return SETTING.Name;
  }

  public static Boolean isErrorEnabled() {
    return SETTING.Error__c;
  }

  public static Boolean isWarnEnabled() {
    return SETTING.Warn__c;
  }

  public static Boolean isInfoEnabled() {
    return SETTING.Info__c;
  }

  public static Boolean isOtherEnabled() {
    return SETTING.Other__c;
  }

  public static Boolean enableError() {
    SETTING.Error__c = true;

    upsert SETTING;
    return isErrorEnabled();
  }

  public static Boolean enableWarn() {
    SETTING.Warn__c = true;

    upsert SETTING;
    return isWarnEnabled();
  }

  public static Boolean enableInfo() {
    SETTING.Info__c = true;

    upsert SETTING;
    return isInfoEnabled();
  }

  public static Boolean enableOther() {
    SETTING.Other__c = true;

    upsert SETTING;
    return isOtherEnabled();
  }
}