public with sharing class LogsController {

  @AuraEnabled
  public static List<String> getLogFields() {
    return CommonUtilities.getFields('Log__c');
  }
  
  @AuraEnabled
  public static List<ListView> getListViews(String sObjectType){
    return [SELECT Id,DeveloperName ,SobjectType, Name
    FROM   ListView
    WHERE SobjectType =:sObjectType];
  }

  @AuraEnabled
  public static List<Map<String, String>> getLoggingLevels() {
    List<Map<String, String>> loggingLevels     = new List<Map<String, String>> {};
    List<Schema.PicklistEntry> pickListEntries  = Log__c.Logging_Level__c.getDescribe().getPicklistValues();

    loggingLevels.add(
      new Map<String, String> {
        'label' => 'ALL',
        'value' => 'ALL'
      }
    );

    for(Schema.PicklistEntry entry: pickListEntries) {
      loggingLevels.add(
        new Map<String, String> {
          'label' => entry.getLabel(),
          'value' => entry.getValue()
        }
      );
    }

    return loggingLevels;
  }

  /**
  Fetches the amount of logs available.

  Throw an error if object or fields are not accessible.
   */
  @AuraEnabled
  public static Integer logsCount() {
    Logger.start('LogsController', 'logsCount');

    List<String> logFields = getLogFields();

    CommonUtilities.checkReadAndThrowAura(Log__c.getSObjectType(), logFields);

    Logger.stop();

    return [ SELECT COUNT() FROM Log__c ];
  }

  @AuraEnabled
  public static Log__c[] getLogs(String startCount) {
    System.debug(startCount);
    Logger.start('LogsController', 'getLogs');

    List<String> logFields = getLogFields();

    CommonUtilities.checkReadAndThrowAura(Log__c.getSObjectType(), logFields);

    try {
      Log__c[] logs = new Log__c[] {};

      String query = String.format('SELECT {0} FROM Log__c ORDER BY CreatedDate LIMIT 2000 OFFSET {1}', new String[] {
        CommonUtilities.getLogFieldsQuery(logFields), startCount
      });

      logs.addAll((Log__c[]) Database.query(query));

      Logger.stop();
      return logs;
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }

  @AuraEnabled
  public static Log__c[] getLogs2(String level, Log__c lastLog) {
    Logger.start('LogsController', 'getLogs');

    List<String> logFields = getLogFields();

    CommonUtilities.checkReadAndThrowAura(Log__c.getSObjectType(), logFields);

    try {
      Log__c[] logs = new Log__c[] {};

      DateTime createdDate = lastLog != null ? lastLog.CreatedDate : null;

      String[] conditions   = new String[] {};

      if(level != null && !String.isBlank(level) && !'ALL'.equalsIgnoreCase(level)) {
        conditions.add('Logging_Level__c = :level');
      }

      if(createdDate != null) {
        conditions.add('CreatedDate > :createdDate');
      }

      String whereQuery    = !conditions.isEmpty() ? 'WHERE ' + String.join(conditions, ' AND ') : '';

      String query = String.format('SELECT {0} FROM {1} {2} ORDER BY CreatedDate LIMIT 750', new String[] {
        CommonUtilities.getLogFieldsQuery(logFields), 'Log__c', whereQuery
      });

      logs.addAll((Log__c[]) Database.query(query));

      Logger.stop();
      return logs;
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }

  @AuraEnabled
  public static Boolean logsAvailable(String level) {
    Logger.start('LogsController', 'logsAvailable');
    try {
      if('ALL'.equalsIgnoreCase(level)) {
        Log__c log = [ SELECT Id, Logging_Level__c FROM Log__c LIMIT 1 ];
      } else {
        Log__c log = [ SELECT Id, Logging_Level__c FROM Log__c WHERE Logging_Level__c = :level LIMIT 1 ];
      }

      return true;
    } catch (Exception ex) {
      Logger.error(ex);
      return false;
    } finally {
      Logger.stop();
    }
  }
  @AuraEnabled
  public static Integer deleteLogs(String level) {
    Logger.start('LogsController', 'deleteLogs');

    List<String> logFields = getLogFields();

    CommonUtilities.checkReadAndThrowAura(Log__c.getSObjectType(), logFields);
    try {
      String[] conditions   = new String[] {};

      if(level != null && !String.isBlank(level) && !'ALL'.equalsIgnoreCase(level)) {
        conditions.add('Logging_Level__c = :level');
      }

      String whereQuery    = !conditions.isEmpty() ? 'WHERE ' + String.join(conditions, ' AND ') : '';

      String query = String.format('SELECT Id FROM {0} {1} LIMIT 750', new String[] {
        'Log__c', whereQuery
      });

      Database.deleteResult[] results = Database.delete(Database.query(query));

      return results.size();
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }
}