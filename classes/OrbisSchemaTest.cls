@IsTest
public class OrbisSchemaTest {

  @IsTest
  static void testOpenRequestSchema() {
    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.OpenElement openElement = new OrbisSchema.OpenElement();
    envelopeElement.body.content = openElement;

    openElement.username = 'Test Username';
    openElement.password = 'Test Password';


    Test.startTest();
    Dom.Document document = envelopeElement.toDOMDocument();
    Test.stopTest();

    Dom.XmlNode openNode = document.getRootElement()
      .getChildElement('Body', OrbisSchema.NAMESPACE_SOAP)
      .getChildElement('Open', OrbisSchema.NAMESPACE_BVD);

    System.assertEquals(openElement.username, openNode.getChildElement('username', null).getText());
    System.assertEquals(openElement.password, openNode.getChildElement('password', null).getText());
  }

  @IsTest
  static void testOpenResponseDeserialization() {
    String xml = '<?xml version="1.0" encoding="utf-8"?> ' +
      '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"' +
      ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
      ' xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
      '<soap:Body> ' +
      '<OpenResponse xmlns="http://bvdep.com/webservices/"> ' +
      '<OpenResult>SZG8ECYSCJ6F95Y</OpenResult> ' +
      '</OpenResponse> ' +
      '</soap:Body> ' +
      '</soap:Envelope>';

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.OpenResponseElement response = new OrbisSchema.OpenResponseElement();
    envelopeElement.body.content = response;

    Dom.Document document = new Dom.Document();
    document.load(xml);

    Test.startTest();
    envelopeElement.deserializeXML(document);
    Test.stopTest();

    System.assertEquals('SZG8ECYSCJ6F95Y', response.OpenResult);

  }

  @IsTest
  static void testMatchRequestSerialization() {
    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.MatchElement matchElement = new OrbisSchema.MatchElement();
    envelopeElement.body.content = matchElement;

    matchElement.sessionHandle = 'sessionHandle';

    OrbisSchema.MatchCriteriaElement matchCriteria = new OrbisSchema.MatchCriteriaElement();
    matchElement.criteria = matchCriteria;

    matchCriteria.Name = 'Name';
    matchCriteria.Address = 'Address';
    matchCriteria.PostCode = 'PostCode';
    matchCriteria.City = 'City';
    matchCriteria.Country = 'Country';
    matchCriteria.PhoneOrFax = 'PhoneOrFax';
    matchCriteria.EMailOrWebsite = 'EMailOrWebsite';
    matchCriteria.NationalId = 'NationalId';
    matchCriteria.Ticker = 'Ticker';
    matchCriteria.Isin = 'Isin';
    matchCriteria.State = 'State';


    Test.startTest();
    Dom.Document document = envelopeElement.toDOMDocument();
    Test.stopTest();

    Dom.XmlNode matchNode = document.getRootElement()
      .getChildElement('Body', OrbisSchema.NAMESPACE_SOAP)
      .getChildElement('Match', OrbisSchema.NAMESPACE_BVD);

    System.assertEquals(matchElement.sessionHandle, matchNode.getChildElement('sessionHandle', null).getText());
    Dom.XmlNode criteriaNode = matchNode.getChildElement('criteria', null);

    System.assertEquals(matchCriteria.Name, criteriaNode.getChildElement('Name', null).getText());
    System.assertEquals(matchCriteria.Address, criteriaNode.getChildElement('Address', null).getText());
    System.assertEquals(matchCriteria.PostCode, criteriaNode.getChildElement('PostCode', null).getText());
    System.assertEquals(matchCriteria.City, criteriaNode.getChildElement('City', null).getText());
    System.assertEquals(matchCriteria.Country, criteriaNode.getChildElement('Country', null).getText());
    System.assertEquals(matchCriteria.PhoneOrFax, criteriaNode.getChildElement('PhoneOrFax', null).getText());
    System.assertEquals(matchCriteria.EMailOrWebsite, criteriaNode.getChildElement('EMailOrWebsite', null).getText());
    System.assertEquals(matchCriteria.NationalId, criteriaNode.getChildElement('NationalId', null).getText());
    System.assertEquals(matchCriteria.Ticker, criteriaNode.getChildElement('Ticker', null).getText());
    System.assertEquals(matchCriteria.Isin, criteriaNode.getChildElement('Isin', null).getText());
    System.assertEquals(matchCriteria.State, criteriaNode.getChildElement('State', null).getText());
  }

  @IsTest
  static void testMatchResponseDeserialization() {
    String xml = '<?xml version="1.0" encoding="utf-8"?>'
      + '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'
      + '  <soap:Body>'
      + '    <MatchResponse xmlns="http://bvdep.com/webservices/">'
      + '      <MatchResult>'
      + '        <MatchResult>'
      + '          <BvDID>BvDID0</BvDID>'
      + '          <Score>0</Score>'
      + '          <Hint>SelectedCandidate0</Hint>'
      + '          <Name>Name0</Name>'
      + '          <NameInLocalAlphabet>NameInLocalAlphabet0</NameInLocalAlphabet>'
      + '          <Address>Address0</Address>'
      + '          <PostCode>PostCode0</PostCode>'
      + '          <City>City0</City>'
      + '          <Country>Country0</Country>'
      + '          <Region>Region0</Region>'
      + '          <PhoneOrFax>PhoneOrFax0</PhoneOrFax>'
      + '          <EMailOrWebsite>EMailOrWebsite0</EMailOrWebsite>'
      + '          <NationalId>NationalId0</NationalId>'
      + '          <Ticker>Ticker0</Ticker>'
      + '          <Isin>Isin0</Isin>'
      + '          <State>State0</State>'
      + '          <Status>Status0</Status>'
      + '          <BvD9>BvD90</BvD9>'
      + '          <LegalForm>LegalForm0</LegalForm>'
      + '          <ConsolidationCode>ConsolidationCode0</ConsolidationCode>'
      + '          <CustomRule>CustomRule0</CustomRule>'
      + '        </MatchResult>'
      + '        <MatchResult>'
      + '          <BvDID>BvDID1</BvDID>'
      + '          <Score>1</Score>'
      + '          <Hint>SelectedCandidate1</Hint>'
      + '          <Name>Name1</Name>'
      + '          <NameInLocalAlphabet>NameInLocalAlphabet1</NameInLocalAlphabet>'
      + '          <Address>Address1</Address>'
      + '          <PostCode>PostCode1</PostCode>'
      + '          <City>City1</City>'
      + '          <Country>Country1</Country>'
      + '          <Region>Region1</Region>'
      + '          <PhoneOrFax>PhoneOrFax1</PhoneOrFax>'
      + '          <EMailOrWebsite>EMailOrWebsite1</EMailOrWebsite>'
      + '          <NationalId>NationalId1</NationalId>'
      + '          <Ticker>Ticker1</Ticker>'
      + '          <Isin>Isin1</Isin>'
      + '          <State>State1</State>'
      + '          <Status>Status1</Status>'
      + '          <BvD9>BvD91</BvD9>'
      + '          <LegalForm>LegalForm1</LegalForm>'
      + '          <ConsolidationCode>ConsolidationCode1</ConsolidationCode>'
      + '          <CustomRule>CustomRule1</CustomRule>'
      + '        </MatchResult>'
      + '      </MatchResult>'
      + '    </MatchResponse>'
      + '  </soap:Body>'
      + '</soap:Envelope>';
    Dom.Document document = new Dom.Document();
    document.load(xml);

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.MatchResponseElement response = new OrbisSchema.MatchResponseElement();
    envelopeElement.body.content = response;

    envelopeElement.deserializeXML(document);

    System.assertEquals(2, response.matchResultContainer.matchResults.size());
    for (Integer i = 0; i < response.matchResultContainer.matchResults.size(); i++) {
      OrbisSchema.MatchResult matchResult = response.matchResultContainer.matchResults.get(i);

      System.assertEquals(Double.valueOf(i), matchResult.Score);
      System.assertEquals('BvDID' + i, matchResult.BvDID);
      System.assertEquals('SelectedCandidate' + i, matchResult.Hint_x);
      System.assertEquals('Name' + i, matchResult.Name);
      System.assertEquals('NameInLocalAlphabet' + i, matchResult.NameInLocalAlphabet);
      System.assertEquals('Address' + i, matchResult.Address);
      System.assertEquals('PostCode' + i, matchResult.PostCode);
      System.assertEquals('City' + i, matchResult.City);
      System.assertEquals('Country' + i, matchResult.Country);
      System.assertEquals('Region' + i, matchResult.Region);
      System.assertEquals('PhoneOrFax' + i, matchResult.PhoneOrFax);
      System.assertEquals('EMailOrWebsite' + i, matchResult.EMailOrWebsite);
      System.assertEquals('NationalId' + i, matchResult.NationalId);
      System.assertEquals('Ticker' + i, matchResult.Ticker);
      System.assertEquals('Isin' + i, matchResult.Isin);
      System.assertEquals('State' + i, matchResult.State);
      System.assertEquals('Status' + i, matchResult.Status);
      System.assertEquals('BvD9' + i, matchResult.BvD9);
      System.assertEquals('LegalForm' + i, matchResult.LegalForm);
      System.assertEquals('ConsolidationCode' + i, matchResult.ConsolidationCode);
      System.assertEquals('CustomRule' + i, matchResult.CustomRule);
    }
  }

  @IsTest
  static void testGetFieldRequestSerialization() {
    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetAvailableFieldsElement requestElement = new OrbisSchema.GetAvailableFieldsElement();
    envelopeElement.body.content = requestElement;

    requestElement.sessionHandle = 'sessionHandle';
    requestElement.modelId = 'modelId';


    Test.startTest();
    Dom.Document document = envelopeElement.toDOMDocument();
    Test.stopTest();

    Dom.XmlNode requestNode = document.getRootElement()
      .getChildElement('Body', OrbisSchema.NAMESPACE_SOAP)
      .getChildElement('GetAvailableFields', OrbisSchema.NAMESPACE_BVD);

    System.assertEquals(requestElement.sessionHandle, requestNode.getChildElement('sessionHandle', null).getText());
    System.assertEquals(requestElement.modelId, requestNode.getChildElement('modelId', null).getText());
  }

  @IsTest
  static void testGetFieldResponseDeserialization() {
    String xml = '<?xml version="1.0" encoding="utf-8"?>'
      + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
      + '    <soap:Body>'
      + '        <GetAvailableFieldsResponse xmlns="http://bvdep.com/webservices/">'
      + '            <GetAvailableFieldsResult>....</GetAvailableFieldsResult>'
      + '        </GetAvailableFieldsResponse>'
      + '    </soap:Body>'
      + '</soap:Envelope>';
    Dom.Document document = new Dom.Document();
    document.load(xml);


    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetAvailableFieldsResponseElement response = new OrbisSchema.GetAvailableFieldsResponseElement();
    envelopeElement.body.content = response;

    envelopeElement.deserializeXML(document);

    System.assertEquals('....', response.GetAvailableFieldsResult);
  }

  @IsTest
  static void testUpdateMyDataTableRequestSerialization() {
    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.UpdateMyDataTableElement requestElement = new OrbisSchema.UpdateMyDataTableElement();
    envelopeElement.body.content = requestElement;

    requestElement.sessionHandle = 'sessionHandle';
    requestElement.table = 'table';
    requestElement.appendData = true;

    List<OrbisSchema.UpdateMyDataTableDataUploadDataRowElement> uploadDataRows = requestElement.data.uploadDataRows;
    uploadDataRows.addAll(new List<OrbisSchema.UpdateMyDataTableDataUploadDataRowElement>{
      new OrbisSchema.UpdateMyDataTableDataUploadDataRowElement(
        'NL33242490'
      ),

      new OrbisSchema.UpdateMyDataTableDataUploadDataRowElement(
        'NL33242491'
      )
    });


    List<OrbisSchema.UploadValue> uploadValues = uploadDataRows.get(0).values.uploadValues;
    uploadValues.addAll(new List<OrbisSchema.UploadValue>{
      new OrbisSchema.UploadValue(
        'CF00001', '0018000000rsH4K1'
      ),
      new OrbisSchema.UploadValue(
        'CF00002', 'Account1'
      )
    });

    List<OrbisSchema.UploadValue> uploadValues2 = uploadDataRows.get(1).values.uploadValues;
    uploadValues2.addAll(new List<OrbisSchema.UploadValue>{
      new OrbisSchema.UploadValue(
        'CF00003', '0018000000rsH4K2'
      ),
      new OrbisSchema.UploadValue(
        'CF00004', 'Account2'
      )
    });

    Test.startTest();
    Dom.Document document = envelopeElement.toDOMDocument();
    System.debug('toXmlString' + document.toXmlString());
    Test.stopTest();

    Dom.XmlNode requestNode = document.getRootElement()
      .getChildElement('Body', OrbisSchema.NAMESPACE_SOAP)
      .getChildElement('UpdateMyDataTable', OrbisSchema.NAMESPACE_BVD);

    System.assertEquals('sessionHandle', requestNode.getChildElement('sessionHandle', null).getText());
    System.assertEquals('table', requestNode.getChildElement('table', null).getText());

    List<Dom.XmlNode> uploadDataRowsNodes = requestNode.getChildElement('data', null).getChildElements();
    System.assertEquals(uploadDataRows.size(), uploadDataRowsNodes.size());
    System.assertEquals('NL33242490', uploadDataRowsNodes.get(0).getChildElement('BvDID', null).getText());
    System.assertEquals('NL33242491', uploadDataRowsNodes.get(1).getChildElement('BvDID', null).getText());

    List<Dom.XmlNode> uploadValuesNodes = requestNode.getChildElement('data', null).getChildElement('UploadDataRow', null).getChildElement('Values', null).getChildElements();
    System.assertEquals(uploadValues.size(), uploadValuesNodes.size());
    System.assertEquals('CF00001', uploadValuesNodes.get(0).getChildElement('CustomDataFieldId', null).getText());
    System.assertEquals('0018000000rsH4K1', uploadValuesNodes.get(0).getChildElement('Value', null).getText());
    System.assertEquals('CF00002', uploadValuesNodes.get(1).getChildElement('CustomDataFieldId', null).getText());
    System.assertEquals('Account1', uploadValuesNodes.get(1).getChildElement('Value', null).getText());
  }

  @IsTest
  static void testUpdateMyDataTableRequestDeserialization() {
    String xml = '<?xml version="1.0" encoding="utf-8"?>'
      + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
      + '<soap:Body>'
      + '<UpdateMyDataTableResponse xmlns="http://bvdep.com/webservices/">'
      + '<UpdateMyDataTableResult>'
      + '<Success>true</Success>'
      + '<IndexationProcessId>-1</IndexationProcessId>'
      + '<Errors>'
      + '<string>No valid custom field in the file</string>'
      + '<string>No valid custom field in the file2</string>'
      + '</Errors>'
      + '<Warnings>'
      + '<string>Warning line 1, column \'CF00001ss\': The data is skipped because no matching custom field could be found in table \'CRMLinks\'</string>'
      + '<string>Warning line 2, column \'CF00002sss\': The data is skipped because no matching custom field could be found in table \'CRMLinks\'</string>'
      + '</Warnings>'
      + '<UnMatchedRecords />'
      + '</UpdateMyDataTableResult>'
      + '</UpdateMyDataTableResponse>'
      + '</soap:Body>'
      + '</soap:Envelope>';

    Dom.Document document = new Dom.Document();
    document.load(xml);

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.UpdateMyDataTableResponseElement response = new OrbisSchema.UpdateMyDataTableResponseElement();
    envelopeElement.body.content = response;
    envelopeElement.deserializeXML(document);

    System.assertEquals(true, response.result.success);
    System.assertEquals(2, response.result.errors.stringElements.size());
    System.assertEquals('No valid custom field in the file', response.result.errors.stringElements.get(0).stringElement);
    System.assertEquals('No valid custom field in the file2', response.result.errors.stringElements.get(1).stringElement);

    System.assertEquals(2, response.result.warnings.stringElements.size());
    System.assertEquals('Warning line 1, column \'CF00001ss\': The data is skipped because no matching custom field could be found in table \'CRMLinks\'', response.result.warnings.stringElements.get(0).stringElement);
    System.assertEquals('Warning line 2, column \'CF00002sss\': The data is skipped because no matching custom field could be found in table \'CRMLinks\'', response.result.warnings.stringElements.get(1).stringElement);

  }

  @IsTest
  static void testFaultCodeDeserialization() {
    String xml = '<?xml version="1.0" encoding="utf-8"?> ' +
      '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
      'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
      'xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
      '<soap:Body> ' +
      '<soap:Fault> ' +
      '<faultcode>soap:Server</faultcode> ' +
      '<faultstring>' +
      'System.Web.Services.Protocols.SoapException: Server was unable to process request.' +
      ' ---&gt; System.InvalidOperationException: Invalid handle 2D5ZECYSCEOW38J1 at bvd.ApplicationRoot.Web.Session.SessionHandle' +
      '..ctor(String strHandle) ' +
      'at ProxyRemoteAccess.Stat..ctor(String methodName, String sessionID, String princParamID, ' +
      'String secParamID, DateTime dateOfRequest, Boolean timer) ' +
      'in Q:\\QuickFixes\\Releases\\Net\\webservices.bvdep.com\\RootVersion\\web\\StatsCollector.cs:line 60 ' +
      'at ProxyRemoteAccess.RemoteAccess.add2Stats(String session, String method, String princParam, ' +
      'String secParam) in Q:\\QuickFixes\\Releases\\Net\\webservices.bvdep.com\\RootVersion\\web\\RemoteAc' +
      'cess.asmx.cs:line 97 at ProxyRemoteAccess.RemoteAccess.GetAvailableModels(String sessionHandle) ' +
      'in Q:\\QuickFixes\\Releases\\Net\\webservices.bvdep.com\\RootVersion\\web\\RemoteAccess.asmx.cs:lin' +
      'e 453 --- End of inner exception stack trace ---' +
      '</faultstring> ' +
      '<detail /> ' +
      '</soap:Fault> ' +
      '</soap:Body> ' +
      '</soap:Envelope>';

    Dom.Document document = new Dom.Document();
    document.load(xml);


    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.FaultElement response = new OrbisSchema.FaultElement();
    envelopeElement.body.content = response;

    envelopeElement.deserializeXML(document);

    System.assertEquals('soap:Server', response.faultcode);
    System.assert(response.faultstring.startsWith('System.Web.Services.Protocols.SoapException: '));
  }

  @IsTest
  static void testGetOwnershipStructureSerialization() {
    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetOwnershipStructureElement requestElement = new OrbisSchema.GetOwnershipStructureElement();
    envelopeElement.body.content = requestElement;

    requestElement.sessionHandle = 'sessionHandle';
    requestElement.subjectCompanyBvDID = 'subjectCompanyBvDID';
    requestElement.flowType = OrbisSchema.FlowType.All;
    requestElement.minimumPercentage = 5.0;
    requestElement.useIntegratedOwnership = true;


    Test.startTest();
    Dom.Document document = envelopeElement.toDOMDocument();
    Test.stopTest();

    Dom.XmlNode requestNode = document.getRootElement()
      .getChildElement('Body', OrbisSchema.NAMESPACE_SOAP)
      .getChildElement('GetOwnershipStructure', OrbisSchema.NAMESPACE_BVD);

    System.assertEquals(requestElement.sessionHandle, requestNode.getChildElement('sessionHandle', null).getText());
    System.assertEquals(requestElement.subjectCompanyBvDID, requestNode.getChildElement('subjectCompanyBvDID', null).getText());
    System.assertEquals('All', requestNode.getChildElement('flowType', null).getText());
    System.assertEquals('5.0', requestNode.getChildElement('minimumPercentage', null).getText());
    System.assertEquals('true', requestNode.getChildElement('useIntegratedOwnership', null).getText());
  }

  @IsTest
  static void testGetOwnershipStructureResponseDeserialization() {
    String xml = '<?xml version="1.0" encoding="utf-8"?>' +
      '<soap:Envelope ' +
      'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
      'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
      'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<soap:Body>' +
      '<GetOwnershipStructureResponse ' +
      'xmlns="http://bvdep.com/webservices/">' +
      '<GetOwnershipStructureResult>' +
      '<Nodes>' +

      '<OwnershipGraphNode>' +
      '<BvDID>BVD_01</BvDID>' +
      '<Name>Name1</Name>' +
      '<EntityType>EntityType</EntityType>' +
      '<CountryISO2Code>USD</CountryISO2Code>' +
      '<City>London</City>' +
      '<BvDIndependenceIndicator>BvDIndependenceIndicator</BvDIndependenceIndicator>' +
      '<IsInWorldCompliance>true</IsInWorldCompliance>' +
      '<Distance>197</Distance>' +
      '<IntegratedPercentage>15</IntegratedPercentage>' +
      '<IsIntegratedPercentageEstimation>false</IsIntegratedPercentageEstimation>' +
      '</OwnershipGraphNode>' +

      '<OwnershipGraphNode>' +
      '<BvDID>BVD_02</BvDID>' +
      '<Name>Name2</Name>' +
      '<EntityType>EntityType2</EntityType>' +
      '<CountryISO2Code>GBP</CountryISO2Code>' +
      '<City>London</City>' +
      '<BvDIndependenceIndicator>BvDIndependenceIndicator2</BvDIndependenceIndicator>' +
      '<IsInWorldCompliance>false</IsInWorldCompliance>' +
      '<Distance>156</Distance>' +
      '<IntegratedPercentage>60</IntegratedPercentage>' +
      '<IsIntegratedPercentageEstimation>false</IsIntegratedPercentageEstimation>' +
      '</OwnershipGraphNode>' +

      '</Nodes>' +
      '<Edges>' +
      '<OwnershipGraphEdge>' +
      '<ParentBvDID>BVD_01</ParentBvDID>' +
      '<ChildBvDID>BVD_02</ChildBvDID>' +
      '<Source>Source1</Source>' +
      '<Date>Date1</Date>' +
      '<DirectPercentage>DirectPercentage1</DirectPercentage>' +
      '<TotalPercentage>TotalPercentage1</TotalPercentage>' +
      '</OwnershipGraphEdge>' +

      '<OwnershipGraphEdge>' +
      '<ParentBvDID>BVD_02</ParentBvDID>' +
      '<ChildBvDID>BVD_01</ChildBvDID>' +
      '<Source>Source2</Source>' +
      '<Date>Date2</Date>' +
      '<DirectPercentage>DirectPercentage2</DirectPercentage>' +
      '<TotalPercentage>TotalPercentage2</TotalPercentage>' +
      '</OwnershipGraphEdge>' +
      '</Edges>' +

      '</GetOwnershipStructureResult>' +
      '</GetOwnershipStructureResponse>' +
      '</soap:Body>' +
      '</soap:Envelope>';

    Dom.Document document = new Dom.Document();
    document.load(xml);

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetOwnershipStructureResponse responseElement = new OrbisSchema.GetOwnershipStructureResponse();
    envelopeElement.body.content = responseElement;
    envelopeElement.deserializeXML(document);


    List<OrbisSchema.OwnershipGraphNode> ownershipGraphNodes = responseElement.getOwnershipStructureResult.nodes.ownershipGraphNodes;
    System.assertEquals(2, ownershipGraphNodes.size());
    System.assertEquals('BVD_01', ownershipGraphNodes.get(0).BvDID);
    System.assertEquals('Name1', ownershipGraphNodes.get(0).Name);
    System.assertEquals(true, ownershipGraphNodes.get(0).IsInWorldCompliance);
    System.assertEquals(197, ownershipGraphNodes.get(0).Distance);
    System.assertEquals(15.0, ownershipGraphNodes.get(0).IntegratedPercentage);
    System.assertEquals(false, ownershipGraphNodes.get(0).IsIntegratedPercentageEstimation);

    List<OrbisSchema.OwnershipGraphEdge> ownershipGraphEdges = responseElement.getOwnershipStructureResult.edges.ownershipGraphEdges;
    System.assertEquals(2, ownershipGraphEdges.size());
    System.assertEquals('BVD_01', ownershipGraphEdges.get(0).ParentBvDID);
    System.assertEquals('BVD_02', ownershipGraphEdges.get(0).ChildBvDID);
    System.assertEquals('Source1', ownershipGraphEdges.get(0).Source);
    System.assertEquals('Date1', ownershipGraphEdges.get(0).Date_x);
    System.assertEquals('DirectPercentage1', ownershipGraphEdges.get(0).DirectPercentage);
    System.assertEquals('TotalPercentage1', ownershipGraphEdges.get(0).TotalPercentage);
  }

  @IsTest
  static void testGetDataRequestSerialization() {
    String expectedXml = '<?xml version="1.0" encoding="UTF-8"?>' +
      '<soap:Envelope ' +
      'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
      'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
      'xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
      '<soap:Body>' +
      '<GetData xmlns="http://bvdep.com/webservices/">' +
      '<sessionHandle>SESSION_ID</sessionHandle>' +
      '<selection>' +
      '<Token>TOKEN</Token>' +
      '<SelectionCount>1</SelectionCount>' +
      '</selection>' +
      '<query>ID</query>' +
      '<fromRecord>2</fromRecord>' +
      '<nrRecords>3</nrRecords>' +
      '<resultFormat>XML</resultFormat>' +
      '</GetData>' +
      '</soap:Body>' +
      '</soap:Envelope>';

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetDataRequestElement requestElement = new OrbisSchema.GetDataRequestElement();
    envelopeElement.body.content = requestElement;

    requestElement.sessionHandle = 'SESSION_ID';
    requestElement.selection.Token = 'TOKEN';
    requestElement.selection.SelectionCount = 1;

    requestElement.query = 'ID';
    requestElement.fromRecord = 2;
    requestElement.nrRecords = 3;

    System.assertEquals(expectedXml,
      envelopeElement.toDOMDocument().toXmlString());
  }

  @IsTest
  static void testGetDataResponseDeserialization() {
    String responseXML = '<?xml version="1.0" encoding="utf-8"?>'
      + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
      + '<soap:Body>'
      + '<GetDataResponse xmlns="http://bvdep.com/webservices/">'
      + '<GetDataResult>&lt;?xml version="1.0" encoding="utf-16"?&gt;'
      + '&lt;queryResults version="1.0" xmlns="http://www.bvdep.com/schemas/RemoteAccessDataResults.xsd"&gt;'
      + '&lt;record id="TEST1_U" dataModelId="IND" selectionId="TEST1"&gt;'
      + '&lt;item field="ID" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;&lt;/item&gt;'
      + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;JOHN DOE 1&lt;/item&gt;'
      + '&lt;item field="ADDR" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR#1&lt;/item&gt;'
      + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR2#1&lt;/item&gt;'
      + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;00-001&lt;/item&gt;'
      + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;CITY#1&lt;/item&gt;'
      + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;COUNTRY#1&lt;/item&gt;'
      + '&lt;/record&gt;'
      + '&lt;record id="TEST2_U" dataModelId="IND" selectionId="TEST2"&gt;'
      + '&lt;item field="ID" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;&lt;/item&gt;'
      + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;JOHN DOE 2&lt;/item&gt;'
      + '&lt;item field="ADDR" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR#2&lt;/item&gt;'
      + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR2#2&lt;/item&gt;'
      + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;00-002&lt;/item&gt;'
      + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;CITY#2&lt;/item&gt;'
      + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;COUNTRY#2&lt;/item&gt;'
      + '&lt;/record&gt;'
      + '&lt;record id="TEST3_U" dataModelId="IND" selectionId="TEST3"&gt;'
      + '&lt;item field="ID" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;&lt;/item&gt;'
      + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;JOHN DOE 3&lt;/item&gt;'
      + '&lt;item field="ADDR" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR#3&lt;/item&gt;'
      + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR2#3&lt;/item&gt;'
      + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;00-003&lt;/item&gt;'
      + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;CITY#3&lt;/item&gt;'
      + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;COUNTRY#3&lt;/item&gt;'
      + '&lt;/record&gt;'
      + '&lt;record id="TEST4_U" dataModelId="IND" selectionId="TEST4"&gt;'
      + '&lt;item field="ID" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;&lt;/item&gt;'
      + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;JOHN DOE 4&lt;/item&gt;'
      + '&lt;item field="ADDR" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR#4&lt;/item&gt;'
      + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" units="0" fieldType="Value"&gt;ADDR2#4&lt;/item&gt;'
      + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;00-004&lt;/item&gt;'
      + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;CITY#4&lt;/item&gt;'
      + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;COUNTRY#4&lt;/item&gt;'
      + '&lt;/record&gt;'
      + '&lt;/queryResults&gt;</GetDataResult>'
      + '</GetDataResponse>'
      + '</soap:Body>'
      + '</soap:Envelope>';
    Dom.Document document = new Dom.Document();
    document.load(responseXML);

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetDataResponseElement responseElement = new OrbisSchema.GetDataResponseElement();
    envelopeElement.body.content = responseElement;
    envelopeElement.deserializeXML(document);

    System.assertNotEquals(null, responseElement.queryResults);
    System.assertNotEquals(null, responseElement.queryResults.records);
    System.assertEquals(4, responseElement.queryResults.records.size());

    Integer i = 1;
    for (OrbisSchema.RecordElement recordElement : responseElement.queryResults.records) {
      Map<String, OrbisSchema.FieldElement> fields = recordElement.getFieldMap();
      System.assertEquals('',fields.get('ID').value);
      System.assertEquals('TEST'+i, recordElement.selectionId);
      System.assertEquals('JOHN DOE '+i,fields.get('NAME').value);
      System.assertEquals('ADDR#'+i,fields.get('ADDR').value);
      System.assertEquals('ADDR2#'+i,fields.get('ADDR2').value);
      System.assertEquals('00-00'+i,fields.get('POSTCODE').value);
      System.assertEquals('CITY#'+i,fields.get('CITY').value);
      System.assertEquals('COUNTRY#'+i,fields.get('COUNTRY').value);
      ++i;
    }
  }

  @IsTest
  static void testFindByBVDIdRequestSerialization() {
    String expectedXml = '<?xml version="1.0" encoding="UTF-8"?>' +
      '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
      'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
      'xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
      '<soap:Body>' +
      '<FindByBVDId xmlns="http://bvdep.com/webservices/">' +
      '<sessionHandle>SESSION_ID</sessionHandle>' +
      '<id>UED0000001</id>' +
      '</FindByBVDId>' +
      '</soap:Body>' +
      '</soap:Envelope>';

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.FindByBVDIdRequestElement requestElement = new OrbisSchema.FindByBVDIdRequestElement();
    envelopeElement.body.content = requestElement;

    requestElement.sessionHandle = 'SESSION_ID';
    requestElement.id = 'UED0000001';


    System.assertEquals(expectedXml,
      envelopeElement.toDOMDocument().toXmlString());
  }

  @IsTest
  static void testFindByBVDIdResponseDeserialization() {
    String responseXML = '<?xml version="1.0" encoding="UTF-8"?>' +
      '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
      'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
      'xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
      '<soap:Body>' +
      '<FindByBVDIdResponse xmlns="http://bvdep.com/webservices/">' +
      '<FindByBVDIdResult>' +
      '<Token>2</Token>' +
      '<SelectionCount>1</SelectionCount>' +
      '</FindByBVDIdResult>' +
      '</FindByBVDIdResponse>' +
      '</soap:Body>' +
      '</soap:Envelope>';
    Dom.Document document = new Dom.Document();
    document.load(responseXML);

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.FindByBVDIdResponseElement responseElement = new OrbisSchema.FindByBVDIdResponseElement();
    envelopeElement.body.content = responseElement;
    envelopeElement.deserializeXML(document);

    System.assertEquals('2', responseElement.FindByBVDIdResult.token);
    System.assertEquals(1, responseElement.FindByBVDIdResult.selectionCount);
  }

  @IsTest
  static void testGetLabelsFromModelRequestSerialization() {
    String expectedXml = '<?xml version="1.0" encoding="UTF-8"?>'
      + '<soap:Envelope' +
      ' xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"' +
      ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
      ' xmlns:xsd="http://www.w3.org/2001/XMLSchema"' +
      '>'
      + '<soap:Body>'
      + '<GetLabelsFromModel xmlns="http://bvdep.com/webservices/">'
      + '<sessionHandle>SESSION_ID</sessionHandle>'
      + '<fieldDefs>'
      + '<FieldDefinition>'
      + '<FieldCode>NAME</FieldCode>'
      + '<PresentationLineId>string</PresentationLineId>'
      + '<ModelId>UNIVERSAL</ModelId>'
      + '</FieldDefinition>'
      + '</fieldDefs>'
      + '<fullLabels>true</fullLabels>'
      + '</GetLabelsFromModel>'
      + '</soap:Body>'
      + '</soap:Envelope>';

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetLabelsFromModelRequestElement requestElement = new OrbisSchema.GetLabelsFromModelRequestElement();
    envelopeElement.body.content = requestElement;

    requestElement.sessionHandle = 'SESSION_ID';
    requestElement.fullLabels = true;

    OrbisSchema.GetLabelsFromModelFieldDefinitionElement fieldDef = new OrbisSchema.GetLabelsFromModelFieldDefinitionElement();
    fieldDef.FieldCode = 'NAME';
    fieldDef.PresentationLineId = 'string';
    fieldDef.ModelId = 'UNIVERSAL';

    requestElement.fieldDefs.fieldDefs.add(fieldDef);


    System.assertEquals(expectedXml,
      envelopeElement.toDOMDocument().toXmlString());
  }

  @IsTest
  static void testGetLabelsFromModelResponseDeserialization() {
    String responseXML = '<?xml version="1.0" encoding="utf-8"?>'
      + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
      + '<soap:Body>'
      + '<GetLabelsFromModelResponse xmlns="http://bvdep.com/webservices/">'
      + '<GetLabelsFromModelResult>'
      + '<string>Company name</string>'
      + '<string>Addition of company in database</string>'
      + '</GetLabelsFromModelResult>'
      + '</GetLabelsFromModelResponse>'
      + '</soap:Body>'
      + '</soap:Envelope>';

    Dom.Document document = new Dom.Document();
    document.load(responseXML);

    OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
    OrbisSchema.GetLabelsFromModelResponseElement responseElement = new OrbisSchema.GetLabelsFromModelResponseElement();
    envelopeElement.body.content = responseElement;
    envelopeElement.deserializeXML(document);

    System.assertEquals('Company name', responseElement.result.results.get(0));
    System.assertEquals('Addition of company in database', responseElement.result.results.get(1));
  }
}