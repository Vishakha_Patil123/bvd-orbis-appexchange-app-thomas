public with sharing class FieldMappingController {

    @AuraEnabled
    public static Orbis_Field__c[] getOrbisFields(String objectType){
        Logger.start('FieldMappingController', 'getOrbisFields');
        try {
            return [SELECT Id,Name,Regular_Name__c,SObject__c,Type__c
            FROM Orbis_Field__c
            WHERE SObject__c =:objectType];
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Orbis_Field__c[] getOrbisMappedFields(String selectedObject){
        Logger.start('FieldMappingController', 'getOrbisMappedFields');
        try {

            return [SELECT Id,
                    Name,
                    Regular_Name__c,
                    Type__c,
                    SObject__c
            FROM Orbis_Field__c
            WHERE SObject__c =:selectedObject
            ];
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Orbis_Field__c[] insertOrbisFields(Orbis_Field__c[] orbisFields){
        Logger.start('FieldMappingController', 'insertOrbisFields');
        try {
            if(BVDUserPermissionCheck.permissionToCreateSobject('Orbis_Field__c', TRUE, new List<String>{'Name', 'Regular_Name__c', 'SObject__c', 'Type__c'})) {
                insert orbisFields;
            }else {
                Logger.error('Not enough privileges to create Orbis Fields');
                throw new AuraHandledException('Not enough privileges to create Orbis Fields');
            }
            return orbisFields;

        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static List<Field_Matrix__mdt> getMatrixLine(String sfFieldType){
        Logger.start('FieldMappingController', 'getMatrixLine');
        try {
            List<Field_Matrix__mdt> fieldCrossections = [
                    SELECT ID,
                            SF_Field_Type__c,
                            BvD_Field_Type__c,
                            Availability__c,
                            Warning__c
                    FROM Field_Matrix__mdt
                    WHERE SF_Field_Type__c =:sfFieldType
            ];
            return fieldCrossections;
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }


    }

    @AuraEnabled
    public static FieldMatrix.CrossSection getCrossSection(String sObjectType, String sfField, String bvdType){
        Logger.start('FieldMappingController', 'getCrossSection');
        try {
            return FieldMatrix.getCrossSection(sObjectType,sfField,bvdType);
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Map<String, Field_Mapping__c[]> unExcludeFieldMapping(String sObjectName, List<Field_Mapping__c> fieldMappings) {
        Logger.start('FieldMappingController', 'unExcludeFieldMapping');

        Bvd_Setup__c setup = AdministrationController.getBvDSetup();

        if(!setup.SSO_Confirmed__c) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
        }

        if(String.isBlank(setup.Datasource_Id__c)) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
        }

        try {
            Set<String> fieldsToDelete = new Set<String> {};

            Field_Mapping__c[] fields = new Field_Mapping__c[] {};

            for (Field_Mapping__c field : fieldMappings) {
                System.debug(field.Name__c);
                fieldsToDelete.add(field.Name__c);
            }

            if(BVDUserPermissionCheck.permissionToDeleteSobject('Field_Mapping__c', TRUE)) {
                delete [ SELECT Id, Name__c
                         FROM Field_Mapping__c
                         WHERE Name__c IN :fieldsToDelete ];
            }
            else{
                Logger.error('Not enough privileges to delete Field Mappings');
                throw new AuraHandledException('Not enough privileges to delete Field Mappings');
             }

            return FieldMappingController.getMapping(sObjectName);
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Map<String, Field_Mapping__c[]> getMapping(String sObjectName) {
        SObject[] mapped                   = new SObject[] {};
        SObject[] excluded                 = new SObject[] {};
        SObject[] available                = new SObject[] {};
        SObject[] restricted               = new SObject[] {};
        Map<String, SObject> fieldMapping  = new Map<String, SObject> {};
        Map<String, String> fieldsAndTypes = AdministrationController.getFieldsNameAndTypes(sObjectName);
      Map<String, SObjectField> describeFields = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap();

        for (Field_Mapping__c mapping: (Field_Mapping__c[]) CommonUtilities.queryAllFieldsWithCondition(
                'Field_Mapping__c',
                //'WHERE SObject__c = :sObjectName'
                'WHERE SObject__c = \'' + sObjectName + '\''
        )) {
            String type  = mapping.Type__c;
            String field = mapping.Sf_Field__c;
            if(!AdministrationController.isExcluded(sObjectName, type, field) && describeFields.get(field).getDescribe().isUpdateable()) {
                Boolean excluding = mapping.Excluded__c;

                System.debug('Excluded ?');
                System.debug(excluding);

                if(excluding) {
                    excluded.add(mapping);
                } else {
                    String actualType = fieldsAndTypes.get(mapping.SF_Field__c);
                    if(type != actualType){
                        mapping.Type__c = actualType;
                        if(BVDUserPermissionCheck.permissionToUpdateSobject('Field_Mapping__c', TRUE, new List<String> {'Type__c', 'SF_Field__c', 'Excluded__c'})) {
                            update mapping;
                        } else {
                            Logger.error('Not enough privileges to update Field Mapping');
                            throw new AuraHandledException('Not enough privileges to update Field Mapping');
                        }
                    }
                    mapping.Type_Info__c = getLengthByType(mapping.SObject__c,mapping.Type__c, mapping.SF_Field__c);
                    mapping.Invalid__c = !FieldMatrix.isMapable(mapping.SObject__c,mapping.SF_Field__c,mapping.BvD_Field_Type__c);
                    mapped.add(mapping);
                }
                fieldMapping.put(field, mapping);
            } else {
                if(BVDUserPermissionCheck.permissionToDeleteSobject('Field_Mapping__c', TRUE)) {
                    delete mapping;
                }else {
                    Logger.error('Not enough privileges to delete Field Mapping');
                    throw new AuraHandledException('Not enough privileges to delete Field Mapping');
                }
            }
        }

        for (String field : fieldsAndTypes.keySet()) {
            String type = fieldsAndTypes.get(field);

            if(!AdministrationController.isExcluded(sObjectName, type, field) && !fieldMapping.containsKey(field)) {
                Field_Mapping__c mapping = new Field_Mapping__c(
                        Name__c           = String.format('{0}.{1}', new String[] { sObjectName, field }),
                        Action__c         = '',
                        BvD_Field__c   	  = '',
                        BvD_Field_Type__c = '',
                        Excluded__c       = false,
                        Master__c         = '',
                        Reason__c         = '',
                        SF_Field__c       = field,
                        SF_Field_Label__c = Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field).getDescribe()
                                .getLabel(),
                        SObject__c        = sObjectName,
                        Type__c           = type,
                        Type_Info__c      = getLengthByType(sObjectName, type, field)
                );

              if(describeFields.get(field).getDescribe().isUpdateable()){
                available.add(mapping);
              } else {
                restricted.add(mapping);
              }

              fieldMapping.put(field, mapping);
            }
        }
        System.debug(DateTime.now());

        return new Map<String, SObject[]> {
                'mapped'    => mapped,
                'excluded'  => excluded,
                'available' => available,
                'restricted' => restricted
        };
    }

    @AuraEnabled
    public static AdministrationController.Field[] getOrbisAvailableFields() {
        Logger.start('FieldMappingController', 'getOrbisAvailableFields');
        OrbisAPI orbisAPI         = new OrbisAPI();
        OrbisRequest openRequest  = new OrbisOpenRequest()
                .setUsername('{!HTMLENCODE($Credential.Username)}')
                .setPassword('{!HTMLENCODE($Credential.Password)}')
                .setEndpoint('callout:Orbis');

        OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());

        OrbisRequest request = new OrbisGetAvailableFieldsRequest()
                .setModelID('UNIVERSAL')
                .setSessionID(openResponse.getSessionID());


        try {
            OrbisGetAvailableFieldsResponse response = (OrbisGetAvailableFieldsResponse) orbisAPI.send(request);

            Logger.info(openRequest.toHttpRequest());
            Logger.info(request.toHttpRequest());

            return AdministrationController.toFieldsArray(response.getAvailableFields());
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.flush();
        }
    }

    @AuraEnabled
    public static List<OrbisFields__c> updateGetLabelsFromModels() {
        Logger.start('FieldMappingController', 'getLabelsFromModels');

        try {
            List<OrbisFields__c> oldOrbisFields = [SELECT Id FROM OrbisFields__c];
            AdministrationController.Field[] orbisFields = getOrbisAvailableFields();
            OrbisAPI orbisAPI = new OrbisAPI();
            OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());
            OrbisGetLabelsFromModelRequest orbisRequest = new OrbisGetLabelsFromModelRequest()
                    .setSessionID(openResponse.getSessionID())
                    .setFullLabels(true);
            for (AdministrationController.Field field : orbisFields) {
                orbisRequest.addField(field.code,'UNIVERSAL');
            }
            OrbisRequest request = orbisRequest;
            OrbisGetLabelsFromModelResponse response = (OrbisGetLabelsFromModelResponse) orbisAPI.send(request);
            List<String> labels = response.getLabels();
            if(BVDUserPermissionCheck.permissionToDeleteSobject('OrbisFields__c', TRUE)) {
              delete oldOrbisFields;
            } else {
              Logger.error('Not enough privileges to delete Orbis Fields');
              throw new AuraHandledException('Not enough privileges to delete Orbis Fields');
            }
            System.debug(labels.size());
            System.debug(orbisFields.size());
            for(Integer i = 0;i<labels.size();i++){
                orbisFields.get(i).name = labels.get(i);
            }
            List<OrbisFields__c> newOrbisFields = new List<OrbisFields__c>();
            for(AdministrationController.Field field : orbisFields){
                OrbisFields__c newOrbisField = new OrbisFields__c();
                newOrbisField.Label__c = field.name;
                newOrbisField.Type__c = field.type;
                newOrbisField.Name = field.code;
                newOrbisFields.add(newOrbisField);
            }

            if(BVDUserPermissionCheck.permissionToCreateSobject('OrbisFields__c', TRUE, new List<String> {'Label__c', 'Type__c', 'Name'})) {
                insert newOrbisFields;
            }else {
                Logger.error('Not enough privileges to create Orbis Fields');
                throw new AuraHandledException('Not enough privileges to create Orbis Fields record');
            }

            return newOrbisFields;

        } catch (Exception ex) {
            Logger.error(ex);
            System.debug(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static List<OrbisFields__c> getLabelsFromModels(String objectType) {
        Logger.start('FieldMappingController', 'getLabelsFromModels');

        try {
            List<OrbisFields__c> orbisFields = new List<OrbisFields__c>();
                    if(objectType.equalsIgnoreCase('Contact')){
                orbisFields.addAll([SELECT Id,
                        Name,
                        Label__c,
                        Type__c
                FROM OrbisFields__c
                WHERE Name
                LIKE 'CPYCONTACTS_%'
                ]);


            } else {
                        orbisFields.addAll([SELECT Id,
                        Name,
                        Label__c,
                        Type__c
                FROM OrbisFields__c]);
                return orbisFields;
            }
            return orbisFields;


        } catch (Exception ex) {
            Logger.error(ex);
            System.debug(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Map<String, Field_Mapping__c[]> getAccountMapping() {
        Logger.start('AdministrationController', 'getAccountMapping');
        System.debug(DateTime.now());
        try {
            return getMapping('Account');

        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            System.debug(DateTime.now());
            Logger.stop();
            System.debug(DateTime.now());

        }
    }

    @AuraEnabled
    public static Map<String, Field_Mapping__c[]> getLeadMapping() {
        Logger.start('AdministrationController', 'getLeadMapping');
        try {
            return getMapping('Lead');
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Map<String, Field_Mapping__c[]> getContactMapping() {
        Logger.start('AdministrationController', 'getContactMapping');
        try {
            return getMapping('Contact');
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    private static String getLengthByType(String sObjectName,String type,String field){
        if(type.toUpperCase() == 'DOUBLE'){
            return String.valueOf(Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field).getDescribe()
                    .getPrecision() - Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field)
                    .getDescribe().getScale()+','+Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field)
                    .getDescribe().getScale());
        } else {
            return  String.valueOf(Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().get(field).getDescribe()
                    .getLength());
        }
    }

}