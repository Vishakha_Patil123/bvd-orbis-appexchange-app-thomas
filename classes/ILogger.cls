/**
* @author	 		Hubert Jaskolski
* @date		 		18/03/2014
* @description 		Class is an interface used to define methods in Error Logging Framework.
* @see 				Logger.cls
*/
public interface ILogger {
	/**
	* @description 	Adds Info message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    void info(Object innerObject);
	/**
	* @description 	Adds Debug message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    void debug(Object innerObject);
	/**
	* @description 	Adds Warn message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    
    void warn(Object innerObject);
	/**
	* @description 	Adds Error message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    void error(Object innerObject);
	/**
	* @description 	Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name to empty for any following log entries.
	*/
    void start();
	/**
	* @description 	Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name for any following log entries.
	* @param		String className <p> Sets the ClassName__C field for Error_Log__c object
	* @param		String methodName <p> Sets the MethodName__C field for Error_Log__c object
	*/
    void start(String className, String methodName);
	/**
	* @description 	Stops the logging buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	*/
    void stop();
	/**
	* @description 	Flush the logging buffer and writes everything to file / DB, if start() and stop() functions calls difference equals zero.
	*/
    void flush();
}