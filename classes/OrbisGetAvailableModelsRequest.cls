public with sharing class OrbisGetAvailableModelsRequest extends OrbisRequest {
    private OrbisSchema.GetAvailableModelsElement requestSchema
            = new OrbisSchema.GetAvailableModelsElement();

    public override OrbisResponse getOrbisResponse(HttpResponse response) {
        return new OrbisGetAvailableModelsResponse(response);
    }

    public OrbisGetAvailableModelsRequest setSessionID(String sessionID) {
        requestSchema.sessionHandle = sessionID;
        return this;
    }

    protected override String getSOAPAction() {
        return 'http://bvdep.com/webservices/GetAvailableModels';
    }

    protected override OrbisSchema.RequestElement getRequestSchema() {
        return requestSchema;
    }
}