/**
* @author
* @date
* @description      Helper class used to generate a random GuID (Group ID) for the single transaction
*/
global class GuidGenerator {
    
    global static String generate() {
        Blob generatedBlob = Crypto.GenerateAESKey(128);
        String hex = EncodingUtil.ConvertTohex(generatedBlob);
        String guid = hex.substring(0, 8)
            + '-' + hex.substring(8, 12)
            + '-' + hex.substring(12, 16)
            + '-' + hex.substring(16, 20)
            + '-' + hex.substring(20);

        return guid.toUpperCase();
    }
    /**
    * @description  Possible hex characters
    */
    private static String kHexChars = '0123456789abcdef';
    /**
    * @description  Generates new random GUId
    */
    global static String NewGuid() {
        String returnValue = '';
        Integer nextByte = 0;
        for (Integer i=0; i<16; i++) {
            if (i==4 || i==6 || i==8 || i==10)
                returnValue += '-';
            nextByte = (Math.round(Math.random() * 255)-128) & 255;
            if (i==6) {
                nextByte = nextByte & 15;
                nextByte = nextByte | (4 << 4);
            }
            if (i==8) {
                nextByte = nextByte & 63;
                nextByte = nextByte | 128;
            }
            returnValue += getCharAtIndex(kHexChars, nextByte >> 4);
            returnValue += getCharAtIndex(kHexChars, nextByte & 15);
        }
        return returnValue;
    }
    /**
    * @description  Returns the String (hex) character for choosen index
    * @param        String str <p> String to get the character from (hex)
    * @param        Integer index <p> Char Index
    */
    global static String getCharAtIndex(String str, Integer index) {
        if (str == null) return null;
        if (str.length() <= 0) return str;
        if (index == str.length()) return null;
        return str.substring(index, index+1);
    }
    
}