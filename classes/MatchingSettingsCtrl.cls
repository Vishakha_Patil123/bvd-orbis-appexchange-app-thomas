/**
* Controller for Matching Setting component
* Handles retrieve and save of matching custom settings
* */
public with sharing class MatchingSettingsCtrl {

  /**
  * Saves settings data in Custom Settings
  * */
  @AuraEnabled
  public static void saveSettings(String settingsJSON) {
    try {
      Logger.start('MatchingSettingsCtrl', 'saveSettings');
      Settings setting = (Settings) JSON.deserialize(settingsJSON, Settings.class);
      setting.save();
      Logger.stop();
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }

  /**
  * Retrieves custom settings data and wraps in Settings wrapper
  * */
  @AuraEnabled
  public static Settings getSettings() {
    try {
      Logger.start('MatchingSettingsCtrl', 'getSettings');
      return new Settings();
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.debug(ex.getStackTraceString());
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }


  /**
  * Wrapper for Custom Settings data
  * */
  public class Settings {
    @AuraEnabled public Map<String, List<SFField>> fieldsByObject;
    @AuraEnabled public Map<String, Map<String, String>> fieldMappingsByObject;
    @AuraEnabled public Map<String, List<String>> columnsByObject;

    public Settings() {
      this.fieldsByObject = getFields();
      this.fieldMappingsByObject = getMappings();
      this.columnsByObject = getColumns();
    }

    /**
    * Saves data in Custom Settings
    * */
    public void save() {
      saveFieldMappings();
      saveColumns();
    }


    /**
    * Retrieves readable fields of Account, Contact and Lead
    * */
    private Map<String, List<SFField>> getFields() {
      return new Map<String, List<SFField>>{
        'Account' => getAllReadableFields('Account'),
        'Contact' => getAllReadableFields('Contact'),
        'Lead' => getAllReadableFields('Lead')
      };
    }

    private List<SFField> getAllReadableFields(String sObjectType) {
      SObjectDescribe sObjectResult = SObjectDescribe.getDescribe(sObjectType);
      List<SFField> sfFields = new List<MatchingSettingsCtrl.SFField>();

      for (SObjectField field : sObjectResult.getFields().values()) {
        DescribeFieldResult fieldDescribe = field.getDescribe();

        if (FieldMappingWhitelist.contains(sObjectResult, fieldDescribe)) {
          sfFields.add(new SFField(fieldDescribe));
        }
      }

      sfFields.sort();
      return sfFields;
    }

    /**
    * Retrieves exising field mappings
    * */
    private Map<String, Map<String, String>> getMappings() {
      Map<String, Map<String, String>> result = new Map<String, Map<String, String>>{
        'Account' => new Map<String, String>(),
        'Contact' => new Map<String, String>(),
        'Lead' => new Map<String, String>()
      };

      for (Matching_Mapping__c setting : Matching_Mapping__c.getAll().values()) {
        result.get(setting.Object__c).put(setting.BvD_Field__c, setting.SF_Field__c);
      }

      return result;
    }

    /**
    * Retrieves existing column selection
    * */
    private Map<String, List<String>> getColumns() {
      Matching_Columns__c[] settings = [
        SELECT Object__c, Column__c, Order__c
        FROM Matching_Columns__c
        ORDER BY Object__c, Order__c
        LIMIT 50000
      ];

      Map<String, List<String>> result = new Map<String, List<String>>{
        'Account' => new List<String>(),
        'Contact' => new List<String>(),
        'Lead' => new List<String>()
      };

      for (Matching_Columns__c col : settings) {
        result.get(col.Object__c).add(col.Column__c);
      }
      return result;
    }

    /**
    * Deletes old Field Mappings and saves new
    * */
    private void saveFieldMappings() {
      delete Matching_Mapping__c.getAll().values();

      List<Matching_Mapping__c> toInsert = new List<Matching_Mapping__c>();
      for (String sObjectType : fieldMappingsByObject.keySet()) {
        Map<String, String> mappings = fieldMappingsByObject.get(sObjectType);

        for (String bvdField : mappings.keySet()) {
          String sfField = mappings.get(bvdField);

          if (String.isNotBlank(sfField)) {
            toInsert.add(new Matching_Mapping__c(
              Name = sObjectType + '.' + bvdField,
              BvD_Field__c = bvdField,
              SF_Field__c = sfField,
              Object__c = sObjectType
            ));
          }
        }
      }
      insert toInsert;
    }

    /**
    * Deletes old Column selection and saves new
    * */
    private void saveColumns() {
      delete Matching_Columns__c.getAll().values();

      List<Matching_Columns__c> toInsert = new List<Matching_Columns__c>();
      for (String sObjectType : columnsByObject.keySet()) {
        List<String> columns = columnsByObject.get(sObjectType);

        for (Integer i = 0; i < columns.size(); i++) {
          String column = columns[i];

          toInsert.add(new Matching_Columns__c(
            Name = sObjectType + '.' + column,
            Object__c = sObjectType,
            Column__c = column,
            Order__c = i
          ));
        }
      }
      insert toInsert;
    }
  }

  public class SFField implements Comparable {
    @AuraEnabled public String name;
    @AuraEnabled public String label;

    public SFField(DescribeFieldResult fieldDescribe) {
      this.name = fieldDescribe.getName();
      this.label = fieldDescribe.getLabel();
    }

    public Integer compareTo(Object compareTo) {
      SFField compareToField = (SFField) compareTo;
      return label.compareTo(compareToField.label);
    }
  }
}