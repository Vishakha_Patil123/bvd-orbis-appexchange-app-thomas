public with sharing class OrbisGetDataRequest extends OrbisRequest {
    private OrbisSchema.GetDataRequestElement requestSchema = new OrbisSchema.GetDataRequestElement();

    public override OrbisResponse getOrbisResponse(HttpResponse response) {
        return new OrbisGetDataResponse(response);
    }
    protected override String getSOAPAction() {
        return 'http://bvdep.com/webservices/GetData';
    }
    protected override OrbisSchema.RequestElement getRequestSchema() {
        return this.requestSchema;
    }

    public OrbisGetDataRequest setSessionID(String value) {
        requestSchema.sessionHandle = value;
        return this;
    }

    public OrbisGetDataRequest setQuery(String value) {
        requestSchema.query = value;
        return this;
    }

    public OrbisGetDataRequest setFromRecord(Integer value) {
        requestSchema.fromRecord = value;
        return this;
    }

    public OrbisGetDataRequest setNrRecords(Integer value) {
        requestSchema.nrRecords = value;
        return this;
    }


    public OrbisGetDataRequest setSelection(String token, Integer selectionCount) {
        requestSchema.selection.token = token;
        requestSchema.selection.selectionCount = selectionCount;
        return this;
    }
}