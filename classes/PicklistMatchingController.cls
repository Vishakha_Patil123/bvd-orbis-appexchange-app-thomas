global class PicklistMatchingController extends VisualEditor.DynamicPickList {
    global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('Select field', null);
        return defaultValue;
    }

    global override VisualEditor.DynamicPickListRows getValues() {
        VisualEditor.DynamicPickListRows  myValues = new VisualEditor.DynamicPickListRows();

        //        todo: Schema.getGlobalDescribe().get('Account').getDescribe().fields.getMap().values() as function in "CommonUtility file"
        VisualEditor.DataRow value4 = new VisualEditor.DataRow('Country', 'Matching_Companies_Country_Column,Country');
        myValues.addRow(value4);
        VisualEditor.DataRow value5 = new VisualEditor.DataRow('City', 'Matching_Companies_City_Column,City');
        myValues.addRow(value5);
        VisualEditor.DataRow value6 = new VisualEditor.DataRow('Postal Code', 'Matching_Companies_Post_Code_Column,PostCode');
        myValues.addRow(value6);
        VisualEditor.DataRow value7 = new VisualEditor.DataRow('NationalId', 'Matching_Companies_National_ID_Column,NationalId');
        myValues.addRow(value7);
        VisualEditor.DataRow value8 = new VisualEditor.DataRow('Email or Website', 'Matching_Companies_Email_Column,EMailOrWebsite');
        myValues.addRow(value8);
        return myValues;
    }
}