public with sharing class MatchingGeneralSettings {
  private final static String SETTING_NAME = 'BvD - Matching General';
  private final static Matching_General__c SETTING;

  static {
    SETTING = Matching_General__c.getInstance(SETTING_NAME);

    if (SETTING == null) {
      SETTING = new Matching_General__c(
        Name = SETTING_NAME,
        Percentage__c = false
      );
    }
  }

  public static Matching_General__c getSetting() {
    return SETTING;
  }
}