/**
* This async. job updates Orbis tables with Salesforce Ids
* */
public class AsyncOrbisUpdater implements Queueable, Database.AllowsCallouts {
  private Map<String, String> bvdIds;
  private String sObjectType;
  private OrbisAPI orbisAPI;
  private String sessionId;

  public AsyncOrbisUpdater(String sObjectType, Map<String, String> bvdIds) {
    this.bvdIds = bvdIds;
    this.sObjectType = sObjectType;
    this.orbisAPI = new OrbisAPI();
  }

  public void execute(QueueableContext context) {
    try {
      openOrbisSession();
      updateOrbisTable();
    } catch (Exception ex) {
      Logger.error(ex);
    }
  }

/**
* Open Session with Orbis Webservice
* */
  private void openOrbisSession() {
    OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());
    this.sessionId = openResponse.getSessionID();
  }

  private void updateOrbisTable() {
    OrbisUpdateMyDataTableRequest request = new OrbisUpdateMyDataTableRequest()
      .setAppendData(true)
      .setTable('CFE_CRMLinks')
      .setSessionID(this.sessionId);

    for (String bvdId : bvdIds.keySet()) {
      String sfId = bvdIds.get(bvdId);

      request.updateRecord(bvdId, new Map<String, String>{
        'CF_CRMId' => sfId,
        'CF_CRMTable' => this.sObjectType
      });
    }

    OrbisUpdateMyDataTableResponse response = (OrbisUpdateMyDataTableResponse) orbisAPI.send(request);
  }
}