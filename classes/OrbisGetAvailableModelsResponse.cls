public with sharing class OrbisGetAvailableModelsResponse extends OrbisResponse {
    private OrbisSchema.GetAvailableModelsResponseElement responseSchema
            = new OrbisSchema.GetAvailableModelsResponseElement();
    private List<Model> models = new List<Model>();

    public OrbisGetAvailableModelsResponse(HttpResponse response) {
        super(response);
        readEscapedResponse();
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseSchema;
    }

    public List<Model> getAvailableModels() {
        return models;
    }

    public class Model {
        public String id;
        public String name;
        public String parentModelId;
    }

    private void readEscapedResponse() {
        String modelsSchema = responseSchema.GetAvailableModelsResult;

        if (String.isNotEmpty(modelsSchema)) {
            Dom.Document document = new Dom.Document();
            document.load(modelsSchema.unescapeXml());

            for (Dom.XmlNode modelNode : document.getRootElement().getChildElements()) {
                Model model = new Model();
                model.id = modelNode.getAttribute('id', null);
                model.name = modelNode.getAttribute('name', null);
                model.parentModelId = modelNode.getAttribute('parentModelId', null);

                models.add(model);
            }
        }
    }
}