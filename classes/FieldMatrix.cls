public with sharing class FieldMatrix {
  public final static String
    STATUS_SUCCESS = 'Success',
    STATUS_WARNING = 'Warning',
    STATUS_ERROR = 'Error',
    STATUS_NOT_AVAILABLE = 'Not Available',
    WARNING_TRUNCATION = 'Value may be truncated',
    WARNING_PICKLIST = 'Picklist may have restricted values',
    WARNING_BOOLEAN = 'Only "0" and "1" values are allowed';
  private final static Map<String, CrossSection> MATRIX;

  /**
  * Initializes matrix from custom metadata
  * */
  static {
    MATRIX = new Map<String, CrossSection>();
    List<Field_Matrix__mdt> fieldCrosssections = [
      SELECT ID,
        SF_Field_Type__c,
        BvD_Field_Type__c,
        Availability__c,
        Warning__c
      FROM Field_Matrix__mdt
      LIMIT 50000
    ];

    for (Field_Matrix__mdt fm : fieldCrosssections) {
      String key = fm.SF_Field_Type__c + fm.BvD_Field_Type__c;
      MATRIX.put(key, new CrossSection(fm));
    }
  }

  /**
  * Wrapper for Field Matrix record
  * */
  public class CrossSection {
    @AuraEnabled public String availability;
    @AuraEnabled public String warning;

    public CrossSection(String availability, String warning) {
      this.availability = availability;
      this.warning = warning;
    }

    public CrossSection(Field_Matrix__mdt fmMD) {
      this(fmMD.Availability__c, fmMD.Warning__c);
    }

    public Boolean isAvailable() {
      return this.availability == STATUS_SUCCESS
        || this.availability == STATUS_WARNING;
    }
  }

  /**
  * Returns Field Matrix crosssection for given Salesforce field (field and object required)
  * and BvD Field Type
  * @param sObjectType - API Name of the sObject field belong to - ex. Account, Contact, Lead
  * @param sfField - API name of the Salesforce field - ex. 'Name', 'BillingCity'.
  * @param bvdType - Type of the BvD Field - ex. 'String', 'Date', 'Number' etc.
  * */
  public static CrossSection getCrossSection(String sObjectType, String sfField, String bvdType) {
    SObjectDescribe sObjDescribe = SObjectDescribe.getDescribe(sObjectType);
    if (sObjDescribe == null) return NotAvailable(Label.BvD_FieldMatrix_Warning_SObjectNotAvailable);

    SObjectField fielDescribe = sObjDescribe.getField(sfField);
    if (fielDescribe == null) return NotAvailable(Label.BvD_FieldMatrix_Warning_SFFieldNotAvailable);

    DescribeFieldResult field = fielDescribe.getDescribe();
    if (!field.isAccessible()) return NotAvailable(Label.BvD_FieldMatrix_Warning_SFFieldNotAccessible);
    if (field.isAutoNumber()) return NotAvailable(Label.BvD_FieldMatrix_Warning_SFFieldAutonumber);
    if (field.isCalculated()) return NotAvailable(Label.BvD_FieldMatrix_Warning_SFFieldFormula);

    String fieldKey = field.getType().name() + bvdType;
    if (!MATRIX.containsKey(fieldKey)) {
      return NotAvailable(Label.BvD_FieldMatrix_Warning_MappingNotAvailable);
    } else {
      return MATRIX.get(fieldKey);
    }
  }

  /**
  * @return true if bvd field type is mappable to sf field type
  * */
  public static Boolean isMapable(String sObjectType, String sfField, String bvdType) {
    FieldMatrix.CrossSection crossSection = FieldMatrix.getCrossSection(sObjectType, sfField, bvdType);
    return crossSection.isAvailable();
  }

  private static CrossSection NotAvailable(String warning) {
    return new CrossSection(STATUS_NOT_AVAILABLE, warning);
  }
}