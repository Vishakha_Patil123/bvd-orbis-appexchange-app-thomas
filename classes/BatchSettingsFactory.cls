/**
* Factory class for Batch Setting
* Caches all Custom Settings records to minimize number of SOQL Queries
* and returns instance for given sObject.
* */
public with sharing class BatchSettingsFactory {
  private static Map<String, Batch_Settings__c> settingPerObject;

  static {
    BatchSettingsFactory.settingPerObject = new Map<String, Batch_Settings__c>();
    List<Batch_Settings__c> settings = [
      SELECT ID,
        Object__c,
        Job_ID__c,
        Disabled__c,
        Start_Date__c,
        Start_Hour__c,
        End_Date__c,
        Batch_Size__c,
        Frequency__c,
        Day__c

      FROM Batch_Settings__c
    ];
    for (Batch_Settings__c setting : settings) {
      settingPerObject.put(setting.Object__c, setting);
    }
  }

  /**
  * Returns BatchSettings instance for given sObject Type
  * */
  public static BatchSettings getSetting(SObjectType sObjType) {
    return getSetting(String.valueOf(sObjType));
  }
  public static BatchSettings getSetting(String sObjType) {
    if (settingPerObject.containsKey(sObjType)) {
      return new BatchSettings(settingPerObject.get(sObjType));
    } else {
      return new BatchSettings(createDefault(sObjType));
    }
  }

  /**
  * Return default mock setting for sObjectType
  * */
  private static Batch_Settings__c createDefault(String sObjType) {
    Batch_Settings__c newSetting = new Batch_Settings__c(
      Name = sObjType + ' Batch Setting',
      Object__c = sObjType,
      Job_ID__c = null,
      Disabled__c = true,
      Start_Date__c = Date.today(),
      Start_Hour__c = 0,
      End_Date__c = null,
      Batch_Size__c = 50,
      Frequency__c = 'daily',
      Day__c = 'Mon'
    );
    settingPerObject.put(sObjType, newSetting);
	if(BVDUserPermissionCheck.permissionToCreateSobject('Batch_Settings__c', true,
                                                        new List<String>{'Name', 'Object__c', 'Job_ID__c', 'Disabled__c',
                                                                         'Start_Date__c', 'Start_Hour__c', 'End_Date__c',
                                                                         'Batch_Size__c', 'Frequency__c', 'Day__c'})){
      insert newSetting;
    }else{
      Logger.error('Not enough privileges to create Field mapping');
      throw new AuraHandledException('Not enough privileges to create Field mapping');
    }
    return newSetting;
  }
}