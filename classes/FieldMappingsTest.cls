@IsTest
public class FieldMappingsTest {

  private static List<String>
    ACCOUNT_FIELDS = new List<String>{
      'Id',
      'Name',
      'BvD_Id__c',
      'ShippingState',
      'ShippingCity',
      'ShippingCountry',
      'ShippingPostalCode',
      'Phone',
      'Website',
      'Industry'
    },
    BVD_FIELDS = new List<String>{
      'NAME',
      'BVD_ID_NUMBER',
      'BVD_FIELD_0',
      'BVD_FIELD_1',
      'BVD_FIELD_2',
      'BVD_FIELD_3',
      'BVD_FIELD_4',
      'BVD_FIELD_5',
      'BVD_FIELD_6',
      'BVD_FIELD_7'
    };

  @TestSetup
  public static void setup() {
    Map<String, SObjectField> accountFieldsMap = Schema.getGlobalDescribe().get('Account')
      .getDescribe()
      .fields
      .getMap();

    List<SObjectField> fieldsToMap = new List<SObjectField>{
      Account.Name,
      Account.ShippingState,
      Account.ShippingCity,
      Account.ShippingCountry,
      Account.ShippingPostalCode,
      Account.Phone,
      Account.Website,
      Account.Industry
    };

    Integer i = 0;
    List<Field_Mapping__c> lFieldMappings = new List<Field_Mapping__c>();
    for (SObjectField field : fieldsToMap) {
      lFieldMappings.add(
        new Field_Mapping__c(
          SObject__c = 'Account',
          SF_Field__c = field.getDescribe().getName(),
          BvD_Field__c = 'BVD_FIELD_' + (i++),
          Master__c = Math.mod(i, 2) == 0 ? FieldMappings.MASTER_SF : FieldMappings.MASTER_BVD,
          Action__c = Math.mod(i, 2) == 0 ? FieldMappings.ACTION_TYPE_AUTOMATIC : FieldMappings.ACTION_TYPE_MANUAL
        )
      );
    }

    insert lFieldMappings;

    System.assertEquals(
      fieldsToMap.size(),
      lFieldMappings.size()
    );
  }


  /**
  * Test if Field Mapping service returns all Salesforce fields
  * */
  @IsTest
  static void testGetSalesforceFields() {
    FieldMappings mappings = FieldMappings.getInstance('Account');
    List<String> fields = new List<String>(mappings.getSalesforceFields());

    System.assertEquals(ACCOUNT_FIELDS, fields);
  }

  /**
  * Test if Field Mapping service returns all BvD Fields
  * */
  @IsTest
  static void testGetBVDFields() {
    FieldMappings mappings = FieldMappings.getInstance('Account');
    List<String> fields = new List<String>(mappings.getBvDFields());

    System.assertEquals(BVD_FIELDS, fields);
  }

  /**
  * Test if Field Mapping service returns all Salesforce fields
  *  as comma separated string
  * */
  @IsTest
  static void testGetSalesforceFieldsCommaSeparated() {
    FieldMappings mappings = FieldMappings.getInstance('Account');
    System.assertEquals(
      String.join(ACCOUNT_FIELDS, ','),
      mappings.getSalesforceFieldsCommaSeparated()
    );
  }

  /**
  * Test if Field Mapping service returns all BvD fields
  *  as comma separated string
  * */
  @IsTest
  static void testGetBVDFieldsCommaSeparated() {
    FieldMappings mappings = FieldMappings.getInstance('Account');
    System.assertEquals(
      String.join(BVD_FIELDS, ','),
      mappings.getBvDFieldsCommaSeparated()
    );
  }

  /**
  * Test wrapper accessor method / Coverage test
  * */
  @IsTest
  static void testWrapperMasterField() {
    FieldMappings mappings = FieldMappings.getInstance('Account');

    for (FieldMapping fm : mappings.getFieldMappings()) {
      Field_Mapping__c record = fm.fieldMapping;

      System.assertEquals(record.Master__c, fm.getMasterField());
      System.assertEquals(record.Action__c == FieldMappings.ACTION_TYPE_AUTOMATIC, fm.isAutomatic());
    }
  }
}