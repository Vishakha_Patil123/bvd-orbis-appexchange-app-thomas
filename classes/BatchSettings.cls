/**
* Accessor class for Batch Settings (Custom Setting)
* Provides access methods to Batch_Setting__C custom setting fields,
* transforms data into more useful form, creates CronExpression from the setting.
* This object is meant to by constructed by BatchSettingsFactory class
* */
public with sharing class BatchSettings {
  private Batch_Settings__c settings;

  public BatchSettings(Batch_Settings__c settings) {
    this.settings = settings;
  }

  public Integer getBatchSize() {
    return Integer.valueOf(settings.Batch_Size__c);
  }

  public String getObjectName() {
    return settings.Object__c;
  }

  public SObjectType getSObjectType() {
    return Schema.getGlobalDescribe().get(settings.Object__c);
  }

  /**
  * Returns Cron Expr. from custom setting's frequecy and Start Hour
  * */
  public String getCronExpression() {
    String template = new Map<String, String>{
      null => '0 0 {0} * * ?',
      'daily' => '0 0 {0} * * ?',
      'weekly' => '0 0 {0} ? * {1}',
      'monthly' => '0 0 {0} L * ?'
    }.get(settings.Frequency__c);

    System.debug(new List<Object>{template, getStartHour(), getDay()});
    return String.format(template, new String[]{
      ('' + getStartHour()),
      getDay()
    });
  }

  public Boolean isDisabled() {
    return settings.Disabled__c;
  }

  public Boolean isEnabled() {
    return !settings.Disabled__c;
  }

  public Date getStartDate() {
    return settings.Start_Date__c;
  }

  public Integer getStartHour() {
    if (settings.Start_Hour__c == null) return 0;
    return Math.mod(Integer.valueOf(settings.Start_Hour__c), 24);
  }

  public Date getEndDate() {
    return settings.End_Date__c;
  }

  public Id getJobID() {
    return this.settings.Job_ID__c;
  }

  public String getScheduledJobName() {
    return String.format(Label.BvD_Schedule_Job_Name, new String[]{
      getObjectName()
    });
  }

  public String getDay(){
    return this.settings.Day__c;
  }

  public void setJobID(Id jobID) {
    this.settings.Job_ID__c = jobID;
  }

  public void disable() {
    this.settings.Disabled__c = true;
  }

  public void enable() {
    this.settings.Disabled__c = false;
  }

  public void setStartHour(Integer hour) {
    this.settings.Start_Hour__c = Math.mod(hour, 24);
  }

  public void setStartDate(Date value) {
    this.settings.Start_Date__c = value;
  }

  public void setEndDate(Date value) {
    this.settings.End_Date__c = value;
  }

  public void setFrequency(Frequency value) {
    this.settings.Frequency__c = value.name().toLowerCase();
  }

  public void updateSetting() {
      if(BVDUserPermissionCheck.permissionToUpdateSobject('Batch_Settings__c', true,
                                                             new List<String>{'Batch_Size__c', 'Object__c', 'Frequency__c','End_Date__c',
                                                                 'Start_Date__c','Start_Hour__c','Disabled__c' ,'Job_ID__c' ,'Day__c'})){
    update this.settings;
    }else{
          Logger.error('Not enough privileges to update Field mapping');
          throw new AuraHandledException('Not enough privileges to update Field mapping');        }                                                 
  }

  public enum Frequency {
    Daily,
    Weekly,
    Monthly
  }
}