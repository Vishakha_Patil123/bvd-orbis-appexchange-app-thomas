public with sharing class OrbisGetAvailableDatasourcesResponse extends OrbisRestResponse{
  private List<OrbisSchema.Datasource> Products;

  public List<OrbisSchema.Datasource> getDataSources() {
    return Products;
  }
}