public with sharing class OrbisResponseException extends Exception {

    public OrbisResponseException(HttpResponse response) {
        OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();
        OrbisSchema.FaultElement faultElement = new OrbisSchema.FaultElement();
        envelopeElement.body.content = faultElement;
        envelopeElement.deserializeXML(response.getBodyDocument());

        setMessage(faultElement.faultstring);
    }
}