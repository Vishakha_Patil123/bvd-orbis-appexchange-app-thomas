/**
* @author	 		Hubert Jaskolski
* @date		 		18/03/2014
* @description 		Class is an implementation of the ILogger interface; the class defines the methods used in Error Logging Framework.
* @see 				ILogger.cls
*/
public class Logger implements ILogger {
	/**
	* @description 	Adds Info message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    public static void info(Object innerObject) 	{addLogToBuffer(innerObject, System.loggingLevel.INFO);Logger_DB.writeToDB();}
	/**
	* @description 	Adds Debug message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    public static void debug(Object innerObject) 	{addLogToBuffer(innerObject, System.loggingLevel.DEBUG);Logger_DB.writeToDB();}
	/**
	* @description 	Adds Warn message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    public static void warn(Object innerObject) 	{addLogToBuffer(innerObject, System.loggingLevel.WARN);Logger_DB.writeToDB();}
	/**
	* @description 	Adds Error message to ErrorLog buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	* @param		Object innerObject <p> Object to log
	*/
    public static void error(Object innerObject) 	{addLogToBuffer(innerObject, System.loggingLevel.ERROR);Logger_DB.writeToDB();}
    public static void error(Object innerObject, String jobId) 	{addLogToBuffer(innerObject, System.loggingLevel.ERROR, jobId);Logger_DB.writeToDB();}
	/**
	* @description 	Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name to empty for any following log entries.
	*/
    public static void start()										{Logger_DB.start();}
	/**
	* @description 	Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name for any following log entries.
	* @param		String className <p> Sets the ClassName__C field for Error_Log__c object
	* @param		String methodName <p> Sets the MethodName__C field for Error_Log__c object
	*/
    public static void start(String className, String methodName)	{Logger_DB.start(className, methodName);}
	/**
	* @description 	Stops the logging buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero.
	*/
    public static void stop(){Logger_DB.stop();}
	/**
	* @description 	Flush the logging buffer and writes everything to file / DB, if start() and stop() functions calls difference equals zero.
	*/
    public static void flush(){Logger_DB.flush();}
	/**
	* @description 	Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name for any following log entries.
	* @param		Object innerObject <p> Object to log
	* @param		System.LoggingLevel loggingLevel <p> Logging level for the entry
	*/
    private static void addLogToBuffer(Object innerObject, System.LoggingLevel loggingLevel) {
        if(innerObject instanceof HTTPRequest)				{Logger_DB.addToLog(loggingLevel, (HTTPRequest)innerObject);}
        else if(innerObject instanceof HTTPResponse)		{Logger_DB.addToLog(loggingLevel, (HTTPResponse)innerObject);}
        else if(innerObject instanceof Exception)			{Logger_DB.addToLog(loggingLevel, (Exception)innerObject);}
        else 												Logger_DB.addToLog(loggingLevel, String.valueOf(innerObject));
    }
  private static void addLogToBuffer(Object innerObject, System.LoggingLevel loggingLevel, String jobId) {
    if(innerObject instanceof HTTPRequest)				{Logger_DB.addToLog(loggingLevel, (HTTPRequest)innerObject);}
    else if(innerObject instanceof HTTPResponse)		{Logger_DB.addToLog(loggingLevel, (HTTPResponse)innerObject);}
    else if(innerObject instanceof Exception)			{Logger_DB.addToLog(loggingLevel, (Exception)innerObject, jobId);}
    else 												Logger_DB.addToLog(loggingLevel, String.valueOf(innerObject));
  }
}