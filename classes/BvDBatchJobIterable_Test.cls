@IsTest
public with sharing class BvDBatchJobIterable_Test {
  @isTest
  static void updateSubIterable_01() {
    Contact contact = new Contact(
      FirstName = 'test',
      LastName = 'test',
      BvD_Company_Id__c = 'Test',
      BvD_Id__c = 'Sub',
      Unlinked_from_BvD__c = false
    );
    insert contact;

      BvDBatchJobIterable.UpdateSubIterable sub = new BvDBatchJobIterable.UpdateSubIterable(
        'Test', new List<String> { 'Sub' }, Contact.getSObjectType()
      );

    try {
      sub.iterator();
      sub.hasNext();
      sub.next();
      System.assert(true); // should fail
    } catch (Exception ex) {
      System.assert(false); // Never reached
    }
  }

  @isTest
  static void insertSubIterable_01() {
    Contact contact = new Contact(
      FirstName = 'test',
      LastName = 'test',
      BvD_Company_Id__c = 'Test',
      BvD_Id__c = 'Sub',
      Unlinked_from_BvD__c = false
    );
    insert contact;

    BvDBatchJobIterable.InsertSubIterable sub = new BvDBatchJobIterable.InsertSubIterable(
      'Test', new List<String> { 'Sub' }
    );

    try {
      sub.iterator();
      sub.hasNext();
      sub.next();
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
  }
}