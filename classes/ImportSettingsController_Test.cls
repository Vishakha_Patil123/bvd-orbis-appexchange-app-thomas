@IsTest
public with sharing class ImportSettingsController_Test {

  @isTest
  static void setImportSettingConfigured_01() {
    try {
      ImportSettingsController.setImportSettingConfigured();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setImportSettingConfigured_02() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;

      upsert setup;

      ImportSettingsController.setImportSettingConfigured();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setImportSettingConfigured_03() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';

      upsert setup;

      ImportSettingsController.setImportSettingConfigured();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setImportSettingConfigured_04() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';
      setup.Field_Mapping_Configured__c = true;

      upsert setup;

      ImportSettingsController.setImportSettingConfigured();
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void getImportSettings_01() {
      try {
        Import_Setting__c[] settings = ImportSettingsController.getImportSettings();
        System.assert(false);
      } catch (Exception ex) {
        System.assert(true);
      }
  }

  @isTest
  static void getImportSettings_02() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;

      upsert setup;
      Import_Setting__c[] settings = ImportSettingsController.getImportSettings();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getImportSettings_03() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';

      upsert setup;
      Import_Setting__c[] settings = ImportSettingsController.getImportSettings();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getImportSettings_04() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';
      setup.Field_Mapping_Configured__c = true;

      upsert setup;
      Import_Setting__c[] settings = ImportSettingsController.getImportSettings();
      System.assert(settings.isEmpty());
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getImportSettings_05() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';
      setup.Field_Mapping_Configured__c = true;

      upsert setup;

      insert new Import_Setting__c(
        Name = 'Account.Name',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );

      Import_Setting__c[] settings = ImportSettingsController.getImportSettings();

      System.assert(!settings.isEmpty());
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void setImportSettings_01() {
      try {
        Import_Setting__c setting = new Import_Setting__c(
          Name = 'Account.Name',
          SObject__c = 'Account',
          Type__c = 'STRING',
          Processing__c = 'Automatic'
        );

        ImportSettingsController.setImportSettings(new Import_Setting__c[] {
          setting
        });
        System.assert(false);
      } catch (Exception ex) {
        System.assert(true);
      }
  }

  @isTest
  static void setImportSettings_02() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;

      upsert setup;

      Import_Setting__c setting = new Import_Setting__c(
        Name = 'Account.Name',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );

      ImportSettingsController.setImportSettings(new Import_Setting__c[] {
        setting
      });
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setImportSettings_03() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';

      upsert setup;

      Import_Setting__c setting = new Import_Setting__c(
        Name = 'Account.Name',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );

      ImportSettingsController.setImportSettings(new Import_Setting__c[] {
        setting
      });
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void setImportSettings_04() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';
      setup.Field_Mapping_Configured__c = true;

      upsert setup;

      Import_Setting__c setting = new Import_Setting__c(
        Name = 'Account.Name',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );

      Import_Setting__c[] result = ImportSettingsController.setImportSettings(new Import_Setting__c[] {
        setting
      });

      System.assert(!result.isEmpty());
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void setImportSettings_05() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';
      setup.Field_Mapping_Configured__c = true;

      upsert setup;

      Import_Setting__c setting = new Import_Setting__c(
        Name = 'Account.Name',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );

      Import_Setting__c[] result = ImportSettingsController.setImportSettings(new Import_Setting__c[] {
        setting
      });

      Import_Setting__c[] finalResult = ImportSettingsController.setImportSettings(result);

      System.assert(!result.isEmpty());
      System.assertEquals(
          result.size(),
          finalResult.size()
      );
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void setImportSettings_06() {
    try {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

      setup.SSO_Confirmed__c = true;
      setup.Datasource_Id__c = 'id';
      setup.Field_Mapping_Configured__c = true;

      upsert setup;

      Import_Setting__c setting = new Import_Setting__c(
        Name = 'Account.Name',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );
      Import_Setting__c setting2 = new Import_Setting__c(
        Name = 'Account.Id',
        SObject__c = 'Account',
        Type__c = 'STRING',
        Processing__c = 'Automatic'
      );

      Import_Setting__c[] result = ImportSettingsController.setImportSettings(new Import_Setting__c[] {
        setting
      });

      Import_Setting__c[] finalResult = ImportSettingsController.setImportSettings(new Import_Setting__c[] {
        result[0],
        setting2
      });

      System.assert(!result.isEmpty());
      System.assertNotEquals(
        result.size(),
        finalResult.size()
      );
    } catch (Exception ex) {
      System.assert(false);
    }
  }
}