public with sharing class OrbisGetAvailableFieldsResponse extends OrbisResponse {
    private OrbisSchema.GetAvailableFieldsResponseElement responseElement
            = new OrbisSchema.GetAvailableFieldsResponseElement();
    private List<Field> fields = new List<Field>();

    public OrbisGetAvailableFieldsResponse(HttpResponse response) {
        super(response);
        readEscapedResponse();
    }

    public List<Field> getAvailableFields() {
        return fields;
    }

    public class Field {
        public String code;
        public String type;
        public DataDimensions dataDimensions;
    }

    public class DataDimensions {
        public String default_x;
        public List<DataDimension> dataDimensions = new List<DataDimension>();
    }

    public class DataDimension {
        public String name;
        public String type;
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseElement;
    }

    private void readEscapedResponse() {
        String fieldsSchema = responseElement.GetAvailableFieldsResult;

        if (String.isNotEmpty(fieldsSchema)) {
            Dom.Document document = new Dom.Document();
            document.load(fieldsSchema.unescapeXml());

            for (Dom.XmlNode fieldNode : document.getRootElement().getChildElements()) {
                Field field = new Field();
                field.code = fieldNode.getAttribute('code', null);
                field.type = fieldNode.getAttribute('type', null);

                Dom.XmlNode dataDimNode = fieldNode.getChildElement('DataDimensions', null);
                if (dataDimNode != null) {
                    DataDimensions dataDimensions = new DataDimensions();
                    dataDimensions.default_x = dataDimNode.getAttribute('default', null);
                    field.dataDimensions = dataDimensions;

                    for (Dom.XmlNode dimensionNode : dataDimNode.getChildElements()) {
                        DataDimension dimension = new DataDimension();
                        dimension.name = dimensionNode.getAttribute('name', null);
                        dimension.type = dimensionNode.getAttribute('type', null);

                        dataDimensions.dataDimensions.add(dimension);
                    }
                }

                this.fields.add(field);
            }
        }
    }
}