/**
* Domain Layer for BvD Job Work Items
* */
public with sharing class BvDJobWorkItems {
  public final static String
    STATUS_SUCCESS = 'Success',
    STATUS_NOT_STARTED = 'Not Started',
    STATUS_ERROR = 'Error',
    ERROR_COMPANY_NOT_EXISTS = 'Company does not exist in Orbis',
    ERROR_INVALID_FIELD_MAPPING = 'Invalid Field Mapping',
    ERROR_VALUE_ASSIGNMENT = 'Value assignment Error',
    ERROR_DML = 'DML Error';

}