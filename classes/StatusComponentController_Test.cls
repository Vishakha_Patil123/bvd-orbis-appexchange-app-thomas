@IsTest
public with sharing class StatusComponentController_Test {

  @isTest
  static void getContext_01() {
    try {
      ContainerCompanyUrl containerCompanyUrl = StatusComponentController.getContext();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getContext_02() {
    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    setup.Import_Settings_Configured__c = true;
    setup.Batchjob_Settings_Confirmed__c = true;
    insert setup;
    try {
      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      ContainerCompanyUrl containerCompanyUrl = StatusComponentController.getContext();
      System.assert(true);
      Test.stopTest();
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.assert(false);
    }
  }

  @isTest
  static void fetchObject_01() {
    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    setup.Import_Settings_Configured__c = true;
    setup.Batchjob_Settings_Confirmed__c = true;
    insert setup;

    Bvd_Setup__c result = (Bvd_Setup__c) StatusComponentController.fetchObject('BvD_Setup__c', setup.Id);

    System.assertEquals(
        setup.Datasource_Id__c,
        result.Datasource_Id__c
    );
  }
}