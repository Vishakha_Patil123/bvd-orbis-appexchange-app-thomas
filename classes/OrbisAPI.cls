/**
* @description
* Service class used to send OrbisRequest through Http.
* Each OrbisRequest concrete class can be sent via send method.
* OrbisResponse associated with that request (by OrbisRequest.getOrbisResponse method)
* */
public with sharing class OrbisAPI {
  private final static String STATUS_OK = 'OK';

  public OrbisResponse send(OrbisRequest request) {
    HttpRequest httpRequest;
    HttpResponse httpResponse;

    try {
      Logger.start('OrbisAPI', 'send');
      httpRequest = request.toHttpRequest();
      httpResponse = new Http().send(httpRequest);

      if (httpResponse.getStatus() == STATUS_OK) {
        OrbisResponse response = request.getOrbisResponse(httpResponse);

        if (response instanceof OrbisRestResponse) {
          OrbisRestResponse restResponse = (OrbisRestResponse) response;

          if (restResponse.hasError()) {
            throw new OrbisResponseException(restResponse.getError());
          }
        }

        Logger.info(httpRequest);
        Logger.info(httpResponse);
        System.debug('response=='+response);
        return response;
      } else {
        throw new OrbisResponseException(httpResponse);
      }
    } catch (Exception ex) {
      Logger.error(ex);
      if (httpRequest != null) Logger.error(httpRequest);
      if (httpResponse != null) Logger.error(httpResponse);

      OrbisResponseException orbisEx = new OrbisResponseException();
      orbisEx.initCause(ex);
      orbisEx.setMessage(ex.getMessage());
      throw orbisEx;
    }
  }
}