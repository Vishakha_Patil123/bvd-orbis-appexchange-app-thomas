/**
 * @author       Hubert Jaskolski
 * @description  The class contains static methods used for Logging the Error log into DataBase.
 * @see          Logger.cls
 */
public class Logger_DB {

	/**********************************************************************
	*
	*							CONSTANTS
	*
	**********************************************************************/

	/**
	 * @description Enum for types of the Logs
	 */
    public enum logType	{
        REQUEST,
        RESPONSE,
        APEX
    }

  public final static String SETTING_NAME = 'BvD - Logs Settings';

	/**********************************************************************
	*
	*						BUFFER DECLARATION
	*
	**********************************************************************/

	/**
	 * @description Log Error Buffer List
	 */
    public static List<Log__c> errorLogBuffer = new List<Log__c>();

	/**********************************************************************
	*
	*						PUBLIC VARIABLES
	*
	**********************************************************************/

	/**
	 * @description Log Start Counter
	 */
    public static Integer logStartCounter = 0;

	/**
	 * @description Log Entity Class Name
	 */
    public static String className;

	/**
	 * @description Log Entity Method Name
	 */
    public static String methodName;

	/**
	 * @description Log Entity Class Name Level Map (for multi-level logging)
	 */
    public static Map<Integer, String> classNameLevelMap = new Map<Integer, String>();

	/**
	 * @description Log Entity Method Name Level Map (for multi-level logging)
	 */
    public static Map<Integer, String> methodNameLevelMap = new Map<Integer, String>();

	/**********************************************************************
	*
	*					GUID - TRANSACTION LOG GROUP ID
	*
	**********************************************************************/
	/**
	 * @description Guid for the logs in one transaction
	 */
    private static String guid = GuidGenerator.NewGuid();

//	/**********************************************************************
//	*
//	*						CUSTOM SETTINGS
//	*
//	**********************************************************************/
//
    /**
     *
     */
  private static Log_Settings__c logSettings;

  static {
        logSettings = Log_Settings__c.getInstance(SETTING_NAME);

        if(logSettings == null) {
          logSettings = new Log_Settings__c( Name = SETTING_NAME, Error__c = TRUE );
        }
  }
//	/**
//	 * @description Current user's Logging Level Settings (Custom Settings Login_Level_Settings)
//	 */
    private static Map<String, Boolean> currentUserLoggingLevelSettingsMap {
        get {
            if (currentUserLoggingLevelSettingsMap == null) {
                    currentUserLoggingLevelSettingsMap = new Map<String, Boolean>{
                            String.valueOf(System.LoggingLevel.ERROR)	=> logSettings.Error__c,
                            String.valueOf(System.LoggingLevel.WARN)	=> logSettings.Warn__c,
                            String.valueOf(System.LoggingLevel.INFO)	=> logSettings.Info__c,
                            String.valueOf(System.LoggingLevel.DEBUG)	=> logSettings.Other__c
                    };
            }
            return currentUserLoggingLevelSettingsMap;
        }
        set;
    }
//
//	/**
//	 * @description	Checks whether the log should be added to buffer for current user
//	 */
    private static void checkLoggingLevelSettingsAndWriteToBuffer(Log__c singleLog) {
        Boolean canWrite = currentUserLoggingLevelSettingsMap.get(singleLog.Logging_Level__c);

        if(canWrite) {
            errorLogBuffer.add(singleLog);
        }
    }


	/**********************************************************************
	*
	*							PUBLIC METHODS
	*
	**********************************************************************/

	/**
	 * @description	Adds HTTP Request to buffer
	 */
    public static void addToLog(System.loggingLevel logLevel, HTTPRequest req) {
      String jsonBody = req.getBody()         == null ? null : req.getBody().left(131000);
      String xmlBody  = req.getBodyDocument() == null ? null : req.getBodyDocument().toXmlString().left(131000);

        Log__c singleLog = new Log__c(
                Logging_Level__c = String.valueOf(logLevel),
                Type__c = String.valueOf(logType.REQUEST),
                Apex_Class_Name__c = className != null ? className.left(255) : null,
                Apex_Method_Name__c = methodName != null ? methodName.left(255) : null,
                Guid__c = guid,
                HTTP_Endpoint__c = req.getEndpoint() != null ? req.getEndpoint().left(32768).left(255) : null,
                HTTP_Method__c = req.getMethod() != null ? req.getMethod().left(255).left(255) : null,
                HTTP_Body__c = req.getBodyDocument() != null ? xmlBody : jsonBody,
                Message__c = req.getBodyDocument() != null ? req.getHeader('SOAPAction') : req.getEndpoint().left(32768).left(255)
        );

        checkLoggingLevelSettingsAndWriteToBuffer(singleLog);
    }

	/**
	 * @description	Adds HTTP Response to buffer
	 */
    public static void addToLog(System.loggingLevel logLevel, HTTPResponse res) {
        Log__c singleLog = new Log__c(
                Logging_Level__c = String.valueOf(logLevel),
                Type__c = String.valueOf(logType.RESPONSE),
                Apex_Class_Name__c = className != null ? className.left(255) : null,
                Apex_Method_Name__c = methodName != null ? methodName.left(255) : null,
                Message__c = res.toString(),
                Guid__c = guid,
                HTTP_Error_Status__c = res.getStatus() != null ? res.getStatus().left(255) : null,
                HTTP_Error_Code__c = String.valueOf(res.getStatusCode()) != null ? String.valueOf(res.getStatusCode()).left(255) : null,
                HTTP_Body__c = res.getBody() != null ? res.getBody().left(32768) : null
        );

        checkLoggingLevelSettingsAndWriteToBuffer(singleLog);
    }

	/**
	 * @description	Adds message to buffer
	 */
    public static void addToLog(System.loggingLevel logLevel, String message) {
        Log__c singleLog = new Log__c(
                Logging_Level__c = String.valueOf(logLevel),
                Type__c = String.valueOf(logType.APEX),
                Apex_Class_Name__c = className != null ? className.left(255) : null,
                Apex_Method_Name__c = methodName != null ? methodName.left(255) : null,
                Guid__c = guid,
                Message__c = message != null ? message.left(32768) : null
        );
        //errorLogBuffer.add(singleLog);
        checkLoggingLevelSettingsAndWriteToBuffer(singleLog);
    }

	/**
	 * @description	Adds error to buffer
	 */
    public static void addToLog(System.loggingLevel logLevel, Exception e) {
        Log__c singleLog = new Log__c(
                Logging_Level__c = String.valueOf(logLevel),
                Type__c = String.valueOf(logType.APEX),
                Apex_Class_Name__c = className != null ? className.left(255) : null,
                Apex_Method_Name__c = methodName != null ? methodName.left(255) : null,
                Guid__c = guid,
                Message__c = e.getMessage() != null ? e.getMessage().left(32768) : null,
                Apex_Exception_Type__c = e.getTypeName() != null ? e.getTypeName().left(32768) : null,
                Apex_Stack_Trace__c = e.getStackTraceString() != null ? e.getStackTraceString().left(32768) : null
        );
        //errorLogBuffer.add(singleLog);
        checkLoggingLevelSettingsAndWriteToBuffer(singleLog);
    }

  public static void addToLog(System.loggingLevel logLevel, Exception e, String jobId) {
    Log__c singleLog = new Log__c(
      Logging_Level__c = String.valueOf(logLevel),
      Type__c = String.valueOf(logType.APEX),
      Apex_Class_Name__c = className != null ? className.left(255) : null,
      Apex_Method_Name__c = methodName != null ? methodName.left(255) : null,
      Guid__c = guid,
      Message__c = e.getMessage() != null ? e.getMessage().left(32768) : null,
      Apex_Exception_Type__c = e.getTypeName() != null ? e.getTypeName().left(32768) : null,
      Apex_Stack_Trace__c = e.getStackTraceString() != null ? e.getStackTraceString().left(32768) : null,
      BvD_Job__c = jobId
    );
    //errorLogBuffer.add(singleLog);
    checkLoggingLevelSettingsAndWriteToBuffer(singleLog);
  }

	/**
	 * @description 	Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name to empty for any following log entries
	 */
    public static void start() {

        classNameLevelMap.put(logStartCounter,null);
        methodNameLevelMap.put(logStartCounter,null);

        logStartCounter++;
        className = null;
        methodName = null;
    }

	/**
	 * @description Starts the logging buffer - if used, it waits for stop() funcion call before writing the buffer to file / DB.<p> It sets class name and method name for any following log entries
	 * @param       className       Sets the ClassName__C field for Log__c object
	 * @param       methodName      Sets the MethodName__C field for Log__c object
	 */
    public static void start(String classNameString, String methodNameString) {

        classNameLevelMap.put(logStartCounter,className);
        methodNameLevelMap.put(logStartCounter,methodName);

        logStartCounter++;
        className = classNameString;
        methodName = methodNameString;
    }

    public static void flush() {

        //the flush will log everything in the last entry
        logStartCounter--;
        if (logStartCounter < 0) logStartCounter = 0;

        className = classNameLevelMap.get(logStartCounter);
        methodName = methodNameLevelMap.get(logStartCounter);

        //resets everything
        logStartCounter=0;

        writeToDB();
    }

	/**
	 * @description 	Stops the logging buffer and writes it to file / DB, if start() and stop() functions calls difference equals zero
	 */
    public static void stop() {

        logStartCounter--;
        if (logStartCounter < 0) {
            logStartCounter = 0;
            system.debug(LoggingLevel.ERROR,'#### stopping without anything started ####');
        }

        className = classNameLevelMap.get(logStartCounter);
        methodName = methodNameLevelMap.get(logStartCounter);

        writeToDB();
    }

	/**
	 * @description 	Writes the Error Log buffer to file / DB, if start() and stop() functions calls difference equals zero
	 */
    public static void writeToDB() {
        if (logStartCounter == 0) {

            if (Limits.getLimitDmlStatements() == 0) return;	//added this, sometimes we are running in a readonly mode

            if (Limits.getDMLStatements() < Limits.getLimitDmlStatements()) {
                system.debug('#### insert the log into the DB ####');
				if(BVDUserPermissionCheck.permissionToCreateSobject('Log__c', true,
                                                                   new List<String>{'Logging_Level__c, Type__c', 
                                                                        'Apex_Class_Name__c',  'Apex_Method_Name__c', 
                                                                        'Guid__c', 'Apex_Exception_Type__c', 
																		'Apex_Stack_Trace__c'})){
					insert Logger_DB.errorLogBuffer;
			    }
                Logger_DB.errorLogBuffer.clear();
            } else {
                //Logger_DB.emailAdminUsersPublicGroup('DML Statements Limit Exception detected by Logger', errorLogBuffer);
                Logger_DB.errorLogBuffer.clear();
            }
        }
    }


	/**********************************************************************
	*
	*						HELPER FUNCTIONS
	*
	**********************************************************************/

	/**
	 * @description Get User Ids for chosen Public Groups
	 * @param 		String[] groupNames<p> Public Group Names to get User Ids from
	 * @return 		Id[] <p> User Ids for choosen Public Groups
	 */
  @TestVisible
    private static Id[] getUserIdsFromGroup(String[] groupNames) {
		/** Store the results in a set so we don't get duplicates */
        Id[] result = new Id[]{};
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();
        Id[] groupIdProxys = new Id[]{};
		/** Loop through all group members in a group */
            if(BVDUserPermissionCheck.permissionToAccessSobject('GroupMember', false, new List<String>{'UserOrGroupId'})){
                for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where Group.Name in :groupNames]) {
                    if (((String)m.UserOrGroupId).startsWith(userType)) {
                        /** If the user or group id is a user */
                        result.add(m.UserOrGroupId);
                    } else if (((String)m.UserOrGroupId).startsWith(groupType)) {
                        /** If the user or group id is a group */
                        /** Note: there may be a problem with governor limits if this is called too many times */
                        /** Call this function again but pass in the group found within this group */
                        groupIdProxys.add(m.UserOrGroupId);
                    }
                }
            }else{
                Logger.error('Not enough privileges to access GroupMember');
                Logger.stop();
                throw new AuraHandledException('Not enough privileges to access GroupMember');
            }

        if (groupIdProxys.size() > 0) {
            result.addAll(GetUSerIdsFromGroup(groupIdProxys));
        }

        System.debug('Found the group members: ' + result);
        return result;
    }

	/**
	 * @description Returns empty String value instead of null for the choosen String value
	 * @param 		String originalString <p> String to check
	 * @return 		String <p> Empty String value instead of null or orginalString
	 */
  @TestVisible
    private static String nullValue(String originalString) {
        if (String.isBlank(originalString))
            return '';
        return originalString;
    }

  @TestVisible
    private static String formatDateTime(DateTime dt, String timeZoneId) {
        return (((null != dt) && (String.isNotBlank(timeZoneId))) ? dt.format(DATETIME_FORMAT_STR, timeZoneId) : '');
    }

    private final static String DATETIME_FORMAT_STR = 'yyyy-MM-dd HH:mm:ss';
}