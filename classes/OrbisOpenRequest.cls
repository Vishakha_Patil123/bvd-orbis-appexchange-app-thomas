public with sharing class OrbisOpenRequest extends OrbisRestRequest {
    private String Username = UserInfo.getUserName();
    private String FirstName = UserInfo.getFirstName();
    private String LastName = UserInfo.getLastName();
    private String EMail = UserInfo.getUserEmail();

    public OrbisOpenRequest() {
    }

    public OrbisOpenRequest setUsername(String username) {
        return this;
    }

    public OrbisOpenRequest setPassword(String password) {
        return this;
    }

    public override Type getOrbisResponseType() {
        return OrbisOpenResponse.class;
    }

    protected override String getPartialUrl() {
      Bvd_Setup__c setup = BvDSetupSettings.getSetting();

        return String.format('/{0}/session/orbis', new String[]{
          setup.Organization_Token__c + UserInfo.getOrganizationId()
        });
    }
}