/******************************************************************************************************************************************************************
@purpose      : BVD Utils class
@created Date : 16 january 2018
*******************************************************************************************************************************************************************/
public with sharing class BVDUserPermissionCheck {

	/**
	 *@ purppose : To check whether user has create(insert) permission for sobject.
	 *@ param    : 1)sObjectName 2)prefixNameSpace
	 *@ return   : userHasAccess(Boolean)
	*/
	public static Boolean permissionToCreateSobject ( String sObjectName,
													  Boolean prefixNameSpace,
													  List<String> sObjectFields ) {

		Boolean userHasAccess = true;
		String nameSpacePrefix = '';

		if(String.isNotBlank(sObjectName) && sObjectFields != null && prefixNameSpace != NULL) {

           	if(prefixNameSpace){
				nameSpacePrefix = getPackageNameSpace();
               	sObjectName = nameSpacePrefix+sObjectName;
           	}

           	Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(sObjectName);

			if(sobjectType != NULL){

				Schema.DescribeSObjectResult objectDescribe = sobjectType.getDescribe();

	           	if(objectDescribe.isCreateable()) {

					Map<String, Schema.SObjectField> schemaFieldMap = objectDescribe.fields.getMap();
					Schema.SObjectField schemaField;

					for(String fieldToCheck: sObjectFields){

						schemaField = schemaFieldMap.get(nameSpacePrefix + fieldToCheck);

						if(schemaField != NULL){
							if(!schemaField.getDescribe().isCreateable()) {
			                    userHasAccess = false;
								break;
			                }
						}
		            }
	           	}else{
	           		userHasAccess = false;
				}
			}else{
				userHasAccess = false;
			}
       	}else{
			userHasAccess = false;
		}

		return userHasAccess;

	}

	/**
	 *@ purppose : To check whether user has upadate permission for sobject.
	 *@ param    : 1)sObjectName 2)prefixNameSpace
	 *@ return   : userHasAccess(Boolean)
	*/
	public static Boolean permissionToUpdateSobject ( String sObjectName,
													  Boolean prefixNameSpace,
													  List<String> sObjectFields ) {

		Boolean userHasAccess = true;
		String nameSpacePrefix = '';

		if(String.isNotBlank(sObjectName) && sObjectFields != null && prefixNameSpace != NULL) {

			if(prefixNameSpace){
				nameSpacePrefix = getPackageNameSpace();
			   	sObjectName = nameSpacePrefix+sObjectName;
			}

			Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(sObjectName);

			if(sobjectType != NULL){

				Schema.DescribeSObjectResult objectDescribe = sobjectType.getDescribe();

				if(objectDescribe.isUpdateable()) {

					Map<String, Schema.SObjectField> schemaFieldMap = objectDescribe.fields.getMap();
					Schema.SObjectField schemaField;

					for(String fieldToCheck: sObjectFields){

						schemaField = schemaFieldMap.get(nameSpacePrefix + fieldToCheck);

						if(schemaField != NULL){

							if(!schemaField.getDescribe().isUpdateable()) {
								userHasAccess = false;
								break;
							}
						}
					}
				}else{
					userHasAccess = false;
				}
			}else{
				userHasAccess = false;
			}
		}else{
			userHasAccess = false;
		}

		return userHasAccess;
	}

	/**
	 *@ purppose : To check whether user has Upsert permission for sobject.
	 *@ param    : 1)sObjectName 2)prefixNameSpace
	 *@ return   : userHasAccess(Boolean)
	*/
	public static Boolean permissionToUpsertSobject ( String sObjectName,
													  Boolean prefixNameSpace,
													  List<String> sObjectFields ) {
 		Boolean userHasAccess = false;

 		if(permissionToCreateSobject(sObjectName, prefixNameSpace, sObjectFields) &&
 		   permissionToUpdateSobject(sObjectName, prefixNameSpace, sObjectFields)){

 			userHasAccess = true;
 		}
 		return userHasAccess;
	}

	/**
	 *@ purppose : To check whether user has delete permission for sobject.
	 *@ param    : 1)sObjectName 2)prefixNameSpace
	 *@ return   : userHasAccess(Boolean)
	*/
	public static Boolean permissionToDeleteSobject (String sObjectName, Boolean prefixNameSpace) {

 		Boolean userHasAccess = false;
		String nameSpacePrefix = '';

		if(String.isNotBlank(sObjectName) && prefixNameSpace != NULL) {

			if(prefixNameSpace){
				nameSpacePrefix = getPackageNameSpace();
				sObjectName = nameSpacePrefix+sObjectName;
			}

			Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(sObjectName);

			if(sobjectType != NULL){

				Schema.DescribeSObjectResult objectDescribe = sobjectType.getDescribe();

				if(objectDescribe.isDeletable()) {
					userHasAccess = true;
				}
			}
		}

		return userHasAccess;
	}

	/**
	 *@ purppose : To check whether user has undelete permission for sobject.
	 *@ param    : 1)sObjectName 2)prefixNameSpace
	 *@ return   : userHasAccess(Boolean)
	*/
	public static Boolean permissionToUndeleteSobject (String sObjectName, Boolean prefixNameSpace){

		Boolean userHasAccess = false;
		String nameSpacePrefix = '';

		if(String.isNotBlank(sObjectName) && prefixNameSpace != NULL) {

			if(prefixNameSpace){
				nameSpacePrefix = getPackageNameSpace();
			   	sObjectName = nameSpacePrefix+sObjectName;
			}

			Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(sObjectName);

			if(sobjectType != NULL){

				Schema.DescribeSObjectResult objectDescribe = sobjectType.getDescribe();

				if(objectDescribe.isUndeletable()) {
					userHasAccess = true;
				}
			}
		}

		return userHasAccess;
	}



	/**
	 *@purppose : To check whether user has Access permission for sobject.
	 *@param    : 1)sObjectName 2)prefixNameSpace
	 *@return   : userHasAccess(Boolean)
	*/
   	public static Boolean permissionToAccessSobject (String sObjectName, Boolean prefixNameSpace, List<String> sObjectFields) {

      Boolean userHasAccess = true;
			String nameSpacePrefix = '';

       	if(String.isNotBlank(sObjectName) && sObjectFields != null && prefixNameSpace != NULL) {

			if(prefixNameSpace){
				nameSpacePrefix = getPackageNameSpace();
			   	sObjectName = nameSpacePrefix+sObjectName;
			}

           	Schema.DescribeSObjectResult objectDescribe = Schema.getGlobalDescribe().get(sObjectName).getDescribe();

           	if(objectDescribe.isAccessible()) {

				Map<String, Schema.SObjectField> schemaFieldMap = objectDescribe.fields.getMap();
				Schema.SObjectField schemaField;

				for(String fieldToCheck: sObjectFields){

					schemaField = schemaFieldMap.get(nameSpacePrefix + fieldToCheck);

					if(schemaField != NULL){
						if(!schemaField.getDescribe().isAccessible()) {
		                    userHasAccess = false;
							break;
		                }
					}
	            }

           	}else{
           		userHasAccess = false;
			}
       	}

   		return userHasAccess;
   	}

	/**
	 *@purpose : Get sobject fiels to check the user permission
	 *@param   : 1) sObjectName 2)permissionType(create, update, delete, undelete)
	 *@return  : list of fields(list String)
	 */
	private static List<String> getSobjectFields (String sObjectName, String premissionType) {

		List<String> sObjectFields = new List<String>();

		if(String.isNotBlank(sObjectName) && String.isNotBlank(premissionType)){


			/*Map<String, Schema.SObjectType> globalMap = Schema.getGlobalDescribe();
			Schema.DescribeSObjectResult objResult = globalMap.get(sObjectName).getDescribe();
			Map<String, Schema.FieldSet> fieldSetMap = objResult.fieldSets.getMap();

			if(fieldSetMap != NULL){

				String fieldSetName = sObjectName.removeEnd('_c') + 'FieldSet';
				Schema.FieldSet  fieldSet = fieldSetMap.get(fieldSetName);

				for(Schema.FieldSetMember fieldSetMember :fieldSet.getFields()){
					sObjectFields.add(fieldSetMember.getFieldPath());
				}
			}*/

		}

		return sObjectFields;
	}

	/**
	*@purpose : To get prosperVue package namespace.
	*@param   : -
	*@return  : namespace(String)
	*/
   	public static String getPackageNameSpace(){

	   String namespace;

	   List<ApexClass> apexclassList = [SELECT NamespacePrefix
										FROM ApexClass
										WHERE Name = 'BVDUserPermissionCheck'];

		if(apexclassList != null) {
		   namespace = apexclassList[0].NamespacePrefix != NULL ? apexclassList[0].NamespacePrefix+'__' : '';
	   	}else {
		   namespace = '';
	   	}

	   return namespace;
   	}

}