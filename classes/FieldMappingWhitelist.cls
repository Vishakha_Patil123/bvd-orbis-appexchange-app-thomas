/**
* This class determines if field is appropriate for Orbis field mapping
* */
public with sharing class FieldMappingWhitelist {
  private static final Set<DisplayType> FIELD_TYPE_BLACKLIST = new Set<DisplayType>{
    DisplayType.ADDRESS,
    DisplayType.BASE64,
    DisplayType.COMPLEXVALUE,
    DisplayType.DATACATEGORYGROUPREFERENCE,
    DisplayType.ENCRYPTEDSTRING,
    DisplayType.ID,
    DisplayType.JSON,
    DisplayType.REFERENCE,
    DisplayType.SOBJECT
  };

  private static final Set<String> COMMON_FIELDS_BLACKLIST = new Set<String>{
    'LastActivityDate',
    'LastViewedDate',
    'LastReferencedDate',
    'IsDeleted',
    'SystemModstamp'
  };

  private static final Map<SObjectType, Set<String>> OBJECT_SPECIFIC_FIELDS_BLACKLIST = new Map<SObjectType, Set<String>>{
    Account.getSObjectType() => new Set<String>{
      'DandbCompanyId',
      'DunsNumber',
      'Jigsaw',
      'JigsawCompanyId'
    },

    Contact.getSObjectType() => new Set<String>{
      'Jigsaw',
      'JigsawContactId'
    },

    Lead.getSObjectType() => new Set<String>{
    }
  };


  /**
  * Returns true if this Salesforce field can be used in Salesforce - Orbis Field Mapping
  * */
  public static Boolean contains(String sObjectType, String field) {
    SObjectDescribe sObjDescribe = SObjectDescribe.getDescribe(sObjectType);
    if (sObjDescribe == null) return false;

    SObjectField fielDescribe = sObjDescribe.getField(field);
    if (fielDescribe == null) return false;

    return contains(sObjDescribe, fielDescribe.getDescribe());
  }

  public static Boolean contains(SObjectDescribe sObjectDescribe, DescribeFieldResult field) {
    return sObjectDescribe != null
      && field != null
      && field.isAccessible()
      && !field.isAutoNumber()
      && !field.isCalculated()
      && !FIELD_TYPE_BLACKLIST.contains(field.getType())
      && !COMMON_FIELDS_BLACKLIST.contains(field.getName())
      && !OBJECT_SPECIFIC_FIELDS_BLACKLIST.get(sObjectDescribe.getSObjectType()).contains(field.getName());
  }
}