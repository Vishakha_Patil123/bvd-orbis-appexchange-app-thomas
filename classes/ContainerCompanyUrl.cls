public with sharing class ContainerCompanyUrl {

  @AuraEnabled
  public String baseUrl { get; set; }

  @AuraEnabled
  public String url { get; set; }

  @AuraEnabled
  public String urlCRM { get; set; }

  @AuraEnabled
  public String uri { get; set; }

  @AuraEnabled
  public String uriCRM { get; set; }

  @AuraEnabled
  public String recordUri { get; set; }

  @AuraEnabled
  public String recordUriCRM { get; set; }

  @AuraEnabled
  public String recordContactUri { get; set; }

  @AuraEnabled
  public String recordContactUriCRM { get; set; }

  @AuraEnabled
  public String context { get; set; }

  public ContainerCompanyUrl(String url, String context) {
    System.URL link     = new System.URL(url);
    System.URL crmLink  = new System.URL(url + '/CRM');

    this.baseUrl    = String.format('{0}://{1}', new String[] { link.getProtocol(), link.getAuthority() });
    this.url        = link.toExternalForm();
    this.urlCRM     = crmLink.toExternalForm();
    this.context    = context;

    this.setUri();
    this.setRecordUri();
  }

  private String buildUri(String base, Map<String, String> parameters) {
    String queryString = '';

    for (String key : parameters.keySet()) {
      queryString = queryString + String.format('{0}={1}&', new String [] { key, parameters.get(key) });
    }

    return String.format('{0}?{1}', new String [] {
      base,
      queryString
    }).removeEnd('?').removeEnd('&');
  }

  private void setUri() {
    uri = buildUri(
      url, new Map<String, String> {
        'context'      => context
      }
    );
    uriCRM = buildUri(
      urlCRM, new Map<String, String> {
        'targetOrigin' => EncodingUtil.urlEncode(System.Url.getSalesforceBaseUrl().toExternalForm().replace('my.salesforce.com', 'lightning.force.com'), 'UTF-8'),
        'context'      => context
      }
    );
  }

  private void setRecordUri() {

    recordUri = buildUri(
      url + '/report/bvdid/{0}', new Map<String, String> {
        'context'      => context
      }
    );
    recordUriCRM = buildUri(
      urlCRM + '/report/bvdid/{0}', new Map<String, String> {
        'targetOrigin' => EncodingUtil.urlEncode(System.Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8'),
        'context'      => context
      }
    );

    recordContactUri = buildUri(
      url.replace('Companies', 'contacts') + '/report/UniqueId/{0}', new Map<String, String> {
        'context'      => context
      }
    );
    recordContactUriCRM = buildUri(
      urlCRM.replace('Companies', 'contacts') + '/report/UniqueId/{0}', new Map<String, String> {
        'targetOrigin' => EncodingUtil.urlEncode(System.Url.getSalesforceBaseUrl().toExternalForm(), 'UTF-8'),
        'context'      => context
      }
    );
  }
}