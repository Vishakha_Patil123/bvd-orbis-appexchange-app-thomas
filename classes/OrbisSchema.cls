public with sharing class OrbisSchema {
  public final static String
    NAMESPACE_XSI = 'http://www.w3.org/2001/XMLSchema-instance',
    NAMESPACE_XSD = 'http://www.w3.org/2001/XMLSchema',
    NAMESPACE_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/',
    NAMESPACE_BVD = 'http://bvdep.com/webservices/',
    NAMESPACE_DATARESULT = 'http://www.bvdep.com/schemas/RemoteAccessDataResults.xsd';

  public class EnvelopeElement extends XmlNode.XmlRootNode {
    public BodyElement body = new BodyElement();

    protected override void serializeToXML() {
      createThisNode('Envelope', NAMESPACE_SOAP, 'soap');
      setNamespace('xsi', NAMESPACE_XSI);
      setNamespace('xsd', NAMESPACE_XSD); 
      setNamespace('soap', NAMESPACE_SOAP);

      addChildElement(body);
    }

    protected override void deserializeXML() {
      deserializeChild(body);
    }
  }

  public class BodyElement extends XmlNode {
    public BodyContentElement content;

    protected override void serializeToXML() {
      createThisNode('Body', NAMESPACE_SOAP, 'soap');
      addChildElement(content);
    }

    protected override void deserializeXML() {
      deserializeChild(content);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Body', NAMESPACE_SOAP);
    }
  }


  public abstract class BodyContentElement extends XmlNode {
  }
  public abstract class RequestElement extends BodyContentElement {
    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return null;
    }
    protected override void deserializeXML() {
    }
  }
  public abstract class ResponseElement extends BodyContentElement {
    protected override void serializeToXML() {
    }
  }


  public class OpenElement extends RequestElement {
    public String username;
    public String password;

    protected override void serializeToXML() {
      createThisNode('Open');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('username', username);
      addChildTextElement('password', password);
    }
  }

  public class OpenResponseElement extends ResponseElement {
    public String OpenResult;

    protected override void deserializeXML() {
      this.OpenResult = getStringValue('OpenResult', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('OpenResponse', NAMESPACE_BVD);
    }
  }


  public class MatchElement extends RequestElement {
    public String sessionHandle;
    public MatchCriteriaElement criteria = new MatchCriteriaElement();
    public String exclusionFlags;

    protected override void serializeToXML() {
      createThisNode('Match');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('sessionHandle', sessionHandle);
      addChildElement(criteria);
      addChildTextElement('exclusionFlags', exclusionFlags);
    }
  }

  public class MatchCriteriaElement extends RequestElement {
    public String Name;
    public String Address;
    public String PostCode;
    public String City;
    public String Country;
    public String PhoneOrFax;
    public String EMailOrWebsite;
    public String NationalId;
    public String Ticker;
    public String Isin;
    public String State;
    public String BvD9;

    protected override void serializeToXML() {
      createThisNode('criteria');
      addChildTextElement('Name', Name);
      addChildTextElement('Address', Address);
      addChildTextElement('PostCode', PostCode);
      addChildTextElement('City', City);
      addChildTextElement('Country', Country);
      addChildTextElement('PhoneOrFax', PhoneOrFax);
      addChildTextElement('EMailOrWebsite', EMailOrWebsite);
      addChildTextElement('NationalId', NationalId);
      addChildTextElement('Ticker', Ticker);
      addChildTextElement('Isin', Isin);
      addChildTextElement('State', State);
      addChildTextElement('BvD9', BvD9);
    }
  }


  public class MatchResponseElement extends ResponseElement {
    public MatchResultContainer matchResultContainer = new MatchResultContainer();

    protected override void deserializeXML() {
      deserializeChild(matchResultContainer);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('MatchResponse', NAMESPACE_BVD);
    }
  }

  public class MatchResultContainer extends ResponseElement {
    public List<MatchResult> matchResults = new List<MatchResult>();

    protected override void deserializeXML() {
      deserializeChildren(matchResults, 'MatchResult', MatchResult.class);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('MatchResult', NAMESPACE_BVD);
    }
  }

  public class MatchResult extends ResponseElement {
    public String Hint_x;
    public String BvDID;
    public Double Score;
    public String Name;
    public String NameInLocalAlphabet;
    public String Address;
    public String PostCode;
    public String City;
    public String Country;
    public String Region;
    public String PhoneOrFax;
    public String EMailOrWebsite;
    public String NationalId;
    public String Ticker;
    public String Isin;
    public String State;
    public String Status;
    public String BvD9;
    public String LegalForm;
    public String ConsolidationCode;
    public String CustomRule;

    protected override void deserializeXML() {
      this.Hint_x = getStringValue('Hint', NAMESPACE_BVD);
      this.BvDID = getStringValue('BvDID', NAMESPACE_BVD);
      this.Score = getDoubleValue('Score', NAMESPACE_BVD);
      this.Name = getStringValue('Name', NAMESPACE_BVD);
      this.NameInLocalAlphabet = getStringValue('NameInLocalAlphabet', NAMESPACE_BVD);
      this.Address = getStringValue('Address', NAMESPACE_BVD);
      this.PostCode = getStringValue('PostCode', NAMESPACE_BVD);
      this.City = getStringValue('City', NAMESPACE_BVD);
      this.Country = getStringValue('Country', NAMESPACE_BVD);
      this.Region = getStringValue('Region', NAMESPACE_BVD);
      this.PhoneOrFax = getStringValue('PhoneOrFax', NAMESPACE_BVD);
      this.EMailOrWebsite = getStringValue('EMailOrWebsite', NAMESPACE_BVD);
      this.NationalId = getStringValue('NationalId', NAMESPACE_BVD);
      this.Ticker = getStringValue('Ticker', NAMESPACE_BVD);
      this.Isin = getStringValue('Isin', NAMESPACE_BVD);
      this.State = getStringValue('State', NAMESPACE_BVD);
      this.Status = getStringValue('Status', NAMESPACE_BVD);
      this.BvD9 = getStringValue('BvD9', NAMESPACE_BVD);
      this.LegalForm = getStringValue('LegalForm', NAMESPACE_BVD);
      this.ConsolidationCode = getStringValue('ConsolidationCode', NAMESPACE_BVD);
      this.CustomRule = getStringValue('CustomRule', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return null;
    }
  }


  public class GetAvailableModelsElement extends RequestElement {
    public String sessionHandle;

    protected override void serializeToXML() {
      createThisNode('GetAvailableModels');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('sessionHandle', sessionHandle);
    }
  }

  public class GetAvailableModelsResponseElement extends ResponseElement {
    public String GetAvailableModelsResult;

    protected override void deserializeXML() {
      this.GetAvailableModelsResult = getStringValue('GetAvailableModelsResult', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetAvailableModelsResponse', NAMESPACE_BVD);
    }

  }


  public class GetAvailableFieldsElement extends RequestElement {
    public String sessionHandle;
    public String modelId;

    protected override void serializeToXML() {
      createThisNode('GetAvailableFields');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('sessionHandle', sessionHandle);
      addChildTextElement('modelId', modelId);
    }
  }

  public class GetAvailableFieldsResponseElement extends ResponseElement {
    public String GetAvailableFieldsResult;

    protected override void deserializeXML() {
      this.GetAvailableFieldsResult = getStringValue('GetAvailableFieldsResult', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetAvailableFieldsResponse', NAMESPACE_BVD);
    }
  }

  public class UpdateMyDataTableElement extends RequestElement {
    public String sessionHandle;
    public String table;
    public Boolean appendData;
    public UpdateMyDataTableDataElement data = new UpdateMyDataTableDataElement();

    protected override void serializeToXML() {
      createThisNode('UpdateMyDataTable');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('sessionHandle', sessionHandle);
      addChildTextElement('table', table);
      addChildTextElement('appendData', appendData);
      addChildElement(data);
    }

  }

  public class UpdateMyDataTableDataElement extends RequestElement {
    public List<UpdateMyDataTableDataUploadDataRowElement> uploadDataRows = new List<UpdateMyDataTableDataUploadDataRowElement>();

    protected override void serializeToXML() {
      createThisNode('data');
      for (UpdateMyDataTableDataUploadDataRowElement uploadDataRowElement : uploadDataRows) {
        addChildElement(uploadDataRowElement);
      }
    }
  }

  public class UpdateMyDataTableDataUploadDataRowElement extends RequestElement {
    public String BvDID;
    public UpdateMyDataTableDataUploadDataRowValuesElement values = new UpdateMyDataTableDataUploadDataRowValuesElement();

    public UpdateMyDataTableDataUploadDataRowElement(String BvDID) {
      this.BvDID = BvDID;
    }

    public UpdateMyDataTableDataUploadDataRowElement() {
    }

    protected override void serializeToXML() {
      createThisNode('UploadDataRow');
      addChildTextElement('BvDID', BvDID);
      addChildElement(values);
    }
  }

  public class UpdateMyDataTableDataUploadDataRowValuesElement extends RequestElement {
    public List<UploadValue> uploadValues = new List<UploadValue>();

    protected override void serializeToXML() {
      createThisNode('Values');
      for (UploadValue uploadValue : uploadValues) {
        addChildElement(uploadValue);
      }
    }
  }

  public class UploadValue extends RequestElement {
    public String CustomDataFieldId;
    public String value;

    public UploadValue(String CustomDataFieldId, String value) {
      this.CustomDataFieldId = CustomDataFieldId;
      this.value = value;
    }

    protected override void serializeToXML() {
      createThisNode('UploadValue');
      setAttribute('xsi:type', 'UploadStringValue', null, null);
      addChildTextElement('CustomDataFieldId', CustomDataFieldId);
      addChildTextElement('Value', value);
    }
  }

  public class UpdateMyDataTableResponseElement extends ResponseElement {

    public UpdateMyDataTableResultElement result = new UpdateMyDataTableResultElement();

    protected override void deserializeXML() {
      deserializeChild(result);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('UpdateMyDataTableResponse', NAMESPACE_BVD);
    }
  }

  public class UpdateMyDataTableResultElement extends ResponseElement {

    public Boolean success;
    public Integer IndexationProcessId;
    public UpdateMyDataTableResultErrorsElement errors = new UpdateMyDataTableResultErrorsElement();
    public UpdateMyDataTableResultWarningsElement warnings = new UpdateMyDataTableResultWarningsElement();
    public UpdateMyDataTableResultUnMatchedRecordsElement unMatchedRecords = new UpdateMyDataTableResultUnMatchedRecordsElement();

    protected override void deserializeXML() {
      this.success = getBooleanValue('Success', NAMESPACE_BVD);
      this.IndexationProcessId = getIntegerValue('IndexationProcessId', NAMESPACE_BVD);
      deserializeChild(errors);
      deserializeChild(warnings);
      deserializeChild(unMatchedRecords);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('UpdateMyDataTableResult', NAMESPACE_BVD);
    }
  }

  public class UpdateMyDataTableResultErrorsElement extends ResponseElement {
    public List<StringElement> stringElements = new List<StringElement>();

    protected override void deserializeXML() {
      deserializeChildren(stringElements, 'string', StringElement.class);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Errors', NAMESPACE_BVD);
    }
  }

  public class UpdateMyDataTableResultWarningsElement extends ResponseElement {
    public List<StringElement> stringElements = new List<StringElement>();

    protected override void deserializeXML() {
      deserializeChildren(stringElements, 'string', StringElement.class);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Warnings', NAMESPACE_BVD);
    }
  }

  public class UpdateMyDataTableResultUnMatchedRecordsElement extends ResponseElement {
    public List<StringElement> stringElements = new List<StringElement>();
    protected override void deserializeXML() {
      deserializeChildren(stringElements, 'string', StringElement.class);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Errors', null);
    }
  }

  public class StringElement extends ResponseElement {
    public String stringElement;

    protected override void deserializeXML() {
      this.stringElement = getStringValue();
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return null;
    }
  }

  public class FaultElement extends ResponseElement {
    public String faultcode;
    public String faultstring;

    protected override void deserializeXML() {
      this.faultcode = getStringValue('faultcode', null);
      this.faultstring = getStringValue('faultstring', null);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Fault', NAMESPACE_SOAP);
    }
  }

  public enum FlowType {
    All,
    Shareholders,
    Subsidiaries
  }

  public class GetOwnershipStructureElement extends RequestElement {
    public String sessionHandle;
    public String subjectCompanyBvDID;
    public FlowType flowType;
    public Decimal minimumPercentage;
    public Boolean useIntegratedOwnership;

    protected override void serializeToXML() {
      createThisNode('GetOwnershipStructure');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('sessionHandle', sessionHandle);
      addChildTextElement('subjectCompanyBvDID', subjectCompanyBvDID);
      if (flowType != null) addChildTextElement('flowType', flowType.name());
      addChildTextElement('minimumPercentage', minimumPercentage);
      addChildTextElement('useIntegratedOwnership', useIntegratedOwnership);
    }
  }


  public class GetOwnershipStructureResponse extends ResponseElement {
    public GetOwnershipStructureResult getOwnershipStructureResult = new GetOwnershipStructureResult();

    protected override void deserializeXML() {
      deserializeChild(getOwnershipStructureResult);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetOwnershipStructureResponse', NAMESPACE_BVD);
    }
  }

  public class GetOwnershipStructureResult extends ResponseElement {
    public Nodes nodes = new Nodes();
    public Edges edges = new edges();

    protected override void deserializeXML() {
      deserializeChild(nodes);
      deserializeChild(edges);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetOwnershipStructureResult', NAMESPACE_BVD);
    }
  }

  public class Nodes extends ResponseElement {
    public List<OwnershipGraphNode> ownershipGraphNodes = new List<OwnershipGraphNode>();

    protected override void deserializeXML() {
      deserializeChildren(ownershipGraphNodes, 'OwnershipGraphNode', OwnershipGraphNode.class);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Nodes', NAMESPACE_BVD);
    }
  }

  public class OwnershipGraphNode extends ResponseElement {
    public String BvDID;
    public String Name;
    public String EntityType;
    public String CountryISO2Code;
    public String City;
    public String BvDIndependenceIndicator;
    public Boolean IsInWorldCompliance;
    public Integer Distance;
    public Double IntegratedPercentage;
    public Boolean IsIntegratedPercentageEstimation;

    protected override void deserializeXML() {
      this.BvDID = getStringValue('BvDID', NAMESPACE_BVD);
      this.Name = getStringValue('Name', NAMESPACE_BVD);
      this.EntityType = getStringValue('EntityType', NAMESPACE_BVD);
      this.CountryISO2Code = getStringValue('CountryISO2Code', NAMESPACE_BVD);
      this.City = getStringValue('City', NAMESPACE_BVD);
      this.BvDIndependenceIndicator = getStringValue('BvDIndependenceIndicator', NAMESPACE_BVD);
      this.IsInWorldCompliance = getBooleanValue('IsInWorldCompliance', NAMESPACE_BVD);
      this.Distance = getIntegerValue('Distance', NAMESPACE_BVD);
      this.IntegratedPercentage = getDoubleValue('IntegratedPercentage', NAMESPACE_BVD);
      this.IsIntegratedPercentageEstimation = getBooleanValue('IsIntegratedPercentageEstimation', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return null;
    }
  }

  public class Edges extends ResponseElement {
    public List<OwnershipGraphEdge> ownershipGraphEdges = new List<OwnershipGraphEdge>();

    protected override void deserializeXML() {
      deserializeChildren(ownershipGraphEdges, 'OwnershipGraphEdge', OwnershipGraphEdge.class);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('Edges', NAMESPACE_BVD);
    }
  }

  public class OwnershipGraphEdge extends ResponseElement {
    public String ParentBvDID;
    public String ChildBvDID;
    public String Source;
    public String Date_x;
    public String DirectPercentage;
    public String TotalPercentage;

    protected override void deserializeXML() {
      this.ParentBvDID = getStringValue('ParentBvDID', NAMESPACE_BVD);
      this.ChildBvDID = getStringValue('ChildBvDID', NAMESPACE_BVD);
      this.Source = getStringValue('Source', NAMESPACE_BVD);
      this.Date_x = getStringValue('Date', NAMESPACE_BVD);
      this.DirectPercentage = getStringValue('DirectPercentage', NAMESPACE_BVD);
      this.TotalPercentage = getStringValue('TotalPercentage', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return null;
    }
  }


  public class GetDataRequestElement extends RequestElement {
    public String sessionHandle;
    public String query;
    public Integer fromRecord;
    public Integer nrRecords;
    public GetDataSelection selection = new GetDataSelection();
    public GetDataResultFormat resultFormat = GetDataResultFormat.XML;

    protected override void serializeToXML() {
      createThisNode('GetData');
      setNamespace('', NAMESPACE_BVD);

      addChildTextElement('sessionHandle', sessionHandle);
      addChildElement(selection);
      addChildTextElement('query', query);
      addChildTextElement('fromRecord', fromRecord);
      addChildTextElement('nrRecords', nrRecords);
      if (resultFormat != null) {
        addChildTextElement('resultFormat', resultFormat.name());
      } else {
        addChildTextElement('resultFormat', GetDataResultFormat.CSV.name());
      }
    }
  }

  public class GetDataSelection extends RequestElement {
    public String token;
    public Integer selectionCount;

    protected override void serializeToXML() {
      createThisNode('selection');
      addChildTextElement('Token', token);
      addChildTextElement('SelectionCount', selectionCount);
    }
  }

  public enum GetDataResultFormat {
    XML,
    XMLDynamicTags,
    XBRL,
    HTML,
    CSV,
    MSDataSet,
    Custom,
    QualifiedXML,
    XML_UTF8,
    MSDataSet_2
  }

  public class GetDataResponseElement extends ResponseElement {
    public QueryResultsElement queryResults = new QueryResultsElement();

    protected override void deserializeXML() {
      String getDataResultXml = getStringValue('GetDataResult', NAMESPACE_BVD).unescapeXml();

      Dom.Document result = new Dom.Document();
      result.load(getDataResultXml);
      queryResults.deserializeXML(result);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetDataResponse', NAMESPACE_BVD);
    }
  }

  public class QueryResultsElement extends XmlNode.XmlRootNode {
    public List<RecordElement> records = new List<RecordElement>();

    protected override void serializeToXML() {
    }

    protected override void deserializeXML() {
      deserializeChildren(records, 'record', RecordElement.class);
    }
  }

  public class RecordElement extends ResponseElement {
    public List<FieldElement> fields = new List<FieldElement>();
    public String id;
    public String dataModelId;
    public String selectionId;
    public Map<String, FieldElement> fieldmap;

    protected override void deserializeXML() {
      this.deserializeChildren(fields, 'item', FieldElement.class);
      this.id = getStringAttribute('id', null);
      this.dataModelId = getStringAttribute('dataModelId', null);
      this.selectionId = getStringAttribute('selectionId', null);
      this.fieldmap = getFieldMap();
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
  //      return parentNode.getChildElement('record', NAMESPACE_DATARESULT);
      return null;
    }

    public Map<String, FieldElement> getFieldMap() {
      Map<String, FieldElement> fieldmap = new Map<String, FieldElement>();
      for (FieldElement fe : fields) {
        fieldmap.put(fe.field, fe);
      }
      return fieldmap;
    }

    public FieldElement getField(String name) {
      if (fieldmap.containsKey(name)) {
        return fieldmap.get(name);
      } else {
        return new FieldElement();
      }
    }
  }

  public static Set<String> ORBIS_BOOLEAN_TRUE_VALUES = new Set<String>{
    'Yes', 'Ja', 'Oui', 'Ja', 'Si', 'Si', 'Sim', 'Да', 'はい', '是', '是'
  };

  public class SubFieldElement extends ResponseElement {
   public String index;
   public String resultType = 'NotAvailable';
   public String value;

   protected override void deserializeXML() {
     this.resultType = getStringAttribute('resultType', null);
     this.value = getStringValue();
     this.index = getStringAttribute('index', null);
   }

   protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
     return null;
   }
  }

  public class FieldElement extends ResponseElement {
    public List<SubFieldElement> subFields = new List<SubFieldElement>();
    public String alias;
    public String dimSelType;
    public String occurs;
    public String dim;
    public String index;
    public String field;
    public String fieldType;
    public String valueType;
    public String resultType = 'NotAvailable';
    public String value;
    public String format;

    protected override void deserializeXML() {
      this.deserializeChildren(subFields, 'childItem', SubFieldElement.class);

      if(subFields.isEmpty()) {
        this.value = getStringValue();
      }

      if(subFields.size() == 1 && String.isBlank(subFields[0].value)) {
        subFields.clear();
      }

      this.field = getStringAttribute('field', null);
      this.fieldType = getStringAttribute('fieldType', null);
      this.valueType = getStringAttribute('valueType', null);
      this.resultType = getStringAttribute('resultType', null);
      this.format = getStringAttribute('format', null);
      this.alias = getStringAttribute('alias', null);
      this.dimSelType = getStringAttribute('dimSelType', null);
      this.occurs = getStringAttribute('occurs', null);
      this.dim = getStringAttribute('dim', null);
      this.index = getStringAttribute('index', null);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return null;
    }

    public List<SubFieldElement> getChildren() {
      return subFields;
    }

    public Object getAs(DisplayType fieldType) {
      try {
        if (fieldType == DisplayType.DOUBLE) return getDouble();
        if (fieldType == DisplayType.INTEGER) return getInteger();
        if (fieldType == DisplayType.DATE) return getDate();
        if (fieldType == DisplayType.DATETIME) return getDatetime();
        if (fieldType == DisplayType.PERCENT) return getDecimal();
        if (fieldType == DisplayType.CURRENCY) return getDecimal();
        if (fieldType == DisplayType.BOOLEAN) return getBoolean();
        return getString();
      } catch (Exception ex) {
        return getString();
      }
    }

    public Double getDouble() {
      return Double.valueOf(value);
    }

    public Decimal getDecimal() {
      return Decimal.valueOf(value);
    }

    public Integer getInteger() {
      return Integer.valueOf(value);
    }

    public Boolean getBoolean() {
      return ORBIS_BOOLEAN_TRUE_VALUES.contains(value);
    }

    public String getString() {
      return value;
    }

    public Date getDate() {
      String datetimeString = value.replace('/', '-');
      if (format == 'YYYY/MM') {
        return Date.valueOf(datetimeString + '-01');
      } else if (format == 'YYYY') {
        return Date.valueOf(datetimeString + '-01-01');
      } else {
        return Date.valueOf(datetimeString);
      }
    }

    public Datetime getDatetime() {
      return Datetime.newInstance(getDate(), Time.newInstance(0, 0, 0, 0));
    }
  }

  public class FindByBVDIdRequestElement extends RequestElement {
    public String sessionHandle;
    public String id;

    protected override void serializeToXML() {
      createThisNode('FindByBVDId');
      setNamespace('', NAMESPACE_BVD);

      addChildTextElement('sessionHandle', sessionHandle);
      addChildTextElement('id', id);
    }
  }

  public class FindByBVDIdResponseElement extends ResponseElement {
    public FindByBVDIdResultElement FindByBVDIdResult = new FindByBVDIdResultElement();

    protected override void deserializeXML() {
      deserializeChild(FindByBVDIdResult);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('FindByBVDIdResponse', NAMESPACE_BVD);
    }
  }

  public class FindByBVDIdResultElement extends ResponseElement {
    public String token;
    public Integer selectionCount;

    protected override void deserializeXML() {
      this.token = getStringValue('Token', NAMESPACE_BVD);
      this.selectionCount = getIntegerValue('SelectionCount', NAMESPACE_BVD);
    }

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('FindByBVDIdResult', NAMESPACE_BVD);
    }
  }

  public class Datasource {
    public String Id;
    public String Label;
    public String WebServiceUrl;
    public String WebsiteUrl;
  }


  public class GetLabelsFromModelRequestElement extends RequestElement {
    public String sessionHandle;
    public GetLabelsFromModelFieldDefsElement fieldDefs = new GetLabelsFromModelFieldDefsElement();
    public Boolean fullLabels;

    protected override void serializeToXML() {
      createThisNode('GetLabelsFromModel');
      setNamespace('', NAMESPACE_BVD);

      addChildTextElement('sessionHandle', sessionHandle);
      addChildElement(fieldDefs);
      addChildTextElement('fullLabels', fullLabels);
    }
  }

  public class GetLabelsFromModelFieldDefsElement extends RequestElement {
    public List<GetLabelsFromModelFieldDefinitionElement> fieldDefs = new List<OrbisSchema.GetLabelsFromModelFieldDefinitionElement>();

    protected override void serializeToXML() {
      createThisNode('fieldDefs');
      setNamespace('', NAMESPACE_BVD);
      addChildrenElements(fieldDefs);
    }
  }

  public class GetLabelsFromModelFieldDefinitionElement extends RequestElement {
    public String FieldCode;
    public String PresentationLineId;
    public String ModelId;

    protected override void serializeToXML() {
      createThisNode('FieldDefinition');
      setNamespace('', NAMESPACE_BVD);
      addChildTextElement('FieldCode', FieldCode);
      addChildTextElement('PresentationLineId', PresentationLineId);
      addChildTextElement('ModelId', ModelId);
    }
  }

  public class GetLabelsFromModelResponseElement extends ResponseElement {
    public GetLabelsFromModelResultElement result = new GetLabelsFromModelResultElement();

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetLabelsFromModelResponse', NAMESPACE_BVD);
    }

    protected override void deserializeXML() {
      deserializeChild(result);
    }
  }

  public class GetLabelsFromModelResultElement extends ResponseElement {
    public List<String> results = new List<String>();

    protected override Dom.XmlNode locateInParent(Dom.XmlNode parentNode) {
      return parentNode.getChildElement('GetLabelsFromModelResult', NAMESPACE_BVD);
    }

    protected override void deserializeXML() {
      for (Dom.XmlNode node : this.getChildElements()) {
        results.add(node.getText());
      }
    }
  }
}