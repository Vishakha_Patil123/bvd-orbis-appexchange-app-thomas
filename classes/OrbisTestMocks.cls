/**
* Mock Dispatcher for Orbis Callouts.
* This class should be used in Unit Tests to setup mock, it internally
* recognizes which mock should be used by Request's Soap Action header
* and dispatches handling to that mock subclass.
* */
public class OrbisTestMocks implements HttpCalloutMock {
  private OrbisMockFactory factory;

  public OrbisTestMocks() {
    this.factory = new OrbisMockFactory();
  }

  public OrbisTestMocks(OrbisMockFactory factory) {
    this.factory = factory;
  }

  public HttpResponse respond(HttpRequest request) {
    OrbisMock mock = factory.getMock(request);
    return mock.respond(request);
  }


  public virtual class OrbisMockFactory {
    public OrbisMock getMock(HttpRequest request) {
      /*SOAP Callouts*/
      String soapAction = request.getHeader('SOAPAction');
      if (soapAction == 'http://bvdep.com/webservices/Open') return getOpenMock();
      if (soapAction == 'http://bvdep.com/webservices/GetAvailableFields') return getGetAvailableFieldsMock();
      if (soapAction == 'http://bvdep.com/webservices/GetAvailableModels') return getGetAvailableModelsMock();
      if (soapAction == 'http://bvdep.com/webservices/Match') return getMatchMock();
      if (soapAction == 'http://bvdep.com/webservices/GetOwnershipStructure') return getGetOwnershipStructureMock();
      if (soapAction == 'http://bvdep.com/webservices/FindByBVDId') return getFindByBvDIdMock();
      if (soapAction == 'http://bvdep.com/webservices/GetData') return getGetDataMock();
      if (soapAction == 'http://bvdep.com/webservices/UpdateMyDataTable') return getUpdateMyDataTableMock();
      if (soapAction == 'http://bvdep.com/webservices/GetLabelsFromModel') return getGetLabelsFromModelMock();

      /*REST Callouts*/
      String endpoint = request.getEndpoint();
      if (endpoint.endsWith('/session/orbis')) return getOpenRestMock();
      if (endpoint.endsWith('/products')) return getGetAvailableDatasourcesResponseMock();

      throw new OrbisTestMocks.OrbisMockException('Unsupported Mock type: ' + soapAction);
    }

    protected virtual OrbisMock getOpenMock() {
      return new OrbisTestMocks.OrbisOpenMock();
    }
    protected virtual OrbisMock getOpenRestMock() {
      return new OrbisTestMocks.OrbisOpenRestMock();
    }
    protected virtual OrbisMock getGetAvailableFieldsMock() {
      return new OrbisTestMocks.OrbisGetAvailableFieldsMock();
    }
    protected virtual OrbisMock getGetAvailableModelsMock() {
      return new OrbisTestMocks.OrbisGetAvailableModelsMock();
    }
    protected virtual OrbisMock getMatchMock() {
      return new OrbisTestMocks.OrbisMatchMock();
    }
    protected virtual OrbisMock getGetOwnershipStructureMock() {
      return new OrbisTestMocks.OrbisGetOwnershipStructureMock();
    }
    protected virtual OrbisMock getFindByBvDIdMock() {
      return new OrbisTestMocks.OrbisFindByBvDIdMock();
    }
    protected virtual OrbisMock getGetDataMock() {
      return new OrbisTestMocks.OrbisGetDataMock();
    }
    protected virtual OrbisMock getGetAvailableDatasourcesResponseMock() {
      return new OrbisTestMocks.OrbisGetAvailableDatasourcesResponseMock();
    }
    protected virtual OrbisMock getUpdateMyDataTableMock() {
      return new OrbisTestMocks.OrbisGetUpdateMyDataTableMock();
    }
    protected virtual OrbisMock getGetLabelsFromModelMock() {
      return new OrbisTestMocks.OrbisGetLabelsFromModelMock();
    }
  }
  public class OrbisMockException extends Exception {
  }


  public abstract class OrbisMock implements HttpCalloutMock {
    public HttpResponse respond(HttpRequest request) {
      HttpResponse response = new HttpResponse();
      response.setHeader('Content-Type', getContentType());
      response.setBody(getBody());
      response.setStatusCode(getStatusCode());
      response.setStatus(getStatus());

      return response;
    }

    protected abstract String getBody();
    protected virtual Integer getStatusCode() {
      return 200;
    }
    protected virtual String getStatus() {
      return 'OK';
    }
    protected virtual String getContentType() {
      return 'text/xml; charset=utf-8';
    }
  }

  public class OrbisOpenMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope ' +
        'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
        '<soap:Body>' +
        '<OpenResponse xmlns="http://bvdep.com/webservices/">' +
        '<OpenResult>3BCSECYSC82VHUU</OpenResult>' +
        '</OpenResponse>' +
        '</soap:Body>' +
        '</soap:Envelope>';
    }
  }

  public class OrbisOpenRestMock extends OrbisMock {
    protected override String getBody() {
      return String.join(new String[]{
        '{',
        '"ProductId": "orbis",',
        '"Error": null,',
        '"Session": "3BCSECYSC82VHUU"',
        '}'
      }, '\n');
    }
    protected override String getContentType() {
      return 'application/json; charset=utf-8';
    }
  }

  public class OrbisFaultMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?> ' +
        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
        '<soap:Body> ' +
        '<soap:Fault> ' +
        '<faultcode>soap:Server</faultcode> ' +
        '<faultstring>' +
        'System.Web.Services.Protocols.SoapException: Server was unable to process request.' +
        ' ---&gt; System.InvalidOperationException: Invalid handle 2D5ZECYSCEOW38J1 at bvd.ApplicationRoot.Web.Session.SessionHandle' +
        '..ctor(String strHandle) ' +
        'at ProxyRemoteAccess.Stat..ctor(String methodName, String sessionID, String princParamID, ' +
        'String secParamID, DateTime dateOfRequest, Boolean timer) ' +
        'in Q:\\QuickFixes\\Releases\\Net\\webservices.bvdep.com\\RootVersion\\web\\StatsCollector.cs:line 60 ' +
        'at ProxyRemoteAccess.RemoteAccess.add2Stats(String session, String method, String princParam, ' +
        'String secParam) in Q:\\QuickFixes\\Releases\\Net\\webservices.bvdep.com\\RootVersion\\web\\RemoteAc' +
        'cess.asmx.cs:line 97 at ProxyRemoteAccess.RemoteAccess.GetAvailableModels(String sessionHandle) ' +
        'in Q:\\QuickFixes\\Releases\\Net\\webservices.bvdep.com\\RootVersion\\web\\RemoteAccess.asmx.cs:lin' +
        'e 453 --- End of inner exception stack trace ---' +
        '</faultstring> ' +
        '<detail /> ' +
        '</soap:Fault> ' +
        '</soap:Body> ' +
        '</soap:Envelope>';
    }
    protected override Integer getStatusCode() {
      return 500;
    }
    protected override String getStatus() {
      return 'Internal Server Error';
    }
  }

  public class OrbisGetAvailableModelsMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope ' +
        'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema"> ' +
        '<soap:Body> ' +
        '<GetAvailableModelsResponse ' +
        'xmlns="http://bvdep.com/webservices/"> ' +
        '<GetAvailableModelsResult>&lt;Models&gt;&lt;Model id="A" name="All templates" /&gt;&lt;Model id="C" name="Industrial companies" parentModelId="A" /&gt;' +
        '&lt;Model id="B" name="Banks" parentModelId="A" /&gt;&lt;Model id="I" name="Insurance companies" parentModelId="A" /&gt;' +
        '&lt;Model id="P" name="Insurance companies (composite)" parentModelId="I" /&gt;&lt;Model id="L" name="Insurance companies (life)" parentModelId="I" /&gt;' +
        '&lt;Model id="N" name="Insurance companies (non life)" parentModelId="I" /&gt;&lt;Model id="CWVB" name="Industrial WVB" parentModelId="C" /&gt;' +
        '&lt;Model id="CMULTEX" name="Industrial Multex" parentModelId="C" /&gt;&lt;Model id="IND_IT" name="Italy" parentModelId="C" /&gt;' +
        '&lt;Model id="IND_IT_ICS" name="Companies" parentModelId="IND_IT" /&gt;&lt;Model id="IND_IT_FIN" name="Financial intermediaries" parentModelId="IND_IT" /&gt;' +
        '&lt;Model id="IND_IT_SIM" name="Stockbrokers" parentModelId="IND_IT" /&gt;&lt;Model id="IND_IT_BNK" name="Banks" parentModelId="IND_IT" /&gt;' +
        '&lt;Model id="IND_IT_INS" name="Insurances" parentModelId="IND_IT" /&gt;&lt;/Models&gt;' +
        '</GetAvailableModelsResult> ' +
        '</GetAvailableModelsResponse> ' +
        '</soap:Body> ' +
        '</soap:Envelope>';
    }
  }

  public class OrbisGetAvailableFieldsMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope ' +
        'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><GetAvailableFieldsResponse ' +
        'xmlns="http://bvdep.com/webservices/">' +
        '<GetAvailableFieldsResult>' +
        '&lt;Fields&gt;' +
        '&lt;Field code="1001" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1002" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1039" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1011" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1003" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1004" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1005" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;Field code="1006" type="MoneyValue"&gt;' +
        '&lt;DataDimensions default="NrOfYears"&gt;' +
        '&lt;DataDimension name="NrOfYears" type="Yearly" /&gt;' +
        '&lt;/DataDimensions&gt;' +
        '&lt;/Field&gt;' +
        '&lt;/Fields&gt;' +
        '</GetAvailableFieldsResult>' +
        '</GetAvailableFieldsResponse>' +
        '</soap:Body>' +
        '</soap:Envelope>';
    }
  }

  public class OrbisMatchMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope ' +
        'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
        '<soap:Body>' +
        '<MatchResponse xmlns="http://bvdep.com/webservices/">' +
        '<MatchResult>' +
        '<MatchResult>' +
        '<BvDID>PT501234567</BvDID>' +
        '<Score>1</Score>' +
        '<Hint>PotentialCandidate</Hint>' +
        '<Name>Test 1</Name>' +
        '<Address>RUA D DO AEROPORTO DE LISBOA, EDIFICIO 120</Address>' +
        '<PostCode>1700-008</PostCode>' +
        '<City>LISBOA</City>' +
        '<Country>PT</Country>' +
        '<Region>Lisboa | Lisboa</Region>' +
        '<PhoneOrFax>+351 123456789</PhoneOrFax>' +
        '<EMailOrWebsite>www.test.pt</EMailOrWebsite>' +
        '<NationalId>123456789</NationalId>' +
        '<Ticker>Unlisted</Ticker>' +
        '<Status>Active</Status>' +
        '<LegalForm>Public limited company - SA</LegalForm>' +
        '<ConsolidationCode>U2</ConsolidationCode>' +
        '</MatchResult>' +
        '<MatchResult>' +
        '<BvDID>GBML1234567</BvDID>' +
        '<Score>0.5</Score>' +
        '<Hint>PotentialCandidate</Hint>' +
        '<Name>A N A</Name>' +
        '<Address>TestE HOUSE 200 MOCK ROAD</Address>' +
        '<PostCode>W6 7NL</PostCode>' +
        '<City>LONDON</City>' +
        '<Country>GB</Country>' +
        '<PhoneOrFax>+44 20 1674 2424</PhoneOrFax>' +
        '<EMailOrWebsite>www.test2.com</EMailOrWebsite>' +
        '<Ticker>Unlisted</Ticker>' +
        '<Status>Active</Status>' +
        '<LegalForm>Unincorporated company</LegalForm>' +
        '<ConsolidationCode>LF</ConsolidationCode>' +
        '</MatchResult>' +
        '</MatchResult>' +
        '</MatchResponse>' +
        '</soap:Body>' +
        '</soap:Envelope>';
    }
  }

  public class OrbisMatchNoResultsMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope ' +
        'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema">' +
        '<soap:Body>' +
        '<MatchResponse xmlns="http://bvdep.com/webservices/">' +
        '<MatchResult />' +
        '</MatchResponse>' +
        '</soap:Body>' +
        '</soap:Envelope>';
    }
  }

  public class OrbisGetOwnershipStructureMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>' +
        '<soap:Envelope ' +
        'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' +
        'xmlns:xsd="http://www.w3.org/2001/XMLSchema" ' +
        'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
        '<soap:Body>' +
        '<GetOwnershipStructureResponse ' +
        'xmlns="http://bvdep.com/webservices/">' +
        '<GetOwnershipStructureResult>' +
        '<Nodes>' +

        '<OwnershipGraphNode>' +
        '<BvDID>BVD_01</BvDID>' +
        '<Name>Name1</Name>' +
        '<EntityType>EntityType</EntityType>' +
        '<CountryISO2Code>USD</CountryISO2Code>' +
        '<City>London</City>' +
        '<BvDIndependenceIndicator>BvDIndependenceIndicator</BvDIndependenceIndicator>' +
        '<IsInWorldCompliance>true</IsInWorldCompliance>' +
        '<Distance>197</Distance>' +
        '<IntegratedPercentage>15</IntegratedPercentage>' +
        '<IsIntegratedPercentageEstimation>false</IsIntegratedPercentageEstimation>' +
        '</OwnershipGraphNode>' +

        '<OwnershipGraphNode>' +
        '<BvDID>BVD_02</BvDID>' +
        '<Name>Name2</Name>' +
        '<EntityType>EntityType2</EntityType>' +
        '<CountryISO2Code>GBP</CountryISO2Code>' +
        '<City>London</City>' +
        '<BvDIndependenceIndicator>BvDIndependenceIndicator2</BvDIndependenceIndicator>' +
        '<IsInWorldCompliance>false</IsInWorldCompliance>' +
        '<Distance>156</Distance>' +
        '<IntegratedPercentage>60</IntegratedPercentage>' +
        '<IsIntegratedPercentageEstimation>false</IsIntegratedPercentageEstimation>' +
        '</OwnershipGraphNode>' +

        '</Nodes>' +
        '<Edges>' +
        '<OwnershipGraphEdge>' +
        '<ParentBvDID>BVD_01</ParentBvDID>' +
        '<ChildBvDID>BVD_02</ChildBvDID>' +
        '<Source>Source1</Source>' +
        '<Date>Date1</Date>' +
        '<DirectPercentage>DirectPercentage1</DirectPercentage>' +
        '<TotalPercentage>TotalPercentage1</TotalPercentage>' +
        '</OwnershipGraphEdge>' +

        '<OwnershipGraphEdge>' +
        '<ParentBvDID>BVD_02</ParentBvDID>' +
        '<ChildBvDID>BVD_01</ChildBvDID>' +
        '<Source>Source2</Source>' +
        '<Date>Date2</Date>' +
        '<DirectPercentage>DirectPercentage2</DirectPercentage>' +
        '<TotalPercentage>TotalPercentage2</TotalPercentage>' +
        '</OwnershipGraphEdge>' +
        '</Edges>' +

        '</GetOwnershipStructureResult>' +
        '</GetOwnershipStructureResponse>' +
        '</soap:Body>' +
        '</soap:Envelope>';
    }
  }

  public class OrbisFindByBvDIdMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>'
        + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
        + '    <soap:Body>'
        + '        <FindByBVDIdResponse xmlns="http://bvdep.com/webservices/">'
        + '            <FindByBVDIdResult>'
        + '                <Token>2</Token>'
        + '                <SelectionCount>1</SelectionCount>'
        + '            </FindByBVDIdResult>'
        + '        </FindByBVDIdResponse>'
        + '    </soap:Body>'
        + '</soap:Envelope>';
    }
  }

  public class OrbisGetDataMock extends OrbisMock {
    protected override String getBody() {
      String xmlString = '<?xml version="1.0" encoding="utf-8"?>'
        + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
        + '<soap:Body>'
        + '<GetDataResponse xmlns="http://bvdep.com/webservices/">'
        + '<GetDataResult>&lt;?xml version="1.0" encoding="utf-16"?&gt;'
        + '&lt;queryResults version="1.0" xmlns="http://www.bvdep.com/schemas/RemoteAccessDataResults.xsd"&gt;';

      for (Integer i = 0; i < 50; i++) {
        xmlString += '&lt;record id="TEST1_U" dataModelId="C" selectionId="TEST#'+i+'"&gt;'
          + '&lt;item field="BVD_ID_NUMBER" valueType="AccountValue" resultType="String" fieldType="String"&gt;TEST#'+i+'&lt;/item&gt;'
          + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;JOHN DOES&lt;/item&gt;'
          + '&lt;item field="ADDR" valueType="AccountValue" resultType="String" fieldType="String"&gt;STREET 22A LOK. 42&lt;/item&gt;'
          + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" fieldType="String"&gt;&lt;/item&gt;'
          + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;02-672&lt;/item&gt;'
          + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;WARSZAWA&lt;/item&gt;'
          + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;Poland&lt;/item&gt;'
          + '&lt;item field="CLOSDATE" valueType="AccountValue" resultType="DateTime" fieldType="Date"&gt;2015/12/31&lt;/item&gt;'
          + '&lt;item field="ADDRESS_UPDATE" valueType="AccountValue" resultType="DateTime" fieldType="Date" format="YYYY/MM"&gt;2017/03&lt;/item&gt;'
          + '&lt;item field="YEAR_LAST_ACCOUNTS" valueType="AccountValue" resultType="DateTime" fieldType="Date" format="YYYY"&gt;2016&lt;/item&gt;'
          + '&lt;/record&gt;';
      }
      xmlString += '&lt;/queryResults&gt;</GetDataResult>'
        + '</GetDataResponse>'
        + '</soap:Body>'
        + '</soap:Envelope>';

      return xmlString;
    }
  }

  public class OrbisOutdatedIdGetDataMock extends OrbisMock {
    protected override String getBody() {
      String xmlString = '<?xml version="1.0" encoding="utf-8"?>'
        + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
        + '<soap:Body>'
        + '<GetDataResponse xmlns="http://bvdep.com/webservices/">'
        + '<GetDataResult>&lt;?xml version="1.0" encoding="utf-16"?&gt;'
        + '&lt;queryResults version="1.0" xmlns="http://www.bvdep.com/schemas/RemoteAccessDataResults.xsd"&gt;';

      for (Integer i = 0; i < 50; i++) {
        xmlString += '&lt;record id="TEST1_U" dataModelId="C" selectionId="TEST#'+i+'"&gt;'
          + '&lt;item field="BVD_ID_NUMBER" valueType="AccountValue" resultType="String" fieldType="String"&gt;TEST_NEW#'+i+'&lt;/item&gt;'
          + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;JOHN DOES&lt;/item&gt;'
          + '&lt;item field="ADDR" valueType="AccountValue" resultType="String" fieldType="String"&gt;STREET 22A LOK. 42&lt;/item&gt;'
          + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" fieldType="String"&gt;&lt;/item&gt;'
          + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;02-672&lt;/item&gt;'
          + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;WARSZAWA&lt;/item&gt;'
          + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;Poland&lt;/item&gt;'
          + '&lt;item field="CLOSDATE" valueType="AccountValue" resultType="DateTime" fieldType="Date"&gt;2015/12/31&lt;/item&gt;'
          + '&lt;item field="ADDRESS_UPDATE" valueType="AccountValue" resultType="DateTime" fieldType="Date" format="YYYY/MM"&gt;2017/03&lt;/item&gt;'
          + '&lt;item field="YEAR_LAST_ACCOUNTS" valueType="AccountValue" resultType="DateTime" fieldType="Date" format="YYYY"&gt;2016&lt;/item&gt;'
          + '&lt;/record&gt;';
      }
      xmlString += '&lt;/queryResults&gt;</GetDataResult>'
        + '</GetDataResponse>'
        + '</soap:Body>'
        + '</soap:Envelope>';

      return xmlString;
    }
  }

  public class OrbisGetAvailableDatasourcesResponseMock extends OrbisMock {
    protected override String getBody() {
      return '{'
        + '    "Error": null,'
        + '    "Products": ['
        + '        {'
        + '            "Id": "Orbis",'
        + '            "Label": "Orbis",'
        + '            "WebServiceUrl": "https://webservices.bvdep.com/orbis4/remoteaccess.asmx",'
        + '            "WebsiteUrl": "https://orbis4.bvdinfo.com"'
        + '        }'
        + '    ]'
        + '}';
    }
    protected override String getContentType() {
      return 'application/json; charset=utf-8';
    }
  }

  public class OrbisGetUpdateMyDataTableMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>'
        + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
        + '<soap:Body>'
        + '<UpdateMyDataTableResponse xmlns="http://bvdep.com/webservices/">'
        + '<UpdateMyDataTableResult>'
        + '<Success>false</Success>'
        + '<IndexationProcessId>-1</IndexationProcessId>'
        + '<Errors>'
        + '<string>No valid custom field in the file</string>'
        + '<string>No valid custom field in the file</string>'
        + '</Errors>'
        + '<Warnings>'
        + '<string>Warning line 1, column \'CF00001ss\': The data is skipped because no matching custom field could be found in table \'CRMLinks\'.</string>'
        + '<string>Warning line 1, column \'CF00002sss\': The data is skipped because no matching custom field could be found in table \'CRMLinks\'.</string>'
        + '</Warnings>'
        + '<UnMatchedRecords />'
        + '</UpdateMyDataTableResult>'
        + '</UpdateMyDataTableResponse>'
        + '</soap:Body>'
        + '</soap:Envelope>';
    }
  }

  public class OrbisGetLabelsFromModelMock extends OrbisMock {
    protected override String getBody() {
      return '<?xml version="1.0" encoding="utf-8"?>'
        + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
        + '<soap:Body>'
        + '<GetLabelsFromModelResponse xmlns="http://bvdep.com/webservices/">'
        + '<GetLabelsFromModelResult>'
        + '<string>Company name</string>'
        + '<string>Addition of company in database</string>'
        + '</GetLabelsFromModelResult>'
        + '</GetLabelsFromModelResponse>'
        + '</soap:Body>'
        + '</soap:Envelope>';
    }
  }
}