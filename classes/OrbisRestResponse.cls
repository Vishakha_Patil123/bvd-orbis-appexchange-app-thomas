/**
* Rest variant of Orbis Response
* */
public virtual with sharing class OrbisRestResponse extends OrbisResponse {
  private String ProductId;
  private String Error;

  public static OrbisResponse getInstance(HttpResponse response, Type type) {
    return (OrbisResponse) JSON.deserialize(response.getBody(), type);
  }

  public String getProductId() {
    return this.ProductId;
  }

  public String getError() {
    return this.Error;
  }

  public Boolean hasError(){
    return String.isNotBlank(getError());
  }

  protected override OrbisSchema.ResponseElement getResponseSchema() {
    return null;
  }
}