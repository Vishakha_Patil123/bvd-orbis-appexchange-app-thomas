/**
* Selector Layer for BvD Jobs sObject.
* Checks user permission to access field and objects and returns query results
* */
public with sharing class BvDJobsSelector {

  public static List<BvD_Job__c> getAllJobsWithWorkItems() {
    CommonUtilities.checkReadAndThrowAura(BvD_Job__c.getSObjectType(), new List<String>{
      'Name', 'Object__c', 'Type__c', 'Processing__c', 'Execution_Time__c', 'Status__c', 'Failure_Reason__c'
    });
    CommonUtilities.checkReadAndThrowAura(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'ID', 'Status__c'
    });


    return [
      SELECT ID,
        Name,
        Object__c,
        Type__c,
        Processing__c,
        Execution_Time__c,
        Status__c,
        Failure_Reason__c,

      (
        SELECT
          ID,
          Status__c
        FROM BvD_Job_Work_Items__r
      )
      FROM BvD_Job__c
      WHERE Processing__c != 'Dry Run'
      ORDER BY CreatedDate DESC
    ];
  }

  public static BvD_Job__c getJobById(Id jobId) {
    CommonUtilities.checkReadAndThrowAura(BvD_Job__c.getSObjectType(), new List<String>{
      'Name', 'Object__c', 'Type__c', 'Processing__c', 'Execution_Time__c', 'Status__c', 'Failure_Reason__c'
    });

    return [
      SELECT ID,
        Status__c,
        Object__c,
        Processing__c,
        Failure_Reason__c,
        Execution_Time__c,
        Type__c
      FROM BvD_Job__c
      WHERE ID = :jobId
    ];
  }

  public static BvD_Job__c getJobWithWorkItemsById(Id jobId) {
    CommonUtilities.checkReadAndThrowAura(BvD_Job__c.getSObjectType(), new List<String>{
      'Name', 'Object__c', 'Type__c', 'Processing__c', 'Execution_Time__c', 'Status__c', 'Failure_Reason__c'
    });
    CommonUtilities.checkReadAndThrowAura(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'ID', 'Status__c', 'Failure_Reason__c', 'RecordId__c', 'Record_Name__c', 'BvD_Id__c', 'BvD_Sub_Id__c', 'Error_Details__c'
    });

    return [
      SELECT ID,
        Name,
        Status__c,
        Object__c,
        Processing__c,
        Failure_Reason__c,
        Execution_Time__c,
        Type__c, (
        SELECT
          ID,
          Status__c,
          Failure_Reason__c,
          RecordId__c,
          Record_Name__c,
          BvD_Id__c,
          BvD_Sub_Id__c,
          Error_Details__c
        FROM BvD_Job_Work_Items__r
      )
      FROM BvD_Job__c
      WHERE ID = :jobId
    ];
  }

  public static Map<Id, BvD_Job__c> getJobsWithWorkItemsById(Set<String> jobIds) {
    CommonUtilities.checkReadAndThrowAura(BvD_Job__c.getSObjectType(), new List<String>{
      'Name', 'Object__c', 'Type__c', 'Processing__c', 'Execution_Time__c', 'Status__c', 'Failure_Reason__c'
    });
    CommonUtilities.checkReadAndThrowAura(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'ID', 'Status__c', 'Failure_Reason__c', 'RecordId__c', 'Record_Name__c', 'BvD_Id__c','Error_Details__c'
    });

    return new Map<Id, BvD_Job__c> ([
      SELECT ID,
        Name,
        Status__c,
        Object__c,
        Processing__c,
        Failure_Reason__c,
        Execution_Time__c,
        Type__c, (
        SELECT
          ID,
          Status__c,
          Failure_Reason__c,
          RecordId__c,
          Record_Name__c,
          BvD_Id__c,
          Error_Details__c
        FROM BvD_Job_Work_Items__r
      )
      FROM BvD_Job__c
      WHERE ID IN :jobIds
    ]);
  }
}