@IsTest
public class BvDSchedulerTest {

  @TestSetup
  static void setup() {
    Batch_Settings__c settings = new Batch_Settings__c(
      Name = 'Account Batch Setting',
      Object__c = 'Account',
      Batch_Size__c = 50,
      Job_ID__c = null,
      Disabled__c = true,
      End_Date__c = Date.today().addYears(1),
      Start_Hour__c = 0,
      Start_Date__c = Date.today(),
      Frequency__c = 'daily',
      Day__c = 'MON'
    );
    insert settings;
  }

  @IsTest
  static void testCronExpression() {
    BvDScheduler scheduler = new BvDScheduler(Account.getSObjectType());
    Id jobID = scheduler.scheduleJob();

    CronTrigger ct = [
      SELECT NextFireTime
      FROM CronTrigger
      WHERE ID = :jobID
    ];
    System.assertEquals(Datetime.newInstance(
      Date.today().addDays(1),
      Time.newInstance(0, 0, 0, 0)
    ), ct.NextFireTime);
  }

  @IsTest
  static void testRescheduleOnSettingChange() {
    BatchSettings settings = BatchSettingsFactory.getSetting(Account.getSObjectType());
    System.assertEquals(null, settings.getJobID());

    Test.startTest();

    /*Job is not scheduled - Jobs are schedule according to setting*/
    BvDScheduler.updateSchedules();
    settings = BatchSettingsFactory.getSetting(Account.getSObjectType());
    System.assertNotEquals(null, settings.getJobID());
    String firstId = settings.getJobID();
    String firstCronExpresion = [SELECT ID,CronExpression FROM CronTrigger WHERE Id = :settings.getJobId()].CronExpression;
    System.assertEquals(settings.getCronExpression(), firstCronExpresion);

    settings.setFrequency(BatchSettings.Frequency.Weekly);
    settings.updateSetting();

    System.assertNotEquals(settings.getCronExpression(), firstCronExpresion);
    BvDScheduler.updateSchedules();

    /*Setting changed - Job is rescheduled*/
    settings = BatchSettingsFactory.getSetting(Account.getSObjectType());
    String secondId = settings.getJobID();
    String secondCronExpression = [SELECT ID,CronExpression FROM CronTrigger WHERE Id = :settings.getJobId()].CronExpression;
    System.assertNotEquals(firstId, secondId);
    System.assertNotEquals(firstCronExpresion, secondCronExpression);

    /*Reschedule without update - Nothing is reschedules*/
    BvDScheduler.updateSchedules();
    settings = BatchSettingsFactory.getSetting(Account.getSObjectType());
    String thirdId = settings.getJobID();
    String thirdCronExpression = [SELECT ID,CronExpression FROM CronTrigger WHERE Id = :settings.getJobId()].CronExpression;
    System.assertEquals(secondId, thirdId);
    System.assertEquals(secondCronExpression, thirdCronExpression);

    Test.stopTest();
  }
}