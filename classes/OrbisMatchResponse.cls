public with sharing class OrbisMatchResponse extends OrbisResponse {
    private OrbisSchema.MatchResponseElement responseSchema = new OrbisSchema.MatchResponseElement();

    public OrbisMatchResponse(HttpResponse response) {
        super(response);
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseSchema;
    }

    public List<OrbisSchema.MatchResult> getMatchResults(){
        return responseSchema.matchResultContainer.matchResults;
    }
}