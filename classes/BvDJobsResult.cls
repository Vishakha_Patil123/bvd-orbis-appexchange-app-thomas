/**
* Wrapper for BvD Job result for frontend use.
* */
public with sharing class BvDJobsResult {
  public final static String
    STATUS_SUCCESS = 'SUCCESS',
    STATUS_ERROR = 'ERROR',
    ERROR_MANUAL_FIELD_MAPPING = 'ERROR_MANUAL_FIELD_MAPPING',
    ERROR_FIELD_MAPPING = 'ERROR_FIELD_MAPPING',
    ERROR_ORBIS_SERVICE = 'ERROR_ORBIS_SERVICE',
    ERROR_EXECUTION = 'ERROR_EXECUTION',
    ERROR_DML = 'ERROR_DML',
    ERROR_INSUFFICIENT_PRIVILEGES = 'ERROR_INSUFFICIENT_PRIVILEGES';


  @AuraEnabled public String status;
  @AuraEnabled public String error;
  @AuraEnabled public Id jobId;

  public BvDJobsResult(BvD_Job__c job) {
    this.jobId = job.Id;
    this.status = job.Status__c == BvDJobs.STATUS_SUCCESS ? STATUS_SUCCESS : STATUS_ERROR;
    this.error = new Map<String, String>{
      BvDJobs.ERROR_FIELD_MAPPING => ERROR_FIELD_MAPPING,
      BvDJobs.ERROR_AUTH => ERROR_ORBIS_SERVICE,
      BvDJobs.ERROR_SERVICE => ERROR_ORBIS_SERVICE,
      BvDJobs.ERROR_EXECUTION => ERROR_EXECUTION,
      BvDJobs.ERROR_MANUAL_FIELD_MAPPING => ERROR_MANUAL_FIELD_MAPPING,
      BvDJobs.ERROR_DML => ERROR_DML,
      null => ''
    }.get(job.Failure_Reason__c);
  }

  public BvDJobsResult(Exception ex) {
    this.status = STATUS_ERROR;
    this.error = ERROR_EXECUTION;
  }

}