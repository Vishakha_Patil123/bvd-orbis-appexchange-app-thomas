@Istest
public with sharing class TestUtilities {

    public static Profile getProfile(String name) {
        return [
                SELECT Id
                FROM Profile
                WHERE Name = :name
        ];
    }

    public static User getStandardUser() {
        return getUser(Crypto.getRandomInteger() + '@orbis.unit_test.com', 'Standard User');
    }

    public static User getStandardUser(String username) {
        return getUser(username, 'Standard User');
    }

    public static User getSystemAdminUser(String username) {
        return getUser(username, 'System Administrator');
    }


    public static User getUser(String username, String profileName) {
        User user = new User(
                Alias = 'standt',
                Email = 'standarduser@testorg.com',
                EmailEncodingKey = 'UTF-8',
                LastName = 'Testing',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = getProfile(profileName).Id,
                TimeZoneSidKey = 'America/Los_Angeles',
                UserName = username
        );

        return user;
    }

    public static Log__c[] insertLogs(Integer amount) {
        Log__c[] logs = new Log__c[]{
        };

        for (Integer index = 0; index < amount; index++) {
            logs.add(
                    new Log__c(
                            Logging_Level__c = 'WARN',
                            Message__c = 'Test ' + index,
                            Type__c = 'APEX',
                            Guid__c = String.valueOf(index)
                    )
            );
        }

        insert logs;
        return logs;
    }

    public static PermissionSetAssignment assignUserToPermissionSet(Id userId, Id permissionSetId) {
        return new PermissionSetAssignment(
                AssigneeId = userId,
                PermissionSetId = permissionSetId
        );
    }

    public static PermissionSetAssignment getPermissionSetAssignmentByPermissionSetId(Id permissionSetId) {
        return [
                SELECT Id, AssigneeId, PermissionSetId
                FROM PermissionSetAssignment
                WHERE PermissionSetId = :permissionSetId
                LIMIT 1
        ];
    }

    public static PermissionSet getPermissionSetByName(String permissionSetName) {
        return [
                SELECT Id, Name
                FROM PermissionSet
                WHERE Name = :permissionSetName
                LIMIT 1
        ];
    }

    public static BvD_Job__c createBvDJob(String anObject, String type, String status) {
        return new BvD_Job__c(
                Object__c = anObject,
                Type__c = type,
                Execution_Time__c = Datetime.now(),
                Status__c = status,
                Failure_Reason__c = 'Field Mapping Conflict',
                Processing__c = 'Manual'
        );
    }

    public static BvD_Job_Work_Item__c createBvDJobWorkItem(BvD_Job__c job, String status, Id recordId) {
        return new BvD_Job_Work_Item__c(
                BvD_Job__c = job.Id,
                RecordId__c = recordId,
                Status__c = status
        );
    }

    public static User getBvDAdmininistrator(String username) {

        try {
           User user = [
                    Select Id
                    FROM User
                    WHERE Username = :username
            ];

            return user;
        } catch (QueryException ex) {
            User bvdAdminUser = TestUtilities.getUser(username, 'System Administrator');
            insert bvdAdminUser;

            PermissionSetAssignment permissionSetAssignment = assignUserToPermissionSet(bvdAdminUser.Id, getPermissionSetByName('BvD_System_Administrator').Id);
            insert permissionSetAssignment;

            return bvdAdminUser;
        }
    }
}