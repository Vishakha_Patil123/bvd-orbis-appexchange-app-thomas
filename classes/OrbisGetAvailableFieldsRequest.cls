public with sharing class OrbisGetAvailableFieldsRequest extends OrbisRequest {
    private OrbisSchema.GetAvailableFieldsElement requestSchema
            = new OrbisSchema.GetAvailableFieldsElement();

    public override OrbisResponse getOrbisResponse(HttpResponse response) {
        return new OrbisGetAvailableFieldsResponse(response);
    }

    protected override String getSOAPAction() {
        return 'http://bvdep.com/webservices/GetAvailableFields';
    }

    protected override OrbisSchema.RequestElement getRequestSchema() {
        return requestSchema;
    }

    public OrbisGetAvailableFieldsRequest setSessionID(String sessionID){
        requestSchema.sessionHandle = sessionID;
        return this;
    }

    public OrbisGetAvailableFieldsRequest setModelID(String modelID){
        requestSchema.modelId = modelID;
        return this;
    }
}