public class OrbisGetOwnershipStructureRequest extends OrbisRequest {
    private OrbisSchema.GetOwnershipStructureElement requestSchema = new OrbisSchema.GetOwnershipStructureElement();

    public override OrbisResponse getOrbisResponse(HttpResponse response) {
        return new OrbisGetOwnershipStructureResponse(response);
    }

    protected override String getSOAPAction() {
        return 'http://bvdep.com/webservices/GetOwnershipStructure';
    }

    protected override OrbisSchema.RequestElement getRequestSchema() {
        return requestSchema;
    }

    public OrbisGetOwnershipStructureRequest setSessionHandle(String sessionHandle) {
        requestSchema.sessionHandle = sessionHandle;
        return this;
    }

    public OrbisGetOwnershipStructureRequest setSubjectCompanyBvDID(String subjectCompanyBvDID) {
        requestSchema.subjectCompanyBvDID = subjectCompanyBvDID;
        return this;
    }

    public OrbisGetOwnershipStructureRequest setFlowType(OrbisSchema.FlowType flowType) {
        requestSchema.flowType = flowType;
        return this;
    }

    public OrbisGetOwnershipStructureRequest setMinimumPercentage(Decimal minimumPercentage) {
        requestSchema.minimumPercentage = minimumPercentage;
        return this;
    }

    public OrbisGetOwnershipStructureRequest setUseIntegratedOwnership(Boolean useIntegratedOwnership) {
        requestSchema.useIntegratedOwnership = useIntegratedOwnership;
        return this;
    }
}