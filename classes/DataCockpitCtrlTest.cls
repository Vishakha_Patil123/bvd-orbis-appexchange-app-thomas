@IsTest
public with sharing class DataCockpitCtrlTest {

  public static String BVD_ADMIN_USERNAME = 'testy.BVDAdmin@4c.dev.orbis.test';

  @TestSetup
  static void setup() {

    System.runAs(TestUtilities.getBvDAdmininistrator(BVD_ADMIN_USERNAME)) {

      List<BvD_Job__c> jobs = new List<BvD_Job__c>{
        TestUtilities.createBvDJob('Account', BvDJobs.TYPE_INSERT, BvDJobs.STATUS_ERROR),
        TestUtilities.createBvDJob('Lead', BvDJobs.TYPE_UPDATE, BvDJobs.STATUS_SUCCESS),
        TestUtilities.createBvDJob('Lead', BvDJobs.TYPE_UPDATE, BvDJobs.STATUS_ERROR)
      };
      insert jobs;

      BvD_Job__c job = jobs.get(0);
      BvD_Job__c leadSObjectJob = jobs.get(2);
      List<BvD_Job_Work_Item__c> workItems = new List<BvD_Job_Work_Item__c>{
        TestUtilities.createBvDJobWorkItem(job, BvDJobWorkItems.STATUS_SUCCESS, '001000000000000'),
        TestUtilities.createBvDJobWorkItem(job, BvDJobWorkItems.STATUS_SUCCESS, '001000000000000'),
        TestUtilities.createBvDJobWorkItem(job, BvDJobWorkItems.STATUS_ERROR, '001000000000000'),
        TestUtilities.createBvDJobWorkItem(leadSObjectJob, BvDJobWorkItems.STATUS_SUCCESS, '001000000000000')
      };
      insert workItems;
    }
  }

  @IsTest
  static void testGetBatchJobs() {
    System.runAs(TestUtilities.getBvDAdmininistrator(BVD_ADMIN_USERNAME)) {

      System.Test.startTest();
      List<DataCockpitCtrl.BatchJob> batchJobs = DataCockpitCtrl.getBatchJobs();
      System.Test.stopTest();

      System.assertEquals(3, batchJobs.size());
      System.assertEquals('Error', batchJobs.get(0).status);
      System.assertEquals(2, batchJobs.get(0).successNbr);
      System.assertEquals(1, batchJobs.get(0).errorsNbr);
    }
  }

  @IsTest
  static void testRetryBatchJob() {
    System.runAs(TestUtilities.getBvDAdmininistrator(BVD_ADMIN_USERNAME)) {
      BvD_Job__c bvdJob = [
        SELECT Id, Failure_Reason__c, Status__c, Object__c
        FROM BvD_Job__c
        WHERE Status__c = 'Error'
        AND Object__c = 'Lead'
        LIMIT 1
      ];

      System.Test.startTest();
      DataCockpitCtrl.retryBatchJob(bvdJob.Id);
      System.Test.stopTest();

      BvD_Job__c bvdJobProcessed = [
        SELECT Failure_Reason__c, Status__c
        FROM BvD_Job__c
        WHERE Id = :bvdJob.Id
      ];

      System.assertEquals(BvDJobs.STATUS_SUCCESS, bvdJobProcessed.Status__c);
    }
  }

  @IsTest
  static void testProcessBatchJob() {
    System.runAs(TestUtilities.getBvDAdmininistrator(BVD_ADMIN_USERNAME)) {
      BvD_Job__c bvdJob = [
        SELECT Id, Failure_Reason__c, Status__c, Object__c
        FROM BvD_Job__c
        WHERE Status__c = 'Error'
        AND Object__c = 'Lead'
        LIMIT 1
      ];

      System.Test.startTest();
      DataCockpitCtrl.processBatchJob(bvdJob.Id);
      System.Test.stopTest();

      BvD_Job__c bvdJobProcessed = [
        SELECT Failure_Reason__c, Status__c
        FROM BvD_Job__c
        WHERE Id = :bvdJob.Id
      ];

      System.assertEquals(BvDJobs.STATUS_SUCCESS, bvdJobProcessed.Status__c);
    }
  }
}