/**
* Apex controller for Update Action Button Lightning Component
* Which updates record (executes BvDJob) and returns results.
* */
public with sharing class UpdateActionButtonCtrl {

  @AuraEnabled
  public static BvD_Job__c isRecordSynced(Id recordId) {
    sObject record = getRecord(recordId);
    BvD_Job__c job = BvDJobs.updateRecordsImmediatly(new List<sObject>{record}, BvDJobs.PROCESSING_DRY_RUN);
    return job;
  }

  private static sObject getRecord(Id recordId) {
    return Database.query('SELECT ID,BvD_Id__c FROM '+recordId.getSObjectType()+' WHERE ID = :recordId');
  }
}