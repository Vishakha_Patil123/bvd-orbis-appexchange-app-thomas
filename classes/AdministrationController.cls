public with sharing class AdministrationController {

  public static final Set<String> EXCLUDEDTYPES = new Set<String> {
          'REFERENCE', 'ID'
  };

  public static final Set<String> COMMONEXCLUDEDFIELDS = new Set<String> {
          'CreatedDate',
          'LastModifiedDate',
          'SystemModstamp',
          'IsDeleted',
          'BvD_Id__c'
  };
  public static final Map<String, Set<String>> EXCLUDEDFIELDS = new Map<String, Set<String>> {
          'Account' => new Set<String> {
                  'LastActivityDate' ,
                  'LastViewedDate',
                  'LastReferencedDate',
                  'Jigsaw',
                  'JigsawCompanyId',
                  'DunsNumber'
          },
          'Lead'    => new Set<String> {
                  'Jigsaw',
                  'JigsawContactId',
                  'CompanyDunsNumber'
          },
          'Contact' => new Set<String> {
                  'Jigsaw',
                  'JigsawContactId'
          }
  };


  public class DataSourceResponse {
    @AuraEnabled
    public String Error { get; set; }
    @AuraEnabled
    public DataSource[] Products { get; set; }
  }

  public class DataSource {
    @AuraEnabled
    public String Id { get; set; }
    @AuraEnabled
    public String Label { get; set; }
    @AuraEnabled
    public String WebServiceUrl { get; set; }
    @AuraEnabled
    public String WebsiteUrl { get; set; }
  }

  public class Field {
    @AuraEnabled
    public String code { get; set; }
    @AuraEnabled
    public String type { get; set; }
    @AuraEnabled
    public String name { get; set; }
    @AuraEnabled
    public AdministrationController.DataDimensions dataDimensions { get; set; }

    public AdministrationController.Field(OrbisGetAvailableFieldsResponse.Field field) {
      code = field.code;
      type = field.type;

      if(field.dataDimensions != null) {
        dataDimensions = new AdministrationController.DataDimensions(field.dataDimensions);
      }
    }
  }

  public class DataDimensions {
    @AuraEnabled
    public String default_x { get; set; }
    @AuraEnabled
    public List<AdministrationController.DataDimension> dataDimensions { get; set; }

    public AdministrationController.DataDimensions(OrbisGetAvailableFieldsResponse.DataDimensions dataDimensions) {
      this.dataDimensions = new List<AdministrationController.DataDimension>();
      default_x           = dataDimensions.default_x;

      if(dataDimensions != null) {
        this.dataDimensions = toDataDimensionArray(dataDimensions.dataDimensions);
      }
    }
  }

  public class DataDimension {
    @AuraEnabled
    public String name { get; set; }
    @AuraEnabled
    public String type { get; set; }

    public AdministrationController.DataDimension(OrbisGetAvailableFieldsResponse.DataDimension dataDimension) {
      name = dataDimension.name;
      type = dataDimension.type;
    }
  }

  public static AdministrationController.DataDimension[] toDataDimensionArray(OrbisGetAvailableFieldsResponse.DataDimension[] dataDimensions) {
    AdministrationController.DataDimension[] dimensions = new AdministrationController.DataDimension[] {};

    for (OrbisGetAvailableFieldsResponse.DataDimension dataDimension : dataDimensions) {
      dimensions.add(new AdministrationController.DataDimension(dataDimension));
    }

    return dimensions;
  }

  public static AdministrationController.Field[] toFieldsArray(OrbisGetAvailableFieldsResponse.Field[] orbisFields) {
    AdministrationController.Field[] fields = new AdministrationController.Field[] {};

    for (OrbisGetAvailableFieldsResponse.Field field : orbisFields) {
      fields.add(new AdministrationController.Field(field));
    }

    return fields;
  }

  public static void checkSetupPrivileges() {

    System.debug('CHECKSETUPPRIVILEGES');
    System.debug(!Schema.Bvd_Setup__c.getSObjectType().getDescribe().isCreateable());
    System.debug(!Schema.Bvd_Setup__c.getSObjectType().getDescribe().isUpdateable());
    if(!Schema.Bvd_Setup__c.getSObjectType().getDescribe().isCreateable()) {
      Logger.error('Not enough privileges to create setup');
      Logger.stop();
      throw new AuraHandledException('Not enough privileges to create setup');
    } else if(!Schema.Bvd_Setup__c.getSObjectType().getDescribe().isUpdateable()) {
      Logger.error('Not enough privileges to create setup');
      Logger.stop();
      throw new AuraHandledException('Not enough privileges to update setup');
    }

  }

  @AuraEnabled
  public static String getOrgId(){
    return UserInfo.getOrganizationId();
  }

  @AuraEnabled
  public static AdministrationController.DataSourceResponse getAvailableDataSources() {
    Logger.start('AdministrationController', 'getAvailableDataSources');

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();

    Http http           = new Http();
    HttpRequest request = new HttpRequest();

    String endpoint       = String.format('callout:Setup/{0}/products', new String[] {
            setup.Organization_Token__c + UserInfo.getOrganizationId()
    });

    request.setMethod('GET');
    request.setHeader('Accept', 'application/json');
    request.setEndpoint(endpoint);

    try {
      HttpResponse response = Http.send(request);

      Logger.info(request);
      Logger.info(response);

      AdministrationController.DataSourceResponse dataSource = (AdministrationController.DataSourceResponse) JSON.deserialize(
              response.getBody(),
              Type.forName('AdministrationController.DataSourceResponse')
      );

      return dataSource;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static BvD_Setup__c getBvDSetup() {
    Logger.start('AdministrationController', 'getBvDSetup');
    try {
      return BvDSetupSettings.getSetting();
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void setDataSource(String label, String id, String webserviceUrl, String websiteUrl) {
    Logger.start('AdministrationController', 'setDataSource');
    BvD_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(!String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException('Datasource already set');
    }

    checkSetupPrivileges();

    setup.Datasource_Label__c = label;
    setup.Datasource_Id__c    = id;
    setup.Datasource_Webservice_URL__c = webserviceUrl;
    setup.Datasource_Website_URL__c    = websiteUrl;

    try {
      if(BVDUserPermissionCheck.permissionToUpsertSobject('BvD_Setup__c', true,
                                                          new List<String>{'Name', 'Field_Mapping_Configured__c',
                                                                           'Import_Settings_Configured__c','SSO_Confirmed__c',
                                                                           'Batchjob_Settings_Confirmed__c',
                                                                           'REST_URL__c, Organization_Token__c'})){
        upsert setup;
      }
      else{
        Logger.error('Not enough privileges to create/update bvd setup');
        throw new AuraHandledException('Not enough privileges to create/updare bvd setup');
      }

    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void setConfirmSSO() {
    Logger.start('AdministrationController', 'setConfirmSSO');
    BvD_Setup__c setup = getBvDSetup();

    if(setup.SSO_Confirmed__c) {
      throw new AuraHandledException('SSO already set');
    }

    checkSetupPrivileges();

    setup.SSO_Confirmed__c = true;

    try {
      upsert setup;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void setFieldMappingConfigured() {
    Logger.start('AdministrationController', 'setFieldMappingConfigured');
    BvD_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    checkSetupPrivileges();

    setup.Field_Mapping_Configured__c = true;

    try {
      upsert setup;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void setImportSettingConfigured() {
    Logger.start('AdministrationController', 'setImportSettingConfigured');
    BvD_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!setup.Field_Mapping_Configured__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_FieldMapping_Missing);
    }

    checkSetupPrivileges();

    setup.Import_Settings_Configured__c = true;

    try {
      upsert setup;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void setBatchJobSettingConfigured() {
    Logger.start('AdministrationController', 'setBatchJobSettingConfigured');
    BvD_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!setup.Field_Mapping_Configured__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_FieldMapping_Missing);
    }

    if(!setup.Import_Settings_Configured__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_ImportSettings_Missing);
    }

    checkSetupPrivileges();

    setup.Batchjob_Settings_Confirmed__c = true;

    try {
      upsert setup;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  public static Boolean isExcluded(String sObjectName, String type, String field) {

    return  EXCLUDEDTYPES.contains(type)
            || COMMONEXCLUDEDFIELDS.contains(field)
            || !Schema.getGlobalDescribe().containsKey(sObjectName)
            || !Schema.getGlobalDescribe().get(sObjectName).getDescribe().fields.getMap().containsKey(field)
            || (EXCLUDEDFIELDS.containsKey(sObjectName) && EXCLUDEDFIELDS.get(sObjectName).contains(field));
  }

  @AuraEnabled
  public static Map<String, String> getFieldsNameAndTypes(String objectName){
    Logger.start('AdministrationController', 'getFieldsNameAndTypes');

    Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    Schema.SObjectType leadSchema             = schemaMap.get(objectName);
    Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
    Map<String,String> fieldNameAndType       = new Map<String,String>();

    for (String fieldName: fieldMap.keySet()) {

      String name = fieldMap.get(fieldName).getDescribe().getName();
      String type = fieldMap.get(fieldName).getDescribe().getType().name();
      Boolean isRequired = fieldMap.get(fieldName).getDescribe().isNillable();

      if(!EXCLUDEDTYPES.contains(type))
        fieldNameAndType.put(
                name,
                type
        );
    }

    Logger.stop();
    return fieldNameAndType;
  }

  @AuraEnabled
  public static AdministrationController.Field[] getOrbisAvailableFields() {
    Logger.start('AdministrationController', 'getOrbisAvailableFields');
    OrbisAPI orbisAPI         = new OrbisAPI();
    OrbisRequest openRequest  = new OrbisOpenRequest()
            .setUsername('{!HTMLENCODE($Credential.Username)}')
            .setPassword('{!HTMLENCODE($Credential.Password)}')
            .setEndpoint('callout:Orbis');

    OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());

    OrbisRequest request = new OrbisGetAvailableFieldsRequest()
            .setModelID('UNIVERSAL')
            .setSessionID(openResponse.getSessionID());


    try {
      OrbisGetAvailableFieldsResponse response = (OrbisGetAvailableFieldsResponse) orbisAPI.send(request);

      Logger.info(openRequest.toHttpRequest());
      Logger.info(request.toHttpRequest());

      return AdministrationController.toFieldsArray(response.getAvailableFields());
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static AdministrationController.Field[] getLabelsFromModels() {
    Logger.start('AdministrationController', 'getLabelsFromModels');

    try {

      AdministrationController.Field[] orbisFields = getOrbisAvailableFields();

      OrbisAPI orbisAPI = new OrbisAPI();
      OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());
      OrbisGetLabelsFromModelRequest orbisRequest = new OrbisGetLabelsFromModelRequest()
              .setSessionID(openResponse.getSessionID())
              .setFullLabels(true);
      for (AdministrationController.Field field : orbisFields) {
        orbisRequest.addField(field.code,'UNIVERSAL');
      }
      OrbisRequest request = orbisRequest;
      OrbisGetLabelsFromModelResponse response = (OrbisGetLabelsFromModelResponse) orbisAPI.send(request);
      List<String> labels = response.getLabels();

      System.debug(labels.size());
      System.debug(orbisFields.size());
      for(Integer i = 0;i<labels.size();i++){
        orbisFields.get(i).name = labels.get(i);
      }
      return orbisFields;

    } catch (Exception ex) {
      Logger.error(ex);
      System.debug(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }


  @AuraEnabled
  public static List<AdministrationController.Field> getFilteredOrbisAvailableFields(String objectType) {
    Logger.start('AdministrationController', 'getFilteredOrbisAvailableFields');

    try {
      Orbis_Field__c[] orbisFields = getOrbisMappedFields(objectType);
      AdministrationController.Field[] fields = getLabelsFromModels();
      List<AdministrationController.Field> filteredFields = new List<AdministrationController.Field>();
      Map<String,String> fieldNames = new Map<String,String>();
      for (Orbis_Field__c field : orbisFields) {
        fieldNames.put(field.Name,field.Name);
      }
      for (AdministrationController.Field field : fields) {
        if(!fieldNames.containsKey(field.code)){
          filteredFields.add(field);
        }
      }
      return filteredFields;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }



  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> getMapping(String sObjectName) {
    SObject[] mapped                   = new SObject[] {};
    SObject[] excluded                 = new SObject[] {};
    SObject[] available                = new SObject[] {};
    Map<String, SObject> fieldMapping  = new Map<String, SObject> {};
    Map<String, String> fieldsAndTypes = getFieldsNameAndTypes(sObjectName);

    for (Field_Mapping__c mapping: (Field_Mapping__c[]) CommonUtilities.queryAllFieldsWithCondition(
            'Field_Mapping__c',
            //'WHERE SObject__c = :sObjectName'
            'WHERE SObject__c = \'' + sObjectName + '\''
    )) {
      String type  = mapping.Type__c;
      String field = mapping.Sf_Field__c;
      if(!isExcluded(sObjectName, type, field)) {
        Boolean excluding = mapping.Excluded__c;

        System.debug('Excluded ?');
        System.debug(excluding);

        if(excluding) {
          excluded.add(mapping);
        } else {
          mapping.Invalid__c = !FieldMatrix.isMapable(mapping.SObject__c,mapping.SF_Field__c,mapping.BvD_Field_Type__c);
          mapped.add(mapping);
        }
        fieldMapping.put(field, mapping);
      } else {
        if(BVDUserPermissionCheck.permissionToDeleteSobject('Field_Mapping__c', true)){
          delete mapping;
        }else{
          Logger.error('Not enough privileges to delete Field Mapping');
          throw new AuraHandledException('Not enough privileges to delete Field Mapping');
        }
    }
  }


    for (String field : fieldsAndTypes.keySet()) {
      String type = fieldsAndTypes.get(field);

      if(!isExcluded(sObjectName, type, field) && !fieldMapping.containsKey(field)) {
        Field_Mapping__c mapping = new Field_Mapping__c(
                Name__c           = String.format('{0}.{1}', new String[] { sObjectName, field }),
                Action__c         = '',
                BvD_Field__c   	  = '',
                BvD_Field_Type__c = '',
                Excluded__c       = false,
                Master__c         = '',
                Reason__c         = '',
                Sf_Field__c       = field,
                SObject__c        = sObjectName,
                Type__c           = type
        );


        fieldMapping.put(field, mapping);
        available.add(mapping);
      }
    }
    System.debug(DateTime.now());

    return new Map<String, SObject[]> {
            'mapped'    => mapped,
            'excluded'  => excluded,
            'available' => available
    };
  }
  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> getAccountMapping() {
    Logger.start('AdministrationController', 'getAccountMapping');
    System.debug(DateTime.now());
    try {
      return FieldMappingController.getMapping('Account');

    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      System.debug(DateTime.now());
      Logger.stop();
      System.debug(DateTime.now());

    }
  }
  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> getLeadMapping() {
    Logger.start('AdministrationController', 'getLeadMapping');
    try {
      return FieldMappingController.getMapping('Lead');
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }
  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> getContactMapping() {
    Logger.start('AdministrationController', 'getContactMapping');
    try {
      return FieldMappingController.getMapping('Contact');
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> updateFieldMapping(String sObjectName, Field_Mapping__c[] fieldMappings) {
    Logger.start('AdministrationController', 'updateFieldMapping');


    for (Field_Mapping__c f: fieldMappings) {
      System.debug(f);
    }
    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    try {
      Field_Mapping__c[] toInsert = new Field_Mapping__c[] {};
      Field_Mapping__c[] toUpdate = new Field_Mapping__c[] {};

      for (Field_Mapping__c fieldMapping : fieldMappings) {
        System.debug(fieldMapping.Id);
        if(!String.isBlank(fieldMapping.Id)) {
          toUpdate.add(fieldMapping);
        } else {
          toInsert.add(fieldMapping);
        }
      }

      if(!toInsert.isEmpty()) {
        if(BVDUserPermissionCheck.permissionToCreateSobject('Field_Mapping__c', true,
                                                            new List<String>{'BvD_Field__c', 'Action__c', 'Master__c'})){
          insert toInsert;
        }else{
          Logger.error('Not enough privileges to create Field mapping');
          throw new AuraHandledException('Not enough privileges to create Field mapping');
        }
      }

      if(!toUpdate.isEmpty()) {
        if(BVDUserPermissionCheck.permissionToUpdateSobject('Field_Mapping__c', true,
                                                             new List<String>{'BvD_Field__c', 'Action__c', 'Master__c'})){
          update toUpdate;

        }else{
          Logger.error('Not enough privileges to update Field mapping');
          throw new AuraHandledException('Not enough privileges to update Field mapping');        }
      }

      return FieldMappingController.getMapping(sObjectName);
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> unExcludeFieldMapping(String sObjectName, List<Field_Mapping__c> fieldMappings) {
    Logger.start('AdministrationController', 'unExcludeFieldMapping');

    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    try {
      Set<String> fieldsToDelete = new Set<String> {};

      Field_Mapping__c[] fields = new Field_Mapping__c[] {};

      for (Field_Mapping__c field : fieldMappings) {
        System.debug(field.Name__c);
        fieldsToDelete.add(field.Name__c);
      }

      if(BVDUserPermissionCheck.permissionToDeleteSobject('Field_Mapping__c', true)){
        delete [
                SELECT Id, Name__c
                FROM Field_Mapping__c
                WHERE Name__c IN :fieldsToDelete
               ];
      }else{
        Logger.error('Not enough privileges to delete Field mapping');
        throw new AuraHandledException('Not enough privileges to delete Field mapping');
      }

      return FieldMappingController.getMapping(sObjectName);
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Map<String, Field_Mapping__c[]> importFieldMapping(String sObjectName, List<Field_Mapping__c> fieldMappings) {
    Logger.start('AdministrationController', 'importFieldMapping');

    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    try {
      Set<String> fieldsToDelete = new Set<String> {};

      Field_Mapping__c[] fields = new Field_Mapping__c[] {};

      for (Field_Mapping__c field : fieldMappings) {
        fieldsToDelete.add(field.Name__c);
        fields.add(field.clone(false));
      }
      if(BVDUserPermissionCheck.permissionToDeleteSobject('Field_Mapping__c', true)){

        delete [
                SELECT Id, Name__c
                FROM Field_Mapping__c
                WHERE Name__c IN :fieldsToDelete
              ];
      }else{
        Logger.error('Not enough privileges to delete Field mapping');
        throw new AuraHandledException('Not enough privileges to delete Field mapping');
      }

      if(BVDUserPermissionCheck.permissionToCreateSobject('Field_Mapping__c', true,
                                                         new List<String>{'BvD_Field__c', 'Action__c', 'Master__c'})){
        insert fields;

      }else{
        Logger.error('Not enough privileges to create Field mapping');
        throw new AuraHandledException('Not enough privileges to create Field mapping');
      }

      BvDScheduler.updateSchedules();
      return getMapping(sObjectName);
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Import_Setting__c[] getImportSettings() {
    Logger.start('AdministrationController', 'getImportSettings');

    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!setup.Field_Mapping_Configured__c) {
      throw new AuraHandledException('TODO LABEL field mapping not configured');
    }

    //TODO
    try{
      return [SELECT Id,Name,SObject__c,Type__c,Processing__c FROM Import_Setting__c ORDER BY SObject__c];

    }
    catch (Exception ex){
      System.debug(ex);
      throw new AuraHandledException(ex.getMessage());
    }
    finally{
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Import_Setting__c[] setImportSettings(Import_Setting__c[] importSettings) {
    Logger.start('AdministrationController', 'getImportSettings');

    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!setup.Field_Mapping_Configured__c) {
      throw new AuraHandledException('TODO LABEL field mapping not configured');
    }

    try {
      Import_Setting__c[] toInsert = new Import_Setting__c[] {};
      Import_Setting__c[] toUpdate = new Import_Setting__c[] {};

      for (Import_Setting__c importSetting : importSettings) {
        if(importSetting.Id != null) {
          toUpdate.add(importSetting);
        } else {
          toInsert.add(importSetting);
        }
      }

      if(!toInsert.isEmpty()) {
        if(BVDUserPermissionCheck.permissionToCreateSobject('Import_Setting__c', true,
                                                            new List<String>{'Processing__c', 'SObject__c', 'Type__c'})){
          insert toInsert;

        }else{
          Logger.error('Not enough privileges to create Import setting');
          throw new AuraHandledException('Not enough privileges to create Import setting');
        }
      }

      if(!toUpdate.isEmpty()) {
        if(BVDUserPermissionCheck.permissionToUpdateSobject('Import_Setting__c', true,
                                                            new List<String>{'Processing__c', 'SObject__c', 'Type__c'})){
          update toUpdate;
        }else{
          Logger.error('Not enough privileges to update Import setting');
          throw new AuraHandledException('Not enough privileges to update Import setting');
        }
      }

      if(!setup.Import_Settings_Configured__c) {
        setup.Import_Settings_Configured__c = true;
        if(BVDUserPermissionCheck.permissionToUpdateSobject('BvD_Setup__c', true, new List<String>{'Import_Settings_Configured__c'})){
         update setup;
        }else{
          Logger.error('Not enough privileges to update BVD setup');
          throw new AuraHandledException('Not enough privileges to update BVD setup');
        }
      }
      BvDScheduler.updateSchedules();
      return getImportSettings();
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Batch_Settings__c[] getBatchJobSettings() {
    Logger.start('AdministrationController', 'getBatchJobSettings');

    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!setup.Field_Mapping_Configured__c) {
      throw new AuraHandledException('TODO LABEL field mapping not configured');
    }

    if(!setup.Import_Settings_Configured__c) {
      throw new AuraHandledException('TODO LABEL field mapping not configured');
    }

    try{
      return Batch_Settings__c.getAll().values();
    }
    catch (Exception ex){
      System.debug(ex);
      throw new AuraHandledException(ex.getMessage());
    }
    finally{
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Batch_Settings__c[] setBatchSettings(Batch_Settings__c[] batchJobSettings,String startDate,String endDate) {
    Logger.start('AdministrationController', 'setBatchSettings');

    Bvd_Setup__c setup = getBvDSetup();

    if(!setup.SSO_Confirmed__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
    }

    if(String.isBlank(setup.Datasource_Id__c)) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!setup.Field_Mapping_Configured__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_FieldMapping_Missing);
    }


    if(!setup.Import_Settings_Configured__c) {
      throw new AuraHandledException(System.Label.BvD_Admin_ImportSettings_Missing);
    }

    try {
      Batch_Settings__c[] toInsert = new Batch_Settings__c[] {};
      Batch_Settings__c[] toUpdate = new Batch_Settings__c[] {};
      for (Batch_Settings__c batchJobSetting : batchJobSettings) {
        //batchJobSetting.Start_Date__c = deserializeDate(startDate);
        //batchJobSetting.End_Date__c = deserializeDate(endDate);

        if(batchJobSetting.Id != null) {
          toUpdate.add(batchJobSetting);
        } else {
          toInsert.add(batchJobSetting);
        }
      }
      if(!toInsert.isEmpty()) {
        //check create permission
        if(BVDUserPermissionCheck.permissionToCreateSobject('Batch_Settings__c', true,
                                                            new List<String>{'Batch_Size__c', 'Cron_Exp__c', 'Day__c',
                                                                             'Disabled__c', 'End_Date__c', 'Frequency__c',
                                                                             'Job_ID__c', 'Object__c', 'Start_Date__c',
                                                                             'Start_Hour__c'})){
          insert toInsert;

        }else{
          Logger.error('Not enough privileges to  create batch settings');
          throw new AuraHandledException('Not enough privileges to create batch settings');
        }
      }
      if(!toUpdate.isEmpty()) {
        if(BVDUserPermissionCheck.permissionToUpdateSobject('Batch_Settings__c', true,
                                                            new List<String>{'Batch_Size__c', 'Cron_Exp__c', 'Day__c',
                                                                             'Disabled__c', 'End_Date__c', 'Frequency__c',
                                                                             'Job_ID__c', 'Object__c', 'Start_Date__c', 'Start_Hour__c'})){
            update toUpdate;

        }else{
          Logger.error('Not enough privileges to update batch settings');
          throw new AuraHandledException('Not enough privileges to update batch settings');
        }
      }

      setBatchJobSettingConfigured();
      return getBatchJobSettings();
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Orbis_Field__c[] getOrbisFields(String objectType){
    Logger.start('AdministrationController', 'getOrbisFields');
    try {
      return [SELECT Id,Name,Regular_Name__c,SObject__c,Type__c
              FROM Orbis_Field__c
              WHERE SObject__c =:objectType];
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Orbis_Field__c[] insertOrbisFields(Orbis_Field__c[] orbisFields){
    Logger.start('AdministrationController', 'insertOrbisFields');
    try {
      if(BVDUserPermissionCheck.permissionToCreateSobject('Orbis_Field__c', true,
                                                         new List<String>{'Name', 'OwnerId', 'Regular_Name__c', 'SObject__c', 'Type__c'})){
       insert orbisFields;

      }else{
        Logger.error('Not enough privileges to create Orbis Field');
        throw new AuraHandledException('Not enough privileges to create Orbis Field');
      }
      return orbisFields;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Orbis_Field__c[] getOrbisMappedFields(String selectedObject){
    Logger.start('AdministrationController', 'getOrbisMappedFields');
    try {

      return [SELECT Id,
                     Name,
                     Regular_Name__c,
                     Type__c,
                     SObject__c
              FROM Orbis_Field__c
              WHERE SObject__c =:selectedObject
      ];
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void deleteOrbisMappedFields(Id mappedField){
    Logger.start('AdministrationController', 'deleteOrbisMappedFields');
    try {
      Orbis_Field__c orbisField = new Orbis_Field__c();
      System.debug(mappedField);
      orbisField.Id = mappedField;
      if(BVDUserPermissionCheck.permissionToDeleteSobject('Orbis_Field__c', true)){
        delete orbisField;
      }else{
        Logger.error('Not enough privileges to delete orbis Field');
        throw new AuraHandledException('Not enough privileges to delete orbis Field');
      }
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  private static Datetime deserializeDatetime(String purchasePriceDate) {
    Datetime parsedDatetime = (Datetime) JSON.deserialize(
      '"' + purchasePriceDate + '"',
      Datetime.Class
    );
    return Datetime.newInstance(
    parsedDatetime.getTime()
    );
  }

  @TestVisible
  private static Date deserializeDate(String purchaseDate){
    return deserializeDatetime(purchaseDate).date();
  }

  @AuraEnabled
  public static FieldMatrix.CrossSection getCrossSection(String sObjectType, String sfField, String bvdType){
    Logger.start('AdministrationController', 'getCrossSection');
    try {
      return FieldMatrix.getCrossSection(sObjectType,sfField,bvdType);
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static List<Field_Matrix__mdt> getMatrixLine(String sfFieldType){
    List<Field_Matrix__mdt> fieldCrossections = [
            SELECT ID,
                    SF_Field_Type__c,
                    BvD_Field_Type__c,
                    Availability__c,
                    Warning__c
            FROM Field_Matrix__mdt
            WHERE SF_Field_Type__c =:sfFieldType
    ];
    return fieldCrossections;

  }
}