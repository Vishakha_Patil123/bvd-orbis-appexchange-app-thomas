@IsTest
public with sharing class LogsController_Test {


  /*
   Test if a user, with permissions, fetches an empty amount of records
   */
  @isTest
  static void logsCount_01() {

    Integer count = LogsController.logsCount();

    System.assertEquals(
      0,
      count
    );
  }

  /*
   Test if a user, with permissions, fetches the good amount of records
   */
  @isTest
  static void logsCount_02() {
    TestUtilities.insertLogs(1);

    Integer count = LogsController.logsCount();

    System.assertEquals(
      1,
      count
    );
  }

  /*
   Test if a user, without permissions, catches an error
   */
  @isTest
  static void logsCount_03() {
    TestUtilities.insertLogs(1);
    User user     = TestUtilities.getStandardUser();

    System.runAs(user) {
      try {
        LogsController.logsCount();
        System.assert(false);
      } catch (Exception ex) {
        System.assert(true);
      }

    }
  }

  /*
    Test if a user, with permissions, fetches the 62 first logs
   */
  @isTest
  static void getLogs_01() {
    TestUtilities.insertLogs(62);

    Log__c[] logs = LogsController.getLogs('0');

    System.assertEquals(
      62,
      logs.size()
    );
  }

  /*
    Test if a user, with permissions, fetches the 0 first logs
   */
  @isTest
  static void getLogs_02() {
    TestUtilities.insertLogs(0);

    Log__c[] logs = LogsController.getLogs('0');

    System.assertEquals(
      0,
      logs.size()
    );
  }

  /*
    Test if a user, with permissions, fetches the 0 first logs
   */
  @isTest
  static void getLogs_05() {
    TestUtilities.insertLogs(1001);

    Log__c[] logs = LogsController.getLogs('1000');

    System.assertEquals(
      1,
      logs.size()
    );
  }

  /*
    Test if a user, without permissions, is not allow to query logs
   */
  @isTest
  static void getLogs_03() {
    TestUtilities.insertLogs(62);

    User user     = TestUtilities.getStandardUser();

    System.runAs(user) {
      try {
        LogsController.getLogs('0');
        System.assert(false);
      } catch (Exception ex) {
        System.assert(true);
      }

    }
  }

  /*
    Test if a common exception is raised
   */
  @isTest
  static void getLogs_04() {
    TestUtilities.insertLogs(62);

    User user     = TestUtilities.getStandardUser();

    System.runAs(user) {
      try {
        LogsController.getLogs(null);
        System.assert(false);
      } catch (Exception ex) {
        System.assert(true);
      }

    }
  }

  @isTest
  static void getListViews_01() {
    try {
      List<ListView> result = LogsController.getListViews('Log__c');
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }

  }

  @isTest
  static void getLoggingLevels_01() {
      List<Map<String, String>> result = LogsController.getLoggingLevels();

    System.assert(!result.isEmpty());
  }

  @isTest
  static void logsAvailable_01() {
    try {
      System.assert(!LogsController.logsAvailable('ALL'));
    } catch (Exception ex) {
      System.assert(false); // Never reached
    }
  }

  @isTest
  static void logsAvailable_02() {
    try {
      Logger.start();
      Logger.error('test');
      Logger.stop();
      System.assert(LogsController.logsAvailable('ALL'));
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void deleteLogs_01() {
    Logger.start();
    Logger.error('test');
    Logger.stop();

    Integer result = LogsController.deleteLogs('ERROR');

    System.assertEquals(
        1,
        result
    );
  }
}