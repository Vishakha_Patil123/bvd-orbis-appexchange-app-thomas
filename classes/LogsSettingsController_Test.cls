@IsTest
public with sharing class LogsSettingsController_Test {

  @isTest
  static void checkLogsSetupPrivileges_01() {
    try {
      LogsSettingsController.checkLogsSetupPrivileges();
      System.assert(true); // should fail
    } catch (Exception ex) {
      System.assert(false); // Never reached
    }
  }

  @isTest
  static void getLogSettings_01() {
    Log_Settings__c logSettings = LogsSettingsController.getLogSettings();

    System.assertEquals(
      null,
      logSettings.Id
    );
  }

  @isTest
  static void getLogSettings_02() {
      Log_Settings__c logSettings = LogsSettingsController.getLogSettings();


    System.assertEquals(
        null,
        logSettings.Id
    );
      try{
          LogsSettingsController.insertLogsSettings(true, true, true, false);
          
          Log_Settings__c result = LogsSettingsController.getLogSettings();
          
          System.assertEquals(
              true,
              result.Info__c
          );
          System.assertEquals(
              true,
              result.Info__c
          );
          System.assertEquals(
              true,
              result.Warn__c
          );
          System.assertEquals(
              false,
              result.Error__c
          );
      }catch(Exception ex){
          System.debug('Exception : ' + ex);
      }
    
  }

  @isTest
  static void getLogsFields_01() {
    String[] result = LogsSettingsController.getLogsSettingsFields();

    System.assert(!result.isEmpty());
  }
}