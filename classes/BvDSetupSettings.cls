/**
* Accessor class for Bvd_Setup__c custom setting
* */
public with sharing class BvDSetupSettings {
  private final static String SETTING_NAME = 'BvD - Setup';
  private final static Bvd_Setup__c SETTING;

  static {
    SETTING = Bvd_Setup__c.getInstance(SETTING_NAME);

    if (SETTING == null) {
      SETTING = new Bvd_Setup__c(
        Name = SETTING_NAME,
        Field_Mapping_Configured__c = false,
        Import_Settings_Configured__c = false,
        SSO_Confirmed__c = false,
        Batchjob_Settings_Confirmed__c = false,
        REST_URL__c = 'https://clientlogin.bvdinfo.com/crm/salesforce/',
        Organization_Token__c = GuidGenerator.generate().remove('-').left(16)
      );
    }
  }

  public static Bvd_Setup__c getSetting() {
    return SETTING;
  }

  public static String getDatasourceID() {
    return SETTING.Datasource_Id__c;
  }

  public static String getDatasourceLabel() {
    return SETTING.Datasource_Label__c;
  }

  public static String getDatasourceWebserviceURL() {
    return SETTING.Datasource_Webservice_URL__c;
  }

  public static String getDatasourceWebsiteURL() {
    return SETTING.Datasource_Website_URL__c;
  }

  public static String getRestURL() {
    return SETTING.REST_URL__c;
  }

  public static Boolean isFieldMappingConfigured() {
    return SETTING.Field_Mapping_Configured__c;
  }

  public static Boolean isImportSettingsConfigured() {
    return SETTING.Import_Settings_Configured__c;
  }

  public static Boolean isSSOConfigured() {
    return SETTING.SSO_Confirmed__c;
  }

  public static Boolean isBatchJobSettingsConfigured() {
    return SETTING.Batchjob_Settings_Confirmed__c;
  }

  @TestVisible
  private static void confirmAll() {
    SETTING.SSO_Confirmed__c = true;
    SETTING.Datasource_Id__c = 'test';
    SETTING.Field_Mapping_Configured__c = true;
    SETTING.Import_Settings_Configured__c = true;
    SETTING.Batchjob_Settings_Confirmed__c = true;
    if(BVDUserPermissionCheck.permissionToUpsertSobject('SETTING', true,
                                                            new List<String>{'Import_Settings_Configured__c', 'Batchjob_Settings_Confirmed__c','SSO_Confirmed__c', 'Datasource_Id__c', 'Field_Mapping_Configured__c'})){
          upsert SETTING;
			}else{
			  Logger.error('Not enough privileges to upsert Field Setting');
			  throw new AuraHandledException('Not enough privileges to upsert Setting');
			}
  }
}