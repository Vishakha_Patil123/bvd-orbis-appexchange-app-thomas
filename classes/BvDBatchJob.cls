/**
* @description
* BvD Batch Job executes action for all sObjects in given iterable.
* For each record, BvD Work Item is created during execution.
* */
public with sharing virtual class BvDBatchJob
  implements Database.Batchable<BvD_Job_Work_Item__c>,
    Database.Stateful,
    Database.AllowsCallouts {

  private BvD_Job__c job;
  private BvDBatchJobIterable iterable;
  private List<BvDAction.Result> actionResults;


  public BvDBatchJob(BvD_Job__c job, BvDBatchJobIterable iterable) {
    this.job = job;
    this.iterable = iterable;
    this.actionResults = new List<BvDAction.Result>();
  }


  /**
  * @description Executes this batch job with Batch Size from setting for current Object
  * @return BvD Job Id
  * */
  public Id executeBatch() {
    BatchSettings settings = BatchSettingsFactory.getSetting(job.Object__c);
    if (String.isEmpty(job.Id) && BVDUserPermissionCheck.permissionToCreateSobject('BvD_Job__c', true,
                                                                                    new List<String>{'Object__c', 'Status__c',
                                                                                                     'Type__c', 'Failure_Reason__c'})) {
      insert this.job;
    }else{
          Logger.error('Not enough privileges to create Job');
          throw new AuraHandledException('Not enough privileges to create Job');
        }
    Database.executeBatch(this, settings.getBatchSize());

    return job.Id;
  }

  /**
  * Executes action immediately and return
  * BvD Job Id.
  * */
  public BvD_Job__c executeImmediatly() {
    this.job.Status__c = BvDJobs.STATUS_IN_PROGRESS;

    List<BvD_Job_Work_Item__c> workItems = new List<BvD_Job_Work_Item__c>();
    Iterator<BvD_Job_Work_Item__c> it = this.iterable.iterator();
    while (it.hasNext()) {
      workItems.add(it.next());
    }

    this.execute(null, workItems);
    this.finish(null);
    return job;
  }


  /**
  * BvD Job Start logic
  * Sets BvD Job in Scheduled status, sets Execution Time to now
  * */
  public Iterable<BvD_Job_Work_Item__c> start(Database.BatchableContext bc) {
    this.job.Status__c = BvDJobs.STATUS_IN_PROGRESS;
    if(BVDUserPermissionCheck.permissionToUpdateSobject('BvD_Job__c', true,
                                                        new List<String>{'Status__c'})){
        update this.job;
    }else{
    	Logger.error('Not enough privileges to create/Update Job');
        throw new AuraHandledException('Not enough privileges to create/Update Job');
    }
    return this.iterable;
  }

  /**
  * Executes BvD Action for batch of records
  * */
  public void execute(Database.BatchableContext bc, List<BvD_Job_Work_Item__c> records) {
    Logger.start('BvDBatchJob', 'execute');
    BvDAction.Result result = BvDAction.getInstance(job.Type__c).execute(job, records);
    this.actionResults.add(result);
    Logger.stop();
  }

        public void finish(Database.BatchableContext bc) {
            for (BvDAction.Result result : actionResults) {
                if (result.status != BvDJobs.STATUS_IN_PROGRESS) {
                    job.Status__c = result.status;
                    job.Failure_Reason__c = result.error;
                }
            }
            
            /*If all results are in progress, then the job succeeded*/
            if (job.Status__c == BvDJobs.STATUS_IN_PROGRESS) {
                job.Status__c = BvDJobs.STATUS_SUCCESS;
            }
            
            this.job.Execution_Time__c = Datetime.now();
            System.debug('job: ' + JSON.serializePretty(job));
            if(BVDUserPermissionCheck.permissionToUpdateSobject('BvD_Job__c', true,new List<String>{'Execution_Time__c'})){
                upsert this.job;
            }else{
                    Logger.error('Not enough privileges to create/Update Job');
                    throw new AuraHandledException('Not enough privileges to create/Update Job');
                }
            }
        }