@IsTest
public class ConflictResolverCtrlTest {

  @TestSetup
  static void setup() {
    List<Account> lAccounts = new List<Account>();
    for (Integer i = 0; i < 10; i++) {
      lAccounts.add(new Account(
        Name = 'Test Account #' + i,
        BillingCity = 'Test City #' + i,
        BillingStreet = 'Test Street#' + i,
        BillingState = 'Test State #' + i,
        BillingCountry = 'USA',
        BillingPostalCode = '21-50' + i
      ));
    }
    insert lAccounts;

    BvD_Job__c job = new BvD_Job__c(
      Processing__c = BvDJobs.PROCESSING_AUTOMATIC,
      Status__c = BvDJobs.STATUS_ACTION_REQUIRED,
      Failure_Reason__c = BvDJobs.ERROR_MANUAL_FIELD_MAPPING,
      Object__c = 'Account',
      Type__c = BvDJobs.TYPE_UPDATE
    );
    insert job;

    List<BvD_Job_Work_Item__c> lWorkItems = new List<BvD_Job_Work_Item__c>();
    for (Account account : lAccounts) {
      lWorkItems.add(new BvD_Job_Work_Item__c(
        Status__c = BvDJobWorkItems.STATUS_ERROR,
        RecordId__c = account.Id,
        Record_Name__c = account.Name,
        BvD_Job__c = job.Id
      ));
    }
    insert lWorkItems;

    Integer i = 0;
    List<Work_Item_Field_Conflict__c> lConflicts = new List<Work_Item_Field_Conflict__c>();
    for (BvD_Job_Work_Item__c workItem : lWorkItems) {
      lConflicts.addAll(new List<Work_Item_Field_Conflict__c>{
        new Work_Item_Field_Conflict__c(
          Work_Item__c = workItem.Id,
          Choice__c = 'BvD',
          Field__c = 'BillingCity',
          BvD_Value__c = 'BvD BillingCity',
          Salesforce_Value__c = lAccounts.get(i).BillingCity),

        new Work_Item_Field_Conflict__c(
          Work_Item__c = workItem.Id,
          Choice__c = 'BvD',
          Field__c = 'BillingStreet',
          BvD_Value__c = 'BvD BillingStreet',
          Salesforce_Value__c = lAccounts.get(i).BillingStreet),

        new Work_Item_Field_Conflict__c(
          Work_Item__c = workItem.Id,
          Choice__c = 'BvD',
          Field__c = 'BillingState',
          BvD_Value__c = 'BvD BillingState',
          Salesforce_Value__c = lAccounts.get(i).BillingState),

        new Work_Item_Field_Conflict__c(
          Work_Item__c = workItem.Id,
          Choice__c = 'BvD',
          Field__c = 'BillingCountry',
          BvD_Value__c = 'BvD BillingCountry',
          Salesforce_Value__c = lAccounts.get(i).BillingCountry),

        new Work_Item_Field_Conflict__c(
          Work_Item__c = workItem.Id,
          Choice__c = 'BvD',
          Field__c = 'BillingPostalCode',
          BvD_Value__c = 'BvD BillingPostalCode',
          Salesforce_Value__c = lAccounts.get(i).BillingPostalCode)
      });
      ++i;
    }
    insert lConflicts;
  }

  @IsTest
  static void testBackendReturnsWrappedRecordsWithConflicts() {
    BvD_Job__c job = [SELECT ID FROM BvD_Job__c];
    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);


    Test.startTest();
    List<ConflictResolverCtrl.SyncedRecord> syncedRecords = ConflictResolverCtrl.getWorkItems(job.Id);
    Test.stopTest();


    System.assertEquals(workItems.size(), syncedRecords.size());
  }

  @IsTest
  static void testSalesforceRealtimeDataIsReturnedInConflict() {
    BvD_Job__c job = [SELECT ID FROM BvD_Job__c];
    List<BvD_Job_Work_Item__c> lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    List<Work_Item_Field_Conflict__c> lConflicts = new List<Work_Item_Field_Conflict__c>();

    for (BvD_Job_Work_Item__c wi : lWorkItems) {
      for (Work_Item_Field_Conflict__c conflict : wi.Field_Conflicts__r) {
        conflict.Salesforce_Value__c = 'Test';
        lConflicts.add(conflict);
      }
    }
    update lConflicts;


    Test.startTest();
    List<ConflictResolverCtrl.SyncedRecord> syncedRecords = ConflictResolverCtrl.getWorkItems(job.Id);
    Test.stopTest();


    for (ConflictResolverCtrl.SyncedRecord syncedRecord : syncedRecords) {
      for (ConflictResolverCtrl.SyncedField field : syncedRecord.fields) {
        System.assertNotEquals('Test', field.SFValue);
      }
    }
  }

  @IsTest
  static void testUpdateChangesWorkItems() {
    BvD_Job__c job = [SELECT ID FROM BvD_Job__c];


    Test.startTest();
    List<ConflictResolverCtrl.SyncedRecord> syncedRecords = ConflictResolverCtrl.getWorkItems(job.Id);

    for (ConflictResolverCtrl.SyncedRecord syncedRecord : syncedRecords) {
      for (ConflictResolverCtrl.SyncedField field : syncedRecord.fields) {
        field.choice = 'Salesforce';
      }
    }
    String jsonString = JSON.serialize(syncedRecords);
    ConflictResolverCtrl.updateRecords(JSON.serialize(syncedRecords));
    Test.stopTest();


    for (Work_Item_Field_Conflict__c fieldConflict : [SELECT ID, BvD_Field_Type__c, Choice__c FROM Work_Item_Field_Conflict__c]) {
      System.assertEquals('Salesforce', fieldConflict.Choice__c);
    }
  }
}