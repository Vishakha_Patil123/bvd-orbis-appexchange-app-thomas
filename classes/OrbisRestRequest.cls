/**
* Rest variant of Orbis Request
* */
public abstract with sharing class OrbisRestRequest extends OrbisRequest {

  public OrbisRestRequest(){
    super();
    this.setEndpoint(BvDSetupSettings.getRestURL());
  }

  public override HttpRequest toHttpRequest() {
    this.request.setBody(JSON.serialize(this));
    return request;
  }

  public override OrbisRequest setEndpoint(String endpoint) {
    return super.setEndpoint(endpoint + getPartialUrl());
  }

  public override OrbisResponse getOrbisResponse(HttpResponse response) {
    return OrbisRestResponse.getInstance(response, getOrbisResponseType());
  }
  protected abstract Type getOrbisResponseType();
  protected abstract String getPartialUrl();

  /*Override SOAP Specific methods*/
  protected override String getSOAPAction() {
    return null;
  }
  protected override OrbisSchema.RequestElement getRequestSchema() {
    return null;
  }

  protected override void setHeaders() {
    this.request.setHeader('Content-Type', 'application/json');
  }
}