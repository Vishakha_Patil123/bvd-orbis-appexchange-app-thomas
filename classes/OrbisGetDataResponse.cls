public with sharing class OrbisGetDataResponse extends OrbisResponse {
    private OrbisSchema.GetDataResponseElement responseElement = new OrbisSchema.GetDataResponseElement();

    public OrbisGetDataResponse(HttpResponse response) {
        super(response);
    }

    public Map<String,OrbisSchema.RecordElement> getRecordsMap() {
      Map<String,OrbisSchema.RecordElement> recordsMap = new Map<String, OrbisSchema.RecordElement>();
      for (OrbisSchema.RecordElement recordElement : responseElement.queryResults.records) {
        recordsMap.put(recordElement.selectionId, recordElement);
      }

      return recordsMap;
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseElement;
    }
}