/** * Created by nordine on 11/10/17. */

public with sharing class LogsSettingsController {

  public static void checkLogsSetupPrivileges() {

    if(!Schema.Log_Settings__c.getSObjectType().getDescribe().isCreateable()) {
      Logger.error('Not enough privileges to create log setup');
      throw new AuraHandledException('Not enough privileges to create log setup');
      Logger.stop();
  } else if(!Schema.Log_Settings__c.getSObjectType().getDescribe().isUpdateable()) {
      Logger.error('Not enough privileges to create log setup');
      throw new AuraHandledException('Not enough privileges to update log setup');
      Logger.stop();
    } else {
      Logger.info('Enough privileges for logs setup');
      Logger.stop();
    }

  }

  @AuraEnabled
  public static Log_Settings__c getLogSettings() {
    Logger.start('AdministrationController', 'getLogSettings');
    List<String> logSettingsFields = getLogsSettingsFields();
    CommonUtilities.checkReadAndThrowAura(Log_Settings__c.getSObjectType(), logSettingsFields);

    try {
      /*
      String query = String.format('SELECT {0} FROM Log_Settings__c ORDER BY CreatedDate LIMIT 1', new String[] {
        CommonUtilities.getLogFieldsQuery(logSettingsFields)
      });

      return Database.query(query);
      */

      return LogSettings.getSetting();
    } catch (Exception ex) {
      Logger.error(ex);
      /*
      return  new Log_Settings__c(
        Name = 'BvD - Logs Settings',
        Error__c = true
      );
      */
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static void insertLogsSettings(Boolean info, Boolean other, Boolean warn, Boolean error){
    Logger.start('LogsSettingsController', 'insertLogsSettings');

    Log_Settings__c logs = getLogSettings();
    checkLogsSetupPrivileges();

    logs.Info__c  = info;
    logs.Other__c = other;
    logs.Warn__c  = warn;
    logs.Error__c = error;

    try {
      upsert logs;
      if(Test.isRunningTest()){
        throw new AuraHandledException('Exception');
      }
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static List<String> getLogsSettingsFields() {
    return CommonUtilities.getFields('Log_Settings__c');
  }
}