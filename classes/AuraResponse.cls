public with sharing class AuraResponse {
  @AuraEnabled public String  error    { get; set; }
  @AuraEnabled public Boolean success  { get; set; }
  @AuraEnabled public Object response  { get; set; }

  private AuraResponse(String error, Boolean success, Object response) {
    this.error    = error;
    this.success  = success;
    this.response = response;
  }

  public static AuraResponse success(Object response) {
    return new AuraResponse(null, true, response);
  }

  public static AuraResponse error(String error) {
    return new AuraResponse(error, false, null);
  }
}