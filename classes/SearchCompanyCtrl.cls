public with sharing class SearchCompanyCtrl {

  @AuraEnabled
  public static Boolean percentageDisplay() {
  	return MatchingGeneralSettings.getSetting().Percentage__c;
  }

  @AuraEnabled
  public static ContainerCompanyUrl getContext() {
    return OrbisContainerController.getSessionId();
  }

  private static Map<String, String> getAccountIdByBvDId(Set<String> bvdIds) {
    Map<String, String> accountIdByBvDId = new Map<String, String> {};

    if(bvdIds != null && !bvdIds.isEmpty()) {
      for (Account account: [SELECT Id, BvD_Id__c FROM Account WHERE BvD_Id__c IN :bvdIds]) {
        accountIdByBvDId.put(account.BvD_Id__c, account.Id);
      }
    }

    return accountIdByBvDId;
  }

  @AuraEnabled
  public static SearchResult[] searchCompany(String serachInputJSON) {
    try {
      Logger.start('SearchCompanyCtrl', 'searchCompany');
      SearchInput input = (SearchInput) JSON.deserialize(serachInputJSON, SearchInput.class);

      OrbisAPI orbisAPI = new OrbisAPI();
      OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());

      OrbisRequest matchRequest = new OrbisMatchRequest()
        .setSessionID(openResponse.getSessionID())
        .setName(input.CompanyName)
        .setAddress(input.Address)
        .setPostCode(input.PostCode)
        .setCity(input.City)
        .setCountry(input.Country)
        .setPhoneOrFax(input.PhoneOrFax)
        .setEMailOrWebsite(input.EMailOrWebsite)
        .setState(input.State)
        .setNationalId(input.NationalId)
        //.setExclusionFlags(OrbisMatchRequest.ExclusionFlags.None);/*Until it's fixed by BVD*/
        .setExclusionFlags(input.ExclusionFlags);

      System.debug('match request');
      System.debug(matchRequest);
      System.debug(JSON.serializePretty(matchRequest.toHttpRequest().getBody()));
      System.debug(matchRequest.toHttpRequest().getBodyDocument());
      OrbisMatchResponse matchResponse = (OrbisMatchResponse) orbisAPI.send(matchRequest);


      Set<String> bvdIds = new Set<String> {};
      for (OrbisSchema.MatchResult matchResult : matchResponse.getMatchResults()) {
        bvdIds.add(matchResult.BvDID);
      }

      Map<String, String> accountIdByBvDId = getAccountIdByBvDId(bvdIds);

      List<SearchResult> searchResults = new List<SearchResult>();
      for (OrbisSchema.MatchResult matchResult : matchResponse.getMatchResults()) {
        SearchResult result = new SearchResult(matchResult);

        if(accountIdByBvDId.containsKey(matchResult.BvDID)) {
          result.SfID = accountIdByBvDId.get(matchResult.BvDID);
        }

        searchResults.add(result);
      }

      return searchResults;

    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.debug(ex.getStackTraceString());

      Logger.error(ex);
      Logger.flush();

      AuraHandledException auraEx = new AuraHandledException(ex.getMessage());
      auraEx.initCause(ex);
      throw auraEx;
    }
  }

  @AuraEnabled
  public static List<Country__mdt> queryCountries() {
    try {
      Logger.start('SearchCompanyCtrl', 'queryCountries');
      List<Country__mdt> countries = [
        SELECT MasterLabel, QualifiedApiName
        FROM Country__mdt
        ORDER BY QualifiedApiName
      ];

      Logger.stop();

      return countries;
    } catch (Exception ex) {

      Logger.error(ex);

      AuraHandledException auraEx = new AuraHandledException(ex.getMessage());
      auraEx.initCause(ex);
      throw auraEx;
    } finally {
      Logger.flush();
    }
  }

  @AuraEnabled
  public static BvD_Job__c insertRecord(String bvdId, String sObjectType) {
    SObjectDescribe describe = SObjectDescribe.getDescribe(sObjectType);
    String processing = Import_Setting__c.getInstance(sObjectType + '.Insert').Processing__c;

    BvD_Job__c job = BvDJobs.insertRecordsImmediatly(bvdId, describe.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    return job;
  }

  public class SearchInput {
    @AuraEnabled public String CompanyName;
    @AuraEnabled public String Address;
    @AuraEnabled public String PostCode;
    @AuraEnabled public String City;
    @AuraEnabled public String Country;
    @AuraEnabled public String PhoneOrFax;
    @AuraEnabled public String EMailOrWebsite;
    @AuraEnabled public String NationalId;
    @AuraEnabled public String State;
    @AuraEnabled public String Isin;
    @AuraEnabled public String BvD9;
    @AuraEnabled public List<String> ExclusionFlags = new List<String>{};
  }

  public class SearchResult {
    @AuraEnabled public String BvDID;
    @AuraEnabled public String SfID;
    @AuraEnabled public Decimal Score;
    @AuraEnabled public String CompanyName;
    @AuraEnabled public String Address;
    @AuraEnabled public String PostCode;
    @AuraEnabled public String City;
    @AuraEnabled public String Country;
    @AuraEnabled public String Region;
    @AuraEnabled public String PhoneOrFax;
    @AuraEnabled public String EMailOrWebsite;
    @AuraEnabled public String NationalId;
    @AuraEnabled public String Ticker;
    @AuraEnabled public String LegalForm;
    @AuraEnabled public String ConsolidationCode;
    @AuraEnabled public Boolean isStarred;


    public SearchResult(OrbisSchema.MatchResult matchResult) {
      this.BvDID = matchResult.BvDID;
      this.Score = Math.round(matchResult.Score * 100);
      this.CompanyName = matchResult.Name;
      this.Address = matchResult.Address;
      this.PostCode = matchResult.PostCode;
      this.City = matchResult.City;
      this.Country = matchResult.Country;
      this.Region = matchResult.Region;
      this.PhoneOrFax = matchResult.PhoneOrFax;
      this.EMailOrWebsite = matchResult.EMailOrWebsite;
      this.NationalId = matchResult.NationalId;
      this.Ticker = matchResult.Ticker;
      this.LegalForm = matchResult.LegalForm;
      this.ConsolidationCode = matchResult.ConsolidationCode;
      this.isStarred = matchResult.Hint_x == 'SelectedCandidate';
    }
  }
}