public with sharing class OrbisUpdateMyDataTableRequest extends OrbisRequest {
    private OrbisSchema.UpdateMyDataTableElement requestSchema
            = new OrbisSchema.UpdateMyDataTableElement();

    public override OrbisResponse getOrbisResponse(HttpResponse response) {
        return new OrbisUpdateMyDataTableResponse(response);
    }

    protected override String getSOAPAction() {
        return 'http://bvdep.com/webservices/UpdateMyDataTable';
    }

    protected override OrbisSchema.RequestElement getRequestSchema() {
        return requestSchema;
    }

    public OrbisUpdateMyDataTableRequest setSessionID(String value) {
        requestSchema.sessionHandle = value;
        return this;
    }

    public OrbisUpdateMyDataTableRequest setTable(String value) {
        requestSchema.table = value;
        return this;
    }

    public OrbisUpdateMyDataTableRequest setAppendData(Boolean value) {
        requestSchema.appendData = value;
        return this;
    }

    public OrbisUpdateMyDataTableRequest addValues(OrbisSchema.UpdateMyDataTableDataUploadDataRowElement uploadDataRow, List<OrbisSchema.UploadValue> uploadValues) {
        uploadDataRow.values.uploadValues.addAll(uploadValues);
        requestSchema.data.uploadDataRows.add(uploadDataRow);

        return this;
    }

    public OrbisUpdateMyDataTableRequest updateRecord(String bvdID, Map<String,String> valuesByFieldId) {
        OrbisSchema.UpdateMyDataTableDataUploadDataRowElement row = new OrbisSchema.UpdateMyDataTableDataUploadDataRowElement();
        row.BvDID = bvdID;

        for (String key : valuesByFieldId.keySet()){
            String value = valuesByFieldId.get(key);

            row.values.uploadValues.add(new OrbisSchema.UploadValue(key, value));
        }

        requestSchema.data.uploadDataRows.add(row);

        return this;
    }
}