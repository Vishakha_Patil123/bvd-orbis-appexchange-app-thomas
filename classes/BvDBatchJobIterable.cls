public abstract with sharing class BvDBatchJobIterable implements Iterable<BvD_Job_Work_Item__c> {
  public abstract Iterator<BvD_Job_Work_Item__c> iterator();


  /**
  * Iterates over records and creates Work Item for each
  * */
  public class RecordsIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private List<sObject> records;
    private Integer i, size;

    public RecordsIterable(List<sObject> records) {
      this.records = records;
      this.i = 0;
      this.size = records.size();
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      return this;
    }

    public Boolean hasNext() {
      return i < size;
    }

    public BvD_Job_Work_Item__c next() {
      sObject record = records.get(i++);

      return new BvD_Job_Work_Item__c(
        RecordId__c = record.Id,
        BvD_Id__c = (String) record.get('BvD_Id__c')
      );
    }
  }
  /**
  * Iterates over records and creates Work Item for each
  * */
  public class RecordsSubIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private List<sObject> records;
    private Integer i, size;

    public RecordsSubIterable(List<sObject> records) {
      this.records = records;
      this.i = 0;
      this.size = records.size();
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      return this;
    }

    public Boolean hasNext() {
      return i < size;
    }

    public BvD_Job_Work_Item__c next() {
      sObject record = records.get(i++);

      return new BvD_Job_Work_Item__c(
        RecordId__c = record.Id,
        BvD_Id__c = (String) record.get('BvD_Company_Id__c'),
        BvD_Sub_Id__c = (String) record.get('BvD_Id__c')
      );
    }
  }

  /**
  * Retries failed job work items
  * */
  public class RetryIterable extends BvDBatchJobIterable {
    private String jobId;

    public RetryIterable(Id jobId) {
      this.jobId = jobId;
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      List<String> retryStatuses = new List<String>{
        BvDJobWorkItems.STATUS_NOT_STARTED,
        BvDJobWorkItems.STATUS_ERROR
      };

      return (Iterator<BvD_Job_Work_Item__c>) Database.getQueryLocator(
        'SELECT ID, BvD_Id__c, BvD_Sub_Id__c, RecordId__c, Record_Name__c, Status__c,'
          + ' (SELECT ID, Salesforce_Value__c, BvD_Value__c, Choice__c, Field__c, BvD_Field_Type__c FROM Field_Conflicts__r)'
          + ' FROM BvD_Job_Work_Item__c'
          + ' WHERE BvD_Job__c = :jobId'
          + ' AND Status__c IN :retryStatuses'
      ).iterator();
    }
  }


  /**
  * Feeds Batch Jobs with sObject from Query Locator
  * */
  public class UpdateIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private String sobjectType;
    private transient Iterator<sObject> queryIterator;
    private Id lastProcessedRecordId;
    private List<String> bvdIds;
    private Integer i, size;

    public UpdateIterable(List<String> bvdIds, SObjectType sObjectType) {
      this(sobjectType);
      this.bvdIds = bvdIds;
    }
    public UpdateIterable(SObjectType sobjectType) {
      this.sobjectType = String.valueOf(sobjectType);
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
        if (queryIterator == null) getQueryLocator();

        return this;
    }

    public Boolean hasNext() {
      return queryIterator.hasNext();
    }

    public BvD_Job_Work_Item__c next() {
        sObject record = queryIterator.next();
        this.lastProcessedRecordId = record.Id;

        return new BvD_Job_Work_Item__c(
          RecordId__c = record.Id,
          Record_Name__c = (String) record.get('Name'),
          BvD_Id__c = (String) record.get('BvD_ID__c')
        );
    }

    private void getQueryLocator() {
      if(bvdIds == null || bvdIds.isEmpty()) {
        this.queryIterator = Database.query(
          'SELECT ID, BvD_ID__c, Name'
            + ' FROM ' + sobjectType
            + ' WHERE BvD_Id__c != null'
            + ' AND Unlinked_from_BvD__c = false'
            + ' AND ID > :lastProcessedRecordId'
            + ' ORDER BY ID ASC'
        ).iterator();
      } else {
        this.queryIterator = Database.query(
          'SELECT ID, BvD_ID__c, Name'
            + ' FROM ' + sobjectType
            + ' WHERE BvD_Id__c IN :bvdIds'
            + ' AND Unlinked_from_BvD__c = false'
            + ' ORDER BY ID ASC'
        ).iterator();
      }
    }
  }

  public class UpdateSubIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private String sobjectType;
    private transient Iterator<sObject> queryIterator;
    private Id lastProcessedRecordId;
    private List<String> bvdSubIds;
    private String bvdId;
    private Integer i, size;

    public UpdateSubIterable(String bvdId, List<String> bvdSubIds, SObjectType sobjectType) {
      this.sobjectType = String.valueOf(sobjectType);
      this.bvdId = bvdId;
      this.bvdSubIds = bvdSubIds;
      this.i = 0;
      this.size = bvdSubIds.size();
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      if (queryIterator == null) getQueryLocator();

      return this;
    }

    public Boolean hasNext() {
      return queryIterator.hasNext();
    }

    public BvD_Job_Work_Item__c next() {
      sObject record = queryIterator.next();
      this.lastProcessedRecordId = record.Id;

      return new BvD_Job_Work_Item__c(
        RecordId__c = record.Id,
        Record_Name__c = (String) record.get('Name'),
        /*
        BvD_Sub_Id__c = (String) record.get('BvD_Company_Id__c')
        BvD_Id__c = (String) record.get('BvD_Id__c'),
        */
        BvD_Sub_Id__c = (String) record.get('BvD_Id__c'),
        BvD_Id__c = (String) record.get('BvD_Company_Id__c')
      );
    }

    private void getQueryLocator() {
      if(bvdSubIds == null || bvdSubIds.isEmpty()) {
        this.queryIterator = Database.query(
          'SELECT ID, BvD_ID__c, BvD_Company_Id__c, Name'
            + ' FROM ' + sobjectType
            + ' WHERE BvD_Id__c != null AND BvD_Company_Id__c != null'
            + ' AND Unlinked_from_BvD__c = false'
            + ' AND ID > :lastProcessedRecordId'
            + ' ORDER BY ID ASC'
        ).iterator();
      } else {
        this.queryIterator = Database.query(
          'SELECT ID, BvD_ID__c, BvD_Company_Id__c, Name'
            + ' FROM ' + sobjectType
            + ' WHERE BvD_Id__c IN :bvdSubIds AND BvD_Company_Id__c = :bvdId'
            + ' AND Unlinked_from_BvD__c = false'
            + ' ORDER BY ID ASC'
        ).iterator();
      }
    }
  }

  /**
  * Iterates over list of BvD Ids and feeds Batch Job with sObjects
  * with filled in BvD ID field
  * */
  public class InsertIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private List<String> bvdIds;
    private Integer i, size;

    public InsertIterable(List<String> bvdIds) {
      this.bvdIds = bvdIds;
      this.i = 0;
      this.size = bvdIds.size();
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      return this;
    }

    public Boolean hasNext() {
      return i < size;
    }

    public BvD_Job_Work_Item__c next() {
      return new BvD_Job_Work_Item__c(
        BvD_Id__c = bvdIds.get(i++)
      );
    }
  }

  public class InsertSubIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private List<String> bvdSubIds;
    private String bvdId;
    private String lookupField;
    private Integer i, size;

    public InsertSubIterable(String bvdId, List<String> bvdSubIds, String lookupField) {
      this(bvdId, bvdSubIds);
      this.lookupField = lookupField;
    }
    public InsertSubIterable(String bvdId, List<String> bvdSubIds) {
      this.bvdId = bvdId;
      this.bvdSubIds = bvdSubIds;
      this.i = 0;
      this.size = bvdSubIds.size();
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      return this;
    }

    public Boolean hasNext() {
      return i < size;
    }

    public BvD_Job_Work_Item__c next() {

      return new BvD_Job_Work_Item__c(
        BvD_Id__c     = bvdId,
        BvD_Sub_Id__c = bvdSubIds.get(i++),
        Lookup_Field_Name__c = lookupField
      );
    }
  }


  /**
  * Iterates over Map of Ids and BvD Ids and feeds Batch job with sObjects
  * with filled in BVD ID field
  * */
  public class LinkIterable extends BvDBatchJobIterable implements Iterator<BvD_Job_Work_Item__c> {
    private Map<Id, String> mapBVDIds;
    private List<Id> ids;
    private Integer i, size;

    public LinkIterable(Map<Id, String> mapBVDIds) {
      this.mapBVDIds = mapBVDIds;
      this.ids = new List<Id>(mapBVDIds.keySet());
      this.i = 0;
      this.size = ids.size();
    }

    public override Iterator<BvD_Job_Work_Item__c> iterator() {
      return this;
    }

    public Boolean hasNext() {
      return i < size;
    }

    public BvD_Job_Work_Item__c next() {
      Id recordId = ids.get(i++);
      String bvdID = mapBVDIds.get(recordId);

      return new BvD_Job_Work_Item__c(
        RecordId__c = recordId,
        BvD_Id__c = bvdID
      );
    }
  }
}