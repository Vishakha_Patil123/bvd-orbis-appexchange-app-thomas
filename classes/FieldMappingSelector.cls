/**
* Selector Layer for  custom setting
* */
public with sharing class FieldMappingSelector {

  /**
  * Returns Mapped fields for given sObject
  * */
  public static List<Field_Mapping__c> getFieldMappingsForObject(String obj) {
    SecurityUtils.checkRead(Field_Mapping__c.getSObjectType(), new List<String>{
      'SObject__c',
      'SF_Field__c',
      'BvD_Field__c',
      'Master__c',
      'Action__c',
      'Excluded__c'
    });

    return [
      SELECT Id,
        SObject__c,
        SF_Field__c,
        BvD_Field__c,
        Master__c,
        Action__c,
        Excluded__c
      FROM Field_Mapping__c
      WHERE SObject__c = :obj
      AND Invalid__c = false
    ];
  }
}