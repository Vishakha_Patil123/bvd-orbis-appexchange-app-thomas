/**
* Selector layer for BVD Job Work Items
* */
public with sharing class BvDJobWorkItemsSelector {

  /**
  * Returns all Work Items for given Job and linked to given records.
  * */
  public static List<BvD_Job_Work_Item__c> getWorkItemsForRecords(Id jobId, List<String> recordIds) {
    SecurityUtils.checkRead(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'RecordId__c', 'Status__c'
    });

    return [
      SELECT Id,RecordId__c, Status__c
      FROM BvD_Job_Work_Item__c
      WHERE BvD_Job__c = :jobId
      AND RecordId__c IN:recordIds
    ];
  }

  /**
  * Returns all Work Items for given Job Id and of given statuses
  * */
  public static List<BvD_Job_Work_Item__c> getWorkItemsWithStatus(Id jobId, List<String> statuses) {
    SecurityUtils.checkRead(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'RecordId__c', 'Status__c'
    });

    return [
      SELECT Id,RecordId__c, Status__c
      FROM BvD_Job_Work_Item__c
      WHERE BvD_Job__c = :jobId
      AND Status__c IN :statuses
    ];
  }

  /**
  * Returns Ids of records referenced by Work Items of given statuses and job
  * */
  public static List<String> getRecordIdsWithStatus(Id jobId, List<String> statuses) {
    List<String> recordIds = new List<String>();
    for (BvD_Job_Work_Item__c wi : getWorkItemsWithStatus(jobId, statuses)) {
      recordIds.add(wi.RecordId__c);
    }

    return recordIds;
  }

  /**
  * Returns Map of Work Items mapped to Record Ids
  * */
  public static Map<String, BvD_Job_Work_Item__c> getWorkItemsByRecordIds(Id jobId, List<sObject> records) {
    List<String> ids = new List<String>();
    for (sObject record : records) ids.add(record.Id);

    return getWorkItemsByRecordIds(jobId, ids);
  }
  public static Map<String, BvD_Job_Work_Item__c> getWorkItemsByRecordIds(Id jobId, List<String> records) {
    Map<String, BvD_Job_Work_Item__c>workItemsByIds = new Map<String, BvD_Job_Work_Item__c>();

    for (BvD_Job_Work_Item__c wi : getWorkItemsForRecords(jobId, records)) {
      workItemsByIds.put(wi.RecordId__c, wi);
    }

    return workItemsByIds;
  }

  /**
  * Returns Work Items with Field Conflicts for given Job Id
  * */
  public static List<BvD_Job_Work_Item__c> getWorkItemsWithConflicts(Id JobID) {
    SecurityUtils.checkRead(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'RecordId__c', 'Status__c', 'Record_Name__c'
    });
    SecurityUtils.checkRead(Work_Item_Field_Conflict__c.getSObjectType(), new List<String>{
      'BvD_Value__c', 'Salesforce_Value__c', 'Choice__c', 'Field__c', 'Work_Item__c'
    });

    return [
      SELECT ID,
        Status__c,
        RecordId__c,
        Record_Name__c,

      (
        SELECT ID,
          BvD_Value__c,
          Salesforce_Value__c,
          Choice__c,
          Field__c,
          Work_Item__c
        FROM Field_Conflicts__r
      )
      FROM BvD_Job_Work_Item__c
      WHERE BvD_Job__c = :jobID
    ];
  }

  /**
* Returns Work Items with more than 1 Field Conflicts for given Job Id
* */
  public static List<BvD_Job_Work_Item__c> getWorkItemsWithAnyConflicts(Id JobID) {
    SecurityUtils.checkRead(BvD_Job_Work_Item__c.getSObjectType(), new List<String>{
      'RecordId__c', 'Status__c', 'Record_Name__c'
    });
    SecurityUtils.checkRead(Work_Item_Field_Conflict__c.getSObjectType(), new List<String>{
      'BvD_Value__c', 'Salesforce_Value__c', 'Choice__c', 'Field__c', 'Work_Item__c'
    });

    return [
      SELECT ID,
        Status__c,
        RecordId__c,
        Record_Name__c,

      (
        SELECT ID,
          BvD_Value__c,
          Salesforce_Value__c,
          Choice__c,
          Field__c,
          Work_Item__c
        FROM Field_Conflicts__r
      )
      FROM BvD_Job_Work_Item__c
      WHERE BvD_Job__c = :jobID
      AND ID IN (SELECT Work_Item__c FROM Work_Item_Field_Conflict__c)
    ];
  }

  public static List<BvD_Job_Work_Item__c> getWorkItemsByJob(Id jobId) {
    SecurityUtils.checkRead(Work_Item_Field_Conflict__c.getSObjectType(), new List<String>{
      'BvD_Value__c', 'Salesforce_Value__c', 'Choice__c', 'Field__c', 'Work_Item__c'
    });

    return [
      SELECT ID,
        Status__c,
        RecordId__c
      FROM BvD_Job_Work_Item__c
      WHERE BvD_Job__c = :jobId
    ];
  }
}