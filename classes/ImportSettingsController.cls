public with sharing class ImportSettingsController {

  public static void isSetupFullConfigured() {
    if(!BvDSetupSettings.isSSOConfigured()) {
      throw new AuraHandledException(System.Label.BvD_Admin_SSO_Missing);
    }

    if(String.isBlank(BvDSetupSettings.getDatasourceID())) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!BvDSetupSettings.isFieldMappingConfigured()) {
      throw new AuraHandledException(System.Label.BvD_Admin_FieldMapping_Missing);
    }
  }

    @AuraEnabled
    public static void setImportSettingConfigured() {
        Logger.start('AdministrationController', 'setImportSettingConfigured');

        isSetupFullConfigured();

        BvD_Setup__c setup = BvDSetupSettings.getSetting();
        setup.Import_Settings_Configured__c = true;

        try {
            upsert setup;
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Import_Setting__c[] getImportSettings() {
        Logger.start('AdministrationController', 'getImportSettings');

        isSetupFullConfigured();

        //TODO
        try{
            return [SELECT Id,Name,SObject__c,Type__c,Processing__c FROM Import_Setting__c ORDER BY SObject__c];
        }
        catch (Exception ex){
            System.debug(ex);
            throw new AuraHandledException(ex.getMessage());
        }
        finally{
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Import_Setting__c[] setImportSettings(Import_Setting__c[] importSettings) {
        Logger.start('AdministrationController', 'getImportSettings');

        isSetupFullConfigured();

        try {
            Import_Setting__c[] toInsert = new Import_Setting__c[] {};
            Import_Setting__c[] toUpdate = new Import_Setting__c[] {};

            for (Import_Setting__c importSetting : importSettings) {
                if(importSetting.Id != null) {
                    toUpdate.add(importSetting);
                } else {
                    toInsert.add(importSetting);
                }
            }

            if(!toInsert.isEmpty()) {
                insert toInsert;
            }

            if(!toUpdate.isEmpty()) {
                update toUpdate;
            }

            return getImportSettings();
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }


}