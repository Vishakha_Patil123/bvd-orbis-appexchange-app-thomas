@IsTest
public with sharing class UnlinkActionButtonCtrlTest {
  private final static String USERNAME = 'UnlinkActionButtonCtrlTest@bvd.unit.test.com';

  @TestSetup
  static void setup() {
    /*TODO: We need user with BvD User permission set*/
    User usr = TestUtilities.getSystemAdminUser(USERNAME);
    insert usr;

    System.runAs(usr) {
      insert new List<sObject>{
        new Account(
          Name = 'Test',
          BvD_Id__c = 'Test'
        ),
        new Contact(
          FirstName = 'John',
          LastName = 'Doe',
          BvD_Id__c = 'Test2'
        ),
        new Lead(
          BvD_Id__c = 'Test3',
          LastName = 'Doe',
          Company = 'Test Company'
        )
      };
    }
  }

  @IsTest
  static void testUnlinkAccount() {
    User usr = [SELECT ID FROM User Where Username = :USERNAME LIMIT 1];
    System.runAs(usr) {
      Account record = [
        SELECT ID,
          BvD_Id__c
        FROM Account
        LIMIT 1
      ];

      System.assertNotEquals(null, record.BvD_Id__c);


      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      UnlinkActionButtonCtrl.unlink(record.Id, null);
      Test.stopTest();


      record = [
        SELECT ID,
          BvD_Id__c
        FROM Account
        LIMIT 1
      ];
      System.assertEquals(null, record.BvD_Id__c);
    }
  }

  @IsTest
  static void testUnlinkContact() {
    User usr = [SELECT ID FROM User Where Username = :USERNAME LIMIT 1];
    System.runAs(usr) {
      Contact record = [
        SELECT ID,
          BvD_Id__c
        FROM Contact
        LIMIT 1
      ];

      System.assertNotEquals(null, record.BvD_Id__c);


      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      UnlinkActionButtonCtrl.unlink(record.Id, null);
      Test.stopTest();


      record = [
        SELECT ID,
          BvD_Id__c
        FROM Contact
        LIMIT 1
      ];
      System.assertEquals(null, record.BvD_Id__c);
    }
  }

  @IsTest
  static void testUnlinkLead() {
    User usr = [SELECT ID FROM User Where Username = :USERNAME LIMIT 1];
    System.runAs(usr) {
      Lead record = [
        SELECT ID,
          BvD_Id__c
        FROM Lead
        LIMIT 1
      ];

      System.assertNotEquals(null, record.BvD_Id__c);


      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      UnlinkActionButtonCtrl.unlink(record.Id, 'reason');
      Test.stopTest();


      record = [
        SELECT ID,
          BvD_Id__c,
          Reason_unlinked_from_BvD__c
        FROM Lead
        LIMIT 1
      ];
      System.assertEquals(null, record.BvD_Id__c);
      System.assertEquals('reason', record.Reason_unlinked_from_BvD__c);
    }
  }

  @IsTest
  static void testUnlinkLead_Error() {
    User usr = [SELECT ID FROM User Where Username = :USERNAME LIMIT 1];
    System.runAs(usr) {
      Lead record = [
        SELECT ID,
          BvD_Id__c
        FROM Lead
        LIMIT 1
      ];

      System.assertNotEquals(null, record.BvD_Id__c);


      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      try {
        UnlinkActionButtonCtrl.unlink(null, 'reason');
        System.assert(false);
      } catch (Exception ex) {
        System.assert(true);
      }
      Test.stopTest();
    }
  }
}