/**
* @description Abstract OrbisRequest class
* Each callout to Orbis must extend this abstract class
* and implement abstract method.
* Concrete class has to be linked with Soap Schema element from OrbisSchema class by
* implementing getRequestSchema method.
* Each request also has to be linked with corresponding response.
* */
public with sharing abstract class OrbisRequest {
    private transient OrbisSchema.EnvelopeElement envelope;
    protected transient HttpRequest request;

    public OrbisRequest() {
        this.envelope = new OrbisSchema.EnvelopeElement();
        this.request = new HttpRequest();
        this.request.setMethod('POST');
        this.request.setTimeout(40000);
        if(String.isNotBlank(BvDSetupSettings.getDatasourceWebserviceURL())){
        this.setEndpoint(BvDSetupSettings.getDatasourceWebserviceURL());
        }
        this.setHeaders();
    }

    protected virtual void setHeaders(){
      this.request.setHeader('Content-Type', 'text/xml');
      this.request.setHeader('SOAPAction', getSOAPAction());
    }

    public virtual HttpRequest toHttpRequest() {
        envelope.body.content = getRequestSchema();
        request.setBodyDocument(envelope.toDOMDocument());

        return request;
    }

    public virtual OrbisRequest setEndpoint(String endpoint) {
        this.request.setEndpoint(endpoint);
        return this;
    }


    public abstract OrbisResponse getOrbisResponse(HttpResponse response);
    protected abstract String getSOAPAction();
    protected abstract OrbisSchema.RequestElement getRequestSchema();
}