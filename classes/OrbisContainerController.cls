public with sharing class OrbisContainerController {

  public static final String IFRAMECREDENTIAL = 'Orbis_iFrame';

  @TestVisible
  private static NamedCredential getIFrameEndpoint() {
    return [
      SELECT DeveloperName, Endpoint
      FROM NamedCredential
      WHERE DeveloperName = :IFRAMECREDENTIAL
      LIMIT 1
    ];
  }

  @TestVisible
  private static Map<String, ContainerRecord> extractIds(ContainerRecord[] records) {
    Map<String, ContainerRecord> recordIds = new Map<String, ContainerRecord> {};

    if(records != null) {
      for (ContainerRecord record: records) {
        recordIds.put(record.id, record);
      }
    }

    return recordIds;
  }
  @TestVisible
  private static Map<String, ContainerRecord> extractIdsWithoutDuplicates(ContainerRecord[] records) {
    Map<String, ContainerRecord> recordIds = extractIds(records);

    for (ContainerRecord containerRecord : recordIds.values()) {
      if(String.isNotBlank(containerRecord.recordId)) {
        recordIds.remove(containerRecord.id);
      }
    }

    return recordIds;
  }
  @TestVisible
  private static Map<String, ContainerRecord> extractIdsDuplicates(ContainerRecord[] records) {
    Map<String, ContainerRecord> recordIds = extractIds(records);

    for (ContainerRecord containerRecord : recordIds.values()) {
      if(String.isBlank(containerRecord.recordId)) {
        recordIds.remove(containerRecord.id);
      }
    }

    return recordIds;
  }

  @TestVisible
  private static Map<String, String> extractCompanyNames(ContainerRecord[] records) {
    Map<String, String> companyNames = new Map<String, String> {};

    if(records != null) {
      for (ContainerRecord record: records) {
        companyNames.put(record.id, record.accountName.toLowerCase());
      }
    }

    return companyNames;
  }

  @TestVisible
  private static Map<String, String> extractContactNames(ContainerRecord[] records) {
    Map<String, String> contactNames = new Map<String, String> {};

    if(records != null) {
      for (ContainerRecord record: records) {
        contactNames.put(record.id, String.format('{0} {1}', new String[] {
          record.firstName, record.lastName
        }).toLowerCase());
      }
    }

    return contactNames;
  }

  @TestVisible
  private static BvD_Job_Work_Item__c[] getJobs(Set<String> jobIds) {
    BvD_Job_Work_Item__c[] jobs = new BvD_Job_Work_Item__c[] {};

    // TODO add fields
    jobs.addAll([
      SELECT
        Id, Name, BvD_Job__c, BvD_Id__c, BvD_Sub_Id__c, Status__c, RecordId__c, Record_Name__c,
        BvD_Job__r.Object__c, BvD_Job__r.Processing__c, BvD_Job__r.Status__c, BvD_Job__r.Failure_Reason__c,
        BvD_Job__r.Type__c, BvD_Job__r.Name
      FROM BvD_Job_Work_Item__c
      WHERE BvD_Job__c IN :jobIds
    ]);

    return jobs;
  }

  public static void isSetupFullConfigured() {
    if(!BvDSetupSettings.isSSOConfigured()) {
      throw new AuraHandledException(System.Label.BvD_Admin_SSO_Missing);
    }

    if(String.isBlank(BvDSetupSettings.getDatasourceID())) {
      throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
    }

    if(!BvDSetupSettings.isFieldMappingConfigured()) {
      throw new AuraHandledException(System.Label.BvD_Admin_FieldMapping_Missing);
    }

    if(!BvDSetupSettings.isImportSettingsConfigured()) {
      throw new AuraHandledException(System.Label.BvD_Admin_ImportSettings_Missing);
    }

    if(!BvDSetupSettings.isBatchJobSettingsConfigured()) {
      throw new AuraHandledException(System.Label.BvD_Admin_Confirm_Batchjob_Settings);
    }
  }

  public static void isOriginValid(String origin) {

    NamedCredential iFrameEndpoint  = getIFrameEndpoint();
    ContainerCompanyUrl companyUrl  = new ContainerCompanyUrl(iFrameEndpoint.Endpoint, null);

    if(origin == null || !Pattern.matches(companyUrl.baseUrl, origin)) {
      throw new AuraHandledException(System.Label.BvD_Orbis_Container_Forbidden_Origin.replace('{0}', origin));
    }
  }


  @TestVisible
  private static ContainerRecord[] attachWorksToRecords(ContainerRecord[] records, BvD_Job_Work_Item__c[] works) {
    ContainerRecord[] attachedRecords = new ContainerRecord[] {};

    Map<String, BvD_Job_Work_Item__c> workByOrbisId = new Map<String, BvD_Job_Work_Item__c> {};

    for (BvD_Job_Work_Item__c work : works) {
      workByOrbisId.put(work.BvD_Id__c, work);
    }

    for (ContainerRecord record : records) {
      record.work = workByOrbisId.get(record.id);
      attachedRecords.add(record);
    }

    return attachedRecords;
  }


  @TestVisible
  private static ContainerRecord[] attachWorksToRecords(ContainerRecord[] records, BvD_Job__c job) {
    ContainerRecord[] attachedRecords = new ContainerRecord[] {};

    Map<String, BvD_Job_Work_Item__c> workByOrbisId = new Map<String, BvD_Job_Work_Item__c> {};

    if(job.BvD_Job_Work_Items__r != null) {
      for (BvD_Job_Work_Item__c work : job.BvD_Job_Work_Items__r) {
        workByOrbisId.put(work.BvD_Id__c, work);
      }
    }

    for (ContainerRecord record : records) {
      record.work = workByOrbisId.get(record.id);
      record.job  = job;

      if(record.contacts != null) {
        for(ContainerRecord containerContact: record.contacts) {
          containerContact.accountRecordId = record.work.RecordId__c;
        }
      }

      attachedRecords.add(record);
    }

    return attachedRecords;
  }

  @TestVisible
  private static ContainerRecord[] unvalidExistingCompanies(
    Map<String, ContainerRecord[]> companies
  ) {
    for(Account account: [
      SELECT Id, Name, BvD_Id__c
      FROM Account
      WHERE Name IN :companies.keySet()
    ]) {
      for(ContainerRecord company: companies.get(account.Name.toLowerCase())) {
        company.recordId = account.Id;
      }
    }

    ContainerRecord[] validateCompanies = new ContainerRecord[] {};

    for(String key: companies.keySet()) {
      for(ContainerRecord record: companies.get(key)) {
        validateCompanies.add(record);
      }
    }

    return validateCompanies;
  }

  @TestVisible
  private static ContainerRecord[] unvalidExistingContacts(
    Map<String, ContainerRecord[]> contacts
  ) {
    for(Contact contact: [
      SELECT Id, Name, BvD_Id__c
      FROM Contact
      WHERE Name IN :contacts.keySet()
    ]) {
      for(ContainerRecord containerContact: contacts.get(contact.Name.toLowerCase())) {
        containerContact.recordId = contact.Id;
      }
    }

    ContainerRecord[] validateContacts = new ContainerRecord[] {};

    for(String key: contacts.keySet()) {
      for(ContainerRecord record: contacts.get(key)) {
        validateContacts.add(record);
      }
    }

    return validateContacts;
  }

  @TestVisible
  private static ContainerRecord[] unvalidExistingLeads(
    Map<String, ContainerRecord[]> leads
  ) {
    Set<String> companyNames = new Set<String> {};
    Set<String> contactNames = new Set<String> {};

    for (String companyOrName : leads.keySet()) {

      // Company Name / Contact Name = 'John Doe||Google'
      // Company Name                = '||Google'
      String[] split = companyOrName.split('\\|\\|');

      if(split.size() > 1) {
        // Account/Contact
        contactNames.add(split[0]);
        companyNames.add(split[1]);

      } else if(split.size() > 0) {
        // Account
        companyNames.add(split[1]);
      }
    }

    for(Lead lead: [
      SELECT Id, Name, FirstName, LastName, Company, BvD_Id__c
      FROM Lead
      WHERE (Company IN :companyNames AND Name IN :contactNames) OR (Company IN :companyNames) ])
    {

      String key = String.format('{0}||{1}', new String [] {
        lead.Name, lead.Company
      }).toLowerCase();

      String keyCompany = String.format('||{0}', new String [] {
        lead.Company
      }).toLowerCase();

      if(leads.containsKey(key)) {
        for(ContainerRecord containerLead: leads.get(key)) {

          containerLead.recordId = lead.Id;

          for(ContainerRecord containerContact: containerLead.contacts) {
            if(lead.FirstName.equalsIgnoreCase(containerContact.firstName) && lead.LastName.equalsIgnoreCase(containerContact.lastName)) {
              containerContact.recordId = lead.Id;
            }
          }
        }
      } else if(leads.containsKey(keyCompany)) {
        for(ContainerRecord containerLead: leads.get(keyCompany)) {
          containerLead.recordId = lead.Id;
        }
      }
    }

    ContainerRecord[] validateLeads = new ContainerRecord[] {};

    Set<String> recordIds = new Set<String> {};

    for(String key: leads.keySet()) {
      for(ContainerRecord record: leads.get(key)) {
        if(!recordIds.contains(record.recordId)) {
          validateLeads.add(record);
          recordIds.add(record.recordId);
        }
      }
    }

    return validateLeads;
  }

  /*
   * Public aura functions
   */

  @AuraEnabled
  public static ContainerCompanyUrl getSessionId() {

    isSetupFullConfigured();

    try {
      NamedCredential iFrameEndpoint  = getIFrameEndpoint();
      OrbisAPI api                    = new OrbisAPI();
      OrbisOpenRequest request        = new OrbisOpenRequest();
      OrbisOpenResponse response      = (OrbisOpenResponse) api.send(request);

      return new ContainerCompanyUrl(iFrameEndpoint.Endpoint, response.getSessionID());

    } catch (QueryException qe) {
      throw new AuraHandledException(System.Label.Orbis_Endpoint_Not_Found);
    } catch (OrbisResponseException oe) {
      throw new AuraHandledException(System.Label.Orbis_Connection_Failed);
    } catch (Exception ex) {
      throw new AuraHandledException(ex.getMessage());
    }
  }

  @AuraEnabled
  public static Map<String, ContainerRecord[]> unvalidExistingRecords(
    String newCompanies,
    String newLeads
  ) {

    ContainerRecord[] companies = newCompanies != null
      ? (ContainerRecord[]) JSON.deserialize(newCompanies, List<ContainerRecord>.class)
      : null;

    ContainerRecord[] leads     = newLeads != null
      ? (ContainerRecord[]) JSON.deserialize(newLeads, List<ContainerRecord>.class)
      : null;

    return unvalidExistingRecords(companies, leads);
  }

  private static Map<String, ContainerRecord[]> unvalidExistingRecords(
    ContainerRecord[] companies,
    ContainerRecord[] leads
  ) {
    Map<String, ContainerRecord[]> validateRecords = new Map<String, ContainerRecord[]> {};

    if(companies != null && !companies.isEmpty()) {
      Map<String, ContainerRecord[]> contactsByName   = new Map<String, ContainerRecord[]> {};
      Map<String, ContainerRecord[]> companiesByName  = new Map<String, ContainerRecord[]> {};

      for(ContainerRecord containerCompany: companies) {
        if(containerCompany.contacts != null && !containerCompany.contacts.isEmpty()) {
          for(ContainerRecord containerContact: containerCompany.contacts) {
            String key = String.format('{0} {1}', new String[] {
              containerContact.firstName,
              containerContact.lastName
            }).toLowerCase();

            if(!contactsByName.containsKey(key)) {
              contactsByName.put(key, new ContainerRecord[] {});
            }

            contactsByName.get(key).add(containerContact);
          }
        }

        String key = String.format('{0}', new String[] { containerCompany.accountName }).toLowerCase();

        if(!companiesByName.containsKey(key)) {
          companiesByName.put(key, new ContainerRecord[] {});
        }

        companiesByName.get(key).add(containerCompany);
      }

      unvalidExistingContacts(contactsByName);
      unvalidExistingCompanies(companiesByName);
      validateRecords.put('companies', companies);


    }

    if(leads != null && !leads.isEmpty()) {
      Map<String, ContainerRecord[]> leadsByNameAndCompany = new Map<String, ContainerRecord[]> {};

      for (ContainerRecord containerLead: leads) {

        // If there is a contact, find the related Lead (Company Name/Contact Name)
        if(containerLead.contacts != null && !containerLead.contacts.isEmpty()) {

          for(ContainerRecord containerContact: containerLead.contacts) {
            String key = String.format('{0} {1}||{2}', new String[] {
              containerContact.firstName, containerContact.lastName, containerLead.accountName
            }).toLowerCase();

            if(!leadsByNameAndCompany.containsKey(key)) {
              leadsByNameAndCompany.put(key, new ContainerRecord[] {});
            }

            leadsByNameAndCompany.get(key).add(containerLead);
          }
          // Related Lead (Company Name) then
        } else {
          String key = String.format('||{0}', new String[] {
            containerLead.accountName
          }).toLowerCase();

          if(!leadsByNameAndCompany.containsKey(key)) {
            leadsByNameAndCompany.put(key, new ContainerRecord[] {});
          }

          leadsByNameAndCompany.get(key).add(containerLead);
        }
      }
      unvalidExistingLeads(leadsByNameAndCompany);
      validateRecords.put('leads', leads);
    }

    return validateRecords;

  }

  @AuraEnabled
  public static List<Map<String, Object>> importContacts(
    String newCompanies,
    String origin
  ) {
    return importContacts(
      (List<ContainerRecord>) JSON.deserialize(newCompanies, List<ContainerRecord>.class),
      origin
    );
  }

  private static List<Map<String, Object>> importContacts(
    List<ContainerRecord> companies,
    String origin
  ) {
    isSetupFullConfigured();
    isOriginValid(origin);
    unvalidExistingRecords(companies, null);

    List<Map<String, Object>> jobList = new List<Map<String, Object>> {};

    for (ContainerRecord containerRecord : companies) {

      if(containerRecord.contacts != null) {
        for (ContainerRecord containerContact: containerRecord.contacts) {
          if(containerContact.recordId == null) {
            BvD_Job__c insertJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.insertRecordsBatch(
              containerContact.accountId,
              new List<String>{ containerContact.id},
              Contact.SObjectType,
              BvDJobs.PROCESSING_AUTOMATIC
            ));

            jobList.add(
              new Map<String, Object> {
                'job'     => insertJob,
                'records' => attachWorksToRecords(new ContainerRecord[] { containerContact }, insertJob)
              }
            );
          } else {
            BvD_Job__c updateJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.updateRecordsBatch(
              containerContact.accountId,
              new List<String>{ containerContact.id},
              Contact.SObjectType,
              BvDJobs.PROCESSING_AUTOMATIC
            ));

            jobList.add(
              new Map<String, Object> {
                'job'     => updateJob,
                'records' => attachWorksToRecords(new ContainerRecord[] { containerContact }, updateJob)
              }
            );
          }
        }
      }

    }

    return jobList;
  }
  @AuraEnabled
  public static List<Map<String, Object>> importLeads(
    String newCompanies,
    String origin
  ) {
    return importLeads(
      (List<ContainerRecord>) JSON.deserialize(newCompanies, List<ContainerRecord>.class),
      origin
    );
  }

  private static List<Map<String, Object>> importLeads(
    List<ContainerRecord> companies,
    String origin
  ) {
    isSetupFullConfigured();
    isOriginValid(origin);
    unvalidExistingRecords(null, companies);

    List<Map<String, Object>> jobList = new List<Map<String, Object>> {};

    for (ContainerRecord containerRecord : companies) {

      if(containerRecord.contacts != null) {
        for (ContainerRecord containerContact: containerRecord.contacts) {
          if(containerContact.recordId == null) {
            BvD_Job__c insertJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.insertRecordsBatch(
              containerContact.accountId,
              new List<String>{ containerContact.id },
              Lead.SObjectType,
              BvDJobs.PROCESSING_AUTOMATIC
            ));

            jobList.add(
              new Map<String, Object> {
                'job'     => insertJob,
                'records' => attachWorksToRecords(new ContainerRecord[] { containerContact }, insertJob)
              }
            );
          } else {
            BvD_Job__c updateJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.updateRecordsBatch(
              containerContact.accountId,
              new List<String>{ containerContact.id},
              Contact.SObjectType,
              BvDJobs.PROCESSING_AUTOMATIC
            ));

            jobList.add(
              new Map<String, Object> {
                'job'     => updateJob,
                'records' => attachWorksToRecords(new ContainerRecord[] { containerContact }, updateJob)
              }
            );
          }
        }
      } else {
        BvD_Job__c insertJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.insertRecordsBatch(
          new List<String>{ containerRecord.id },
          Lead.SObjectType,
          BvDJobs.PROCESSING_AUTOMATIC
        ));

        jobList.add(
          new Map<String, Object> {
            'job'     => insertJob,
            'records' => attachWorksToRecords(new ContainerRecord[] { containerRecord }, insertJob)
          }
        );
      }
    }

    return jobList;
  }

  @AuraEnabled
  public static List<Map<String, Object>> importCompanies(
    String newCompanies,
    String origin
  ) {
    return importCompanies(
      (List<ContainerRecord>) JSON.deserialize(newCompanies, List<ContainerRecord>.class),
      origin
    );
  }

  private static List<Map<String, Object>> importCompanies(
    List<ContainerRecord> companies,
    String origin
  ) {
    isSetupFullConfigured();
    isOriginValid(origin);
    unvalidExistingRecords(companies, null);

    List<Map<String, Object>> jobList = new List<Map<String, Object>> {};

    System.debug('import companies');
    System.debug(companies);
    Map<String, ContainerRecord> insertAccountIds = extractIdsWithoutDuplicates(companies);
    System.debug(insertAccountIds);
    Map<String, ContainerRecord> updateAccountIds = extractIdsDuplicates(companies);
    System.debug(updateAccountIds);

    if(!insertAccountIds.isEmpty()) {
      BvD_Job__c insertJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.insertRecordsBatch(
        new List<String> (insertAccountIds.keySet()), Account.SObjectType, BvDJobs.PROCESSING_AUTOMATIC
      ));

      jobList.add(
        new Map<String, Object> {
          'job'     => insertJob,
          'records' => attachWorksToRecords(insertAccountIds.values(), insertJob)
        }
      );
    }
    if(!updateAccountIds.isEmpty()) {
      BvD_Job__c updateJob = BvDJobsSelector.getJobWithWorkItemsById(BvDJobs.updateRecordsBatch(
        new List<String> (updateAccountIds.keySet()), Account.SObjectType, BvDJobs.PROCESSING_AUTOMATIC
      ));

      jobList.add(
        new Map<String, Object> {
          'job'     => updateJob,
          'records' => attachWorksToRecords(updateAccountIds.values(), updateJob)
        }
      );
    }

    return jobList;
  }


  @AuraEnabled
  public static List<Map<String, Object>> processRecords(
    String newCompanies,
    String newContacts,
    String newLeads,
    String origin
  ) {

    isSetupFullConfigured();
    isOriginValid(origin);

    ContainerRecord[] companies = (ContainerRecord[]) JSON.deserialize(newCompanies, List<ContainerRecord>.class);
    ContainerRecord[] contacts  = (ContainerRecord[]) JSON.deserialize(newContacts, List<ContainerRecord>.class);
    ContainerRecord[] leads     = (ContainerRecord[]) JSON.deserialize(newLeads, List<ContainerRecord>.class);

    Map<String, ContainerRecord> accountIds = extractIds(companies);
    Map<String, ContainerRecord> contactIds = extractIds(contacts);
    Map<String, ContainerRecord> leadIds    = extractIds(leads);

    try {
      List<Map<String, Object>> jobs  = new List<Map<String, Object>> {};

      if(!accountIds.isEmpty()) {
        BvD_Job__c job = BvDJobs.insertRecordsImmediatly(
        new List<String> (accountIds.keySet()), Account.SObjectType, BvDJobs.PROCESSING_AUTOMATIC
        );

        jobs.add(
          new Map<String, Object> {
            'job'     => job,
            'records' => attachWorksToRecords(companies, job)
          }
        );
      }

      if(!contactIds.isEmpty()) {
        BvD_Job__c job = BvDJobs.insertRecordsImmediatly(
          new List<String> (contactIds.keySet()), Contact.SObjectType, BvDJobs.PROCESSING_AUTOMATIC
        );

        jobs.add(
          new Map<String, Object> {
            'job'     => job,
            'records' => attachWorksToRecords(contacts, job)
          }
        );
      }

      if(!leadIds.isEmpty()) {
        BvD_Job__c job = BvDJobs.insertRecordsImmediatly(
          new List<String> (leadIds.keySet()), Lead.SObjectType, BvDJobs.PROCESSING_AUTOMATIC
        );

        jobs.add(
          new Map<String, Object> {
            'job'     => job,
            'records' => attachWorksToRecords(leads, job)
          }
        );
      }

      return jobs;

    } catch (Exception ex) {
      throw new AuraHandledException(ex.getMessage());
    }

  }

  private static List<Map<String, Object>> refreshJobsInformation(List<ContainerRecord> records) {
    Set<String> jobIds                              = new Set<String> {};
    Map<String, List<ContainerRecord>> recordsByJob = new Map<String, List<ContainerRecord>> {};

    for (ContainerRecord record : records) {
      jobIds.add(record.job.Id);

      if(!recordsByJob.containsKey(record.job.Id)) {
        recordsByJob.put(record.job.Id, new List<ContainerRecord> {});
      }

      recordsByJob.get(record.job.Id).add(record);
    }

    Map<Id, BvD_Job__c> refreshedJobs = BvDJobsSelector.getJobsWithWorkItemsById(jobIds);

    List<Map<String, Object>> information = new List<Map<String, Object>> {};

    for (String jobId : recordsByJob.keySet()) {
      List<ContainerRecord> jobRecords = recordsByJob.get(jobId);
      for (ContainerRecord jobRecord : jobRecords) {
        jobRecord.job = refreshedJobs.get(jobId);
      }

      information.add(new Map<String, Object> {
        'job' => refreshedJobs.get(jobId),
        'records' => attachWorksToRecords(jobRecords, refreshedJobs.get(jobId))
      });
    }

    return information;
  }

  @AuraEnabled
  public static List<Map<String, Object>> refreshJobsInformation(String records) {
    Logger.start('OrbisContainerController', 'refreshJobsInformation');
    try {
      return refreshJobsInformation(
        (List<ContainerRecord>) JSON.deserialize(records, Type.forName('List<ContainerRecord>'))
      );
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.flush();
    }
  }

  @AuraEnabled
  public static BvD_Job_Work_Item__c[] refreshJobs(String[] jobIds) {
    Logger.start('OrbisContainerController', 'refreshJobsInformation');
    try {
      return getJobs(new Set<String>(jobIds));
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.flush();
    }
  }
}