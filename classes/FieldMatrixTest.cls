@IsTest
public with sharing class FieldMatrixTest {

  @IsTest
  static void testMappingIsNotAvailableWhenSObjectDoesNotExist() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('TestObject', 'TestField', 'String');

    System.assertEquals(FieldMatrix.STATUS_NOT_AVAILABLE, matrixCrossSection.availability);
    System.assertEquals(false, matrixCrossSection.isAvailable());
    System.assertEquals(false, FieldMatrix.isMapable('TestObject', 'TestField', 'String'));
    System.assertEquals(Label.BvD_FieldMatrix_Warning_SObjectNotAvailable, matrixCrossSection.warning);
  }

  @IsTest
  static void testMappingIsNotAvailableWhenFieldDoesNotExist() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Account', 'TestField', 'String');

    System.assertEquals(FieldMatrix.STATUS_NOT_AVAILABLE, matrixCrossSection.availability);
    System.assertEquals(false, matrixCrossSection.isAvailable());
    System.assertEquals(false, FieldMatrix.isMapable('Account', 'TestField', 'String'));
    System.assertEquals(Label.BvD_FieldMatrix_Warning_SFFieldNotAvailable, matrixCrossSection.warning);
  }

  @IsTest
  static void testMappingIsNotAvailableWhenFieldIsAutonumber() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Case', 'CaseNumber', 'String');

    System.assertEquals(FieldMatrix.STATUS_NOT_AVAILABLE, matrixCrossSection.availability);
    System.assertEquals(false, matrixCrossSection.isAvailable());
    System.assertEquals(false, FieldMatrix.isMapable('Case', 'CaseNumber', 'String'));
    System.assertEquals(Label.BvD_FieldMatrix_Warning_SFFieldAutonumber, matrixCrossSection.warning);
  }

  @IsTest
  static void testMappingIsNotAvailableWhenFieldIsLookup() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Contact', 'AccountId', 'String');

    System.assertEquals(FieldMatrix.STATUS_NOT_AVAILABLE, matrixCrossSection.availability);
    System.assertEquals(false, matrixCrossSection.isAvailable());
    System.assertEquals(false, FieldMatrix.isMapable('Contact', 'AccountId', 'String'));
  }

  @IsTest
  static void testStringToStringMappingIsAvailableWithTruncationWarning() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Account', 'BillingStreet', 'String');

    System.assertEquals(FieldMatrix.STATUS_WARNING, matrixCrossSection.availability);
    System.assertEquals(true, matrixCrossSection.isAvailable());
    System.assertEquals(true, FieldMatrix.isMapable('Account', 'BillingStreet', 'String'));
    System.assertEquals(FieldMatrix.WARNING_TRUNCATION, matrixCrossSection.warning);
  }

  @IsTest
  static void testStringToCheckboxMappingIsAvailableWithBooleanWarning() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Lead', 'HasOptedOutOfFax', 'String');

    //System.assertEquals(FieldMatrix.STATUS_WARNING, matrixCrossSection.availability);
    //System.assertEquals(true, matrixCrossSection.isAvailable());
    //System.assertEquals(true, FieldMatrix.isMapable('Lead', 'HasOptedOutOfFax', 'String'));
    //System.assertEquals(FieldMatrix.WARNING_BOOLEAN, matrixCrossSection.warning);
  }

  @IsTest
  static void testDateToCheckboxMappingIsError() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Lead', 'HasOptedOutOfFax', 'Date');

    //System.assertEquals(FieldMatrix.STATUS_ERROR, matrixCrossSection.availability);
    System.assertEquals(false, matrixCrossSection.isAvailable());
    System.assertEquals(false, FieldMatrix.isMapable('Lead', 'HasOptedOutOfFax', 'Date'));
  }

  @IsTest
  static void testStringToPicklistMappingIsAvailableWithRestrictedPicklistWarning() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Lead', 'LeadSource', 'String');

    System.assertEquals(FieldMatrix.STATUS_WARNING, matrixCrossSection.availability);
    System.assertEquals(true, matrixCrossSection.isAvailable());
    System.assertEquals(true, FieldMatrix.isMapable('Lead', 'LeadSource', 'String'));
    System.assertEquals(FieldMatrix.WARNING_PICKLIST, matrixCrossSection.warning);
  }

  @IsTest
  static void testMappingOutsideOfMatrixIsNotAvailable() {
    FieldMatrix.CrossSection matrixCrossSection = FieldMatrix.getCrossSection('Account', 'Name', 'OtherType');

    System.assertEquals(FieldMatrix.STATUS_NOT_AVAILABLE, matrixCrossSection.availability);
    System.assertEquals(false, matrixCrossSection.isAvailable());
    System.assertEquals(false, FieldMatrix.isMapable('Account', 'Name', 'OtherType'));
    System.assertEquals(Label.BvD_FieldMatrix_Warning_MappingNotAvailable, matrixCrossSection.warning);
  }
}