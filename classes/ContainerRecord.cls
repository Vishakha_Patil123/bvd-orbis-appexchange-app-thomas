public with sharing class ContainerRecord {
  /*
  Common attributes
   */
  @AuraEnabled
  public String id { get; set; }
  @AuraEnabled
  public BvD_Job_Work_Item__c work { get; set; }
  @AuraEnabled
  public BvD_Job__c job { get; set; }
  @AuraEnabled
  public String sObjectName { get; set; }
  @AuraEnabled
  public String recordId { get; set; }

  /*
  Companies attributes
   */
  @AuraEnabled
  public String accountName { get; set; }
  @AuraEnabled
  public ContainerRecord[] contacts { get; set; }

  /*
  Contacts attributes
   */
  @AuraEnabled
  public String firstName { get; set; }
  @AuraEnabled
  public String lastName { get; set; }
  @AuraEnabled
  public String accountId { get; set; }
  @AuraEnabled
  public String accountRecordId { get; set; }
}