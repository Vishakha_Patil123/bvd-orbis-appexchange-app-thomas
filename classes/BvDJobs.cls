/**
* Service class for BvD Jobs
* Has Interface to execute a batch BvD Job over passed records, or schedule job execution
* Batch Jobx can be executed async (default) or immediately for smaller scopes.
* */
public with sharing class BvDJobs {
  public final static String
    STATUS_SUCCESS = 'Success',
    STATUS_ERROR = 'Error',
    STATUS_SCHEDULED = 'Scheduled',
    STATUS_ACTION_REQUIRED = 'Action Required',
    STATUS_IN_PROGRESS = 'In progress',
    PROCESSING_MANUAL = 'Manual',
    PROCESSING_AUTOMATIC = 'Automatic',
    PROCESSING_DRY_RUN = 'Dry Run',
    ERROR_FIELD_MAPPING = 'Field Mapping Conflict',
    ERROR_INVALID_FIELD_MAPPING = 'Invalid Field Mapping',
    ERROR_AUTH = 'Orbis Authorization Error',
    ERROR_SERVICE = 'Service Error',
    ERROR_EXECUTION = 'Execution Error',
    ERROR_MANUAL_FIELD_MAPPING = 'Manual Field Mapping',
    ERROR_MANUAL_START = 'Manual Start',
    ERROR_VALUE_ASSIGNMENT = 'Value assignment Error',
    ERROR_DML = 'DML Error',
    TYPE_LINK = 'Link',
    TYPE_UNLINK = 'Unlink',
    TYPE_INSERT = 'Insert',
    TYPE_UPDATE = 'Update';


  /**
  * Inserts and returns new BVD Job for given sObject and Job Type
  * */
  public static BvD_Job__c newBvDJob(SObjectType sobjectType, String jobType, String processingType) {
    return newBvDJob(String.valueOf(sobjectType), jobType, processingType);
  }
  public static BvD_Job__c newBvDJob(String sobjectType, String jobType, String processingType) {
    BvD_Job__c job = new BvD_Job__c(
      Object__c = sObjectType,
      Status__c = STATUS_SCHEDULED,
      Type__c = jobType,
      Processing__c = processingType,
      Execution_Time__c = null
    );
    return job;
  }

  /**
  * Constructs Batch Job which will run over given records and execute BvD Action on them
  * */
  public static BvDBatchJob createBatchJob(List<sObject> records, String action, String processingType) {
    SObject record          = records.get(0);
    SObjectType sObjectType = record.getSObjectType();
    Set<String> fields      = sObjectType.getDescribe().fields.getMap().keySet();

    if(fields.contains('BvD_Company_Id__c'.toLowerCase())) {
      return new BvDBatchJob(
        BvDJobs.newBvDJob(sObjectType, action, processingType),
        new BvDBatchJobIterable.RecordsSubIterable(records)
      );
    } else {
      return new BvDBatchJob(
        BvDJobs.newBvDJob(sObjectType, action, processingType),
        new BvDBatchJobIterable.RecordsIterable(records)
      );
    }
  }


  /**
  * @description Unlinks given records from BvD.
  * BvD ID is removed from the record and Unlinked checkbox is set to true.
  * BvD Company is updated to remove SF ID.
  * Unlink Reason is expected to be filled in on the record field.
  *
  * @param List of records to unlink
  * @return Id of BvD Job record.
  * */
  public static Id unlinkRecordsBatch(List<sObject> records, String processingType) {
    BvDBatchJob job = BvDJobs.createBatchJob(records, TYPE_UNLINK, processingType);
    return job.executeBatch();
  }

  public static BvD_Job__c unlinkRecordsImmediatly(List<sObject> records, String processingType) {
    BvDBatchJob batchJob = BvDJobs.createBatchJob(records, TYPE_UNLINK, processingType);
    BvD_Job__c bvDJob = batchJob.executeImmediatly();
    return BvDJobsSelector.getJobWithWorkItemsById(bvDJob.Id);
  }

  public static BvD_Job__c unlinkRecordsImmediatly(sObject record, String processingType) {
    return unlinkRecordsImmediatly(new List<sObject>{
      record
    }, processingType);
  }

  public static BvD_Job__c unlinkRecordsImmediatly(Id recordId, String processingType) {
    sObject record = recordId.getSobjectType().newSObject(recordId);
    return unlinkRecordsImmediatly(record, processingType);
  }


  /**
  * @description Links given records to BvD
  * BvD ID is set on record and SF ID is set on the BvD company
  *
  * @param Map of Record IDs to BvD Ids
  * @return Id of BvD Job record.
  * */
  public static Id linkRecordsBatch(Map<Id, String> mapRecordIDByBvDId, sObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createLinkJob(mapRecordIDByBvDId, sObjectType, processingType);
    return job.executeBatch();
  }

  public static BvD_Job__c linkRecordsImmediatly(Map<Id, String> mapRecordIDByBvDId, sObjectType sObjectType, String processingType) {
    BvDBatchJob batchJob = BvDJobs.createLinkJob(mapRecordIDByBvDId, sObjectType, processingType);
    BvD_Job__c bvDJob = batchJob.executeImmediatly();
    return BvDJobsSelector.getJobWithWorkItemsById(bvDJob.Id);
  }

  public static BvD_Job__c linkRecordsImmediatly(Id recordID, String bvdID, String processingType) {
    return BvDJobs.linkRecordsImmediatly(new Map<Id, String>{
      recordID => bvdID
    },
      recordID.getSobjectType(),
      processingType);
  }

  private static BvDBatchJob createLinkJob(Map<Id, String> bvdIDs, SObjectType sObjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sObjectType, TYPE_LINK, processingType),
      new BvDBatchJobIterable.LinkIterable(bvdIDs)
    );
  }


  /**
  * @description Updates all records of give type, linked to BvD, by querying BvD Companies
  * and updating Salesforce records based on Field Mapping
  *
  * @param sObjectType to update
  * @return Id of BvD Job record.
  * */
  public static Id updateRecordsBatch(SObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createUpdateJob(sObjectType, processingType);
    return job.executeBatch();
  }
  public static Id updateRecordsBatch(List<String> bvdIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createUpdateJob(bvdIDs, sObjectType, processingType);
    return job.executeBatch();
  }
  public static Id updateRecordsBatch(String bvdID, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createUpdateJob(bvdID, bvdSubIDs, sObjectType, processingType);
    return job.executeBatch();
  }

  public static BvD_Job__c updateRecordsImmediatly(List<sObject> records, String processingType) {
    BvDBatchJob batchJob = BvDJobs.createBatchJob(records, TYPE_UPDATE, processingType);
    BvD_Job__c bvDJob = batchJob.executeImmediatly();
    return BvDJobsSelector.getJobWithWorkItemsById(bvDJob.Id);
  }

  public static BvD_Job__c updateRecordsImmediatly(sObject record, String processingType) {
    return BvDJobs.updateRecordsImmediatly(new List<sObject>{
      record
    }, processingType);
  }

  private static BvDBatchJob createUpdateJob(SObjectType sobjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sobjectType, TYPE_UPDATE, processingType),
      new BvDBatchJobIterable.UpdateIterable(sobjectType)
    );
  }
  private static BvDBatchJob createUpdateJob(List<String> bvdIDs, SObjectType sobjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sobjectType, TYPE_UPDATE, processingType),
      new BvDBatchJobIterable.UpdateIterable(bvdIDs, sobjectType)
    );
  }
  private static BvDBatchJob createUpdateJob(String bvdID, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sobjectType, TYPE_UPDATE, processingType),
      new BvDBatchJobIterable.UpdateSubIterable(bvdID, bvdSubIDs, sObjectType)
    );
  }


  /**
  * @description Creates a Batch Job which queries BvD Companies by given BvD Id,
  * and creates records of given sObjectType
  * All fields are filled in according to Field Mapping of that sObject Type.
  * @return Id of BvD Job record.
  * */
  public static Id insertRecordsBatch(List<String> bvdIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createInsertJob(bvdIDs, sObjectType, processingType);
    return job.executeBatch();
  }

  public static Id insertRecordsBatch(String bvdID, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createInsertJob(bvdID, bvdSubIDs, sObjectType, processingType);
    return job.executeBatch();
  }

  public static Id insertRecordsBatch(String lookupField, String bvdID, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob job = BvDJobs.createInsertJob(lookupField, bvdID, bvdSubIDs, sObjectType, processingType);
    return job.executeBatch();
  }

  public static BvD_Job__c insertRecordsImmediatly(String bvdId, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob batchJob = BvDJobs.createInsertJob(bvdId, bvdSubIDs, sObjectType, processingType);
    BvD_Job__c bvDJob = batchJob.executeImmediatly();
    return BvDJobsSelector.getJobWithWorkItemsById(bvDJob.Id);

  }

  public static BvD_Job__c insertRecordsImmediatly(List<String> bvdIDs, SObjectType sObjectType, String processingType) {
    BvDBatchJob batchJob = BvDJobs.createInsertJob(bvdIDs, sObjectType, processingType);
    BvD_Job__c bvDJob = batchJob.executeImmediatly();
    return BvDJobsSelector.getJobWithWorkItemsById(bvDJob.Id);

  }

  public static BvD_Job__c insertRecordsImmediatly(String bvdId, SObjectType sObjectType, String processingType) {
    return BvDJobs.insertRecordsImmediatly(new List<String>{
      bvdId
    }, sObjectType, processingType);
  }

  private static BvDBatchJob createInsertJob(List<String> bvdIDs, SObjectType sObjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sObjectType, TYPE_INSERT, processingType),
      new BvDBatchJobIterable.InsertIterable(bvdIDs)
    );
  }

  private static BvDBatchJob createInsertJob(String bvdID, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sObjectType, TYPE_INSERT, processingType),
      new BvDBatchJobIterable.InsertSubIterable(bvdID, bvdSubIDs)
    );
  }

  private static BvDBatchJob createInsertJob(String lookupField, String bvdID, List<String> bvdSubIDs, SObjectType sObjectType, String processingType) {
    return new BvDBatchJob(
      BvDJobs.newBvDJob(sObjectType, TYPE_INSERT, processingType),
      new BvDBatchJobIterable.InsertSubIterable(bvdID, bvdSubIDs, lookupField)
    );
  }


  /**
  * Start "Manual Processing" Job
  * */
  public static Id startJob(Id bvdJobId) {
    return retryJob(bvdJobId);
  }


  /**
  * Schedules execution of existing BvD Job to
  * repeat execution over Not Processed work Items.
  * */
  public static Id retryJob(Id bvdJobId) {
    BvD_Job__c bvDJob = BvDJobsSelector.getJobById(bvdJobId);
    bvDJob.Failure_Reason__c = null;
    bvDJob.Status__c = BvDJobs.STATUS_SCHEDULED;
    bvDJob.Execution_Time__c = null;
    bvDJob.Processing__c = bvDJob.Processing__c == PROCESSING_DRY_RUN ? PROCESSING_AUTOMATIC : bvDJob.Processing__c;

    if(BVDUserPermissionCheck.permissionToUpdateSobject('BvD_Job__c', true,
                                                        new List<String>{'Failure_Reason__c', 'Status__c',
                                                                         'Processing__c', 'Execution_Time__c'})){
        update bvDJob;

    } else{
        Logger.error('Not enough privileges to create BVD Jobs');
        throw new AuraHandledException('Not enough privileges to create BVD Jobs');
    }

    BvDBatchJob job = new BvDBatchJob(bvDJob, new BvDBatchJobIterable.RetryIterable(bvDJob.Id));
    return job.executeBatch();
  }

  /**
  * Schedules execution of existing BvD Job to
  * repeat execution over Not Processed work Items.
  * */
  public static BvD_Job__c retryJobImmediatly(Id bvdJobId) {
      
    BvD_Job__c bvDJob = BvDJobsSelector.getJobById(bvdJobId);
    bvDJob.Failure_Reason__c = null;
    bvDJob.Status__c = BvDJobs.STATUS_SCHEDULED;
    bvDJob.Execution_Time__c = null;
    bvDJob.Processing__c = bvDJob.Processing__c == PROCESSING_DRY_RUN ? PROCESSING_AUTOMATIC : bvDJob.Processing__c;

    BvDBatchJob job = new BvDBatchJob(bvDJob, new BvDBatchJobIterable.RetryIterable(bvDJob.Id));
    job.executeImmediatly();
    return BvDJobsSelector.getJobWithWorkItemsById(bvdJobId);
  }
}