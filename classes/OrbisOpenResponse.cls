public with sharing class OrbisOpenResponse extends OrbisRestResponse {
  private String Session;

  public String getSessionID() {
    return this.Session;
  }
}