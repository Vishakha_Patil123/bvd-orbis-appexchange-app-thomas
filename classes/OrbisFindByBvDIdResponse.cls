public with sharing class OrbisFindByBvDIdResponse extends OrbisResponse {
    private OrbisSchema.FindByBVDIdResponseElement responseElement = new OrbisSchema.FindByBVDIdResponseElement();

    public OrbisFindByBvDIdResponse(HttpResponse response) {
        super(response);
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseElement;
    }

    public String getToken(){
        return responseElement.FindByBVDIdResult.token;
    }

    public Integer getSelectionCount(){
        return responseElement.FindByBVDIdResult.selectionCount;
    }
}