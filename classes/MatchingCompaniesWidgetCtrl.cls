public with sharing class MatchingCompaniesWidgetCtrl {

  @AuraEnabled
  public static BvD_Job__c linkToCompany(Id recordId, String bvdId) {
    Logger.start('AccountMatchingCompaniesCtrl', 'linkAccountToCompany');
    try {
      BvD_Job__c job = BvDJobs.linkRecordsImmediatly(recordId, bvdId, BvDJobs.PROCESSING_AUTOMATIC);
      return job;
    } catch (Exception ex) {
      Logger.error(ex);
      return new BvD_Job__c(Status__c = BvDJobs.STATUS_ERROR, Failure_Reason__c = BvDJobs.ERROR_EXECUTION);
    } finally {
      Logger.stop();
    }
  }

  @AuraEnabled
  public static Data getData(Id recordId) {
    try {
      Logger.start('MatchingCompaniesWidgetCtrl', 'getData');
      return new Data(recordId);
    } catch (Exception ex) {
      System.debug(ex);
      System.debug(ex.getMessage());
      System.debug(ex.getStackTraceString());
      Logger.error(ex);
      Logger.stop();
      AuraHandledException auraEx = new AuraHandledException(ex.getMessage());
      auraEx.initCause(ex);
      throw auraEx;
    }
  }

  public class Data {
    @AuraEnabled public Map<String, String> searchInput;
    @AuraEnabled public List<String> columns;

    private transient String sObjectType;
    private transient Id recordId;
    private transient sObject record;
    private transient Map<String, String> bvdToSFMapping;

    public Data(Id recordId) {
      this.sObjectType = String.valueOf(recordId.getSobjectType());
      this.recordId = recordId;

      this.searchInput = getSearchInput();
      this.columns = getColumns();
    }

    private Map<String, String> getSearchInput() {
      getFieldMapping();
      queryRecord();
      return getValuesMapping();
    }

    private List<String> getColumns() {
      List<String> columns = new List<String>();

      Matching_Columns__c[] matchingColumns = [
        SELECT Column__c
        FROM Matching_Columns__c
        WHERE Object__c = :this.sObjectType
        ORDER BY Order__c
      ];

      for (Matching_Columns__c col : matchingColumns) {
        columns.add(col.Column__c);
      }

      return columns;
    }

    /**
    * Returns BvD Field to SF Field mapping from custom setting
    * */
    private void getFieldMapping() {
      Matching_Mapping__c[] matchingMappings = [
        SELECT SF_Field__c, BvD_Field__c
        FROM Matching_Mapping__c
        WHERE Object__c = :this.sObjectType
      ];

      this.bvdToSFMapping = new Map<String, String>();
      for (Matching_Mapping__c mapping : matchingMappings) {
        bvdToSFMapping.put(mapping.BvD_Field__c, mapping.SF_Field__c);
      }
    }

    /**
    * Queries record with given fields
    * */
    private void queryRecord() {
      List<String> recordFields = new List<String>(bvdToSFMapping.values());
      recordFields.add('Id');
      recordFields.add('BvD_Id__c');
      recordFields = getReadableFields(recordFields);

      String query = String.format('SELECT {0}' +
        ' FROM {1}' +
        ' WHERE ID =:recordId', new String[]{
        String.join(recordFields, ','),
        sObjectType
      });

      this.record = Database.query(query);
    }

    /**
    * Filters readable fields from given
    * */
    private List<String> getReadableFields(List<String> fields) {
      SObjectDescribe sobjDescribe = SObjectDescribe.getDescribe(this.sObjectType);
      Set<String> readableFields = new Set<String>();

      for (String field : fields) {
        Boolean isReadable = sobjDescribe.getField(field).getDescribe().isAccessible();
        if (isReadable) {
          readableFields.add(field);
        }
      }

      return new List<String>(readableFields);
    }

    /**
    * Maps BvD Fields to sObject field Values
    * */
    private Map<String, String> getValuesMapping() {
      Map<String, String> result = new Map<String, String>();

      for (String bvdField : bvdToSFMapping.keySet()) {
        String sfField = bvdToSFMapping.get(bvdField);
        Object fieldValue = record.get(sfField);

        if (fieldValue != null) {
          result.put(bvdField, String.valueOf(fieldValue));
        }
      }

      return result;
    }
  }
}