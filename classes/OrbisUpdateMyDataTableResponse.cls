public with sharing class OrbisUpdateMyDataTableResponse extends OrbisResponse{

    private OrbisSchema.UpdateMyDataTableResponseElement responseSchema = new
            OrbisSchema.UpdateMyDataTableResponseElement();

    public OrbisUpdateMyDataTableResponse(HttpResponse response) {
        super(response);
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseSchema;
    }

    public Boolean getIsSuccess() {
        return responseSchema.result.success;
    }
    
    public Integer getIndexationProcessId() {
        return responseSchema.result.IndexationProcessId;
    }

    public List<OrbisSchema.StringElement> getErrors() {
        return responseSchema.result.errors.stringElements;
    }

    public List<OrbisSchema.StringElement> getWarnings() {
        return responseSchema.result.warnings.stringElements;
    }

    public List<OrbisSchema.StringElement> getUnMatchedRecords() {
        return responseSchema.result.unMatchedRecords.stringElements;
    }

}