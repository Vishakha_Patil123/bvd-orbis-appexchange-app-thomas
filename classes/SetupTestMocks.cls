@IsTest
public class SetupTestMocks implements HttpCalloutMock {

  public HttpResponse respond (HttpRequest request) {
    AdministrationController.DataSourceResponse body  = new AdministrationController.DataSourceResponse();

    HttpResponse response = new HttpResponse();
    response.setStatusCode(200);

    if(request.getHeader('Accept') == null) {

      body.Error = 'JSON only';
      System.assertEquals(  body.Error, 'JSON only');

    } else {
      AdministrationController.DataSource source = new AdministrationController.DataSource();
      source.Id = 'Mock Id';
      source.Label = 'Mock Label';
      System.assertEquals(source.Id, 'Mock Id');
      body.Products = new AdministrationController.DataSource[] { source };
    }

    response.setBody(JSON.serialize(body));

    return response;
  }
}