/**
 * Created by nordine on 07/12/17.
 */
@isTest
private with sharing class FieldMappingController_Test {

    //Create all test data for test method.
    @testSetup
    static void createTestData(){

        Orbis_Field__c testOrbisField = new Orbis_Field__c ( Name = 'Test Orbid',
                SObject__c = 'Account',
                Type__c= 'MoneyValue',
                Regular_Name__c = 'Regular Name' );
        INSERT testOrbisField;

        Bvd_Setup__c bvdSetup = new Bvd_Setup__c ( Name = 'BvD - Setup',
                Datasource_Id__c = 'id',
                Batchjob_Settings_Confirmed__c = true,
                SSO_Confirmed__c = true);

        INSERT bvdSetup;
        System.assertEquals(true, bvdSetup.Id != NULL);

        Field_Mapping__c fm = new Field_Mapping__c ( Name__c = 'Account.BillingCountry',
                SObject__c = 'Account', Sf_Field__c = 'BillingCountry',
                Type__c = 'TEXT', Master__c = 'Salesforce',
                Action__c = 'Automatic');
        INSERT fm;

        List<OrbisFields__c> orbisFieldList = new List<OrbisFields__c>();
        orbisFieldList.add(new OrbisFields__c(Label__c = 'Account', Type__c = 'Account'));
        orbisFieldList.add(new OrbisFields__c(Label__c = 'Contact', Type__c = 'Contact'));

        INSERT orbisFieldList;
    }

    //getOrbisFields method Test
    static testmethod void getOrbisFieldsTest () {
        Test.startTest();
        List<Orbis_Field__c> orbisFieldList = new List<Orbis_Field__c>();
        orbisFieldList = FieldMappingController.getOrbisFields('Account');

        if(orbisFieldList.isEmpty()){
            System.assertEquals(0, orbisFieldList.size());
        }else{
            System.assertEquals(1, orbisFieldList.size());
            System.assertEquals('Account', orbisFieldList[0].SObject__c);
            System.assertEquals('MoneyValue', orbisFieldList[0].Type__c);
        }

        orbisFieldList = FieldMappingController.getOrbisFields('Contact');
        Test.stopTest();
    }

    //getOrbisMappedFields method test.
    static testmethod void getOrbisMappedFieldsTest(){
        Test.startTest();
        List<Orbis_Field__c> orbisMappedFieldList = new List<Orbis_Field__c>();
        orbisMappedFieldList = FieldMappingController.getOrbisMappedFields('Account');

        if(orbisMappedFieldList.isEmpty()){
            System.assertEquals(0, orbisMappedFieldList.size());
        }else{
            System.assertEquals(1, orbisMappedFieldList.size());
            System.assertEquals('Account', orbisMappedFieldList[0].SObject__c);
            System.assertEquals('MoneyValue', orbisMappedFieldList[0].Type__c);
        }
        Test.stopTest();
    }

    //insertOrbisFields method Test
    static testmethod void insertOrbisFieldsTest(){

        Test.startTest();

        List<Orbis_Field__c> orbisFieldListToInsert = new List<Orbis_Field__c>();

        System.assertEquals(1, [SELECT Id, Name FROM Orbis_Field__c].size());

        orbisFieldListToInsert.add(new Orbis_Field__c ( Name = 'Test Account Orbis',
                SObject__c = 'Account',
                Type__c= 'MoneyValue',
                Regular_Name__c = 'Regular Name')
        );

        orbisFieldListToInsert.add(new Orbis_Field__c ( Name = 'Test Lead Orbis',
                SObject__c = 'Lead',
                Type__c= 'MoneyValue',
                Regular_Name__c = 'Regular Name')
        );

        FieldMappingController.insertOrbisFields(orbisFieldListToInsert);

        System.assertEquals(orbisFieldListToInsert.size()+1, [SELECT Id, Name FROM Orbis_Field__c].size());

        Test.stopTest();
    }

    //getMatrixLine method test
    static testmethod void getMatrixLineTest () {
        Test.startTest();
        List<Field_Matrix__mdt> fieldCrossections = new List<Field_Matrix__mdt>();
        fieldCrossections = FieldMappingController.getMatrixLine('UNKNOWNTYPEFORALLTIME');
        System.assertEquals(0, fieldCrossections.size());
        Test.stopTest();
    }

    //getCrossSection method test.
    static testmethod void getCrossSectionTest(){
        Test.startTest();
        FieldMatrix.CrossSection matrixCrossSection = FieldMappingController.getCrossSection('TestObject', 'TestField', 'String');

        System.assertEquals(FieldMatrix.STATUS_NOT_AVAILABLE, matrixCrossSection.availability);
        System.assertEquals(false, matrixCrossSection.isAvailable());
        System.assertEquals(false, FieldMatrix.isMapable('TestObject', 'TestField', 'String'));
        System.assertEquals(Label.BvD_FieldMatrix_Warning_SObjectNotAvailable, matrixCrossSection.warning);
        Test.stopTest();
    }

    //unExcludeFieldMapping method test.
    static testmethod void unExcludeFieldMappingTest () {
        Test.startTest();

        List<Field_Mapping__c> fieldMappingList = new List<Field_Mapping__c>();
        Map<String, List<Field_Mapping__c>> fieldStatusToFieldMappingsMap = new Map<String, List<Field_Mapping__c>>();
        fieldStatusToFieldMappingsMap = FieldMappingController.unExcludeFieldMapping('Account', fieldMappingList);

        if(fieldStatusToFieldMappingsMap.isEmpty()){
            System.assertEquals(true, fieldStatusToFieldMappingsMap.isEmpty());
        }else if(fieldStatusToFieldMappingsMap.containsKey('available')){
            System.assertEquals('Account.Name', fieldStatusToFieldMappingsMap.get('available')[0].Name__c);
        }

        Test.stopTest();
    }

    static testmethod void getOrbisAvailableFieldsTest () {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
        List<AdministrationController.Field> fields = FieldMappingController.getOrbisAvailableFields();
        if(fields != NULL && !fields.isEmpty()){
            System.assertEquals(8, fields.size());
            System.assertEquals('1001', fields[0].code);
        }
        Test.stopTest();
    }

    //updateGetLabelsFromModels method test.
    static testmethod void updateGetLabelsFromModelsTest(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
        List<OrbisFields__c> orbisFieldList = new List<OrbisFields__c>();
        orbisFieldList = FieldMappingController.updateGetLabelsFromModels();
        System.assertEquals(8, orbisFieldList.size());
        System.assertEquals('MoneyValue', orbisFieldList[0].Type__c);
        System.assertEquals('Company name', orbisFieldList[0].Label__c);
        Test.stopTest();
    }

    //getLabelsFromModels method Test
    static testmethod void getLabelsFromModelsTest() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
        List<OrbisFields__c> orbisFieldList = new List<OrbisFields__c>();
        orbisFieldList = FieldMappingController.getLabelsFromModels('Account');
        System.assertEquals(2, orbisFieldList.size());
        System.assertEquals('Account', orbisFieldList[0].Type__c);
        orbisFieldList = FieldMappingController.getLabelsFromModels('Contact');
        Test.stopTest();
    }

    //
    static testmethod void getAccountMappingTest(){
        Test.startTest();
        Map<String, List<Field_Mapping__c>> fieldStatusToFieldMappingsMap = new Map<String, List<Field_Mapping__c>>();
        fieldStatusToFieldMappingsMap = FieldMappingController.getAccountMapping();

        if(fieldStatusToFieldMappingsMap.isEmpty()){
            System.assertEquals(true, fieldStatusToFieldMappingsMap.isEmpty());
        }else if(fieldStatusToFieldMappingsMap.containsKey('available')){
            System.assertEquals(4, fieldStatusToFieldMappingsMap.size());
            System.assertEquals('Account.Name', fieldStatusToFieldMappingsMap.get('available')[0].Name__c);
        }
        Test.stopTest();
    }

    //getLeadMapping method test
    static testmethod void getLeadMappingTest(){
        Test.startTest();
        Map<String, List<Field_Mapping__c>> fieldStatusToFieldMappingsMap = new Map<String, List<Field_Mapping__c>>();
        fieldStatusToFieldMappingsMap = FieldMappingController.getLeadMapping();
        System.debug('orbisFieldList=='+fieldStatusToFieldMappingsMap);
        System.debug('size()=='+fieldStatusToFieldMappingsMap.size());
        if(fieldStatusToFieldMappingsMap.isEmpty()){
            System.assertEquals(true, fieldStatusToFieldMappingsMap.isEmpty());
        }else if(fieldStatusToFieldMappingsMap.containsKey('available')){
            System.assertEquals(4, fieldStatusToFieldMappingsMap.size());
            System.assertEquals('Lead.LastName', fieldStatusToFieldMappingsMap.get('available')[0].Name__c);
        }
        Test.stopTest();
    }

    //getContactMapping method Test
    static testmethod void getContactMappingTest(){
        Test.startTest();
        Map<String, List<Field_Mapping__c>> fieldStatusToFieldMappingsMap = new Map<String, List<Field_Mapping__c>>();
        fieldStatusToFieldMappingsMap = FieldMappingController.getContactMapping();
        if(fieldStatusToFieldMappingsMap.isEmpty()){
            System.assertEquals(true, fieldStatusToFieldMappingsMap.isEmpty());
        }else if(fieldStatusToFieldMappingsMap.containsKey('available')){
            System.assertEquals(4, fieldStatusToFieldMappingsMap.size());
            System.assertEquals('Contact.LastName', fieldStatusToFieldMappingsMap.get('available')[0].Name__c);
        }
        Test.stopTest();
    }
}