public with sharing class OrbisMatchRequest extends OrbisRequest {
  private OrbisSchema.MatchElement matchSchema = new OrbisSchema.MatchElement();

  public override OrbisResponse getOrbisResponse(HttpResponse response) {
    return new OrbisMatchResponse(response);
  }
  protected override String getSOAPAction() {
    return 'http://bvdep.com/webservices/Match';
  }
  protected override OrbisSchema.RequestElement getRequestSchema() {
    return matchSchema;
  }

  public OrbisMatchRequest setSessionID(String value) {
    matchSchema.sessionHandle = value;
    return this;
  }

  public OrbisMatchRequest setName(String value) {
    this.matchSchema.criteria.Name = value;
    return this;
  }
  public OrbisMatchRequest setAddress(String value) {
    this.matchSchema.criteria.Address = value;
    return this;
  }
  public OrbisMatchRequest setPostCode(String value) {
    this.matchSchema.criteria.PostCode = value;
    return this;
  }
  public OrbisMatchRequest setCity(String value) {
    this.matchSchema.criteria.City = value;
    return this;
  }
  public OrbisMatchRequest setCountry(String value) {
    this.matchSchema.criteria.Country = value;
    return this;
  }
  public OrbisMatchRequest setPhoneOrFax(String value) {
    this.matchSchema.criteria.PhoneOrFax = value;
    return this;
  }
  public OrbisMatchRequest setEMailOrWebsite(String value) {
    this.matchSchema.criteria.EMailOrWebsite = value;
    return this;
  }
  public OrbisMatchRequest setNationalId(String value) {
    this.matchSchema.criteria.NationalId = value;
    return this;
  }
  public OrbisMatchRequest setTicker(String value) {
    this.matchSchema.criteria.Ticker = value;
    return this;
  }
  public OrbisMatchRequest setIsin(String value) {
    this.matchSchema.criteria.Isin = value;
    return this;
  }
  public OrbisMatchRequest setState(String value) {
    this.matchSchema.criteria.State = value;
    return this;
  }
  public OrbisMatchRequest setBvD9(String value) {
    this.matchSchema.criteria.BvD9 = value;
    return this;
  }
  public OrbisMatchRequest setExclusionFlags(ExclusionFlags flag) {
    this.matchSchema.exclusionFlags = flag.name();
    return this;
  }
  public OrbisMatchRequest setExclusionFlags(String[] flags) {
    if (flags == null || flags.isEmpty()) {
      this.matchSchema.exclusionFlags = 'None';
    } else {
      this.matchSchema.exclusionFlags = String.join(flags, ' | ');
    }
    return this;
  }

  public enum ExclusionFlags {
    None,
    ExcludeVeryLargeCompanies,
    ExcludeLargeCompanies,
    ExcludeMediumCompanies,
    ExcludeSmallCompanies,
    ExcludePreviousNames,
    ExcludeBranchLocations,
    ExcludeHistorical,
    ExcludeInactive,
    ExcludeUnlisted
  }
}