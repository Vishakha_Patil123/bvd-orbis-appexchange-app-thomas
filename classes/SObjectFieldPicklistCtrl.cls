public with sharing class SObjectFieldPicklistCtrl {

  @AuraEnabled
  public static FieldDescription describeField(String sObjectName, String field) {
    return new FieldDescription(sObjectName, field);
  }

  public class FieldDescription {
    @AuraEnabled public String name;
    @AuraEnabled public String label;
    @AuraEnabled public List<PicklistEntry> picklistEntries;

    public FieldDescription(String sObjectName, String field) {
      DescribeFieldResult fieldDescribe = getFieldDescription(sObjectName, field);

      this.name = fieldDescribe.getName();
      this.label = fieldDescribe.getLabel();
      this.picklistEntries = new List<PicklistEntry>{
        new PicklistEntry('', '')
      };
      for (Schema.PicklistEntry picklistEntry : fieldDescribe.getPicklistValues()) {
        this.picklistEntries.add(new PicklistEntry(picklistEntry));
      }
    }

    private DescribeFieldResult getFieldDescription(String sObjectName, String field) {
      return Schema.getGlobalDescribe()
        .get(sObjectName)
        .getDescribe()
        .fields.getMap()
        .get(field)
        .getDescribe();
    }
  }

  public class PicklistEntry {
    @AuraEnabled public String label;
    @AuraEnabled public String value;

    public PicklistEntry(Schema.PicklistEntry entry) {
      this(entry.getLabel(), entry.getValue());
    }

    public PicklistEntry(String label, String value) {
      this.label = label;
      this.value = value;
    }
  }
}