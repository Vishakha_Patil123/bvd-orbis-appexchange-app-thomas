public with sharing class ImportSettingController {

    @AuraEnabled
    public static Map<String, Field_Mapping__c[]> importFieldMapping(String sObjectName, List<Field_Mapping__c> fieldMappings) {
        Logger.start('AdministrationController', 'importFieldMapping');

        Bvd_Setup__c setup = AdministrationController.getBvDSetup();

        if(!setup.SSO_Confirmed__c) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
        }

        if(String.isBlank(setup.Datasource_Id__c)) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
        }

        try {
            Set<String> fieldsToDelete = new Set<String> {};

            Field_Mapping__c[] fields = new Field_Mapping__c[] {};

            for (Field_Mapping__c field : fieldMappings) {
                fieldsToDelete.add(field.Name__c);
                fields.add(field.clone(false));
            }
			if(BVDUserPermissionCheck.permissionToDeleteSobject('Field_Mapping__c', true)){
        		delete [
                    	SELECT Id, Name__c
                    	FROM Field_Mapping__c
                    	WHERE Name__c IN :fieldsToDelete
            			];
      		}else{
        		Logger.error('Not enough privileges to delete Field mapping');
        		throw new AuraHandledException('Not enough privileges to delete Field mapping');
      		}
            
            
			if(BVDUserPermissionCheck.permissionToCreateSobject('Field_Mapping__c', true, new List<String>{'Name__c'})){
          		insert fields;
			}else{
			  Logger.error('Not enough privileges to create Field Mapping');
			  throw new AuraHandledException('Not enough privileges to create Field Mapping');
			}
            
            BvDScheduler.updateSchedules();
            return AdministrationController.getMapping(sObjectName);
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Import_Setting__c[] getImportSettings() {
        Logger.start('AdministrationController', 'getImportSettings');

        Bvd_Setup__c setup = AdministrationController.getBvDSetup();

        if(!setup.SSO_Confirmed__c) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
        }

        if(String.isBlank(setup.Datasource_Id__c)) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
        }

        if(!setup.Field_Mapping_Configured__c) {
            throw new AuraHandledException('TODO LABEL field mapping not configured');
        }

        //TODO
        try{
            return [SELECT Id,Name,SObject__c,Type__c,Processing__c FROM Import_Setting__c ORDER BY SObject__c];

        }
        catch (Exception ex){
            System.debug(ex);
            throw new AuraHandledException(ex.getMessage());
        }
        finally{
            Logger.stop();
        }
    }

    @AuraEnabled
    public static Import_Setting__c[] setImportSettings(Import_Setting__c[] importSettings) {
        Logger.start('AdministrationController', 'getImportSettings');

        Bvd_Setup__c setup = AdministrationController.getBvDSetup();

        if(!setup.SSO_Confirmed__c) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_No_SSO);
        }

        if(String.isBlank(setup.Datasource_Id__c)) {
            throw new AuraHandledException(System.Label.BvD_Admin_Datasource_Missing);
        }

        if(!setup.Field_Mapping_Configured__c) {
            throw new AuraHandledException('TODO LABEL field mapping not configured');
        }

        try {
            Import_Setting__c[] toInsert = new Import_Setting__c[] {};
            Import_Setting__c[] toUpdate = new Import_Setting__c[] {};

            for (Import_Setting__c importSetting : importSettings) {
                if(importSetting.Id != null) {
                    toUpdate.add(importSetting);
                } else {
                    toInsert.add(importSetting);
                }
            }

            if(!toInsert.isEmpty()) {
                if(BVDUserPermissionCheck.permissionToCreateSobject('Import_Setting__c', true, new List<String>{'Processing__c','SObject__c','Type__c'})){
                    insert toInsert;
                }else{
                    Logger.error('Not enough privileges to create Import Setting');
                    throw new AuraHandledException('Not enough privileges to create Import Setting');
                }    
            }

            if(!toUpdate.isEmpty()) {
                if(BVDUserPermissionCheck.permissionToUpdateSobject('Import_Setting__c', true, new List<String>{'Processing__c','SObject__c','Type__c'})){
                	update toUpdate;
                }else{
                    Logger.error('Not enough privileges to update ');
                    throw new AuraHandledException('Not enough privileges to update  ');        
                }
               
            }

            if(!setup.Import_Settings_Configured__c) {
                setup.Import_Settings_Configured__c = true;
                if(BVDUserPermissionCheck.permissionToUpdateSobject('setup', true, new List<String>{'Import_Settings_Configured__c'})){
                	update setup;
                }else{
                    Logger.error('Not enough privileges to update Setup');
                    throw new AuraHandledException('Not enough privileges to update Setup ');        
                }
            }

            BvDScheduler.updateSchedules();
            return getImportSettings();
        } catch (Exception ex) {
            Logger.error(ex);
            throw new AuraHandledException(ex.getMessage());
        } finally {
            Logger.stop();
        }
    }
}