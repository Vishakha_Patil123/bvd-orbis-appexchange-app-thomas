/**
* Wrapper class for Field Mapping record
* */
public class FieldMapping {
  @TestVisible private Field_Mapping__c fieldMapping;
  private DescribeFieldResult describe;

  public FieldMapping(Field_Mapping__c fieldMapping) {
    this.fieldMapping = fieldMapping;
    this.describe = getFieldDescribe();
  }

  private DescribeFieldResult getFieldDescribe() {
    try {
      return SObjectDescribe.getDescribe(fieldMapping.SObject__c).getDescribe()
        .fields.getMap().get(getSFField())
        .getDescribe();
    } catch (Exception ex) {
      return null;
    }
  }

  public String getQuery() {
    return fieldMapping.Query__c;
  }

  public String getDimension() {
    return fieldMapping.Data_Dimension__c;
  }

  public String getBvDField() {
    return fieldMapping.BvD_Field__c;
  }

  public String getSFField() {
    return fieldMapping.SF_Field__c;
  }

  public String getMasterField() {
    return fieldMapping.Master__c;
  }

  public DisplayType getSFFieldType() {
    return describe.getType();
  }

  public Boolean isAutomatic() {
    return fieldMapping.Action__c == FieldMappings.ACTION_TYPE_AUTOMATIC;
  }

  public Boolean isManual() {
    return fieldMapping.Action__c == FieldMappings.ACTION_TYPE_MANUAL;
  }

  public Boolean isExcluded() {
    return fieldMapping.Excluded__c;
  }

  public Field_Mapping__c invalidate() {
    fieldMapping.Invalid__c = true;
    return fieldMapping;
  }
}