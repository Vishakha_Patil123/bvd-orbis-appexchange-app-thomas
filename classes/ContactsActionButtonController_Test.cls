@IsTest
public with sharing class ContactsActionButtonController_Test {

  @isTest
  static void importContacts_01() {
    Account account = new Account(
      Name = 'test'
    );

    insert account;

    ContactsActionButtonController.CompanyContact contact    = new ContactsActionButtonController.CompanyContact(account.Id);
    contact.department = 'Senior management';
    contact.firstName = 'Philip';
    contact.function = 'Business Manager';
    contact.id = 'P215252026';
    contact.companyId = 'TEST1_U';
    contact.lastName = 'Demeulemeester';

    ContactsActionButtonController.CompanyContact[] contacts = new ContactsActionButtonController.CompanyContact[] {
      contact
    };

    Test.startTest();
    try {
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      ContactsActionButtonController.importContact(JSON.serialize(contacts), account.Id);
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
    Test.stopTest();
  }

  @isTest
  static void importContacts_02() {
    Account account = new Account(
      Name = 'test'
    );

    insert account;

    ContactsActionButtonController.CompanyContact contact    = new ContactsActionButtonController.CompanyContact(account.Id);
    contact.department = 'Senior management';
    contact.firstName = 'Philip';
    contact.function = 'Business Manager';
    contact.id = 'P215252026';
    contact.companyId = 'TEST1_U';
    contact.lastName = 'Demeulemeester';

    ContactsActionButtonController.CompanyContact contact2   = new ContactsActionButtonController.CompanyContact(account.Id);
    contact2.department = 'Senior management';
    contact2.firstName = 'Philip';
    contact2.function = 'Business Manager';
    contact2.id = 'P215252026';
    contact2.companyId = 'TEST1_U';
    contact2.lastName = 'Demeulemeester';

    ContactsActionButtonController.CompanyContact[] contacts = new ContactsActionButtonController.CompanyContact[] {
      contact, contact2
    };

    Test.startTest();
    try {
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      ContactsActionButtonController.importContact(JSON.serialize(contacts), account.Id);
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
    Test.stopTest();
  }

  @isTest
  static void importContacts_03() {
    Account account = new Account(
      Name = 'test'
    );

    insert account;

    Contact SFContact = new Contact(
      FirstName = 'Philip',
      LastName = 'Demeulemeester',
      AccountId = account.Id
    );

    Contact SFContact2 = new Contact(
      FirstName = 'Philip',
      LastName = 'Demeulemeester',
      AccountId = account.Id
    );

    insert new Contact[] { SFContact, SFContact2 };

    ContactsActionButtonController.CompanyContact contact    = new ContactsActionButtonController.CompanyContact(account.Id);
    contact.department = 'Senior management';
    contact.firstName = 'Philip';
    contact.function = 'Business Manager';
    contact.id = 'P215252026';
    contact.companyId = 'TEST1_U';
    contact.lastName = 'Demeulemeester';
    contact.recordId = SFContact.Id;

    ContactsActionButtonController.CompanyContact contact2   = new ContactsActionButtonController.CompanyContact(account.Id);
    contact2.department = 'Senior management';
    contact2.firstName = 'Philip';
    contact2.function = 'Business Manager';
    contact2.id = 'P215252026';
    contact2.companyId = 'TEST1_U';
    contact2.lastName = 'Demeulemeester';
    contact2.recordId = SFContact2.Id;

    ContactsActionButtonController.CompanyContact[] contacts = new ContactsActionButtonController.CompanyContact[] {
      contact, contact2
    };

    Test.startTest();
    try {
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      ContactsActionButtonController.importContact(JSON.serialize(contacts), account.Id);
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
    Test.stopTest();
  }

  @isTest
  static void importContacts_04() {
    Account account = new Account(
      Name = 'test'
    );

    insert account;

    Contact SFContact = new Contact(
      FirstName = 'Philip',
      LastName = 'Demeulemeester',
      AccountId = account.Id
    );

    insert new Contact[] { SFContact };

    ContactsActionButtonController.CompanyContact contact    = new ContactsActionButtonController.CompanyContact(account.Id);
    contact.department = 'Senior management';
    contact.firstName = 'Philip';
    contact.function = 'Business Manager';
    contact.id = 'P215252026';
    contact.companyId = 'TEST1_U';
    contact.lastName = 'Demeulemeester';
    contact.recordId = SFContact.Id;

    ContactsActionButtonController.CompanyContact[] contacts = new ContactsActionButtonController.CompanyContact[] {
      contact
    };

    Test.startTest();
    try {
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      ContactsActionButtonController.importContact(JSON.serialize(contacts), account.Id);
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
    Test.stopTest();
  }
}