public with sharing class UnlinkActionButtonCtrl {

  @AuraEnabled
  public static BvDJobsResult unlink(Id recordId, String reason) {
    Logger.start('UnlinkActionButtonCtrl', 'unlink');

    try {
      System.debug('try');
      SObject sobj = Id.valueOf(recordId).getSobjectType().newSObject(recordId);
      sobj.put('Reason_unlinked_from_BvD__c', reason);
      System.debug(sobj);
      update sobj;
      System.debug('updated');
      System.debug(sobj);

      BvD_Job__c job = BvDJobs.unlinkRecordsImmediatly(recordId, BvDJobs.PROCESSING_AUTOMATIC);

      System.debug('before return');
      return new BvDJobsResult(job);
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(BvDJobsResult.ERROR_INSUFFICIENT_PRIVILEGES);
    } finally {
      Logger.stop();
    }
  }
}