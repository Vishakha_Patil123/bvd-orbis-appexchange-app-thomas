public with sharing class OrbisGetLabelsFromModelRequest extends OrbisRequest {
  private OrbisSchema.GetLabelsFromModelRequestElement requestElement = new OrbisSchema.GetLabelsFromModelRequestElement();

  public OrbisGetLabelsFromModelRequest addField(String fieldCode, String modelId) {
    OrbisSchema.GetLabelsFromModelFieldDefinitionElement fieldDefinition = new OrbisSchema.GetLabelsFromModelFieldDefinitionElement();
    fieldDefinition.FieldCode = fieldCode;
    fieldDefinition.ModelId = modelId;

    requestElement.fieldDefs.fieldDefs.add(fieldDefinition);
    return this;
  }

  public OrbisGetLabelsFromModelRequest setFullLabels(Boolean value) {
    requestElement.fullLabels = value;
    return this;
  }


  protected override OrbisSchema.RequestElement getRequestSchema() {
    return requestElement;
  }

  public override OrbisResponse getOrbisResponse(HttpResponse response) {
    return new OrbisGetLabelsFromModelResponse(response);
  }

  protected override String getSOAPAction() {
    return 'http://bvdep.com/webservices/GetLabelsFromModel';
  }

  public OrbisGetLabelsFromModelRequest setSessionID(String value) {
    requestElement.sessionHandle = value;
    return this;
  }
}