/**
* Controller for Batch Job List component
* */
public with sharing class DataCockpitCtrl {

  /**
  * Returns Batch Jobs with information about number of errors
  * Jobs are sorted from the newest to the oldest
  * */
  @AuraEnabled
  public static List<BatchJob> getBatchJobs() {
    try {
      Logger.start('DataCockpitCtrl', 'getBatchJobs');
      List<BatchJob> jobs = new List<BatchJob>();
      for (BvD_Job__c job : BvDJobsSelector.getAllJobsWithWorkItems()) {
        jobs.add(new BatchJob(job));
      }
      Logger.stop();
      return jobs;
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }


  /**
   * Retry process Batch Job
   * */
  @AuraEnabled
  public static void retryBatchJob(Id bvdJobId) {
    try {
      Logger.start('DataCockpitCtrl', 'retryBatchJob');
      BvDJobs.retryJob(bvdJobId);

      Logger.stop();
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }

  /**
   * Process Batch Job
   * */
  @AuraEnabled
  public static void processBatchJob(Id bvdJobId) {
    try {
      Logger.start('DataCockpitCtrl', 'processBatchJob');
      BvDJobs.retryJob(bvdJobId);

      Logger.stop();
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }

  /**
   * Process Batch Job
   * */
  @AuraEnabled
  public static void updateAccounts() {
    try {
      Logger.start('DataCockpitCtrl', 'processBatchJob');

      Import_Setting__c setting = Import_Setting__c.getInstance('Account.Update');
      BvDJobs.updateRecordsBatch(Account.getSObjectType(), setting == null? BvDJobs.PROCESSING_MANUAL : setting.Processing__c);

      Logger.stop();
    } catch (Exception ex) {
      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }
  }

  public class BatchJob {
    @AuraEnabled public String id;
    @AuraEnabled public String name;
    @AuraEnabled public String anObject;
    @AuraEnabled public String type;
    @AuraEnabled public String processing;
    @AuraEnabled public Datetime executionTime;
    @AuraEnabled public String status;
    @AuraEnabled public String reason;
    @AuraEnabled public Integer totalNbr;
    @AuraEnabled public Integer errorsNbr;
    @AuraEnabled public Integer successNbr;
    @AuraEnabled public Boolean isVisible;

    public BatchJob(BvD_Job__c job) {
      this.id = job.Id;
      this.name = job.Name;
      this.anObject = job.Object__c;
      this.type = job.Type__c;
      this.processing = job.Processing__c;
      this.executionTime = job.Execution_Time__c;
      this.status = job.Status__c;
      this.reason = job.Failure_Reason__c;
      this.isVisible = true;
      this.countWorkItems(job.BvD_Job_Work_Items__r);
    }

    private void countWorkItems(List<BvD_Job_Work_Item__c> items) {
      this.errorsNbr = this.successNbr = 0;
      this.totalNbr = items.size();

      for (BvD_Job_Work_Item__c wi : items) {
        if (wi.Status__c == BvDJobWorkItems.STATUS_ERROR) {
          ++errorsNbr;
        } else if (wi.Status__c == BvDJobWorkItems.STATUS_SUCCESS) {
          ++successNbr;
        }
      }
    }
  }
}