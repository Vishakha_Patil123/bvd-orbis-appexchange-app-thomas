public with sharing class OrbisGetOwnershipStructureResponse extends OrbisResponse {
    private OrbisSchema.GetOwnershipStructureResponse responseSchema = new OrbisSchema.GetOwnershipStructureResponse();

    public OrbisGetOwnershipStructureResponse(HttpResponse response) {
        super(response);
    }

    protected override OrbisSchema.ResponseElement getResponseSchema() {
        return responseSchema;
    }


    public List<OrbisSchema.OwnershipGraphNode> getNodes() {
        return responseSchema.getOwnershipStructureResult.nodes.ownershipGraphNodes;
    }

    public List<OrbisSchema.OwnershipGraphEdge> getEdges() {
        return responseSchema.getOwnershipStructureResult.edges.ownershipGraphEdges;
    }
}