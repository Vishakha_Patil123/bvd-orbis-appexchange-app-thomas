global class DryRunCleaner implements Database.Batchable<SObject>, Schedulable {

  static DateTime twoHoursAgo = Datetime.now().addHours(-2);
  static DateTime oneWeekAgo  = Datetime.now().addDays(-7);

  public Database.QueryLocator start(Database.BatchableContext context) {
    return Database.getQueryLocator([
      SELECT Id, Processing__c, Status__c, Type__c, CreatedDate
      FROM BvD_Job__c
      WHERE
      (Processing__c = 'Dry Run' AND Status__c != 'In Progress' AND CreatedDate < :twoHoursAgo)
      OR
      (Processing__c = 'Automatic' AND Status__c != 'In Progress' AND CreatedDate < :oneWeekAgo)
    ]);
  }

  public void execute(Database.BatchableContext context, List<SObject> records) {
	if(BVDUserPermissionCheck.permissionToDeleteSobject('BvD_Job__c', true)){
        delete records;
      }else{
        Logger.error('Not enough privileges to delete records');
        throw new AuraHandledException('Not enough privileges to delete records');
      }
    
  }

  global void execute(SchedulableContext context) {
    Database.executeBatch(this);
  }

  Global void finish(Database.BatchableContext context) {
    try {
      Logger.start('DryRunCleaner', 'Dry Run Cleaner Job');

      AsyncApexJob job = [
        SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems
        FROM AsyncApexJob
        WHERE Id = :context.getJobId()
      ];

      Logger.info(String.format('Job cleaner ran with status: {0}. {1} total items with {2} processed and {3} errors', new String[] {
        job.Status, job.TotalJobItems.format(), job.JobItemsProcessed.format(), job.NumberOfErrors.format()
      }));
    } catch (Exception ex) {
      Logger.error(ex);
    } finally {
      Logger.stop();
    }

  }

}