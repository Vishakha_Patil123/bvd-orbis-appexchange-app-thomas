public with sharing class OrbisGetLabelsFromModelResponse extends OrbisResponse {
  private OrbisSchema.GetLabelsFromModelResponseElement responseElement = new OrbisSchema.GetLabelsFromModelResponseElement();

  public OrbisGetLabelsFromModelResponse(HttpResponse response) {
    super(response);
  }

  public List<String> getLabels() {
    return responseElement.result.results;
  }

  protected override OrbisSchema.ResponseElement getResponseSchema() {
    return responseElement;
  }
}