public with sharing class StatusComponentController {

    @AuraEnabled
    public static SObject fetchObject(String sObjectName,String recordId) {
        return CommonUtilities.queryAllFieldsFromId(sObjectName,recordId);
    }

    @AuraEnabled
    public static ContainerCompanyUrl getContext(){
        return OrbisContainerController.getSessionId();
    }

}