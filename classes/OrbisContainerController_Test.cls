@IsTest
public with sharing class OrbisContainerController_Test {

  public static final String BVDID      = 'A2X';
  private final static String USERNAME  = 'OrbisContainerControllerTest@bvd.unit.test.com';

  @TestSetup
  static void addJobs() {

    User usr = TestUtilities.getSystemAdminUser(USERNAME);
    insert usr;

    System.runAs(usr) {
      insert new List<sObject>{
              new Account(
                      Name = 'Test',
                      BvD_Id__c = BVDID
              )
      };
    }
    BvD_Job__c job = new BvD_Job__c(
            Object__c = 'Account',
            Processing__c = 'Automatic',
            Status__c = 'Scheduled',
            Failure_Reason__c = '',
            Type__c = 'Link'
    );

    insert job;

    BvD_Job_Work_Item__c work = new BvD_Job_Work_Item__c(
            BvD_Job__c = job.Id,
            BvD_Id__c = BVDID,
            Status__c = 'Not Started',
            RecordId__c = [ SELECT Id FROM Account LIMIT 1].Id,
            Record_Name__c = ''
    );

    insert work;
  }

  @isTest
  static void getJobsTest () {
    Test.startTest();

    BvD_Job_Work_Item__c work = [SELECT Id, Name, Status__c FROM BvD_Job_Work_Item__c LIMIT 1];
    List<BvD_Job_Work_Item__c> jobWorkItemList = new List<BvD_Job_Work_Item__c>();
    jobWorkItemList = OrbisContainerController.getJobs(new Set<String>{work.Id});

    if(jobWorkItemList.isEmpty()){
      System.assertEquals(0, jobWorkItemList.size());
    }else{
      System.assertEquals(1,jobWorkItemList.size());
      System.assertEquals(work.Status__c, jobWorkItemList[0].Status__c);
    }

    Test.stopTest();
  }

  //isOriginValid method test
  @isTest
  static void isOriginValidTest () {
    Test.startTest();
    //test with null
    try{
      OrbisContainerController.isOriginValid('null');
      System.assert(true);
    }catch(Exception exp){
      System.assert(true);
    }
    Test.stopTest();
  }

  @isTest
  static void attachWorksToRecordsTest(){
    Test.startTest();
    List<ContainerRecord> containerList;
    List<BvD_Job_Work_Item__c> jobWorkItemList = [SELECT Id, Name, BvD_Id__c FROM BvD_Job_Work_Item__c LIMIT 1];
    ContainerRecord container = new ContainerRecord();
    container.id = jobWorkItemList[0].Id;
    containerList = OrbisContainerController.attachWorksToRecords(new List<ContainerRecord>{container}, jobWorkItemList);

    if(containerList.isEmpty()){
      System.assertEquals(0, containerList.size());
    }else{
      System.assertEquals(1, containerList.size());
    }

    Test.stopTest();
  }

  @isTest
  static void importContactsTest(){
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new SetupTestMocks());
    BvD_Job_Work_Item__c jobWorkItem = [SELECT Id, Name, BvD_Id__c FROM BvD_Job_Work_Item__c LIMIT 1];
    List<ContainerRecord> containerRecordList = new List<ContainerRecord>();
    ContainerRecord containerRecord = new ContainerRecord();
    containerRecord.work = jobWorkItem;
    containerRecord.sObjectName = 'contact';
    containerRecord.accountName = 'testAccount';
    containerRecord.firstName = 'test';
    containerRecord.lastName = 'contact';

    containerRecordList.add(containerRecord);
    try{
      OrbisContainerController.importContacts(JSON.serialize(containerRecordList), 'testOrigin');
      System.assert(true);
    }catch(Exception exp){
      System.assert(true);
    }

    Test.stopTest();
  }

  @isTest
  static void importCompaniesTest(){
    Test.startTest();
    BvD_Job_Work_Item__c jobWorkItem = [SELECT Id, Name, BvD_Id__c FROM BvD_Job_Work_Item__c LIMIT 1];
    List<ContainerRecord> containerRecordList = new List<ContainerRecord>();
    ContainerRecord containerRecord = new ContainerRecord();
    containerRecord.work = jobWorkItem;
    containerRecord.sObjectName = 'contact';
    containerRecord.accountName = 'testAccount';
    containerRecord.firstName = 'test';
    containerRecord.lastName = 'contact';

    containerRecordList.add(containerRecord);
    try{
      OrbisContainerController.importCompanies(JSON.serialize(containerRecordList), 'testOrigin');
      System.assert(true);
    }catch(Exception exp){
      System.assert(true);
    }
    Test.stopTest();
  }

  @isTest
  static void processRecordsTest(){
    Test.startTest();
    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    setup.Import_Settings_Configured__c = true;
    setup.Batchjob_Settings_Confirmed__c = true;
    insert setup;

    BvD_Job_Work_Item__c jobWorkItem = [SELECT Id, Name, BvD_Id__c FROM BvD_Job_Work_Item__c LIMIT 1];
    List<ContainerRecord> containerRecordList = new List<ContainerRecord>();
    ContainerRecord containerRecord = new ContainerRecord();
    containerRecord.work = jobWorkItem;
    containerRecord.sObjectName = 'contact';
    containerRecord.accountName = 'testAccount';
    containerRecord.firstName = 'test';
    containerRecord.lastName = 'contact';
    containerRecordList.add(containerRecord);
    List<Map<String, Object>> jobs  = new List<Map<String, Object>> {};

    try {
      jobs = OrbisContainerController.processRecords(JSON.serialize(containerRecordList), JSON.serialize(containerRecordList),
        JSON.serialize(containerRecordList), null);
    } catch (Exception ex) {

    }
    if(jobs.isEmpty()){
      System.assertEquals(0, jobs.size());
    }else{
      System.assertEquals(3, jobs.size());
      /*Map<String, Object> cObjMapFurious = (Map<String, Object>) JSON.deserializeUntyped(jobs[0].get('job'));
      String cObjJsonDrunk = JSON.serialize(cObjMapFurious);
       SObject customObject = (SObject)JSON.deserialize(jobs[0].get('job'), Sobject.class);*/
    }

    Test.stopTest();
  }

  @isTest
  static void getIFrameEndpoint_01() {
    try {
      OrbisContainerController.getIFrameEndpoint();
      System.assert(true);
    } catch (Exception ex) {
      System.assert(false);
    }
  }

  @isTest
  static void getSessionId_01() {

    try {
      OrbisContainerController.getSessionId();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getSessionId_02() {

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    insert setup;
    try {
      OrbisContainerController.getSessionId();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getSessionId_03() {

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    insert setup;
    try {
      OrbisContainerController.getSessionId();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getSessionId_04() {

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    insert setup;
    try {
      OrbisContainerController.getSessionId();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getSessionId_05() {

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    insert setup;
    try {
      OrbisContainerController.getSessionId();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getSessionId_06() {

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    setup.Import_Settings_Configured__c = true;
    insert setup;
    try {
      OrbisContainerController.getSessionId();
      System.assert(false);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void getSessionId_07() {

    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    setup.Import_Settings_Configured__c = true;
    setup.Batchjob_Settings_Confirmed__c = true;
    insert setup;
    try {
      Test.startTest();
      Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
      OrbisContainerController.getSessionId();
      System.assert(true);
      Test.stopTest();
    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.assert(false);
    }

  }

  @isTest
  static void processRecords_01() {
    Bvd_Setup__c setup = BvDSetupSettings.getSetting();
    setup.SSO_Confirmed__c = true;
    setup.Datasource_Id__c = 'a';
    setup.Field_Mapping_Configured__c = true;
    setup.Import_Settings_Configured__c = true;
    setup.Batchjob_Settings_Confirmed__c = true;
    insert setup;

    ContainerRecord[] containerCompanies = new ContainerRecord[] {

    };

    ContainerRecord[] containerContacts  = new ContainerRecord[] {

    };

    ContainerRecord[] containerLeads = new ContainerRecord[] {

    };

    //TODO
    System.assert(true);
  }

  @isTest
  static void refreshJobsInformation_01() {
    try {

      BvD_Job_Work_Item__c[] jobs = OrbisContainerController.refreshJobs(new String[] { [ SELECT Id FROM BvD_Job__c LIMIT 1].Id});
      System.assertEquals(
              1,
              jobs.size()
      );
      System.assert(true);
    } catch (Exception ex) {
      System.assert(true);
    }
  }

  @isTest
  static void extractIdsWithoutDuplicates_01() {
    ContainerRecord record = new ContainerRecord();
    record.recordId = 'test';
    record.id = 'bvdid';

    Map<String, ContainerRecord> result = OrbisContainerController.extractIdsWithoutDuplicates(new ContainerRecord[] { record });

    System.assertEquals(
            0,
            result.size()
    );
  }

  @isTest
  static void extractIdsWithoutDuplicates_02() {
    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';

    Map<String, ContainerRecord> result = OrbisContainerController.extractIdsWithoutDuplicates(new ContainerRecord[] { record });

    System.assertEquals(
            1,
            result.size()
    );
  }

  @isTest
  static void extractCompanyNames_01() {
    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountName = 'test';

    Map<String, String> result = OrbisContainerController.extractCompanyNames(new ContainerRecord[] { record });

    System.assertEquals(
            1,
            result.size()
    );
  }

  @isTest
  static void extractContactNames_01() {
    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountName = 'test';
    record.firstName = 'first';
    record.lastName = 'last';

    Map<String, String> result = OrbisContainerController.extractContactNames(new ContainerRecord[] { record });

    System.assertEquals(
            1,
            result.size()
    );
  }

  @isTest
  static void unvalidExistingCompanies_01() {
    Account account = new Account(Name = 'test');
    insert account;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountName = 'test';

    ContainerRecord[] result = OrbisContainerController.unvalidExistingCompanies(new Map<String, ContainerRecord[]> {
            'test' => new ContainerRecord[] { record }
    });

    System.assertEquals(
            account.Id,
            result[0].recordId
    );
  }

  @isTest
  static void unvalidExistingContacts_01() {
    Contact contact = new Contact(FirstName = 'first', LastName = 'Last');
    insert contact;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.firstName = 'first';
    record.lastName = 'last';

    ContainerRecord[] result = OrbisContainerController.unvalidExistingContacts(new Map<String, ContainerRecord[]>{
            'first last' => new ContainerRecord[]{
                    record
            }
    });

    System.assertEquals(
            contact.Id,
            result[0].recordId
    );
  }

  @isTest
  static void unvalidExistingLeads_01() {
    Lead lead = new Lead(Company = 'Test', FirstName = 'first', LastName = 'Last');
    insert lead;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountname = 'Test';

    ContainerRecord containerContact = new ContainerRecord();
    containerContact.firstName = 'first';
    containerContact.lastName = 'last';

    record.contacts = new ContainerRecord[] { containerContact };

    ContainerRecord[] result = OrbisContainerController.unvalidExistingLeads(new Map<String, ContainerRecord[]> {
            'first last||test' => new ContainerRecord[] { record }
    });

    System.assertEquals(
            lead.Id,
            result[0].recordId
    );
  }

  @isTest
  static void unvalidExistingLeads_02() {
    Lead lead = new Lead(Company = 'Test', FirstName = 'first', LastName = 'Last');
    insert lead;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountname = 'Test';

    ContainerRecord[] result = OrbisContainerController.unvalidExistingLeads(new Map<String, ContainerRecord[]> {
            '||test' => new ContainerRecord[] { record }
    });

    System.assertEquals(
            lead.Id,
            result[0].recordId
    );
  }

  @isTest
  static void unvalidateExistingRecords_01() {
    Lead lead = new Lead(Company = 'Test', FirstName = 'first', LastName = 'Last');
    insert lead;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountname = 'Test';

    Map<String, ContainerRecord[]> result = OrbisContainerController.unvalidExistingRecords(null, JSON.serialize(new ContainerRecord[] { record }));

    System.assertEquals(
            lead.Id,
            result.get('leads')[0].recordId
    );
  }
  @isTest
  static void unvalidateExistingRecords_02() {
    Lead lead = new Lead(Company = 'Test', FirstName = 'first', LastName = 'Last');
    insert lead;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountname = 'Test';
    ContainerRecord contact = new ContainerRecord();
    contact.id = 'bvdid';
    contact.accountname = 'Test';
    contact.firstName = 'first';
    contact.lastName = 'Last';

    record.contacts = new ContainerRecord[] { contact };


    Map<String, ContainerRecord[]> result = OrbisContainerController.unvalidExistingRecords(null, JSON.serialize(new ContainerRecord[] { record }));

    System.assertEquals(
            lead.Id,
            result.get('leads')[0].recordId
    );
    System.assertEquals(
            lead.Id,
            result.get('leads')[0].contacts[0].recordId
    );
  }

  @isTest
  static void unvalidateExistingRecords_03() {
    Account account = new Account(Name= 'Test');
    insert account;

    Contact cnt = new Contact(FirstName = 'first', LastName = 'Last', AccountId = account.Id);
    insert cnt;

    ContainerRecord record = new ContainerRecord();
    record.id = 'bvdid';
    record.accountname = 'Test';
    ContainerRecord contact = new ContainerRecord();
    contact.id = 'bvdid';
    contact.accountname = 'Test';
    contact.firstName = 'first';
    contact.lastName = 'Last';

    record.contacts = new ContainerRecord[] { contact };


    Map<String, ContainerRecord[]> result = OrbisContainerController.unvalidExistingRecords(JSON.serialize(new ContainerRecord[] { record }), null);

    System.assertEquals(
            account.Id,
            result.get('companies')[0].recordId
    );
    System.assertEquals(
            cnt.Id,
            result.get('companies')[0].contacts[0].recordId
    );
  }
}