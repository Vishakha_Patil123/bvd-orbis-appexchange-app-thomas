public with sharing class OrbisGetAvailableDatasourcesRequest extends OrbisRestRequest {

  protected override String getPartialUrl() {
    Bvd_Setup__c setup = BvDSetupSettings.getSetting();

    return String.format('/{0}/products', new String[]{
      setup.Organization_Token__c + UserInfo.getOrganizationId()
    });
  }

  protected override Type getOrbisResponseType(){
    return OrbisGetAvailableDatasourcesResponse.class;
  }

}