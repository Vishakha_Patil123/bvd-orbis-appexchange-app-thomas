@IsTest(IsParallel=true)
public class SearchCompanyCtrlTest {

    private class OrbisFaultMockFactory extends OrbisTestMocks.OrbisMockFactory {
        protected override OrbisMock getMatchMock() {
            return new OrbisFaultMock();
        }
    }

    @TestSetup
    static void setup(){
        Bvd_Setup__c settings = BvDSetupSettings.getSetting();
        settings.REST_URL__c = 'https://www.test.com';
        settings.Datasource_Webservice_URL__c = 'https://www.test.com';
        insert settings;
    }

    @IsTest
    static void testSearchCompanies() {
        SearchCompanyCtrl.SearchInput input = new SearchCompanyCtrl.SearchInput();
        input.CompanyName = 'Test';
        input.Country = 'UK';
        input.City = 'London';

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
        SearchCompanyCtrl.SearchResult[] results = SearchCompanyCtrl.searchCompany(JSON.serialize(input));
        Test.stopTest();

        System.assertEquals(2, results.size());
        System.assertEquals('Test 1', results.get(0).CompanyName);
        System.assertEquals(100, results.get(0).Score);
        System.assertEquals('A N A', results.get(1).CompanyName);
        System.assertEquals(50, results.get(1).Score);
    }

    @IsTest
    static void testSearchCompaniesException() {
        SearchCompanyCtrl.SearchInput input = new SearchCompanyCtrl.SearchInput();
        input.CompanyName = 'Test';
        input.Country = 'UK';
        input.City = 'London';

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new OrbisTestMocks(new OrbisFaultMockFactory()));
        try {
            SearchCompanyCtrl.SearchResult[] results = SearchCompanyCtrl.searchCompany(JSON.serialize(input));
            System.assert(false);
        } catch (AuraHandledException ex){
            System.assert(true);
        }
        Test.stopTest();

    }

  @isTest
  static void percentageDisplay_01() {
      upsert BvDSetupSettings.getSetting();
      System.assert(
        !SearchCompanyCtrl.percentageDisplay()
      );
  }

  @isTest
  static void getSessionId_01() {
    BvDSetupSettings.confirmAll();

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());

    ContainerCompanyUrl result = SearchCompanyCtrl.getContext();
    System.assert(true);
    Test.stopTest();
  }

  @isTest
  static void queryCountries_01() {
      List<Country__mdt> countries = new List<Country__mdt> {};

    countries.addAll(SearchCompanyCtrl.queryCountries());

    System.assert(countries != null);
  }

  @isTest
  static void insertRecord_01() {
      //TEST1_U

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    try {
        BvD_Job__c result = SearchCompanyCtrl.insertRecord('TEST1_U', 'Account');
      System.assert(false); // Never reached
    } catch (Exception ex) {
      System.assert(true); // should fail
    }
    Test.stopTest();
  }
}