public with sharing class CommonUtilities {

  public static List<String> getFields(String sobj) {
    List<String> fields = new List<String> {};

    for(Schema.SObjectField field: Schema.getGlobalDescribe().get(sobj).getDescribe().fields.getMap().values()) {
      fields.add(field.getDescribe().getName());
    }

    return fields;
  }

  public static List<SObject> queryAllFields(String sObjectName) {

    String query = String.format('SELECT {0} FROM {1} ', new String [] {
      String.join(CommonUtilities.getFields(sObjectName), ','),
      sObjectName
    });

    return Database.query(query);
  }

  public static List<SObject> queryAllFieldsWithCondition(String sObjectName, String conditions) {

    String query = String.format('SELECT {0} FROM {1} {2}', new String [] {
      String.join(CommonUtilities.getFields(sObjectName), ','),
      sObjectName,
      conditions
    });

    return Database.query(query);
  }
    
    public static SObject queryAllFieldsFromId(String sObjectName, String recordId) {
        String query = String.format('SELECT {0} FROM {1} WHERE Id = :recordId', new String [] {
            String.join(CommonUtilities.getFields(sObjectName), ','),
                sObjectName
                });
        
        return Database.query(query); 
    }

  public static SObject queryAllFieldsLimitOne(String sObjectName) {

    String query = String.format('SELECT {0} FROM {1} LIMIT 1', new String [] {
      String.join(CommonUtilities.getFields(sObjectName), ','),
      sObjectName
    });

    return Database.query(query);
  }

  public static String getFieldsQuery(String sobj) {
    return String.join(getFields(sobj), ', ');
  }

  public static String getLogFieldsQuery(List<String> fields) {
    return String.join(fields, ', ');
  }

  public static void checkReadAndThrowAura(SObjectType type, List<String> fields) {

    try {
      SecurityUtils.checkRead(type, fields);

    } catch (SecurityUtils.FlsException ex) {

      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    } catch (SecurityUtils.CrudException ex) {

      Logger.error(ex);
      Logger.stop();
      throw new AuraHandledException(ex.getMessage());
    }

  }

  public static String truncate(String str, Integer length){
    return String.isBlank(str) ? '' : str.substring(0,Math.min(str.length(), length));
  }
}