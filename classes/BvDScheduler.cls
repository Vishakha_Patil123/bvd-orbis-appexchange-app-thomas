/**
* Scheduler class which schedules execution of BvD Batch according to the Batch Settings
* BvD Batch is configured to perform "update" action on records.
* */
public with sharing class BvDScheduler implements Schedulable {
  private BatchSettings setting;

  public BvDScheduler(SObjectType sobjType) {
    this.setting = BatchSettingsFactory.getSetting(sobjType);
  }


  /**
  * Check Cron expression on all scheduled jobs and if schedule was changed, reschedule that job
  * */
  public static void updateSchedules() {
    List<SObjectType> sObjects = new List<SObjectType>{
      Account.getSObjectType(), Contact.getSObjectType(), Lead.getSObjectType()
    };

    for (SObjectType sObj : sObjects) {
      BvDScheduler scheduler = new BvDScheduler(sObj);
      if (scheduler.isCronExpressionChanged()) {
        scheduler.terminateJob();
        scheduler.scheduleJob();
      }
    }
  }

  /**
  * Schedule BvD Batch Job for current sObject according to
  * Batch Settings
  * */
  public Id scheduleJob() {
    Id jobId = System.schedule(
      setting.getScheduledJobName(),
      setting.getCronExpression(),
      this);

    setting.setJobID(jobId);
    setting.enable();
    setting.updateSetting();

    return jobId;
  }

  /**
  * @description
  * Executes a Batch Job to update all records of given type
  * */
  public void execute(SchedulableContext sc) {
    this.setting = BatchSettingsFactory.getSetting(setting.getObjectName());
    Import_Setting__c importSetting = Import_Setting__c.getInstance('Account.Update');
    String processing = importSetting == null ? BvdJobs.PROCESSING_AUTOMATIC : importSetting.Processing__c;

    if (setting.isEnabled()) {
      BvDJobs.updateRecordsBatch(setting.getSObjectType(), processing);
    }
  }

  private Boolean isCronExpressionChanged() {
    try {
      CronTrigger ct = [
        SELECT CronExpression
        FROM CronTrigger
        WHERE Id = :setting.getJobID()
      ];

      return ct.CronExpression != setting.getCronExpression();
    } catch (Exception ex) {
      return true;
    }
  }

  /**
  * Termiantes Job and updates Custom Setting -
  * Job Id is cleared, Disabled is set to true
  * */
  public void terminateJob() {
    try {
      System.abortJob(setting.getJobID());
    } catch (Exception ex) {
    }

    setting.setJobID(null);
    setting.disable();
    setting.updateSetting();
  }
}