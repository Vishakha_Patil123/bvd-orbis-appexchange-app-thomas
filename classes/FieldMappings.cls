public with sharing class FieldMappings {
  public final static String
    ACTION_TYPE_AUTOMATIC = 'Automatic',
    ACTION_TYPE_MANUAL = 'Manual',
    MASTER_BVD = 'BvD',
    MASTER_SF = 'Salesforce';
  private final static FieldMappingsFactory Factory = new FieldMappingsFactory();

  private final Map<String, FieldMapping> fieldMappings;
  private Boolean isValid;

  public static FieldMappings getInstance(String sObjectType) {
    return Factory.create(sObjectType);
  }

  public static FieldMappings getInstance(SObjectType sObjectType) {
    return Factory.create(sObjectType);
  }

  private FieldMappings(FieldMapping[] mappings) {
    this.fieldMappings = new Map<String, FieldMapping>();

    for (FieldMapping fm : mappings) {
      this.fieldMappings.put(fm.getSFField(), fm);
    }
  }

  /**
  * Returns list of Field Mappings - pairs of SF and BVD fields
  * */
  public List<FieldMapping> getFieldMappings() {
    return this.fieldMappings.values();
  }

  /*
  * Return a string with all SF fields comma separated
  * */
  public String getSalesforceFieldsCommaSeparated() {
    return String.join(getSalesforceFields(), ',');
  }

  /*
  * Return a string with all SF fields comma separated
  * */
  public String getBvDFieldsCommaSeparated() {
    return String.join(getBvDFields(), ',');
  }

  public String getBvDFieldsQueryCommaSeparated() {
    return String.join(getBvDQueries(), ',');
  }

  /*
  * Return a string with all BvD fields as line comma separated (contact)
  * */
  public String getBvDFieldLinesCommaSeparated() {
    return String.join(getBvDFields(), ',');
  }

  /**
  * @description
  * Returns list of all Salesforce fields from Field Mappings
  * */
  public List<String> getSalesforceFields() {
    Set<String> salesforceFields = new Set<String>{
      'Id', 'Name', 'BvD_Id__c'
    };

    for (FieldMapping fieldMapping : fieldMappings.values()) {
      if (String.isNotBlank(fieldMapping.getSFField()))
        salesforceFields.add(fieldMapping.getSFField());
    }

    return new List<String>(salesforceFields);
  }

  /**
  * @description
  * Returns list of all BvD fields from Field Mappings
  * */
  public List<String> getBvDFields() {
    Set<String> bvdFields = new Set<String>{'NAME','BVD_ID_NUMBER'};

    for (FieldMapping fieldMapping : fieldMappings.values()) {
      if (String.isNotBlank(fieldMapping.getBvDField()))
        bvdFields.add(fieldMapping.getBvDField());
    }

    return new List<String>(bvdFields);
  }

  public List<String> getBvDQueries() {
    Set<String> bvdFields = new Set<String>{'NAME','BVD_ID_NUMBER'};

    for (FieldMapping fieldMapping : fieldMappings.values()) {
      if (!fieldMapping.isExcluded() && String.isNotBlank(fieldMapping.getQuery()))
        bvdFields.add(fieldMapping.getQuery());
    }

    return new List<String>(bvdFields);
  }

  public String getDefaultChoice(String sfField) {
    if (this.fieldMappings.containsKey(sfField)) {
      return fieldMappings.get(sfField).getMasterField();
    } else {
      return null;
    }
  }


  /**
  * Factory Class for FieldMappings
  * Handles FieldMappings creation responsibility
  * */
  public class FieldMappingsFactory {
    private Map<String, FieldMappings> fieldMappingsBySObject = new Map<String, FieldMappings>();

    public FieldMappings create(SObjectType sObjectType) {
      return create(sObjectType.getDescribe().getName());
    }

    public FieldMappings create(String sObjectType) {
      if (instanceNotCreated(sObjectType)) {
        createFieldMappingInstance(sObjectType);
      }

      return fieldMappingsBySObject.get(sObjectType);
    }

    private Boolean instanceNotCreated(String sObjectType) {
      return !fieldMappingsBySObject.containsKey(sObjectType);
    }

    private void createFieldMappingInstance(String sObjectType) {
      List<FieldMapping> mappings = new List<FieldMapping>();

      for (Field_Mapping__c fm : FieldMappingSelector.getFieldMappingsForObject(sObjectType)) {
        mappings.add(new FieldMapping(fm));
      }

      fieldMappingsBySObject.put(sObjectType, new FieldMappings(mappings));
    }
  }
}