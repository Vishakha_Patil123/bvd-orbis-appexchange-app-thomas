/**
* @description Abstract Orbis Response class
* Each callout to Orbis must extend this abstract class
* to parse SOAP response.
* Concrete classes has to implement getResponseSchema method, which returns
* SOAP Schema element of the response (XML node which resides within soap:body).
*
* Orbis Response parse xml internally and provide number of getter methods which
* provide easy access to content of the response.
* */
public with sharing abstract class OrbisResponse {
    private HttpResponse response;
    private Dom.Document document;
    private OrbisSchema.EnvelopeElement envelopeElement = new OrbisSchema.EnvelopeElement();

    protected OrbisResponse() {
    }

    public OrbisResponse(HttpResponse response) {
        this.response = response;
        this.document = response.getBodyDocument();
        this.response = null;

        this.envelopeElement.body.content = getResponseSchema();
        this.envelopeElement.deserializeXML(document);
        this.document = null;
    }

    protected abstract OrbisSchema.ResponseElement getResponseSchema();
}