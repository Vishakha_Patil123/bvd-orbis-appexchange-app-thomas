public with sharing class ContactsActionButtonController {

  public class CompanyContact {
    @AuraEnabled
    public String companyId { get; set; }
    @AuraEnabled
    public String lastName { get; set; }
    @AuraEnabled
    public String firstName { get; set; }
    @AuraEnabled
    public String function { get; set; }
    @AuraEnabled
    public String id { get; set; }
    @AuraEnabled
    public String recordId { get; set; }
    @AuraEnabled
    public String department {get; set;}
    @AuraEnabled
    public Boolean delayed {get; set;}

    public CompanyContact(String companyId) { this.companyId = companyId; }

    public void set(String field, String value) {
      if(String.isNotBlank(value)) {
        if('CPYCONTACTS_HEADER_LastNameOriginalLanguagePreferred' == field) {
          lastName = value;
        }
        else if('CPYCONTACTS_HEADER_FirstNameOriginalLanguagePreferred' == field) {
          firstName = value;
        }
        else if('CPYCONTACTS_MEMBERSHIP_OriginalJobTitle' == field) {
          function = value;
        }
        else if('CPYCONTACTS_HEADER_IdDirector' == field) {
          id = value;
        }
        else if('CPYCONTACTS_MEMBERSHIP_DepartmentFromHierCodeFall2009' == field){
          department = value;
        }

      } else {
        Logger.warn(String.format('Empty value for field: {0}', new String[] { field }));
      }
    }

    public String getId() { return id; }
    public String getCompanyId() { return companyId; }
    public String getKey() { return String.format('{0}|{1}', new String[] { companyId, id }); }
  }

  /*
    Static query attributes
  */
  public static final Set<String> BVDCONTACTFIELDS = new Set<String> {
    'CPYCONTACTS_HEADER_LastNameOriginalLanguagePreferred',
    'CPYCONTACTS_HEADER_FirstNameOriginalLanguagePreferred',
    'CPYCONTACTS_MEMBERSHIP_OriginalJobTitle',
    'CPYCONTACTS_HEADER_IdDirector',
    'CPYCONTACTS_MEMBERSHIP_DepartmentFromHierCodeFall2009'
  };

  public static final String BVDFILTER = 'Filter.Name=ContactsFilter;'
    + 'ContactsFilter.HierarchicCodeToExcludeQueryString=3|4|;'
    + 'ContactsFilter.IfHomeOnlyReturnCountry=1;'
    + 'ContactsFilter.SourcesToExcludeQueryString=99B|59B|69B|70B|0|;'
    + 'ContactsFilter.HierarchicCodeQueryString=0|1|2|';

  public static final String BVDUSING = 'Parameters.RepeatingDimension=NrOfBvDContacts;'
    + 'Parameters.Currency=DefaultRecordCurrency;Parameters.RepeatingMaxCount=100;';

  public static final String BVDDEFINE = 'F1 AS [' + BVDFILTER + '], P1 AS [' + BVDUSING + ']';

  public static final String CONTACTLINES;

  static {
    List<String> selectLines = new List<String> {};

    for (String field : BVDCONTACTFIELDS) {
      String line = String.format('{0} FILTER F1 USING P1 AS {0} ', new String[] {
        field
      });

      selectLines.add(line);
    }

    CONTACTLINES = String.join(selectLines, ',');
  }

  /*
    Private functions
   */
  private static Map<String, OrbisSchema.RecordElement> getCompanies(String[] bvdIds, String context) {
    OrbisAPI orbisAPI = new OrbisAPI();

    OrbisFindByBvDIdResponse findResponse = (OrbisFindByBvDIdResponse) orbisAPI.send(
      new OrbisFindByBvDIdRequest()
        .setSessionID(context)
        .setIDs(bvdIds)
    );

    OrbisGetDataResponse dataResponse = (OrbisGetDataResponse) orbisAPI.send(
      new OrbisGetDataRequest()
        .setSessionID(context)
        .setFromRecord(0)
        .setNrRecords(-1)
        .setQuery(String.format('DEFINE {0}; SELECT {1} FROM RemoteAccess.UNIVERSAL', new String[] {
          BVDDEFINE, CONTACTLINES
        }))
        .setSelection(findResponse.getToken(), findResponse.getSelectionCount())
    );

    return dataResponse.getRecordsMap();
  }

  private static OrbisSchema.RecordElement getCompany(String bvdId, String context) {
    return getCompanies(new String[] { bvdId }, context).get(bvdId);
  }

  private static List<CompanyContact> getContacts(String bvdId, OrbisSchema.RecordElement bvdRecord) {
    Logger.start('ContactsActionButtonController', 'getContacts');

    List<CompanyContact> contacts = new List<CompanyContact> {};

    for (String field: BVDCONTACTFIELDS) {
      OrbisSchema.FieldElement bvdField           = bvdRecord.getField(field);
      List<OrbisSchema.SubFieldElement> children  = bvdField.getChildren();

      for(Integer index = 0; index < children.size(); ++index) {
        if(index < contacts.size()) {
          CompanyContact contact = contacts.get(index);
          contact.set(field, children[index].value);
        } else {
          CompanyContact contact = new CompanyContact(bvdId);
          contact.set(field, children[index].value);
          contacts.add(contact);
        }
      }
    }

    return contacts;
  }

  public static Map<String, CompanyContact> contactByBvDId(List<CompanyContact> contacts) {
    Logger.start('ContactsActionButtonController', 'contactByBvDId');
    Map<String, CompanyContact> contactByBvDId = new Map<String, CompanyContact> {};

    for (CompanyContact contact : contacts) {
      contactByBvDId.put(contact.getId(), contact);
    }

    return contactByBvDId;
  }

  public static List<CompanyContact> assignRecordId(
    Map<String, CompanyContact> contactByBvDId,
    String bvdId
  ) {
    Logger.start('ContactsActionButtonController', 'assignRecordId');

    for (Schema.Contact contact: [
      SELECT Id, BvD_Id__c, BvD_Company_Id__c
      FROM Contact
      WHERE BvD_Id__c IN :contactByBvDId.keySet() AND BvD_Company_Id__c = :bvdId
    ]) {
      contactByBvDId.get(contact.BvD_Id__c).recordId = contact.Id;
    }

    return contactByBvDId.values();
  }


  /*
    Aura functions
   */

  @AuraEnabled
  public static List<CompanyContact> getContacts(String bvdId, String context) {

    try {
      OrbisSchema.RecordElement bvdRecord         = getCompany(bvdId, context);
      Map<String, CompanyContact> contactByBvDId  = contactByBvDId(getContacts(bvdId, bvdRecord));
      List<CompanyContact> contacts               = assignRecordId(contactByBvDId, bvdId);
      return contacts;
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.flush();
    }
  }

  @AuraEnabled
  public static CompanyContact[] importContact(String contact, String recordId) {
    try {
      Logger.start('ContactsActionButtonController', 'importContact');

    return importContact(
      (CompanyContact[]) JSON.deserialize(contact, Type.forName('List<ContactsActionButtonController.CompanyContact>')),
      recordId
    );
    } catch (Exception ex) {
      Logger.error(ex);
      throw new AuraHandledException(ex.getMessage());
    } finally {
      Logger.flush();
    }
  }
  private static CompanyContact[] importContact(CompanyContact[] contacts, String recordId) {
    if(contacts.size() == 1) {
      contacts.set(0, importContact(contacts.get(0), recordId));
    } else {
      for (CompanyContact contact : contacts) {
        contact.delayed = true;
        if(String.isBlank(contact.recordId)) {
          BvDJobs.insertRecordsBatch(
            contact.companyId,
            new List<String> { contact.id },
            Schema.Contact.SObjectType,
            BvDJobs.PROCESSING_AUTOMATIC
          );
        } else {
          BvDJobs.updateRecordsBatch(
            contact.companyId,
            new List<String> { contact.id },
            Schema.Contact.SObjectType,
            BvDJobs.PROCESSING_AUTOMATIC
          );
        }
      }
    }

    return contacts;
  }

  private static CompanyContact importContact(CompanyContact contact, String recordId) {
      if(String.isBlank(contact.recordId)) {
        BvD_Job__c job = BvDJobs.insertRecordsImmediatly(
          contact.companyId,
          new List<String> { contact.Id },
          Schema.Contact.SObjectType,
          BvDJobs.PROCESSING_AUTOMATIC
        );

        contact.recordId = job.BvD_Job_Work_Items__r[0].RecordId__c;
        if(BVDUserPermissionCheck.permissionToUpdateSobject('Contact', true, new List<String>{'recordId', 'AccountId'})){
          update new Contact( Id = contact.recordId, AccountId = recordId );
        }else{
          Logger.error('Not enough privileges to update contact');
          throw new AuraHandledException('Not enough privileges to update contact');        
		}
      } else {
        BvD_Job__c job = BvDJobs.updateRecordsImmediatly(
          [ SELECT Id, BvD_Id__c, BvD_Company_Id__c FROM Contact WHERE Id = :contact.Id],
          BvDJobs.PROCESSING_AUTOMATIC
        );
      }

      return contact;
  }
}