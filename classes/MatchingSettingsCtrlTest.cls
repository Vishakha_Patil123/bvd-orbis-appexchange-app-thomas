@IsTest
public with sharing class MatchingSettingsCtrlTest {

  @TestSetup
  public static void setup() {
    insert new Matching_Mapping__c[]{
      new Matching_Mapping__c(
        Name = 'Account.CompanyName',
        Object__c = 'Account',
        BvD_Field__c = 'CompanyName',
        SF_Field__c = 'Name'
      ),
      new Matching_Mapping__c(
        Name = 'Account.City',
        Object__c = 'Account',
        BvD_Field__c = 'City',
        SF_Field__c = 'BillingCity'
      ),
      new Matching_Mapping__c(
        Name = 'Account.Country',
        Object__c = 'Account',
        BvD_Field__c = 'Country',
        SF_Field__c = 'BillingCountry'
      ),

      new Matching_Mapping__c(
        Name = 'Contact.CompanyName',
        Object__c = 'Contact',
        BvD_Field__c = 'CompanyName',
        SF_Field__c = 'Name'
      ),
      new Matching_Mapping__c(
        Name = 'Contact.Country',
        Object__c = 'Contact',
        BvD_Field__c = 'Country',
        SF_Field__c = 'MailingCountry'
      )
    };

    insert new Matching_Columns__c[]{
      new Matching_Columns__c(
        Name = 'Account.CompanyName',
        Object__c = 'Account',
        Column__c = 'CompanyName',
        Order__c = 0
      ),
      new Matching_Columns__c(
        Name = 'AccountCity.',
        Object__c = 'Account',
        Column__c = 'City',
        Order__c = 1
      ),
      new Matching_Columns__c(
        Name = 'Account.Address',
        Object__c = 'Account',
        Column__c = 'Address',
        Order__c = 2
      ),

      new Matching_Columns__c(
        Name = 'Contact.CompanyName',
        Object__c = 'Contact',
        Column__c = 'CompanyName',
        Order__c = 0
      ),
      new Matching_Columns__c(
        Name = 'Contact.Country',
        Object__c = 'Contact',
        Column__c = 'Country',
        Order__c = 1
      )
    };
  }

  @IsTest
  static void testRetrievedColumns() {
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    System.assertEquals(settings.columnsByObject, new Map<String, List<String>>{
      'Account' => new List<String>{
        'CompanyName', 'City', 'Address'
      },
      'Contact' => new List<String>{
        'CompanyName', 'Country'
      },
      'Lead' => new List<String>()
    });
  }

  @IsTest
  static void testRetrieveMapping() {
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    System.assertEquals(settings.fieldMappingsByObject, new Map<String, Map<String, String>>{
      'Account' => new Map<String, String>{
        'CompanyName' => 'Name',
        'City' => 'BillingCity',
        'Country' => 'BillingCountry'
      },
      'Contact' => new Map<String, String>{
        'CompanyName' => 'Name',
        'Country' => 'MailingCountry'
      },
      'Lead' => new Map<String, String>()
    });
  }

  @IsTest
  static void testFieldsAreAccessible() {
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    for (String sObjectType : settings.fieldsByObject.keySet()) {
      SObjectDescribe describe = SObjectDescribe.getDescribe(sObjectType);

      for (MatchingSettingsCtrl.SFField field : settings.fieldsByObject.get(sObjectType)) {
        System.assertEquals(true,
          describe.getField(field.name).getDescribe().isAccessible());
      }
    }
  }

  @IsTest
  static void testSaveColumns() {
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    settings.columnsByObject = new Map<String, List<String>>{
      'Account' => new List<String>(),
      'Contact' => new List<String>(),
      'Lead' => new List<String>{
        'CompanyName', 'Country'
      }
    };


    Test.startTest();
    MatchingSettingsCtrl.saveSettings(JSON.serialize(settings));
    Test.stopTest();


    settings = MatchingSettingsCtrl.getSettings();
    System.assertEquals(settings.columnsByObject, new Map<String, List<String>>{
      'Account' => new List<String>(),
      'Contact' => new List<String>(),
      'Lead' => new List<String>{
        'CompanyName', 'Country'
      }
    });
  }

  @IsTest
  static void testSaveFieldMappings() {
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    Map<String, Map<String, String>> fieldMappings = new Map<String, Map<String, String>>{
      'Account' => new Map<String, String>(),
      'Contact' => new Map<String, String>(),
      'Lead' => new Map<String, String>{
        'CompanyName' => 'Name',
        'City' => 'BillingCity',
        'Country' => 'BillingCountry'
      }
    };
    settings.fieldMappingsByObject = fieldMappings;


    Test.startTest();
    MatchingSettingsCtrl.saveSettings(JSON.serialize(settings));
    Test.stopTest();


    settings = MatchingSettingsCtrl.getSettings();
    System.assertEquals(fieldMappings, settings.fieldMappingsByObject);
  }

  @IsTest
  static void testException() {
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    settings.columnsByObject = new Map<String, List<String>>{
      'Account' => new List<String>(),
      'Contact' => new List<String>(),
      'Lead' => new List<String>{
        'CompanyName', 'CompanyName'
      }
    };


    try {
      Test.startTest();
      MatchingSettingsCtrl.saveSettings(JSON.serialize(settings));
      Test.stopTest();
      System.assert(false);
    } catch (AuraHandledException ex) {
      System.assert(true);
    }
  }

  @IsTest
  static void testBlacklistedFieldsAreNotListed() {
    Test.startTest();
    MatchingSettingsCtrl.Settings settings = MatchingSettingsCtrl.getSettings();
    Test.stopTest();

    for (MatchingSettingsCtrl.SFField sfField : settings.fieldsByObject.get('Account')) {
      System.assert(FieldMappingWhitelist.contains('Account', sfField.name));
    }
  }
}