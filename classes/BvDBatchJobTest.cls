@IsTest
public with sharing class BvDBatchJobTest {

  private static List<String> getTestBvdIds() {
    List<String> lBvDIds = new List<String>();
    for (Integer i = 0; i < 20; i++) {
      lBvDIds.add('TEST#' + i);
    }
    return lBvDIds;
  }

  @TestSetup
  static void setup() {
    List<Account> accounts = new List<Account>();
    for (Integer i = 0; i < 20; i++) {
      Account account = new Account(
        Name = 'Test Account'
      );
      accounts.add(account);
    }
    for (Integer i = 0; i < 20; i++) {
      Account account = new Account(
        Name = 'Test Account',
        BvD_Id__c = 'TEST#' + (i + 20)
      );
      accounts.add(account);
    }
    insert accounts;

    insert new List<Contact>{
      new Contact(
        FirstName = 'John',
        LastName = 'Doe'
      )
    };

    insert new List<Field_Mapping__c>{
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'NAME',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = 'Name'
      ),
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'ADDR',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Account.ShippingStreet.getDescribe().getName()
      ),
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'ADDR2',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Account.ShippingState.getDescribe().getName()
      ),
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'POSTCODE',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Account.ShippingPostalCode.getDescribe().getName()
      ),
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'CITY',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Account.ShippingCity.getDescribe().getName()
      ),
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'COUNTRY',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Account.ShippingCountry.getDescribe().getName()
      ),
      new Field_Mapping__c(
        SObject__c = 'Account',
        BvD_Field__c = 'IPO_DATE',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Account.Last_BvD_Update__c.getDescribe().getName()
      ),

      new Field_Mapping__c(
        SObject__c = 'Contact',
        BvD_Field__c = 'CLOSDATE',
        Master__c = FieldMappings.MASTER_BVD,
        Action__c = FieldMappings.ACTION_TYPE_AUTOMATIC,
        SF_Field__c = Contact.Birthdate.getDescribe().getName()
      )
    };


    Bvd_Setup__c settings = BvDSetupSettings.getSetting();
    settings.Datasource_Webservice_URL__c = 'https://webservices.bvdep.com/orbis4/remoteaccess.asmx';
    insert settings;
  }

  private static List<Account> getAllAccounts() {
    return [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry,
        BvD_Id__c,
        Unlinked_from_BvD__c
      FROM Account
    ];
  }

  private static List<Account> getAccounts(List<Account> lAccounts) {
    return [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry,
        BvD_Id__c,
        Unlinked_from_BvD__c
      FROM Account
      WHERE ID in:lAccounts
    ];
  }

  private static List<Account> getLinkedAccounts() {
    return [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry,
        BvD_Id__c,
        Unlinked_from_BvD__c
      FROM Account
      WHERE BvD_Id__c != null
    ];
  }

  private static List<Account> getUnlinkedAccounts() {
    return [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry,
        BvD_Id__c,
        Unlinked_from_BvD__c
      FROM Account
      WHERE BvD_Id__c = null
    ];
  }

  @IsTest
  static void testUnlinkAccountBatch() {
    Map<Id, Account> accounts = new Map<ID, Account>(getLinkedAccounts());

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    Id batchJobID = BvDJobs.unlinkRecordsBatch(getLinkedAccounts(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsByJob(batchJobID);
    System.assertEquals(accounts.size(), workItems.size());
    for (BvD_Job_Work_Item__c wi : workItems) {
      System.assertNotEquals(null, wi.RecordId__c);
      System.assert(accounts.containsKey(wi.RecordId__c));
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }

    for (Account acc : getAccounts(accounts.values())) {
      System.assertEquals(null, acc.BvD_Id__c);
      System.assertEquals(true, acc.Unlinked_from_BvD__c);
    }
  }

  @IsTest
  static void testUnlinkAccountImmediatly() {
    Map<Id, Account> accounts = new Map<ID, Account>(getLinkedAccounts());


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.unlinkRecordsImmediatly(accounts.values(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsByJob(job.Id);
    System.assertEquals(accounts.size(), workItems.size());
    for (BvD_Job_Work_Item__c wi : workItems) {
      System.assertNotEquals(null, wi.RecordId__c);
      System.assert(accounts.containsKey(wi.RecordId__c));
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }

    for (Account acc : getAccounts(accounts.values())) {
      System.assertEquals(null, acc.BvD_Id__c);
      System.assertEquals(true, acc.Unlinked_from_BvD__c);
    }
  }


  /**
  * Test if Job will successfully immediately insert Accounts with BvD Data
  * */
  @IsTest
  static void testInsertAccountImmediate() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.insertRecordsImmediatly('TEST#1', Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    BvD_Job_Work_Item__c wi = job.BvD_Job_Work_Items__r.get(0);
    System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);

    Account acc = [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry
      FROM Account
      WHERE BvD_Id__c = 'TEST#1'
    ];
    System.assertEquals('JOHN DOES', acc.Name);
    System.assertEquals('STREET 22A LOK. 42', acc.ShippingStreet);
    System.assertEquals(null, acc.ShippingState);
    System.assertEquals('02-672', acc.ShippingPostalCode);
    System.assertEquals('WARSZAWA', acc.ShippingCity);
    System.assertEquals('Poland', acc.ShippingCountry);
  }

  /**
  * Test if Job will successfully immediately insert Accounts with BvD Data
  * */
  @IsTest
  static void testInsertAccountListImmediate() {
    List<String> lBvDIds = getTestBvdIds();


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.insertRecordsImmediatly(lBvDIds, Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    BvD_Job_Work_Item__c[] lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(lBvDIds.size(), lWorkItems.size());
    for (BvD_Job_Work_Item__c wi : lWorkItems) {
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }


    Account[] lAccounts = [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry
      FROM Account
      WHERE BvD_Id__c IN :lBvDIds
    ];
    System.assertEquals(lBvDIds.size(), lAccounts.size());
    for (Account acc : lAccounts) {
      System.assertEquals('JOHN DOES', acc.Name);
      System.assertEquals('STREET 22A LOK. 42', acc.ShippingStreet);
      System.assertEquals(null, acc.ShippingState);
      System.assertEquals('02-672', acc.ShippingPostalCode);
      System.assertEquals('WARSZAWA', acc.ShippingCity);
      System.assertEquals('Poland', acc.ShippingCountry);
    }
  }


  /**
  * Test if Batch will successfully insert Accounts with BvD Data
  * */
  @IsTest
  static void testInsertAccountBatch() {
    List<String> lBvDIds = getTestBvdIds();


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    ID jobId = BvDJobs.insertRecordsBatch(lBvDIds, Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    BvD_Job_Work_Item__c[] lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(lBvDIds.size(), lWorkItems.size());
    for (BvD_Job_Work_Item__c wi : lWorkItems) {
      System.debug(wi);
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }

    Account[] lAccounts = [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry
      FROM Account
      WHERE BvD_Id__c IN :lBvDIds
    ];
    System.assertEquals(lBvDIds.size(), lAccounts.size());
    for (Account acc : lAccounts) {
      System.assertEquals('JOHN DOES', acc.Name);
      System.assertEquals('STREET 22A LOK. 42', acc.ShippingStreet);
      System.assertEquals(null, acc.ShippingState);
      System.assertEquals('02-672', acc.ShippingPostalCode);
      System.assertEquals('WARSZAWA', acc.ShippingCity);
      System.assertEquals('Poland', acc.ShippingCountry);
    }
  }


  /**
  * @Scenario:
  * Batch fails due to authorization error
  *
  * @Result:
  * Job is saved with error status and Orbis Service failure reason.
  * For each record, Work Item is created in error status
  * */
  @IsTest
  static void testInsertAccountBatchAuthorizationError() {
    List<String> lBvDIds = getTestBvdIds();


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks(new OrbisFaultAuthMockFactory()));
    ID jobId = BvDJobs.insertRecordsBatch(lBvDIds, Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvDJobs.STATUS_ERROR, job.Status__c);

    BvD_Job_Work_Item__c[] lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(lBvDIds.size(), lWorkItems.size());
    for (BvD_Job_Work_Item__c wi : lWorkItems) {
      System.assertEquals(BvDJobWorkItems.STATUS_ERROR, wi.Status__c);
    }
    Integer accountsCount = [
      SELECT Count()
      FROM Account
      WHERE BvD_Id__c IN :lBvDIds
    ];
    System.assertEquals(0, accountsCount);
  }

  /**
  * @Scenario:
  * Batch Job fails at first and user schedules rerun
  *
  * @Result:
  * Failed records are processed again, this time successfully
  * Accounts are created with current mapping
  * */
  @IsTest
  static void testInsertAccountRetry() {
    BvD_Job__c job = new BvD_Job__c(
      Object__c = 'Account',
      Type__c = BvDJobs.TYPE_INSERT,
      Processing__c = BvDJobs.PROCESSING_MANUAL,
      Status__c = BvDJobs.STATUS_ERROR,
      Failure_Reason__c = 'Field Mapping Conflict'
    );
    insert job;

    BvD_Job_Work_Item__c workItem = new BvD_Job_Work_Item__c(
      BvD_Id__c = 'TEST#1',
      Status__c = BvDJobWorkItems.STATUS_ERROR,
      BvD_Job__c = job.Id
    );
    insert workItem;


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    Id jobId = BvDJobs.retryJob(job.Id);
    Test.stopTest();

    job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    BvD_Job_Work_Item__c[] lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(1, lWorkItems.size());
    System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, lWorkItems.get(0).Status__c);

    Account acc = [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry
      FROM Account
      WHERE Id = :lWorkItems.get(0).RecordId__c
    ];

    System.assertEquals('JOHN DOES', acc.Name);
    System.assertEquals('STREET 22A LOK. 42', acc.ShippingStreet);
    System.assertEquals(null, acc.ShippingState);
    System.assertEquals('02-672', acc.ShippingPostalCode);
    System.assertEquals('WARSZAWA', acc.ShippingCity);
    System.assertEquals('Poland', acc.ShippingCountry);
  }


  /**
  * @Scenario
  * Account is linked to the BvD Company
  *
  * @Result
  * BvD Fields are synced to SF
  * */
  @IsTest
  static void testLinkAccountImmediately() {
    Account account = [
      SELECT ID
      FROM Account
      WHERE BvD_Id__c = null
      LIMIT 1
    ];


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.linkRecordsImmediatly(account.Id, 'TEST#1', BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    /*BvD Job assertion*/
    System.assertEquals(BvdJobs.STATUS_SUCCESS, job.Status__c);

    /*Work Item assertions*/
    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsByJob(job.Id);
    System.assertEquals(1, workItems.size());
    BvD_Job_Work_Item__c wi = workItems.get(0);


    System.assertEquals(account.Id, wi.RecordId__c);
    System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);


    account = [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry
      FROM Account
      WHERE Id = :account.Id
    ];
    /*Account Assertions*/
    System.assertEquals('JOHN DOES', account.Name);
    System.assertEquals('STREET 22A LOK. 42', account.ShippingStreet);
    System.assertEquals(null, account.ShippingState);
    System.assertEquals('02-672', account.ShippingPostalCode);
    System.assertEquals('WARSZAWA', account.ShippingCity);
    System.assertEquals('Poland', account.ShippingCountry);
  }

  /**
  * @Scenario
  * Account is linked to the BvD Company
  *
  * @Result
  * BvD Fields are synced to SF
  * */
  @IsTest
  static void testLinkAccountBatch() {
    List<Account> lAccounts = getUnlinkedAccounts();
    List<String> bvdIds = getTestBvdIds();

    Map<Id, String> recordsToLink = new Map<Id, String>();
    for (Integer i = 0; i < lAccounts.size(); i++) {
      recordsToLink.put(lAccounts[i].Id, bvdIds[i]);
    }


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    ID jobId = BvDJobs.linkRecordsBatch(recordsToLink, Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    /*BvD Job assertion*/
    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvdJobs.STATUS_SUCCESS, job.Status__c);

    /*Work Item assertions*/
    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsByJob(job.Id);
    System.assertEquals(recordsToLink.size(), workItems.size());

    for (BvD_Job_Work_Item__c wi : workItems) {
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }

    /*Account Assertions*/
    for (Account account : getAccounts(lAccounts)) {
      System.assertEquals('JOHN DOES', account.Name);
      System.assertEquals('STREET 22A LOK. 42', account.ShippingStreet);
      System.assertEquals(null, account.ShippingState);
      System.assertEquals('02-672', account.ShippingPostalCode);
      System.assertEquals('WARSZAWA', account.ShippingCity);
      System.assertEquals('Poland', account.ShippingCountry);
    }
  }

  /**
  * @Scenario
  * Field Mappings are manual type
  *
  * @Result
  * Manual Field Mappings create Conflict records to be resolved by user
  * */
  @IsTest
  static void testLinkManualFieldsCreatesFieldConflicts() {
    Account account = getUnlinkedAccounts().get(0);

    List<Field_Mapping__c> lFieldMappings = [SELECT ID FROM Field_Mapping__c];
    for (Field_Mapping__c fm : lFieldMappings) {
      fm.Action__c = FieldMappings.ACTION_TYPE_MANUAL;
    }
    update lFieldMappings;


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    ID jobId = BvDJobs.linkRecordsBatch(new Map<Id, String>{
      account.Id => 'TEST#1'
    },
      Account.getSObjectType(),
      BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    /*BvD Job assertion*/
    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvdJobs.STATUS_ACTION_REQUIRED, job.Status__c);

    /*Work Item assertions*/
    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(1, workItems.size());
    BvD_Job_Work_Item__c workItem = workItems.get(0);

    System.assertEquals(account.Id, workItem.RecordId__c);
    System.assertEquals(BvDJobWorkItems.STATUS_ERROR, workItem.Status__c);

    /*Conflicts Assertions*/
    List<Work_Item_Field_Conflict__c> conflicts = workItem.Field_Conflicts__r;
    System.assertEquals(5, conflicts.size());

    for (Work_Item_Field_Conflict__c conflict : conflicts) {
      System.debug(conflict.Field__c + ' ' + conflict.BvD_Value__c);
    }

    System.assertEquals('JOHN DOES', conflicts.get(0).BvD_Value__c);
    System.assertEquals('STREET 22A LOK. 42', conflicts.get(1).BvD_Value__c);
    System.assertEquals('02-672', conflicts.get(2).BvD_Value__c);
    System.assertEquals('WARSZAWA', conflicts.get(3).BvD_Value__c);
    System.assertEquals('Poland', conflicts.get(4).BvD_Value__c);


    account = [
      SELECT ID,
        Name,
        ShippingStreet,
        ShippingState,
        ShippingPostalCode,
        ShippingCity,
        ShippingCountry
      FROM Account
      WHERE Id = :account.Id
    ];
    /*Account Assertions*/
    System.assertNotEquals('JOHN DOES', account.Name);
    System.assertNotEquals('STREET 22A LOK. 42', account.ShippingStreet);
    System.assertNotEquals('02-672', account.ShippingPostalCode);
    System.assertNotEquals('WARSZAWA', account.ShippingCity);
    System.assertNotEquals('Poland', account.ShippingCountry);
  }

  /**
  * @Scenario
  * BvD Job failed due to manual Field Mapping, is rerun again after
  * settling out mappings.
  *
  * @Result
  * Record has values picked in Merge
  * */
  @IsTest
  static void testLinkRetryAssignsSelectedValuesToRecord() {
    BvD_Job__c job = new BvD_Job__c(
      Status__c = BvDJobs.STATUS_ERROR,
      Object__c = 'Account',
      Failure_Reason__c = BvDJobs.ERROR_FIELD_MAPPING,
      Processing__c = BvDJobs.PROCESSING_AUTOMATIC,
      Type__c = BvDJobs.TYPE_LINK
    );
    insert job;

    List<Account> accounts = getUnlinkedAccounts();
    List<BvD_Job_Work_Item__c> workItemsToInsert = new List<BvD_Job_Work_Item__c>();
    for (Account account : accounts) {
      workItemsToInsert.add(new BvD_Job_Work_Item__c(
        Status__c = BvDJobWorkItems.STATUS_ERROR,
        RecordId__c = account.Id,
        BvD_Job__c = job.Id
      ));
    }
    insert workItemsToInsert;


    List<Work_Item_Field_Conflict__c> workItemsConflicts = new List<Work_Item_Field_Conflict__c>();
    for (BvD_Job_Work_Item__c workItem : workItemsToInsert) {
      workItemsConflicts.addAll(new List<Work_Item_Field_Conflict__c>{
        new Work_Item_Field_Conflict__c(
          Salesforce_Value__c = 'SF1',
          BvD_Value__c = 'JOHN DOES',
          Choice__c = 'Salesforce',
          Field__c = 'Name',
          Work_Item__c = workItem.Id
        ),
        new Work_Item_Field_Conflict__c(
          Salesforce_Value__c = 'France',
          BvD_Value__c = 'Poland',
          Choice__c = 'BvD',
          Field__c = 'ShippingCountry',
          Work_Item__c = workItem.Id
        ),
        new Work_Item_Field_Conflict__c(
          Salesforce_Value__c = 'SF3',
          BvD_Value__c = 'WARSZAWA',
          Choice__c = 'BvD',
          Field__c = 'ShippingCity',
          Work_Item__c = workItem.Id
        ),
        new Work_Item_Field_Conflict__c(
          Salesforce_Value__c = 'SF4',
          BvD_Value__c = 'STREET 22A LOK. 42',
          Choice__c = 'BvD',
          Field__c = 'ShippingStreet',
          Work_Item__c = workItem.Id
        ),
        new Work_Item_Field_Conflict__c(
          Salesforce_Value__c = 'SF5',
          BvD_Value__c = '02-672',
          Choice__c = 'BvD',
          Field__c = 'ShippingPostalCode',
          Work_Item__c = workItem.Id
        )
      });
    }
    insert workItemsConflicts;


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvDJobs.retryJob(job.Id);
    Test.stopTest();


    /*BvD Job assertion*/
    job = BvDJobsSelector.getJobById(job.Id);
    System.assertEquals(BvdJobs.STATUS_SUCCESS, job.Status__c);
    System.assert(String.isEmpty(job.Failure_Reason__c));

    /*Work Item assertions*/
    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    for (BvD_Job_Work_Item__c workItem : workItems) {
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, workItem.Status__c);
      System.assertEquals(5, workItem.Field_Conflicts__r.size());
    }

    /*Account Assertions*/
    for (Account account : getAccounts(accounts)) {
      System.assertNotEquals('JOHN DOES', account.Name);
      System.assertEquals('STREET 22A LOK. 42', account.ShippingStreet);
      System.assertEquals('02-672', account.ShippingPostalCode);
      System.assertEquals('WARSZAWA', account.ShippingCity);
      System.assertEquals('Poland', account.ShippingCountry);
    }
  }

  @IsTest
  static void testUpdateSObjectBatch() {
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    Id jobId = BvdJobs.updateRecordsBatch(Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    BvD_Job_Work_Item__c[] lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(
    [SELECT Count() FROM Account WHERE BvD_Id__c != null AND Unlinked_from_BvD__c = false],
      lWorkItems.size()
    );
    for (BvD_Job_Work_Item__c wi : lWorkItems) {
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }
  }

  /**
   * Scenario:
   * Field is mapped to Orbis Date field which comes in YYYY/MM/DD format
   *
   * Result:
   * Date is parsed to Salesforce format and saved on Date field
   */
  @IsTest
  static void testDateParsingFormatYYYYMMDD() {
    testDateParsing(Date.newInstance(2015, 12, 31));
  }

  /**
   * Scenario:
   * Field is mapped to Orbis Date field which comes in YYYY/MM format
   *
   * Result:
   * Date is parsed to Salesforce format and saved on Date field
   */
  @IsTest
  static void testDateParsingFormatYYYYMM() {
    Field_Mapping__c fm = [
      SELECT ID
      FROM Field_Mapping__c
      WHERE SObject__c = 'Contact'
      AND SF_Field__c = 'Birthdate'
    ];

    fm.BvD_Field__c = 'ADDRESS_UPDATE';
    update fm;

    testDateParsing(Date.newInstance(2017, 03, 01));
  }

  /**
   * Scenario:
   * Field is mapped to Orbis Date field which comes in YYYY format
   *
   * Result:
   * Date is parsed to Salesforce format and saved on Date field
   */
  @IsTest
  static void testDateParsingFormatYYYY() {
    Field_Mapping__c fm = [
      SELECT ID
      FROM Field_Mapping__c
      WHERE SObject__c = 'Account'
      AND SF_Field__c = 'Last_BvD_Update__c'
    ];

    fm.BvD_Field__c = 'IPO_DATE';
    update fm;

    testDateParsing(Date.newInstance(2016, 01, 01));
  }

  static void testDateParsing(Datetime expected) {
    //Contact contact = [SELECT ID FROM Contact LIMIT 1];
    Account account = [ SELECT ID FROM Account LIMIT 1 ];

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    Id jobId = BvdJobs.linkRecordsBatch(new Map<Id, String>{
      account.Id => 'TEST#1'
    }, Account.getSObjectType(), BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    BvD_Job__c job = BvDJobsSelector.getJobById(jobId);
    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    BvD_Job_Work_Item__c[] lWorkItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(1, lWorkItems.size());

    for (BvD_Job_Work_Item__c wi : lWorkItems) {
      System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, wi.Status__c);
    }

    Account = [SELECT ID, Last_BvD_Update__c FROM Account WHERE ID = :account.Id];
    System.assertEquals(null, account.Last_BvD_Update__c);
  }


  /**
  * Scenario:
  * Account is updated and there's no difference between Orbis and Salesforce data.
  *
  * Result:
  * Conflict records for manual field mappings are not saved.
  * */
  @IsTest
  static void testManualFieldMappingsCreateNoConflictsWhenNoDifference() {
    /*All Field Mappings are manual*/
    List<Field_Mapping__c> lFieldMappings = [SELECT ID FROM Field_Mapping__c WHERE SObject__c = 'Account'];
    for (Field_Mapping__c fieldMapping : lFieldMappings) {
      fieldMapping.Action__c = FieldMappings.ACTION_TYPE_MANUAL;
    }

    /*Account has the same data as BVD*/
    Account account = new Account(
      Unlinked_from_BvD__c = false,
      BvD_Id__c = 'TEST#1',
      Name = 'JOHN DOES',
      ShippingStreet = 'STREET 22A LOK. 42',
      ShippingState = '',
      ShippingPostalCode = '02-672',
      ShippingCity = 'WARSZAWA',
      ShippingCountry = 'Poland'
    );
    insert account;


    /*Update from BVD*/
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.updateRecordsImmediatly(new List<Account>{
      account
    }, BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    /*Job is success and there are no conflict records*/
    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);
    System.assertEquals(1, job.BvD_Job_Work_Items__r.size());
    System.assertEquals(BvDJobWorkItems.STATUS_SUCCESS, job.BvD_Job_Work_Items__r.get(0).Status__c);

    List<BvD_Job_Work_Item__c> workItemsWithConflicts = BvDJobWorkItemsSelector.getWorkItemsWithAnyConflicts(job.Id);
    System.assert(workItemsWithConflicts.isEmpty());
  }


  /**
  * Scenario:
  * Account is updated and there's at least 1 difference between Orbis and Salesforce data.
  *
  * Result:
  * Conflict records for all manual field mappings are saved.
  * */
  @IsTest
  static void testManualFieldMappingsCreateAllConflictsWhenAtLeastOneDifference() {
    /*All Field Mappings are manual*/
    List<Field_Mapping__c> lFieldMappings = [SELECT ID FROM Field_Mapping__c WHERE SObject__c = 'Account'];
    for (Field_Mapping__c fieldMapping : lFieldMappings) {
      fieldMapping.Action__c = FieldMappings.ACTION_TYPE_MANUAL;
    }
    update lFieldMappings;

    /*Account has the same data as BVD except for one field*/
    Account account = new Account(
      Unlinked_from_BvD__c = false,
      BvD_Id__c = 'TEST#1',
      Name = 'JOHN DOES',
      ShippingStreet = 'STREET 22A LOK. 42',
      ShippingState = '',
      ShippingPostalCode = '02-672',
      ShippingCity = 'Other', /*Different Field*/
      ShippingCountry = 'Poland'
    );
    insert account;


    /*Update from BVD*/
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.updateRecordsImmediatly(new List<Account>{
      account
    }, BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();


    /*Job is in error status and there is conflict for each manual field mapping.
    One mapping yields empty response from Orbis so it does not count towards conflicts*/
    System.assertEquals(BvDJobs.STATUS_ACTION_REQUIRED, job.Status__c);
    System.assertEquals(1, job.BvD_Job_Work_Items__r.size());

    List<BvD_Job_Work_Item__c> workItems = BvDJobWorkItemsSelector.getWorkItemsWithConflicts(job.Id);
    System.assertEquals(1, workItems.size());
    System.assertEquals(BvDJobWorkItems.STATUS_ERROR, workItems.get(0).Status__c);
    System.assertEquals(lFieldMappings.size() - 1, workItems.get(0).Field_Conflicts__r.size() + 1);
  }


  /**
  * Scenario:
  * BvD Id changed in Orbis, but not in Salesforce.
  * Record is updated using the old BvD ID.
  *
  * Result:
  * BvD ID on the record is updated to the most recent one during synchronization.
  * */
  @IsTest
  static void testBvDIdIsUpdatedToTheMostRecentOne() {
    List<Account> lAccounts = getLinkedAccounts();
    for (Account account : lAccounts) {
      System.assert(account.BvD_Id__c.contains('TEST#'));
    }


    /*Update from BVD*/
    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks(new OrbisOutdatedIdMockFactory()));
    BvD_Job__c job = BvDJobs.updateRecordsImmediatly(lAccounts, BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    System.assertEquals(BvDJobs.STATUS_SUCCESS, job.Status__c);

    lAccounts = getAccounts(lAccounts);
    for (Account account : lAccounts) {
      System.assert(account.BvD_Id__c.contains('TEST_NEW#'));
    }
  }


  /**
  * Scenario:
  * Update is run in Dry run mode
  *
  * Result:
  * Conflict records are created if record is different, but the record is not updated.
  * */
  @IsTest
  static void testDryRunDoesNotUpdateTheRecords() {
    List<Account> lAccounts = getLinkedAccounts();


    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks());
    BvD_Job__c job = BvDJobs.updateRecordsImmediatly(lAccounts, BvDJobs.PROCESSING_DRY_RUN);
    Test.stopTest();


    System.assertEquals(BvDJobs.STATUS_ACTION_REQUIRED, job.Status__c);

    List<Account> lAccounts2 = getLinkedAccounts();
    for (Integer i = 0; i < lAccounts.size(); i++) {
      System.assertEquals(lAccounts.get(i), lAccounts2.get(i));
    }
  }


  /**
  * Scenario:
  * One of the record fails during update
  *
  * Result:
  * All other records are updated.
  * */
  @IsTest
  static void testDMLFailureIsHandled() {
    List<Account> lAccounts = getLinkedAccounts();

    Test.startTest();
    Test.setMock(HttpCalloutMock.class, new OrbisTestMocks(new OrbisDMLFailMockFactory()));
    BvD_Job__c job = BvDJobs.updateRecordsImmediatly(lAccounts, BvDJobs.PROCESSING_AUTOMATIC);
    Test.stopTest();

    System.assertEquals(BvDJobs.STATUS_ERROR, job.Status__c);
    System.assertEquals(lAccounts.size(), job.BvD_Job_Work_Items__r.size());
  }

  private class OrbisFaultAuthMockFactory extends OrbisTestMocks.OrbisMockFactory {
    protected override OrbisMock getOpenMock() {
      return new OrbisTestMocks.OrbisFaultMock();
    }
    protected override OrbisMock getGetDataMock() {
      return new OrbisTestMocks.OrbisFaultMock();
    }
  }

  private class OrbisOutdatedIdMockFactory extends OrbisTestMocks.OrbisMockFactory {
    protected override OrbisMock getGetDataMock() {
      return new OrbisTestMocks.OrbisOutdatedIdGetDataMock();
    }
  }

  private class OrbisDMLFailMockFactory extends OrbisTestMocks.OrbisMockFactory {
    protected override OrbisMock getGetDataMock() {
      return new OrbisGetDataDMLFailMock();
    }
  }

  public class OrbisGetDataDMLFailMock extends OrbisTestMocks.OrbisMock {
    protected override String getBody() {
      String xmlString = '<?xml version="1.0" encoding="utf-8"?>'
        + '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'
        + '<soap:Body>'
        + '<GetDataResponse xmlns="http://bvdep.com/webservices/">'
        + '<GetDataResult>&lt;?xml version="1.0" encoding="utf-16"?&gt;'
        + '&lt;queryResults version="1.0" xmlns="http://www.bvdep.com/schemas/RemoteAccessDataResults.xsd"&gt;';

      for (Integer i = 0; i < 50; i++) {
        xmlString += '&lt;record id="TEST1_U" dataModelId="C" selectionId="TEST#' + i + '"&gt;'
          + '&lt;item field="BVD_ID_NUMBER" valueType="AccountValue" resultType="String" fieldType="String"&gt;TEST#' + i + '&lt;/item&gt;'
          + '&lt;item field="NAME" valueType="AccountValue" resultType="String" fieldType="String"&gt;' + (i ==21 ? '' : 'JOHN DOES') + '&lt;/item&gt;'
          + '&lt;item field="ADDR" valueType="AccountValue" resultType="String" fieldType="String"&gt;STREET 22A LOK. 42&lt;/item&gt;'
          + '&lt;item field="ADDR2" valueType="AccountValue" resultType="NotAvailable" fieldType="String"&gt;&lt;/item&gt;'
          + '&lt;item field="POSTCODE" valueType="AccountValue" resultType="String" fieldType="String"&gt;02-672&lt;/item&gt;'
          + '&lt;item field="CITY" valueType="AccountValue" resultType="String" fieldType="String"&gt;WARSZAWA&lt;/item&gt;'
          + '&lt;item field="COUNTRY" valueType="AccountValue" resultType="String" fieldType="String"&gt;Poland&lt;/item&gt;'
          + '&lt;item field="CLOSDATE" valueType="AccountValue" resultType="DateTime" fieldType="Date"&gt;2015/12/31&lt;/item&gt;'
          + '&lt;item field="ADDRESS_UPDATE" valueType="AccountValue" resultType="DateTime" fieldType="Date" format="YYYY/MM"&gt;2017/03&lt;/item&gt;'
          + '&lt;item field="YEAR_LAST_ACCOUNTS" valueType="AccountValue" resultType="DateTime" fieldType="Date" format="YYYY"&gt;2016&lt;/item&gt;'
          + '&lt;/record&gt;';
      }
      xmlString += '&lt;/queryResults&gt;</GetDataResult>'
        + '</GetDataResponse>'
        + '</soap:Body>'
        + '</soap:Envelope>';

      return xmlString;
    }
  }

}