public with sharing class CompanyHierarchyCtr {

  @AuraEnabled
  public static ContainerCompanyUrl getContext() {
    return OrbisContainerController.getSessionId();
  }

  @AuraEnabled
  public static Boolean getAccountRelated(String leadId, String bvdId) {
    if(String.isBlank(leadId) || !leadId.startsWith('00Q')) {
      throw new AuraHandledException('Not a lead');
    }

    if(String.isBlank(bvdId)) {
      throw new AuraHandledException('Not any BvD Id provided');
    }

    try {
      Account account = [ SELECT Id, Name, BvD_Id__c FROM Account WHERE BvD_Id__c = :bvdId LIMIT 1 ];
      return true;
    } catch (Exception ex) {
      return false;
    }
  }

  @AuraEnabled
  public static CompanyHierarchyNode getOwnershipStructure(String bvdId) {
    try {
      Logger.start('CompanyHierarchyCtr', 'getOwnershipStructure');
      OrbisAPI orbisAPI = new OrbisAPI();
      OrbisOpenResponse openResponse = (OrbisOpenResponse) orbisAPI.send(new OrbisOpenRequest());

      OrbisRequest getOwnershipStructureRequest = new OrbisGetOwnershipStructureRequest()
        .setSessionHandle(openResponse.getSessionID())
        .setSubjectCompanyBvDID(bvdId)
        .setFlowType(OrbisSchema.FlowType.All)// default value
        .setMinimumPercentage(1) //default value
        .setUseIntegratedOwnership(false); //default value

      OrbisGetOwnershipStructureResponse response = (OrbisGetOwnershipStructureResponse) orbisAPI.send(getOwnershipStructureRequest);

      Map<String, CompanyHierarchyNode> companyHierarchyNodesMap = initCompanyHierarchyNodesMap(response.getNodes(), response.getEdges());

      return getHierarchyRoot(companyHierarchyNodesMap);

    } catch (Exception ex) {
      System.debug(ex.getMessage());
      System.debug(ex.getStackTraceString());

      Logger.error(ex);
      Logger.stop();

      AuraHandledException auraEx = new AuraHandledException(ex.getMessage());
      auraEx.initCause(ex);
      throw auraEx;
    }
  }

  public static Map<String, CompanyHierarchyNode> initCompanyHierarchyNodesMap(List<OrbisSchema.OwnershipGraphNode> nodes, List<OrbisSchema.OwnershipGraphEdge> edges) {
    Map<String, CompanyHierarchyNode> companyHierarchyNodesMap = new Map<String, CompanyHierarchyNode>();
    for (OrbisSchema.OwnershipGraphNode ownershipGraphNode : nodes) {
      if (!companyHierarchyNodesMap.containsKey(ownershipGraphNode.BvDID)) {
        companyHierarchyNodesMap.put(ownershipGraphNode.BvDID, new CompanyHierarchyNode(ownershipGraphNode));
      }
    }

    return initRelationsBetweenNodes(companyHierarchyNodesMap, edges);
  }

  public static Map<String, CompanyHierarchyNode> initRelationsBetweenNodes(Map<String, CompanyHierarchyNode> companyHierarchyNodesMap, List<OrbisSchema.OwnershipGraphEdge> edges) {

    for (OrbisSchema.OwnershipGraphEdge ownershipGraphEdge : edges) {
      CompanyHierarchyNode companyHierarchyNodeChild = companyHierarchyNodesMap.get(ownershipGraphEdge.ChildBvDID);
      companyHierarchyNodeChild.parentBvdId = ownershipGraphEdge.ParentBvDID;
      companyHierarchyNodesMap.get(ownershipGraphEdge.ParentBvDID).childrenCompanies.add(companyHierarchyNodeChild);
    }

    return initDataFromSF(companyHierarchyNodesMap);
  }

  public static Map<String, CompanyHierarchyNode> initDataFromSF(Map<String, CompanyHierarchyNode> companyHierarchyNodesMap) {
    Map<String, Account> accountsMap = queryAccounts(companyHierarchyNodesMap);
    Map<String, Lead> leadsMap = queryLeads(companyHierarchyNodesMap);

    for (String bvdId : companyHierarchyNodesMap.keySet()) {
      CompanyHierarchyNode companyHierarchyNode = companyHierarchyNodesMap.get(bvdId);
      if (accountsMap.containsKey(bvdId)) {
        Account account = accountsMap.get(bvdId);
        companyHierarchyNode.sfId = account.Id;
      }
    }

    return companyHierarchyNodesMap;
  }

  public static Map<String, Account> queryAccounts(Map<String, CompanyHierarchyNode> companyHierarchyNodesMap) {
    Map<String, Account> accountsMap = new Map<String, Account>();

    List<Account> accounts = [
      SELECT Id, BvD_Id__c, AnnualRevenue
      FROM Account
      WHERE BvD_Id__c IN :companyHierarchyNodesMap.keySet()
    ];

    for (Account account : accounts) {
      if (!accountsMap.containsKey(account.BvD_Id__c)) {
        accountsMap.put(account.BvD_Id__c, account);
      }
    }

    return accountsMap;
  }


  public static Map<String, Lead> queryLeads(Map<String, CompanyHierarchyNode> companyHierarchyNodesMap) {
    Map<String, Lead> leadsMap = new Map<String, Lead>();

    List<Lead> leads = [
      SELECT Id, BvD_Id__c, FirstName, ConvertedContact.Birthdate
      FROM Lead
      WHERE BvD_Id__c IN :companyHierarchyNodesMap.keySet()
    ];

    for (Lead lead : leads) {
      if (!leadsMap.containsKey(lead.BvD_Id__c)) {
        leadsMap.put(lead.BvD_Id__c, lead);
      }
    }

    return leadsMap;
  }


  public static CompanyHierarchyNode getHierarchyRoot(Map<String, CompanyHierarchyNode> companyHierarchyNodesMap) {
    for (CompanyHierarchyNode companyHierarchyNode : companyHierarchyNodesMap.values()) {
      if (String.isEmpty(companyHierarchyNode.parentBvdId)) {
        return companyHierarchyNode;
      }
    }

    return null;
  }

  public class CompanyHierarchyNode {
    @AuraEnabled public String bvdId;
    @AuraEnabled public String sfId;
    @AuraEnabled public String name;
    @AuraEnabled public String entityType;
    @AuraEnabled public String countryISO2Code;
    @AuraEnabled public String city;
    @AuraEnabled public String bvdIndependenceIndicator;
    @AuraEnabled public Boolean isInWorldCompliance;
    @AuraEnabled public Integer distance;
    @AuraEnabled public Double integratedPercentage;
    @AuraEnabled public Boolean isIntegratedPercentageEstimation;
    @AuraEnabled public String parentBvdId;
    @AuraEnabled public List<CompanyHierarchyNode> childrenCompanies;

    public CompanyHierarchyNode() {

    }

    public CompanyHierarchyNode(OrbisSchema.OwnershipGraphNode ownershipGraphNode) {
      this.bvdId = ownershipGraphNode.BvDID;
      this.name = ownershipGraphNode.Name;
      this.entityType = ownershipGraphNode.EntityType;
      this.countryISO2Code = ownershipGraphNode.CountryISO2Code;
      this.city = ownershipGraphNode.City;
      this.bvdIndependenceIndicator = ownershipGraphNode.BvDIndependenceIndicator;
      this.isInWorldCompliance = ownershipGraphNode.IsInWorldCompliance;
      this.distance = ownershipGraphNode.Distance;
      this.integratedPercentage = ownershipGraphNode.IntegratedPercentage;
      this.isIntegratedPercentageEstimation = ownershipGraphNode.IsIntegratedPercentageEstimation;
      this.parentBvdId = '';
      this.sfId = '';
      this.childrenCompanies = new List<CompanyHierarchyNode>();
    }
  }
}